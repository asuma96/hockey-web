@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <div class="error404">
            <section id="content" class="site-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="error404-content"><h2 class="nado"><span>404</span></h2>
                                <h3>The page cannot be found.</h3></div>
                        </div>
                        <div class="container">
                            <div class="search gensearch__wrapper">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

