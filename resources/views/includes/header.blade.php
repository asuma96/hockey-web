<div class="top">
    <div class="page_header">
        <a href="http://hockeyshare.com/"><img class="logo_img"
                                               src="//d3e9hat72fmrwm.cloudfront.net/images/global/hockeyshare-logo.png"
                                               alt="HockeyShare Logo"
            /></a>
        @if( Auth::check())
            <div class="headlink_container page-header-dop">
                <div class="text-search">
                    <a href="http://hockeyshare.com/newsletter/" class="headlinks"
                       title="Subscribe to the HockeyShare Newsletter"><i class="fa fa-envelope fa-2x"></i></a>
                    <a href="http://hockeyshare.com/interact/rss.php" class="headlinks "
                       title="RSS Feeds"><i
                                class="fa fa-rss fa-2x"></i></a>
                    <a href="https://twitter.com/hockeyshare" target="_blank" class="headlinks "
                       title="Follow HockeyShare on Twitter"><i class="fa fa-twitter-square fa-2x"></i></a>
                    <a href="https://facebook.com/hockeyshare" target="_blank" class="headlinks "
                       title="HockeyShare on Facebook"><i class="fa fa-facebook-official fa-2x"></i></a>
                    <a href="https://youtube.com/hockeyshare" target="_blank" class="headlinks "
                       title="YouTube Videos"><i class="fa fa-youtube fa-2x"></i></a>
                    <input type="text" placeholder="Search HockeyShare" name="q"
                           class="search-placeholder home-search header_input"/> <input type="submit" name="Submit"
                                                                                        value="Go"
                                                                                        class="header_search head_search"/>
                </div>
                <span class="header_style">
					                        Logged in as: <a
                            href="http://hockeyshare.com/account/">{{--{{$name}}--}}</a> (<a
                            href="http://hockeyshare.com/login/logout/" style="color:#CCC;">Log Out</a>)
                                    </span>
            </div>
        @else
            <div class="headlink_container page-header-dop">
                <div class="text-search">
                    <a href="http://hockeyshare.com/newsletter/" class="headlinks"
                       title="Subscribe to the HockeyShare Newsletter"><i class="fa fa-envelope fa-2x"></i></a>
                    <a href="http://hockeyshare.com/interact/rss.php" class="headlinks "
                       title="RSS Feeds"><i
                                class="fa fa-rss fa-2x"></i></a>
                    <a href="https://twitter.com/hockeyshare" target="_blank" class="headlinks "
                       title="Follow HockeyShare on Twitter"><i class="fa fa-twitter-square fa-2x"></i></a>
                    <a href="https://facebook.com/hockeyshare" target="_blank" class="headlinks "
                       title="HockeyShare on Facebook"><i class="fa fa-facebook-official fa-2x"></i></a>
                    <a href="https://youtube.com/hockeyshare" target="_blank" class="headlinks "
                       title="YouTube Videos"><i class="fa fa-youtube fa-2x"></i></a>
                    <input type="text" placeholder="Search HockeyShare" name="q"
                           class="search-placeholder home-search header_input"/> <input type="submit" name="Submit"
                                                                                        value="Go"
                                                                                        class="header_search head_search"/>
                </div>
                <span class="logged">
					                    	You are not logged in. <a href="http://hockeyshare.com/login/"
                                                                      class="logged-white">Log in</a> or <a
                            href="http://hockeyshare.com/register/" class="logged-white">Register</a>
                                    </span>
            </div>
        @endif
    </div>
</div>
