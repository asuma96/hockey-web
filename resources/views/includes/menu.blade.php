@if(Auth::check())
    <div id="menu_container" class="menu_red">

        <ul id="menu">

            <li><a href="http://hockeyshare.com/">Home</a></li>

            <li><a href="#" class="drop">Drills &amp; Practices</a>

                <div class="fullwidth">

                    <div class="col_3">
                        <h4>Free Hockey Drills</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/drills/">View Free Hockey Drills</a></li>
                        </ul>
                        <h4>Drill Store</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/drill-store/">View Drill Store</a></li>
                        </ul>
                    </div>

                    <div class="col_3">
                        <h4>My Drills</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/drills/my-drills/">My Drills</a></li>
                            <li><a href="http://hockeyshare.com/drills/add/">Draw New Drill</a></li>
                            <li><a href="http://hockeyshare.com/freetrial" class="hs_forum_pm_count "
                                   title="Try our drill diagramming and practice planning platform free for 30 days!&lt;br /&gt;** NO CREDIT CARD REQUIRED **"
                                   class="menu_a">Free 14-Day Trial</a></li>
                            <li><a href="http://hockeyshare.com/acp/learn-more/">Learn More</a></li>
                            <li><a href="http://hockeyshare.com/acp/compare/">Pricing / Compare Plans</a></li>
                            <li><a href="http://hockeyshare.com/redeem/">Redeem Gift Certificate</a></li>
                        </ul>
                    </div>
                    <div class="col_3">
                        <h4>My Practice Plans</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/plans/">My Practice Plans</a></li>
                            <li><a href="http://hockeyshare.com/drills/practiceplans/edit.php">Create New Practice
                                    Plans</a></li>
                            <li><a href="http://hockeyshare.com/drills/practice_plan_defaults.php">Preferences /
                                    Settings</a></li>
                            <li><a href="http://hockeyshare.com/freetrial/" class="hs_forum_pm_count "
                                   title="Try our drill diagramming and practice planning platform free for 30 days!&lt;br /&gt;** NO CREDIT CARD REQUIRED **"
                                   class="menu_a">Free 14-Day Trial</a></li>
                            <li><a href="http://hockeyshare.com/acp/learn-more/">Learn More</a></li>
                            <li><a href="http://hockeyshare.com/acp/compare/">Pricing / Compare Plans</a></li>
                        </ul>

                    </div>

                    <div class="col_3">
                        <h4>Association/Teams</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/upgrade/acp_assn.php">Upgrade to Team Pack</a></li>
                            <li><a href="http://hockeyshare.com/upgrade/acp_assn.php">Upgrade to Association</a></li>
                        </ul>
                    </div>

                </div>

            </li>

            <li><a href="#" class="drop">Team Manage</a>

                <div class="fullwidth">
                    <div class="col_4">
                        <h4>Team Management Admin</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/teams/">Manage My Teams</a></li>
                            <li><a href="http://hockeyshare.com/teams/addteam.php">Add New Team</a></li>
                            <li><a href="http://hockeyshare.com/stat_tracking_guide.php">Getting Started Guide</a></li>
                            <li><a href="http://hockeyshare.com/teams/video_guides.php">Video Guides</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Team Options</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/search/">Search Teams</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>My Bookmarks</h4>
                        <ul class="list2">
                            <li><i>No bookmarked teams found</i></li>
                        </ul>
                    </div>
                </div>

            </li>

            <li><a href="#" class="drop">Tournaments</a>
                <div class="fullwidth">
                    <div class="col_6">
                        <h4>Find Tournaments</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/tournaments/">View All</a></li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=US">Tournaments in the United
                                    States</a></li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=CA">Tournaments in Canada</a></li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=OTHER">International Tournaments</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col_6">
                        <h4>Add Your Tournament</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/tournaments/list.php">List Your Tournament</a></li>
                            <li>Basic Listings: Free</li>
                            <li>Featured Listings: Starting at $5</li>
                        </ul>
                    </div>

                </div>
            </li>
            <li><a href="http://hockeyshare.com/10000pucks/" class="drop">10k Pucks</a></li>

            <li><a href="http://hockeyshare.com/video/">Video</a>

            </li>

            <li>
                <a href="http://hockeyshare.com/training/">Training</a>
            </li>
            <li>
                <a href="http://hockeyshare.com/blog/">Blog</a>
            </li>

            <li><a href="#" class="drop">More...</a>
                <div class="fullwidth">

                    <div class="col_4">
                        <h4>HockeyShare.com</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/blog/">Blog</a></li>
                            <li><a href="http://hockeyshare.com/search/">Search</a></li>
                            <li><a href="http://hockeyshare.com/interact/contact.php">Contact Us</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Social</h4>
                        <ul class="list2">
                            <li><i class="fa fa-envelope"></i> &nbsp;<a href="http://hockeyshare.com/newsletter/">HockeyShare
                                    Newsletter</a></li>
                            <li><i class="fa fa-envelope"></i> &nbsp;<a href="http://hockeyshare.com/newsletter/">Featured
                                    Drill Newsletter</a></li>
                            <li><i class="fa fa-rss"></i> &nbsp;<a href="http://hockeyshare.com/interact/rss.php">RSS
                                    Feeds</a></li>
                            <li><i class="fa fa-facebook-official"></i> &nbsp;<a
                                        href="https://www.facebook.com/HockeyShare" target="_blank">HockeyShare on
                                    Facebook</a></li>
                            <li><i class="fa fa-twitter-square"></i> &nbsp;<a href="https://twitter.com/hockeyshare"
                                                                              target="_blank">Follow on Twitter</a></li>
                            <li><i class="fa fa-youtube"></i> &nbsp;<a href="https://www.youtube.com/hockeyshare"
                                                                       target="_blank">HockeyShare on YouTube</a></li>
                        </ul>
                    </div>
                    <div class="col_4">
                        <h4>More</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/ttp/attackpad/">AttackPad Hockey Training System</a>
                            </li>
                            <li><a href="http://hockeyshare.com/game-tape/">Game Tape</a> (Beta)</li>
                            <li><a href="http://hockeyshare.com/gifts/">Gift Certificates</a></li>
                        </ul>

                    </div>

                </div>
            </li>

            <li><a href="#" class="drop">Help &amp; Support</a>

                <div class="fullwidth">

                    <div class="col_4">
                        <h4>Adv. Coaching Platform</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/acp/walkthrough-1.php">Adv. Coaching Platform
                                    Overview</a></li>
                            <li><a href="http://hockeyshare.com/acp/association_overview.php">Association/Team
                                    Overview</a></li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-2.php">Diagrammer Tips &amp; Tricks</a>
                            </li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-3.php">Organizing Drills &amp;
                                    Practices</a></li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-4.php">Animating Drills</a></li>
                            <li><a href="http://hockeyshare.com/drills/diagrammer_help.php">Diagrammer Feature Help
                                    Videos</a></li>
                            <li><a href="http://hockeyshare.com/drills/practiceplanner_help.php">Practice Feature
                                    Planner Help Videos</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Other Products</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/stat_tracking_guide.php">Team Manage .::. Getting
                                    Started Guide</a></li>
                            <li><a href="http://hockeyshare.com/teams/video_guides.php">Team Manage .::. Video
                                    Tutorials</a></li>
                            <li><a href="http://hockeyshare.com/support/attackpad.php">AttackPad Training System
                                    Support</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>More</h4>
                        <ul class="list2">
                            <li><a href="https://hockeyshare.groovehq.com/help_center" target="_blank">HockeyShare
                                    Knowledge Base</a></li>
                            <li><a href="http://hockeyshare.com/search/">Search</a></li>
                            <li><a href="http://hockeyshare.com/interact/contact.php">Contact Us</a></li>

                        </ul>
                    </div>

                </div>

            </li>

            <li><a href="#" class="drop">My Account</a>

                <div class="fullwidth">

                    <div class="col_6">
                        <h4>Profile</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/account/">My Profile</a></li>
                            <li><a href="http://hockeyshare.com/account/billing/">My Subscriptions / Billings</a></li>
                            <li><a href="http://hockeyshare.com/redeem/">Redeem Gift Certificate</a></li>
                        </ul>
                    </div>

                    <div class="col_6">
                        <h4>User Information</h4>
                        <ul class="list2">
                            <li>Logged In As: {{--{{$email}}--}}</li>
                            <li>Account Status: <span class="navfree">Standard (Free)</span></li>
                            <li><a href="http://hockeyshare.com/login/logout/">Log Out</a></li>
                        </ul>
                    </div>

                </div>

            </li>

        </ul>

    </div>
@else
    <div id="menu_container" class="menu_red">
        <ul id="menu">
            <li><a href="http://hockeyshare.com/">Home</a></li>
            <li><a href="#" class="drop">Start Here</a>
                <div class="fullwidth">
                    <div class="col_6">
                        <h4>New Here?</h4>
                        <p>Create your <u>free</u> HockeyShare.com account today. Registration is easy and takes
                            just a
                            minute to complete.</p>
                        <p><a href="http://hockeyshare.com/register/"
                              class="links">Click here to register</a>
                        </p>
                    </div>
                    <div class="col_6">
                        <h4>Already Registered?</h4>
                        <br/>
                        <p><a href="http://hockeyshare.com/login/"
                              class="links">Click here to log in</a>
                        </p>
                    </div>
                </div>
            </li>

            <li><a href="#" class="drop">Drills &amp; Practices</a>
                <div class="fullwidth">
                    <div class="col_3">
                        <h4>Free Hockey Drills</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/drills/">View Free Hockey Drills</a></li>
                        </ul>
                        <h4>Drill Store</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/drill-store/">View Drill Store</a></li>
                        </ul>
                    </div>

                    <div class="col_3">
                        <h4>My Drills</h4>
                        <ul class="list2">
                            <li class="notloggedin">You are not logged in</li>
                            <li><a href="http://hockeyshare.com/acp/learn-more/">Learn More</a></li>
                            <li><a href="http://hockeyshare.com/acp/compare/">Pricing / Compare Plans</a></li>
                            <li><a href="http://hockeyshare.com/redeem/">Redeem Gift Certificate</a></li>

                        </ul>
                    </div>
                    <div class="col_3">
                        <h4>My Practice Plans</h4>
                        <ul class="list2">
                            <li class="notloggedin">You are not logged in</li>
                            <li><a href="http://hockeyshare.com/acp/learn-more/">Learn More</a></li>
                            <li><a href="http://hockeyshare.com/acp/compare/">Pricing / Compare Plans</a></li>
                        </ul>
                    </div>

                    <div class="col_3">
                        <h4>Association/Teams</h4>
                        <ul class="list2">
                            <li class="notloggedin">You are not logged in</li>
                        </ul>
                    </div>
                </div>
            </li>
            <li><a href="#" class="drop">Team Manage</a>
                <div class="fullwidth">
                    <div class="col_4">
                        <h4>Team Management Admin</h4>
                        <ul class="list2">
                            <li class="notloggedin">Please log in to manage your teams</li>
                            <li><a href="http://hockeyshare.com/stat_tracking_guide.php">Getting Started
                                    Guide</a>
                            </li>
                            <li><a href="http://hockeyshare.com/teams/video_guides.php">Video Guides</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Team Options</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/search/">Search Teams</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>My Bookmarks</h4>
                        <ul class="list2">
                            <li class="notloggedin">Please log in to access bookmarks</li>
                        </ul>
                    </div>
                </div>
            </li>

            <li><a href="#" class="drop">Tournaments</a>
                <div class="fullwidth">
                    <div class="col_6">
                        <h4>Find Tournaments</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/tournaments/">View All</a></li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=US">Tournaments in the United
                                    States</a></li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=CA">Tournaments in Canada</a>
                            </li>
                            <li><a href="http://hockeyshare.com/tournaments/?loc=OTHER">International
                                    Tournaments</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col_6">
                        <h4>Add Your Tournament</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/tournaments/list.php">List Your Tournament</a>
                            </li>
                            <li>Basic Listings: Free</li>
                            <li>Featured Listings: Starting at $5</li>
                        </ul>
                    </div>
                </div>
            </li>
            <li><a href="http://hockeyshare.com/10000pucks/" class="drop">10k Pucks</a></li>

            <li><a href="http://hockeyshare.com/video/">Video</a>

            </li>
            <li>
                <a href="http://hockeyshare.com/training/">Training</a>
            </li>
            <li>
                <a href="http://hockeyshare.com/blog/">Blog</a>
            </li>
            <li><a href="#" class="drop">More...</a>
                <div class="fullwidth">

                    <div class="col_4">
                        <h4>HockeyShare.com</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/blog/">Blog</a></li>
                            <li><a href="http://hockeyshare.com/search/">Search</a></li>
                            <li><a href="http://hockeyshare.com/interact/contact.php">Contact Us</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Social</h4>
                        <ul class="list2">
                            <li><i class="fa fa-envelope"></i> <a href="http://hockeyshare.com/newsletter/">HockeyShare
                                    Newsletter</a></li>
                            <li><i class="fa fa-envelope"></i> <a href="http://hockeyshare.com/newsletter/">Featured
                                    Drill Newsletter</a></li>
                            <li><i class="fa fa-rss"></i> <a href="http://hockeyshare.com/interact/rss.php">RSS
                                    Feeds</a></li>
                            <li><i class="fa fa-facebook-official"></i> <a
                                        href="http://facebook.com/HockeyShare"
                                        target="_blank">HockeyShare on
                                    Facebook</a></li>
                            <li><i class="fa fa-twitter-square"></i> <a href="http://twitter.com/hockeyshare"
                                                                        target="_blank">Follow on Twitter</a>
                            </li>
                            <li><i class="fa fa-youtube"></i> <a href="http://youtube.com/hockeyshare"
                                                                 target="_blank">HockeyShare
                                    on YouTube</a></li>
                        </ul>
                    </div>
                    <div class="col_4">
                        <h4>More</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/ttp/attackpad/">AttackPad Hockey Training
                                    System</a>
                            </li>
                            <li><a href="http://hockeyshare.com/game-tape/">Game Tape</a> (Beta)</li>
                            <li><a href="http://hockeyshare.com/redeem/">Redeem Gift Certificate</a></li>
                            <li><a href="http://hockeyshare.com/gifts/">Gift Certificates</a></li>
                        </ul>
                    </div>
                </div>
            </li>

            <li><a href="#" class="drop">Help &amp; Support</a>
                <div class="fullwidth">
                    <div class="col_4">
                        <h4>Adv. Coaching Platform</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/acp/walkthrough-1.php">Adv. Coaching Platform
                                    Overview</a></li>
                            <li><a href="http://hockeyshare.com/acp/association_overview.php">Association/Team
                                    Overview</a></li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-2.php">Diagrammer Tips &amp;
                                    Tricks</a>
                            </li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-3.php">Organizing Drills &amp;
                                    Practices</a></li>
                            <li><a href="http://hockeyshare.com/acp/walkthrough-4.php">Animating Drills</a>
                            </li>
                            <li><a href="http://hockeyshare.com/drills/diagrammer_help.php">Diagrammer Feature
                                    Help
                                    Videos</a></li>
                            <li><a href="http://hockeyshare.com/drills/practiceplanner_help.php">Practice
                                    Feature
                                    Planner Help Videos</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>Other Products</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.com/stat_tracking_guide.php">Team Manage .::.
                                    Getting
                                    Started Guide</a></li>
                            <li><a href="http://hockeyshare.com/teams/video_guides.php">Team Manage .::. Video
                                    Tutorials</a></li>
                            <li><a href="http://hockeyshare.com/support/attackpad.php">AttackPad Training
                                    System
                                    Support</a></li>
                        </ul>
                    </div>

                    <div class="col_4">
                        <h4>More</h4>
                        <ul class="list2">
                            <li><a href="http://hockeyshare.groovehq.com/help_center" target="_blank">HockeyShare
                                    Knowledge
                                    Base</a></li>
                            <li><a href="http://hockeyshare.com/search/">Search</a></li>
                            <li><a href="http://hockeyshare.com/interact/contact.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
@endif