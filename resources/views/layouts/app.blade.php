<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="shortcut icon" href="//d3e9hat72fmrwm.cloudfront.net/images/global/favicon.ico" />
    <link href="/css/app.css" rel="stylesheet">

    <link type="text/css" href="//d3e9hat72fmrwm.cloudfront.net/css/tiptip.min.css" rel="stylesheet"/>
    <link type="text/css" href="//d3e9hat72fmrwm.cloudfront.net/fa/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="//d3e9hat72fmrwm.cloudfront.net/css/megamenu_hs_cdn.min.css" type="text/css"
          media="screen"/>
    <link type="text/css" href="//d3e9hat72fmrwm.cloudfront.net/css/hockeyshare_all.min.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          crossorigin="anonymous">

    <!-- Content Information -->
    <title>Hockey Drills, Online Drill Manager, Online Practice Planner, Hockey Tournaments, Hockey Stat
        Tracking</title>
    <meta name="Description"
          content="Free hockey drills, online drill manager/creator, practice planner, tournament listings, coaching supplies, training tools and equipment, hockey news, videos, scores, articles, hockey tape, backyard ice rink supplies and more."/>
    <meta name="Keywords"
          content="hockey drills, online hockey drill diagrammer, hockey drill manager, online practice planner, hockey practice planner, hockey tournaments, training equipment, auctions, articles, news, scores, stat tracking, coaching supplies, hockey video clips, hockey forums, training tools, speed and agility training, plyometric training tools, hockey tape, stickhandling balls, swedish stickhandling balls, hockey pucks, white hockey pucks, coaches whistle, hockey hands, dry erase boards, backyard ice rink kits, hockey dvd, instructional dvds, clear hockey tape, stick wax, fans and parents articles, coaching articles, slickshot shooting pad, rick o shay goal blocker, drill draw diagramming software, hockey nets, hockey goals, 10000 pucks 1 summer contest, puck shooting contest, goalie training manuals, reaction balls, variable goals training dvd, skinner hockey stickhandling dvd, puck bags, hockey rink note pads"/>

    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="/js/search.js"></script>

    <link rel="alternate" type="application/atom+xml" title="HockeyShare Hockey Drills"
          href="http://feedproxy.google.com/hockeysharedrills"/>
    <link rel="alternate" type="application/atom+xml" title="HockeyShare Blog Posts"
          href="http://feeds.feedburner.com/HockeyshareBlog"/>
    <link rel="stylesheet" href="//d3e9hat72fmrwm.cloudfront.net/css/buttons.min.css">
    <link type="text/css" href="//d3e9hat72fmrwm.cloudfront.net/featherlight/featherlight.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css"
          type="text/css" media="all"/>
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="//d3e9hat72fmrwm.cloudfront.net/images/global/favicon.ico"/>


    <script type="text/javascript">

        function checkUsername() {

            var username = $('#username').val();

            var val = $.ajax({
                url: "ajax_username_check.php?username=" + encodeURIComponent(username),
                async: false
            }).responseText;

            if (val == 1) {
                $('#username_available').html('<span class="available_reg">Available</span>');
            } else if (val == 2) {
                $('#username_available').html('');
            } else {
                // TAKEN
                $('#username_available').html('<span class="notavailable_reg">Username Already in Use</span>');
            }
        }

        function checkEmail() {

            var email = $('#email').val();

            var val = $.ajax({
                url: "ajax_email_check.php?email=" + encodeURIComponent(email),
                async: false
            }).responseText;

            if (val == 1) {
                $('#email_status').html('');
            } else if (val == 2) {
                $('#email_status').html('');
            } else {
                $('#email_status').html('<span class="notavailable_reg">NOTE: An account with this email already exists <a href="http://hockeyshare.com/login/">Log in</a>.</span>');
            }
        }
    </script>
    <!-- Drills-->
    <script language="javascript" type="text/javascript">

        var lastViewedCat = 0;
        var lastSortBy = 'drillname';

        // Check current filters for errors
        $(document).ready(function () {
        });

        function checkFilters() {
            var filter_error = false;

            // Sources
            if (!$('#source_1').attr('checked') && !$('#userdrills').attr('checked')) {
                $("#source_error").show();
                filter_error = true;
            } else {
                $("#source_error").hide();
            }

            // Language
            if (!$('#lang_en').attr('checked') && !$('#lang_fr').attr('checked')) {
                $("#lang_error").show();
                filter_error = true;
            } else {
                $("#lang_error").hide();
            }

            // Ice Types
            if (!$('#ice_full').attr('checked') && !$('#ice_half').attr('checked')) {
                $("#ice_error").show();
                filter_error = true;
            } else {
                $("#ice_error").hide();
            }

            // Age Levels
            if (!$('#age_1').attr('checked') && !$('#age_2').attr('checked') && !$('#age_3').attr('checked') && !$('#age_4').attr('checked') && !$('#age_5').attr('checked') && !$('#age_6').attr('checked')) {
                $("#age_error").show();
                filter_error = true;
            } else {
                $("#age_error").hide();
            }

            // Display Filter Warning Message (If Necessary)
            if (filter_error) {
                $("#filter_error").show();
            } else {
                $("#filter_error").hide();
            }

            return filter_error;
        }

        function favUnfav(drillid) {
            $('#fav_' + drillid).html('<images src="/images/large-progress-indicator.gif" height="16" width="16" alt="Loading..." border="0" />');
            $('#fav_' + drillid).load('ajax/toggle_favorites.php?drillid=' + encodeURIComponent(drillid));
        }

        function toggleFav(drillid) {
            $.getJSON('ajax/toggle_favorites_json.php', {
                drillid: drillid
            }, function (data) {
                if (data.status == 'OK') {
                    $('#mf_' + drillid).removeClass("fa-star fa-star-o");
                    if (data.result == 1) {
                        $('#mf_' + drillid).addClass("fa-star");
                    } else {
                        $('#mf_' + drillid).addClass("fa-star-o");
                    }
                } else {
                    alert('Error');
                }

            });
        }

        function loadDrills(categoryid, sortby, userid) {
            var curdiv = document.getElementById('cat_' + categoryid);
            if (curdiv.style.display == "none") {
                curdiv.style.display = '';
            }
            $('#cat_' + categoryid).html('<images src="/images/large-progress-indicator.gif" height="32" width="32" alt="Loading..." border="0" /> Loading Drills');
            $('#cat_' + categoryid).load('ajax/publicDrills.php?catid=' + encodeURIComponent(categoryid) + '&sortby=' + encodeURIComponent(sortby) + '&uid=' + encodeURIComponent(userid));

            lastViewedCat = categoryid;
            lastSortBy = sortby;
        }

        function checkDrillSource(key, value) {

            if (checkFilters()) {
                alert('Please select at leaston one drill source.');
            } else {
                saveFilters(key, value);
            }

        }

        function checkDrillIce(key, value) {
            if (checkFilters()) {
                alert('Please select either half-ice or full-ice drills.');
            } else {
                saveFilters(key, value);
            }

        }

        function checkDrillLang(key, value) {
            if (checkFilters()) {
                alert('Please select at leaston one drill language.');
            } else {
                saveFilters(key, value);
            }
        }


        function checkDrillAge(key, value) {
            if (checkFilters()) {
                alert('Please select at leaston one age level.');
            } else {
                saveFilters(key, value);
            }
        }


        function saveFilters(key, value) {
            var curdiv = document.getElementById('filterSave');
            if (curdiv.style.display == "none") {
                curdiv.style.display = '';
            }

            $('#filterSave').html('<images src="/images/large-progress-indicator.gif" height="32" width="32" alt="Loading..." border="0" /> Updating...');
            $('#filterSave').load('ajax/filterKey.php?key=' + encodeURIComponent(key) + '&value=' + encodeURIComponent(value));

            if (lastViewedCat > 0) {
                setTimeout("loadDrills(lastViewedCat,lastSortBy, 0)", 1000);
            }

            //setTimeout("reloadDrillCounts()", 2000);
            setTimeout("getDrillCountTotal()", 2000);


        }

        function getDrillCount(catid, uid) {
            //$('#found_'+catid).html('<images src="/images/large-progress-indicator.gif" height="16" width="16" alt="Loading..." border="0" />');
            //$('#found_'+catid).load('ajax/publicDrills.php?catid='+encodeURIComponent(catid)+'&uid='+encodeURIComponent(uid));

        }

        function getNewDrillCount(catid, uid, newl) {
            $('#new_' + catid).load('ajax/drillCount.php?catid=' + encodeURIComponent(catid) + '&uid=' + encodeURIComponent(uid) + '&new=' + newl);
        }


        function getDrillCountTotal() {
            $('#displayedCount').html('<images src="/images/large-progress-indicator.gif" height="16" width="16" alt="Loading..." border="0" />');
            $('#displayedCount').load('ajax/drillCount.php?catid=0&uid=' + encodeURIComponent(0));
        }


    </script>
    <script language="javascript" type="text/javascript">
        getDrillCountTotal();
    </script>
    <!-- Drills-->
    <!--lear-more-->
    <script type="text/javascript">
        var diagrammer = document.getElementById('html5-diagrammer');

        var obj = {
            appURL: "http://hockeyshare.com/drills/add/animation/",
            serviceURL: "http://hockeyshare.com/drills/add/animation/service.php",
            userID: "1",
            sesID: "73",
            demoMode: "1",
            drillID: "191674",
            jpgQty: "0", // jpgQty: 0 is low quality and 1 otherwise
            hs_logo: "0",  // hs_logo: 1 to submilate logo and 0 otherwise
            animation_available: "1" // animation flag
        };

        var queryString = document.location.search;

        if (queryString[0] == '?') {
            queryString = queryString.substr(1, queryString.length - 1);
        }

        if (queryString) {
            var params = queryString.split('&');
            for (var i = 0; i < params.length; i++) {
                var arr = params[i].split('=');

                obj[arr[0]] = arr[1];
            }
        }

        var url = "http://hockeyshare.com/drills/html5/";
        var hash = [];
        for (var key in obj) {
            hash.push(key + "=" + obj[key]);
        }

        diagrammer.src = url + '?' + hash.join('&');
    </script>
    <!--lear-more-->

    <style type="text/css">

    </style>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-597508-2', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<!--PLAYER-START-->
<script type="text/javascript">

    function validateForm() {

        if ($('#first').val() == '') {
            alert('Please enter your first name');
        } else if ($('#last').val() == '') {
            alert('Please enter your last name');
        } else if ($('#gender1').val() == '' && $('#gender2').val() == '') {
            alert('Please select your gender');
        } else if ($('#goal').val() == '') {
            alert('Please enter your goal');
            $('#goal').val('10000');
        } else if ($('#birthyear').val() == '') {
            alert('Please select your birth year');
        } else {
            playerForm.submit();
        }

    }
</script>

<!--Team-new-team-->
<script type="text/javascript">
    $(document).ready(function () {
        $("#track_start_date").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
<body>
<!-- VIDEO-->
<script language="javascript">

    function changeVideo(vid) {
        $('#youtube').html('<div align="center" style="border:1px solid #999; padding:10px; width:853px; height:480px; vertical-align:middle; border-radius:5px;"><br /><br /><br /><br /><img src="/images/large-progress-indicator.gif" height="32" width="32" alt="Loading..." border="0" align="absmiddle" style="padding-right:10px;" /> Loading Video...</div>');
        $('#youtube').load('ajax/load_video.php?vid=' + encodeURI(vid));
    }

    $(document).ready(function () {
        changeVideo('MNDA22oZv6Q');
    });

</script>
<!--Blog-->
<script src="/3/js/video.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    // Must come after the video.js library

    // Add VideoJS to all video tags on the page when the DOM is ready
    VideoJS.setupAllWhenReady();

    /* ============= OR ============ */

    // Setup and store a reference to the player(s).
    // Must happen after the DOM is loaded
    // You can use any library's DOM Ready method instead of VideoJS.DOMReady

    /*
     VideoJS.DOMReady(function(){

     // Using the video's ID or element
     var myPlayer = VideoJS.setup("example_video_1");

     // OR using an array of video elements/IDs
     // Note: It returns an array of players
     var myManyPlayers = VideoJS.setup(["example_video_1", "example_video_2", video3Element]);

     // OR all videos on the page
     var myManyPlayers = VideoJS.setup("All");

     // After you have references to your players you can...(example)
     myPlayer.play(); // Starts playing the video for this player.
     });
     */

    /* ========= SETTING OPTIONS ========= */

    // Set options when setting up the videos. The defaults are shown here.

    /*
     VideoJS.setupAllWhenReady({
     controlsBelow: false, // Display control bar below video instead of in front of
     controlsHiding: true, // Hide controls when mouse is not over the video
     defaultVolume: 0.85, // Will be overridden by user's last volume if available
     flashVersion: 9, // Required flash version for fallback
     linksHiding: true // Hide download links when video is supported
     });
     */

    // Or as the second option of VideoJS.setup

    /*
     VideoJS.DOMReady(function(){
     var myPlayer = VideoJS.setup("example_video_1", {
     // Same options
     });
     });
     */

</script>
<script src="//js.honeybadger.io/v0.4/honeybadger.min.js" type="text/javascript"
        data-api_key="040fe08a"></script>


<script type="text/javascript" src="js/all.min.js"></script>
<!-- Include the VideoJS Stylesheet -->
<link rel="stylesheet" href="/3/css/video-js.css" type="text/css" media="screen" title="Video JS">
<!--Register-->
<script type="text/javascript">

    function checkUsername() {

        var username = $('#username').val();

        var val = $.ajax({
            url: "ajax_username_check.php?username=" + encodeURIComponent(username),
            async: false
        }).responseText;

        if (val == 1) {
            // AVAILABLE
            $('#username_available').html('<span class="available_reg">Available</span>');
        } else if (val == 2) {
            // NOTHING ENTERED
            $('#username_available').html('');
        } else {
            // TAKEN
            $('#username_available').html('<span class="notavailable_reg">Username Already in Use</span>');
        }
    }

    function checkEmail() {

        var email = $('#email').val();

        var val = $.ajax({
            url: "ajax_email_check.php?email=" + encodeURIComponent(email),
            async: false
        }).responseText;

        if (val == 1) {
            // AVAILABLE
            $('#email_status').html('');
        } else if (val == 2) {
            // NOTHING ENTERED
            $('#email_status').html('');
        } else {
            // TAKEN
            $('#email_status').html('<span class="notavailable_reg">NOTE: An account with this email already exists <a href="http://hockeyshare.com/login/">Log in</a>.</span>');
        }
    }
</script>
<!--Search-->
<script>
    (function() {
        var cx = '017216014522259273157:58lgojtm5ja';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();
</script>

<!--GAME-TAPE-->


<script language="javascript" src="js/index.js" type="text/javascript">


</script>

<script>
    (function() {
        var cx = '017216014522259273157:58lgojtm5ja';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();
</script>

@yield('images')
@yield('training')
@extends('includes/footer')
</body>

