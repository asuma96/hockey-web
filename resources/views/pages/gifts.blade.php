@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1><img src="/img/gifts.jpg" alt="Gift Subscriptions" height="100" width="100" border="0"
                 align="right"/>HockeyShare Gift Certificates</h1>
        <p>Want to get something useful for your coach? How about a subscription to HockeyShare? Below are the gift
            certificate options available.</p>
        <p>HockeyShare gift certificates and subscriptions are typically available for <strong>immediate use</strong>
            upon purchase, and can be <strong>emailed directly to the recipient</strong> at checkout.</p>

        <div class="gifts">
            <table width="100%">
                <tr>
                    <td width="215" align="center" valign="top"><a
                                href="http://hockeyshare.com/drills/signup_gift.php"><img
                                    src="/img/hs_certificate.jpg" alt="Gift Subscriptions" height="200"
                                    width="200" border="0"/></a></td>
                    <td valign="top">
                        <h3 style="color:#B52222;">1 Year Gift Subscription to Advanced Coaching Platform</h3>
                        <p>Give your coach unlimited access to our Online Drill Diagrammer and Practice Planner with a
                            subscription to HockeyShare's Advanced Coaching Platform. Coaches around the world agree
                            HockeyShare saves them time and helps keep them organized.</p>
                        <p><strong>Delivery</strong>: Instant Digital Delivery, Email Delivery Available (Optional)</p>
                        <p><strong>Total: $75.00</strong>&nbsp;&nbsp;&nbsp;<span class="subtle_nu">WI residents subject to sales tax</span>
                        </p>
                        <p>&raquo; <a href="http://hockeyshare.com/drills/signup_gift.php"
                                      class="gifts_p">Click Here to
                                Purchase</a></p>

                    </td>
            </table>
        </div>
    @include('includes.commercial')
    <br>
</div>
</div>

