@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <!-- Content -->
        <h2>Search HockeyShare</h2>
        <p>Looking for something in particular? Search our site below!</p>
        <gcse:searchbox-only></gcse:searchbox-only>
        <gcse:searchresults-only></gcse:searchresults-only>

        @include('includes.commercial')
        <br>
    </div>
</div>