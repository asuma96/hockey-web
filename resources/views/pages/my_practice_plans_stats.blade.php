@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h2>Practice Plan Stats</h2>

        <form method="post" action="">
            Start Date: <input type="text" name="start" value="2017-05-02"> - End Date: <input type="text" name="end"
                                                                                               value="2017-06-01">
            <input type="submit" name="Submit" value="Go">
        </form>

        <br/>
        @include('includes.commercial')
    </div>
</div>