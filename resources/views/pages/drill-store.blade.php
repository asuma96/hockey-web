@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <div class="drill_store">

            <br/>

            <h1>HockeyShare Drill Store</h1>

            <p>Looking for more drill ideas? Learn from professional coaches with Drill Books in HockeyShare's Drill
                Store. The Drill Books available have been put together to fill specific training needs. When you
                purchase a Drill Book, the drills are automatically imported into your HockeyShare account (drills are
                added to your My Drills page, not emailed as a pdf) for immediate use in your practice plans. Get the
                edge and get fresh ideas with HockeyShare's select Drill Books.</p>

            <div class="thinkstrong_subheader text-primary-color">TTH7 - *SPECIAL* ALL 3 TIM TURK DRILL PACKAGES (TTH4,
                TTH5, TTH6)
                <div class="chapters_scroll text-primary-color">Tim Turk Hockey - 2016</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top">
                        <img src="/img/turk7.jpg" alt="3 Pack Special - by Tim Turk Hockey" class="drill_image"/>
                    </td>
                    <td valign="top" class="resize_text">
                        <div class="drill_info drill_margin ">
                            <p><span class="bold resize_text">Author: </span> <span class="drill_author resize_text">Tim Turk - NHL Skills Coach - Tim Turk Hockey</span>
                            </p>
                            <p><span class="bold resize_text">Drill Count: </span> <span
                                        class="drill_author resize_text">75</span></p>
                        </div>
                        <p class="resize">Special offer! All 3 Tim Turk drill packages (75 total drills). This special
                            offer includes the following products: TTH4, TTH5, TTH6.<br>
                            Visit <a href="http://www.timturkhockey.com" target="_blank">www.timturkhockey.com</a> for
                            more info on Tim Turk.
                        <p>
                            <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=turk7"
                                               class="twitter_button">Purchase for $19.99</a></div>
                    </td>
                </tr>
            </table>

            <div class="thinkstrong_subheader text-primary-color">TTH6 - Shooting Drills Utilizing the NZ
                <div class="chapters_scroll text-primary-color">Tim Turk Hockey - 2016</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top" class="resize_text">
                        <img src="/img/turk6.jpg" alt="Custom Drill Package - by Tim Turk Hockey" class="drill_image"/>
                    </td>
                    <td valign="top">
                        <div class="drill_info drill_margin">
                            <p><span class="bold">Author: </span> <span class="drill_author">Tim Turk - NHL Skills Coach - Tim Turk Hockey</span>
                            </p>
                            <p><span class="bold">Drill Count: </span> <span class="drill_author">25</span></p>
                        </div>
                        <p>Bring your game to the next level with these 25 Shooting drills utilizing the neutral zone
                            with transitions, pivots and multiple passes and shots. Visit <a
                                    href="http://www.timturkhockey.com" target="_blank">www.timturkhockey.com</a> for
                            more info on Tim Turk.
                        <p>
                            <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=turk6"
                                               class="twitter_button">Purchase for $7.99</a></div>
                    </td>
                </tr>
            </table>

            <div class="thinkstrong_subheader text-primary-color">TTH5 - Interactive Drill Package
                <div class="chapters_scroll text-primary-color">Tim Turk Hockey - 2016</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top">
                        <img src="/img/turk5.jpg" alt="Custom Drill Package - by Tim Turk Hockey" class="drill_image"/>
                    </td>
                    <td valign="top">
                        <div class="drill_info drill_margin">
                            <p><span class="bold">Author: </span> <span class="drill_author">Tim Turk - NHL Skills Coach - Tim Turk Hockey</span>
                            </p>
                            <p><span class="bold">Drill Count: </span> <span class="drill_author">25</span></p>
                        </div>
                        <p>25 interactive drills for all ages and levels that include... Continuous Shooting, Direct and
                            Indirect passing. Basic 2 on 0 simulated progressions to help with positioning and elevating
                            NET presence! Visit <a href="http://www.timturkhockey.com" target="_blank">www.timturkhockey.com</a>
                            for more info on Tim Turk.
                        <p>
                            <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=turk5"
                                               class="twitter_button">Purchase for $7.99</a></div>
                    </td>
                </tr>
            </table>

            <div class="thinkstrong_subheader text-primary-color">TTH4 - Custom Drill Package
                <div class="chapters_scroll text-primary-color">Tim Turk Hockey - 2016</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top">
                        <img src="/img/turk4.jpg" alt="Custom Drill Package - by Tim Turk Hockey" class="drill_image"/>

                    </td>
                    <td valign="top">
                        <div class="drill_info drill_margin">
                            <p><span class="bold">Author: </span> <span class="drill_author">Tim Turk - NHL Skills Coach - Tim Turk Hockey</span>
                            </p>
                            <p><span class="bold">Drill Count: </span> <span class="drill_author">25</span></p>
                        </div>
                        <p>25 Custom shooting and scoring drills that can be used throughout the season. For all ages
                            and levels. Drills focus on Shooting, Passing, Timing, Transition, Breakouts, Continuous
                            game situated flow and more! Visit <a href="http://www.turksSnipers.com" target="_blank">www.turksSnipers.com</a>
                            and <a href="http://www.timturkhockey.com" target="_blank">www.timturkhockey.com</a> for
                            more info on Tim Turk.
                        <p>
                            <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=turk4"
                                               class="twitter_button">Purchase for $7.99</a></div>
                    </td>
                </tr>
            </table>
            <div class="thinkstrong_subheader text-primary-color">Advanced Drill Book: Volume 1
                <div class="chapters_scroll text-primary-color">Weiss Tech Hockey - 2014</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top">
                        <img src="/img/wth_adb_1.png" alt="Advanced Drill Book: Volume 1 - by Weiss Tech Hockey"
                             class="drill_image"/>
                    </td>
                    <td valign="top">
                        <div class="drill_info drill_margin">
                            <p><span class="bold">Author: </span> <span class="drill_author">Jeremy Weiss - Weiss Tech Hockey</span>
                            </p>
                            <p><span class="bold">Drill Count: </span> <span class="drill_author">101</span></p>
                        </div>
                        <p>101 Hockey Drills designed specifically for Advanced teams. The drills contained in this book
                            are literally a world-wide compilation. Some are drills I picked up as a player, some are
                            drills I’ve come across from books and other resources, many are drills that have been sent
                            to me from friends, coaches, and acquaintances I've met online.
                        <p>
                            <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=adb1"
                                               class="twitter_button">Purchase for $9.99</a></div>
                    </td>
                </tr>
            </table>

            <div class="thinkstrong_subheader text-primary-color">Advanced Drill Book: Volume 2
                <div class="chapters_scroll text-primary-color">Weiss Tech Hockey - 2015</div>
            </div>

            <table class="package_info">
                <tr>
                    <td valign="top">
                        <img src="/img/wth_adb_2.png" alt="Advanced Drill Book: Volume 2 - by Weiss Tech Hockey"
                             class="drill_image"/>
                    </td>
                    <td valign="top">
                        <div class="drill_info drill_margin">
                            <p><span class="bold">Author: </span> <span class="drill_author">Jeremy Weiss - Weiss Tech Hockey</span>
                            </p>
                            <p><span class="bold">Drill Count: </span> <span class="drill_author">101</span></p>
                        </div>
                        <p>The second installment of our advanced drills series. 101 Hockey Drills designed specifically
                            for Advanced teams. As is the case with Volume 1, the drills contained in this book are
                            literally a world-wide compilation. Some are drills I picked up as a player, some are drills
                            I've come across from books and other resources, many are drills that have been sent to me
                            from friends, coaches, and acquaintances I've met online.
                        <p>
                        <p><span class="bold">Categories:</span> Agility, Angling, Competition, Conditioning, Goalie,
                            Passing, Small Area Games, Shooting, Skating, Warm-up</p>
                        <br/>
                        <div align="center"><a href="http://hockeyshare.com/drill-store/purchase.php?id=adb2"
                                               class="twitter_button">Purchase for $9.99</a></div>
                    </td>
                </tr>
            </table>

            <div class="drills_faq">Frequently Asked Questions</div>
            <div class="package_info">

                <div class="drills_faq_question ">
                    <span class="bold">How soon will the drills be available after purchase?</span>
                    <br><br>
                    <p>Drills are typically imported into your HockeyShare account within 5 minutes of a successful
                        purchase.</p>
                </div>

                <div class="drills_faq_question">
                    <span class="bold">In what format will the drills be imported?</span>
                    <br><br>

                    <p>Drills will be imported as native HockeyShare drills. This will allow you to manipulate the drill
                        just like any other drill you created in your account.</p>
                </div>

                <div class="drills_faq_question">
                    <span class="bold">Why aren't the new drills showing up in my account?</span>
                    <br>
                    <br>
                    <p>If your newly purchased drills aren't showing up in your account yet, click the Reload Drill List
                        link at the top of the My Drills page. Please note that it may take a few minutes to get all the
                        drills from the drill pack imported to your account. If the new drills still are not displaying,
                        please contact us.</p>
                </div>

                <div class="drills_faq_question">
                    <span class="bold">Are there any limitations to the purchased drills?</span>
                    <br><br>

                    <p>The only limitation to the purchased drills is you will not be able to send them via our Drill
                        Inbox feature. Otherwise, the drills will be the same as the rest of them in your My Drills
                        list. </p>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        @include('includes.commercial')
        <br>
    </div>

</div>