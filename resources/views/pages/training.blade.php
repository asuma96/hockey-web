@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Hockey Training Programs</h2>

        <p>HockeyShare is excited to offer the best online training products. In our continued effort to promote the
            game and help coaches and athletes grow, we have created premium content and made strategic partnerships
            with other high-quality content creators. Click on a training program below to learn more.</p>

        <div class="training-logo">
            <p><img src="/img/hs_logo.png" height="65" width="238"
                    alt="Defense Shooting Progression"/></p>

            <p>
                <span class="mental">Defensemen Shooting Progression Video Series</span><br/><span
                        class="subtle_nu"><strong>Program Focus: </strong> Defensemen Development, Position-Specific</span>
            </p>

            <p>Defensemen often get overlooked in training. Get your d corps training with a purpose. This online video
                series is approximately 20 minutes long and covers 17 progressions of shooting progressions to help your
                defensemen contribute more offensively.</p>

            <p><a href="d-vol1/" class="rpbutton">Learn More / View Program</a></p>

        </div>

        <div class="training-logo">

            <p><img src="/img/ada_logo.png" height="65" width="261"
                    alt="Different Approach"/></p>

            <p><span class="mental">Re-Think Your Coaching Process</span><br/><span
                        class="subtle_nu"><strong>Program Focus: </strong> Season Planning, Mental Toughness, Coaching Assessments, Communication</span>
            </p>

            <p><strong><em>A Different Approach</em></strong> is designed to give you a fresh perspective on your
                season. In this program we focus on the elements of planning and building with intention. The program
                contains 18 chapters with video segments describing in detail the topic of the week and offers
                application questions to get the most out of the series.</p>

            <p><a href="different-approach/" class="rpbutton">Learn More / View Program</a></p>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

