@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="logo_10k_container center_10k">
            <div class="logo_10k_container center_10k">
                <a href="http://www.avantlink.com/click.php?tt=cl&amp;mi=11681&amp;pw=189934&amp;url=http%3A%2F%2Fwww.hockeyshot.com%2F"
                   target="_blank"><img src="/img/1000x100_10k.jpg"
                                        class="center_10k" alt="Sponsored by HockeyShot"></a>
            </div>
        </div>
        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="https://www.instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="https://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>
        <div class="section1_10k">
            <h3 class="title-text">What is 10,000 Pucks?</h3>
            <div>
                <div class="section1_right"><img src="/img/10k_player.png" alt="Hockey Player Shooting Puck"
                                                 class="section1_right_img"/></div>
                <p>10,000 Pucks is designed to allow players to track their shots <span class="highlight_10k">365 days a year</span>.
                    Each summer we run a contest to see who can shoot the most pucks - this contest runs from June 1 -
                    August 31. Players can begin tracking shots any time, but only shots logged during the contest dates
                    will count toward the contest totals. Our goal with this platform is not simply focused on the
                    quantity of shots, but on the setting and achieving of goals for hockey players. Best of all, <span
                            class="highlight_10k">10k Pucks is free for all users</span>.</p>

                <h4 class="title-text">Getting Started is Easy</h4>
                <p>It looks like you aren't currently logged in to HockeyShare. If you already have an account, please
                    go to <a class="underline" href="http://hockeyshare.com/login/">www.hockeyshare.com/login</a> to log
                    in to your
                    account. If you don't have one yet, don't worry - <span
                            class="highlight_10k">accounts are free</span> - just head to <a class="underline"
                                                                                             href="http://hockeyshare.com/register/">www.hockeyshare.com/register/</a>
                    to get
                    started, then come back to this page.</p>
            </div>
        </div>
        <div class="section2_10k">
            <h3 class="title-text">Getting Started</h3>

            <div class="center_10k">
                <div class="column_10k">
                    <a href="player/"><i class="fa fa-user fa-5x"></i></a>
                </div>

                <div class="column_10k">
                    <a href="team/"><i class="fa fa-group fa-5x"></i></a>
                </div>

                <div class="column_10k">
                    <a href="association/"><i class="fa fa-cubes fa-5x"></i></a>
                </div>
            </div>

            <div class="center_10k topPad10_10k">
                <div class="column_10k center_10k">
                    <a href="player/"><span class="darkbutton_10k">Player</span></a>
                </div>

                <div class="column_10k">
                    <a href="team/"><span class="medbutton_10k">Team</span></a>
                </div>

                <div class="column_10k">
                    <a href="association/"><span class="lightbutton_10k">Association</span></a>
                </div>
            </div>

            <div class="center_10k topPad10_10k">
                <div class="column_10k">
                    <span class="lightgray_10k">Shoot pucks, track your progress</span>
                </div>

                <div class="column_10k">
                    <span class="lightgray_10k">Manage and track a group of players</span>
                </div>

                <div class="column_10k">
                    <span class="lightgray_10k">Manage and track multiple teams</span>
                </div>
            </div>
            <p>&nbsp;</p>
        </div>

        <div class="section4_10k">
            <div class="section4_content">
                <h3 class="title-text">10k Pucks by the Numbers</h3>
                <p>Shots recorded since 2007: <span class="highlight_10k">88,242,350</span></p>
                <p>Number of currently active players: <span class="highlight_10k">19,920</span></p>
                <p>Number of shots recorded in most recent contest: <span class="highlight_10k">11,461,876</span></p>
                <p>Number of active teams: <span class="highlight_10k">2,498</span></p>
                <p>Number of active associations: <span class="highlight_10k">545</span></p>
                <p>First year of 10k Pucks: <span class="highlight_10k">2007</span></p>
                <p>Number of days per year you can track shots: <span class="highlight_10k">365</span></p>
            </div>
        </div>
        <p>&nbsp;</p>
        @include('includes.commercial')
        <br>
    </div>
</div>
