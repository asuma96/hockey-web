@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <h1>Hockey Training Videos - 141 Total</h1>

        <a class="underline" name="top">
            </a>
        <table width="100%" border="0" cellspacing="3" cellpadding="3">
            <tbody>
            <tr>
                <td align="center" valign="top">
                    <div id="youtube">
                        <iframe width="853" height="480" src="//www.youtube.com/embed/MNDA22oZv6Q?rel=0" frameborder="0"
                                allowfullscreen=""></iframe>
                        <br>
                        <div class="links-video">Share Video: <input type="text" class="video-text-width"
                                                                     value="http://hockeyshare.com/video/?vid=MNDA22oZv6Q">
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>

        <div class="video-systems">
            10,000 Pucks
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" class="underline" href="#top"
                                                    onclick="changeVideo('OD_05YmAXPA');"><img
                                    src="//img.youtube.com/vi/OD_05YmAXPA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-09-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>10,000 Pucks - Thank You</strong>

                        <p class="subtle_nu">10,000 Pucks thank you video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('OD_05YmAXPA')">Watch Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('wsOA89aFMg4');"><img
                                    src="//img.youtube.com/vi/wsOA89aFMg4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-08-29"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #13 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #13 skill video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('wsOA89aFMg4')">Watch Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JF71bHDk1y0');"><img
                                    src="//img.youtube.com/vi/JF71bHDk1y0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-08-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #12 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #12 skill video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('JF71bHDk1y0')">Watch Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('xLiAXJbK38Q');"><img
                                    src="//img.youtube.com/vi/xLiAXJbK38Q/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-08-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #11 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #11 skill video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('xLiAXJbK38Q')">Watch Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('GIknuzUdrnk');"><img
                                    src="//img.youtube.com/vi/GIknuzUdrnk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-08-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #10 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #10 skill video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('GIknuzUdrnk')">Watch Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('L_cht9aLIfc');"><img
                                    src="//img.youtube.com/vi/L_cht9aLIfc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-07-30"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #9 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #9 skill video.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('L_cht9aLIfc')">Watch Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('qUWRh-k7qRg');"><img
                                    src="//img.youtube.com/vi/qUWRh-k7qRg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-07-22"/></a>

                    </td>
                    <td width="185" valign="top"><strong>10,000 Pucks - Quality</strong>

                        <p class="subtle_nu">10,000 Pucks reminder to shoot with quality.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('qUWRh-k7qRg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('XByyzuvwFog');"><img
                                    src="//img.youtube.com/vi/XByyzuvwFog/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-07-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #8 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #8 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('XByyzuvwFog')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('juAdQy_nwgE');"><img
                                    src="//img.youtube.com/vi/juAdQy_nwgE/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-07-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #7 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #7 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('juAdQy_nwgE')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('zbG5fnccat0');"><img
                                    src="//img.youtube.com/vi/zbG5fnccat0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-06-28"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #6 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #6 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('zbG5fnccat0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('kJco82AVVzg');"><img
                                    src="//img.youtube.com/vi/kJco82AVVzg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-06-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #5 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #5 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('kJco82AVVzg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('3kqrJn043L0');"><img
                                    src="//img.youtube.com/vi/3kqrJn043L0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-06-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #4 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #4 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('3kqrJn043L0')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Uw18Zbs0AaE');"><img
                                    src="//img.youtube.com/vi/Uw18Zbs0AaE/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-06-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #3 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #3 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Uw18Zbs0AaE')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('ZPmpoe8QQJk');"><img
                                    src="//img.youtube.com/vi/ZPmpoe8QQJk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-05-28"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #2 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #2 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('ZPmpoe8QQJk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('aL9dnGFfPpc');"><img
                                    src="//img.youtube.com/vi/aL9dnGFfPpc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-05-22"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Week #1 Skill Video</strong>

                        <p class="subtle_nu">10,000 Pucks week #1 skill video.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('aL9dnGFfPpc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('xDjZclbHqV8');"><img
                                    src="//img.youtube.com/vi/xDjZclbHqV8/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-05-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>10,000 Pucks - Introduction</strong>

                        <p class="subtle_nu">10,000 Pucks welcome / introduction.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('xDjZclbHqV8')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Checking
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('FzR7tpfwZ4s');"><img
                                    src="//img.youtube.com/vi/FzR7tpfwZ4s/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-11-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Checking Training</strong>

                        <p class="subtle_nu">Training tips for developing checking skills.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('FzR7tpfwZ4s')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('FgkxNLxLR0s');"><img
                                    src="//img.youtube.com/vi/FgkxNLxLR0s/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-01-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Delivering a Check</strong>

                        <p class="subtle_nu">Key points and explanation of checking.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('FgkxNLxLR0s')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JrDWtvJvt4g');"><img
                                    src="//img.youtube.com/vi/JrDWtvJvt4g/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-01-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Receiving a Check</strong>

                        <p class="subtle_nu">Properly receive a body check along the wall.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('JrDWtvJvt4g')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Defensemen
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JHM6So3B9Dw');"><img
                                    src="//img.youtube.com/vi/JHM6So3B9Dw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-02-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Poke Check</strong>

                        <p class="subtle_nu">Tips and Techniques for an effective poke check.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('JHM6So3B9Dw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('oerpWcL9el4');"><img
                                    src="//img.youtube.com/vi/oerpWcL9el4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-02-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defensemen Hinge Play</strong>

                        <p class="subtle_nu">Basic "hinge" play for defensemen.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('oerpWcL9el4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Jod0DMxxc3k');"><img
                                    src="//img.youtube.com/vi/Jod0DMxxc3k/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-02-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Puck Movement Agility</strong>

                        <p class="subtle_nu">Lateral movement, agility, and puck protection drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Jod0DMxxc3k')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('2fHESOVrqRM');"><img
                                    src="//img.youtube.com/vi/2fHESOVrqRM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-02-01"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Agility "W" Drill</strong>

                        <p class="subtle_nu">Skating and agility drill for D.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('2fHESOVrqRM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('UC6rZsghhvk');"><img
                                    src="//img.youtube.com/vi/UC6rZsghhvk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-11-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defensive Positioning</strong>

                        <p class="subtle_nu">Defensive side positioning concept.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('UC6rZsghhvk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('dabpxb5e3Aw');"><img
                                    src="//img.youtube.com/vi/dabpxb5e3Aw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-10-20"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Zone Pickup</strong>

                        <p class="subtle_nu">Basics of retrieving the puck in the D zone.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('dabpxb5e3Aw')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('HQQBoplgcDI');"><img
                                    src="//img.youtube.com/vi/HQQBoplgcDI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-10-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D NZ Transition Options</strong>

                        <p class="subtle_nu">Options for D retrieving the puck in the NZ.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('HQQBoplgcDI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('dpT8X4wjUlM');"><img
                                    src="//img.youtube.com/vi/dpT8X4wjUlM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-10-04"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Walk to Middle</strong>

                        <p class="subtle_nu">Techniques &amp; options for D to walk to the middle of the ice.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('dpT8X4wjUlM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('8WPEAvUPZDI');"><img
                                    src="//img.youtube.com/vi/8WPEAvUPZDI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Keep-In Drill</strong>

                        <p class="subtle_nu">Drill for D to work on keeping pucks in the zone.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('8WPEAvUPZDI')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('gOXRRwaDFmk');"><img
                                    src="//img.youtube.com/vi/gOXRRwaDFmk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-05-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defense Recovery</strong>

                        <p class="subtle_nu">Recovering from getting beat on a rush.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('gOXRRwaDFmk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('HpgcPLA5LBk');"><img
                                    src="//img.youtube.com/vi/HpgcPLA5LBk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-03-02"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Net Protection</strong>

                        <p class="subtle_nu">Concepts of using the net for protection.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('HpgcPLA5LBk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('x-zpzNcl7mw');"><img
                                    src="//img.youtube.com/vi/x-zpzNcl7mw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-05-05"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Shadow Drill</strong>

                        <p class="subtle_nu">Mobility development drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('x-zpzNcl7mw')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('WaKSHEHo2Co');"><img
                                    src="//img.youtube.com/vi/WaKSHEHo2Co/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-10-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Lateral Movement Tips</strong>

                        <p class="subtle_nu">Drills &amp; tips for D to move to the middle of the ice.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('WaKSHEHo2Co')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>


        <div class="video-systems">
            Forwards
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('vz_xV6ivMqM');"><img
                                    src="//img.youtube.com/vi/vz_xV6ivMqM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-09-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Attacking the Low Seam</strong>

                        <p class="subtle_nu">Concepts and drill base for low seam attack.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('vz_xV6ivMqM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('_nimsPFq0IQ');"><img
                                    src="//img.youtube.com/vi/_nimsPFq0IQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-11-24"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Strong-Side Breakout</strong>

                        <p class="subtle_nu">Teaching &amp; coaching points for strong side breakouts.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('_nimsPFq0IQ')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('t2pI7FHIy14');"><img
                                    src="//img.youtube.com/vi/t2pI7FHIy14/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-11-22"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Attacking the Point</strong>

                        <p class="subtle_nu">Tips for covering the point man.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('t2pI7FHIy14')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('LdlL6QlMhN0');"><img
                                    src="//img.youtube.com/vi/LdlL6QlMhN0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-08-03"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Escape Move</strong>

                        <p class="subtle_nu">Zone entry concept to buy time and space.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('LdlL6QlMhN0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('kuZABXbVMgc');"><img
                                    src="//img.youtube.com/vi/kuZABXbVMgc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-04-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Receiving a Breakout Pass</strong>

                        <p class="subtle_nu">Options when receiving a breakout pass.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('kuZABXbVMgc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Goalies
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('37hrNhmser4');"><img
                                    src="//img.youtube.com/vi/37hrNhmser4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2015-04-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Approaching Off Season Training</strong>

                        <p class="subtle_nu">Tips for planning your off season.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('37hrNhmser4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('RKrTxb1EmHY');"><img
                                    src="//img.youtube.com/vi/RKrTxb1EmHY/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2015-02-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Goaltending Down the Home Stretch</strong>

                        <p class="subtle_nu">Tips for getting your goalies ready.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('RKrTxb1EmHY')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('hBd7avyPNJ8');"><img
                                    src="//img.youtube.com/vi/hBd7avyPNJ8/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-07-01"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Communication in Practices</strong>

                        <p class="subtle_nu">Ideas for communicating with your goaltenders in practices.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('hBd7avyPNJ8')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('dNQHZ1SuIYI');"><img
                                    src="//img.youtube.com/vi/dNQHZ1SuIYI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-05-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Goalie Thoughts on 2014 Playoffs</strong>

                        <p class="subtle_nu">Thoughts from a goalie perspective on the 2014 playoffs.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('dNQHZ1SuIYI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('iSJbTidsgGc');"><img
                                    src="//img.youtube.com/vi/iSJbTidsgGc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-03-20"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Evaluating Tracking</strong>

                        <p class="subtle_nu">Tips for evaluating a goalie's tracking ability.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('iSJbTidsgGc')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('gT7PORUjG-s');"><img
                                    src="//img.youtube.com/vi/gT7PORUjG-s/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-03-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Tryout Selection Tips</strong>

                        <p class="subtle_nu">Tips for selecting goaltenders at tryouts.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('gT7PORUjG-s')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Misc
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('g3B298fPGsA');"><img
                                    src="//img.youtube.com/vi/g3B298fPGsA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-12-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Think Strong Hockey</strong>

                        <p class="subtle_nu">Introducing Think Strong Hockey.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('g3B298fPGsA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('QyUKbMvHumc');"><img
                                    src="//img.youtube.com/vi/QyUKbMvHumc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-08-18"/></a>

                    </td>
                    <td width="185" valign="top"><strong>ALS Ice Bucket Challenge</strong>

                        <p class="subtle_nu">ALS Ice Bucket Challenge!</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('QyUKbMvHumc')">Watch
                                Video</a></p></td>
                    <td width="120">&nbsp;</td>
                    <td width="185">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Off Ice Training
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('rGRex96EmFY');"><img
                                    src="//img.youtube.com/vi/rGRex96EmFY/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-10-17"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Slide Board - Part 3</strong>

                        <p class="subtle_nu">Athletic training on the slide board.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('rGRex96EmFY')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('2ApZwofe6sw');"><img
                                    src="//img.youtube.com/vi/2ApZwofe6sw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-10-04"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Slide Board - Part 2</strong>

                        <p class="subtle_nu">Vary your slide board training.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('2ApZwofe6sw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('gXoSI_kriqc');"><img
                                    src="//img.youtube.com/vi/gXoSI_kriqc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-09-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Slide Board - Part 1</strong>

                        <p class="subtle_nu">Lengthen your hockey stride off ice.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('gXoSI_kriqc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('VPUBQWMXx0I');"><img
                                    src="//img.youtube.com/vi/VPUBQWMXx0I/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-08-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Lateral Plyo Box Walks</strong>

                        <p class="subtle_nu">Exercise to develop leg strength and power in a full range of motion.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('VPUBQWMXx0I')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Iv3a5H4IWJw');"><img
                                    src="//img.youtube.com/vi/Iv3a5H4IWJw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-07-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Plyo Squat Jumps</strong>

                        <p class="subtle_nu">Exercise to develop athletic stance and explosive power.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Iv3a5H4IWJw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('yJXRsS66jS0');"><img
                                    src="//img.youtube.com/vi/yJXRsS66jS0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-07-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Off Ice Shooting Games</strong>

                        <p class="subtle_nu">4 fun games to keep your shooting routine fresh.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('yJXRsS66jS0')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('RqZZZeAN1XM');"><img
                                    src="//img.youtube.com/vi/RqZZZeAN1XM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-12-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Agility Ladder Drills</strong>

                        <p class="subtle_nu">Three quick agility ladder drills.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('RqZZZeAN1XM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('sGZwfSDEayo');"><img
                                    src="//img.youtube.com/vi/sGZwfSDEayo/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-09-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Pushup Routine</strong>

                        <p class="subtle_nu">Routine to build strength and core stability.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('sGZwfSDEayo')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Passing
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('vfnqWHqeCzI');"><img
                                    src="//img.youtube.com/vi/vfnqWHqeCzI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-04-16"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Surrounding the Puck</strong>

                        <p class="subtle_nu">Concept of surrounding the puck to make a pass.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('vfnqWHqeCzI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('3aQlzpiRcWA');"><img
                                    src="//img.youtube.com/vi/3aQlzpiRcWA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-04-10"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Indirect Passing - Basic Overview</strong>

                        <p class="subtle_nu">Basic execution of two indirect passes.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('3aQlzpiRcWA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('heZmN_jyC24');"><img
                                    src="//img.youtube.com/vi/heZmN_jyC24/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-01-18"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Forehand Passing &amp; Receiving</strong>

                        <p class="subtle_nu">Fundamentals for forehand passing &amp; receiving.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('heZmN_jyC24')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('24TlM_abGys');"><img
                                    src="//img.youtube.com/vi/24TlM_abGys/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Stretch Passes</strong>

                        <p class="subtle_nu">Quick transition passes to attack the zone.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('24TlM_abGys')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('581nogU2dDA');"><img
                                    src="//img.youtube.com/vi/581nogU2dDA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Figure 8 Pass &amp; Drive</strong>

                        <p class="subtle_nu">Drill to improve passing whlie off-balance and in transition.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('581nogU2dDA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('DZkZSZ6JaHc');"><img
                                    src="//img.youtube.com/vi/DZkZSZ6JaHc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-11-22"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Figure 8 Passing</strong>

                        <p class="subtle_nu">Drill to develop body control and passing ability.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('DZkZSZ6JaHc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('atMl4lJ2sz0');"><img
                                    src="//img.youtube.com/vi/atMl4lJ2sz0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-11-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Circle Pass Pivot</strong>

                        <p class="subtle_nu">Passing drill to develop passing ability while feet are moving.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('atMl4lJ2sz0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('02n38hMsCKA');"><img
                                    src="//img.youtube.com/vi/02n38hMsCKA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-01-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Low Cycle Drill</strong>

                        <p class="subtle_nu">Fundamental cycling drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('02n38hMsCKA')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Product Reviews
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('mw1vvsQUV4E');"><img
                                    src="//img.youtube.com/vi/mw1vvsQUV4E/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-07-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Online Hockey Training</strong>

                        <p class="subtle_nu">Information on Online Hockey Training on HockeyShare.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('mw1vvsQUV4E')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('TfrqHcEfU38');"><img
                                    src="//img.youtube.com/vi/TfrqHcEfU38/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-01-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Extreme Passing Kit</strong>

                        <p class="subtle_nu">HockeyShot's Extreme Passing Kit review.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('TfrqHcEfU38')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('eyl7iVpvR4I');"><img
                                    src="//img.youtube.com/vi/eyl7iVpvR4I/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-01-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Extreme Passer Pro</strong>

                        <p class="subtle_nu">HockeyShot's Extreme Passer Pro review.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('eyl7iVpvR4I')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('b-uqxNKPYc0');"><img
                                    src="//img.youtube.com/vi/b-uqxNKPYc0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-01-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Extreme Radar</strong>

                        <p class="subtle_nu">HockeyShot's Extreme Radar review.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('b-uqxNKPYc0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JgZ5oAZkxT4');"><img
                                    src="//img.youtube.com/vi/JgZ5oAZkxT4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-10-29"/></a>

                    </td>
                    <td width="185" valign="top"><strong>The Danglator</strong>

                        <p class="subtle_nu">Product review for The Danglator.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('JgZ5oAZkxT4')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Shooting
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('MNDA22oZv6Q');"><img
                                    src="//img.youtube.com/vi/MNDA22oZv6Q/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2015-12-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Effective Off Ice Shooting</strong>

                        <p class="subtle_nu">Tips for shooting pucks with a purpose.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('MNDA22oZv6Q')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('-iVh9bNR7Lo');"><img
                                    src="//img.youtube.com/vi/-iVh9bNR7Lo/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-12-03"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Eyes of the Puck</strong>

                        <p class="subtle_nu">Explanation of the difference between a shooter's view and the puck's
                            view.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('-iVh9bNR7Lo')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('DmtTLZC__y8');"><img
                                    src="//img.youtube.com/vi/DmtTLZC__y8/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-11-20"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Changing Shot Angles</strong>

                        <p class="subtle_nu">How to change the shooting angle.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('DmtTLZC__y8')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('ZJAmx89gDrQ');"><img
                                    src="//img.youtube.com/vi/ZJAmx89gDrQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-01-25"/></a>

                    </td>
                    <td width="185" valign="top"><strong>One Timers</strong>

                        <p class="subtle_nu">Techniques for taking a one-timer.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('ZJAmx89gDrQ')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('3JSuT8DCMtg');"><img
                                    src="//img.youtube.com/vi/3JSuT8DCMtg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-12-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Shooting from a D to D Pass</strong>

                        <p class="subtle_nu">Technique for shooting quickly from a d to d pass.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('3JSuT8DCMtg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('uNwhBaVcKN8');"><img
                                    src="//img.youtube.com/vi/uNwhBaVcKN8/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-12-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Quick Shooting Release</strong>

                        <p class="subtle_nu">Technique for quick shot release.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('uNwhBaVcKN8')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('PZwRV6vpJ9M');"><img
                                    src="//img.youtube.com/vi/PZwRV6vpJ9M/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Give &amp; Go Drill</strong>

                        <p class="subtle_nu">Develop quick passes and the ability to shoot in stride.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('PZwRV6vpJ9M')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JkKXo30ix_4');"><img
                                    src="//img.youtube.com/vi/JkKXo30ix_4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-05"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Cut &amp; Shoot / Circle Drive</strong>

                        <p class="subtle_nu">Drill for developing re-acceleration and shooting in stride.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('JkKXo30ix_4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('UXFPcTrNsQI');"><img
                                    src="//img.youtube.com/vi/UXFPcTrNsQI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-06-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Backhand Off Ice Shooting</strong>

                        <p class="subtle_nu">Backhand shooting technique off ice.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('UXFPcTrNsQI')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('z5pHAo5PQCQ');"><img
                                    src="//img.youtube.com/vi/z5pHAo5PQCQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-06-02"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Off Ice Shooting Tips</strong>

                        <p class="subtle_nu">Guidelines to properly practice shooting off ice.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('z5pHAo5PQCQ')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('x8I_zaDaky0');"><img
                                    src="//img.youtube.com/vi/x8I_zaDaky0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-02-23"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Backhand Shot</strong>

                        <p class="subtle_nu">Backhand shot technique.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('x8I_zaDaky0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('HssioRWnT24');"><img
                                    src="//img.youtube.com/vi/HssioRWnT24/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-02-02"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Slap Shot</strong>

                        <p class="subtle_nu">Technique for the slap shot broken down.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('HssioRWnT24')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Iui1PmkGpeg');"><img
                                    src="//img.youtube.com/vi/Iui1PmkGpeg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-01-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Wrist Shot</strong>

                        <p class="subtle_nu">Explanation of the wrist shot.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Iui1PmkGpeg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('haJw2S7GqOQ');"><img
                                    src="//img.youtube.com/vi/haJw2S7GqOQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-04-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D Escape Turns w/ Shot</strong>

                        <p class="subtle_nu">Pivoting and shot release development drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('haJw2S7GqOQ')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('i3kejEjmvKQ');"><img
                                    src="//img.youtube.com/vi/i3kejEjmvKQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-04-21"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Top Shelf Shooting</strong>

                        <p class="subtle_nu">Drill to practice quick puck elevation.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('i3kejEjmvKQ')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Shi_9sfqKWk');"><img
                                    src="//img.youtube.com/vi/Shi_9sfqKWk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-04-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>5 Puck Agility Shooting</strong>

                        <p class="subtle_nu">Drill to work on puck movement and shot release.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Shi_9sfqKWk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('FdYgAyDCv_I');"><img
                                    src="//img.youtube.com/vi/FdYgAyDCv_I/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-01-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Pepper Drive</strong>

                        <p class="subtle_nu">Base drill to work on net driving.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('FdYgAyDCv_I')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('KDpuXbGzSm0');"><img
                                    src="//img.youtube.com/vi/KDpuXbGzSm0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-01-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Goalie Tip Drill Game</strong>

                        <p class="subtle_nu">Fun game to compete against the goalie.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('KDpuXbGzSm0')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('RmieuM0pASk');"><img
                                    src="//img.youtube.com/vi/RmieuM0pASk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-11-18"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Catch and Release Shots</strong>

                        <p class="subtle_nu">Quick catch-and-release development drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('RmieuM0pASk')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Skating
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('oKx7Ve-pSLA');"><img
                                    src="//img.youtube.com/vi/oKx7Ve-pSLA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2015-04-16"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Turning in a Tight Area</strong>

                        <p class="subtle_nu">Tips and progression for turning in tight spaces.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('oKx7Ve-pSLA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('YEj4xZuaH-0');"><img
                                    src="//img.youtube.com/vi/YEj4xZuaH-0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-01-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Inside Edge Knee Drop</strong>

                        <p class="subtle_nu">Inside edge variation with knee drop.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('YEj4xZuaH-0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('9mDmxEOVU6o');"><img
                                    src="//img.youtube.com/vi/9mDmxEOVU6o/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-03-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Quick Start Training</strong>

                        <p class="subtle_nu">Drill to work on quick starts</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('9mDmxEOVU6o')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('jUnQWpaIqNk');"><img
                                    src="//img.youtube.com/vi/jUnQWpaIqNk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-12-28"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Hip Open D Variation</strong>

                        <p class="subtle_nu">Variation of Hip Open Drill to develop backward acceleration</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('jUnQWpaIqNk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('YU_BjVfbdV4');"><img
                                    src="//img.youtube.com/vi/YU_BjVfbdV4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-12-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Hip Open Drill</strong>

                        <p class="subtle_nu">Drill to develop effective change of direction.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('YU_BjVfbdV4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Ed8uE9ApPhs');"><img
                                    src="//img.youtube.com/vi/Ed8uE9ApPhs/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-11-12"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Developing the Cross Under</strong>

                        <p class="subtle_nu">Tips for developing the cross under push.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Ed8uE9ApPhs')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('tUufh-1bfNw');"><img
                                    src="//img.youtube.com/vi/tUufh-1bfNw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-08-29"/></a>

                    </td>
                    <td width="185" valign="top"><strong>One Foot Stop &amp; Accelerate</strong>

                        <p class="subtle_nu">Skating drill to develop balance, power, and upper body control.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('tUufh-1bfNw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('r_yeZT7Zrqc');"><img
                                    src="//img.youtube.com/vi/r_yeZT7Zrqc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-04-05"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Side Step Stops</strong>

                        <p class="subtle_nu">Quick lateral movement and agility drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('r_yeZT7Zrqc')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('PVymUw081BM');"><img
                                    src="//img.youtube.com/vi/PVymUw081BM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-02-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Forward to Backward Transiition</strong>

                        <p class="subtle_nu">"Mohawk" transition forward to backward.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('PVymUw081BM')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('06kNm7jgFd0');"><img
                                    src="//img.youtube.com/vi/06kNm7jgFd0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-01-05"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Cross Behinds</strong>

                        <p class="subtle_nu">Backward skating drill focusing on outside edges.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('06kNm7jgFd0')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('NJqZBzwNqVA');"><img
                                    src="//img.youtube.com/vi/NJqZBzwNqVA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-11-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Quick Crossovers</strong>

                        <p class="subtle_nu">Quick crossover power skating drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('NJqZBzwNqVA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('TfR67q7iz0o');"><img
                                    src="//img.youtube.com/vi/TfR67q7iz0o/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-11-02"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Lateral Movement Drills</strong>

                        <p class="subtle_nu">3 drills to improve lateral mobility.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('TfR67q7iz0o')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('lHpfH8rGONs');"><img
                                    src="//img.youtube.com/vi/lHpfH8rGONs/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-09-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Advanced Edgework Drill</strong>

                        <p class="subtle_nu">Single foot alternating edges.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('lHpfH8rGONs')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('wiTq-vvQkjw');"><img
                                    src="//img.youtube.com/vi/wiTq-vvQkjw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-08-23"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Inside Edges</strong>

                        <p class="subtle_nu">Mechanics of an inside edge.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('wiTq-vvQkjw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('FN7q90TkeuE');"><img
                                    src="//img.youtube.com/vi/FN7q90TkeuE/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-08-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Outside Edges</strong>

                        <p class="subtle_nu">Breakdown of the outside edge.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('FN7q90TkeuE')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('dajrj8P-mTI');"><img
                                    src="//img.youtube.com/vi/dajrj8P-mTI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-27"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defense Turns</strong>

                        <p class="subtle_nu">Defense turn transition forward to backward.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('dajrj8P-mTI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('JxFZ1uLOPp4');"><img
                                    src="//img.youtube.com/vi/JxFZ1uLOPp4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Double Pivot Drill</strong>

                        <p class="subtle_nu">Skill drill to develop pivoting, puck control, and net driving.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('JxFZ1uLOPp4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('K0P_gpW8Vmc');"><img
                                    src="//img.youtube.com/vi/K0P_gpW8Vmc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-02-15"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Tight Turns / Power Turns</strong>

                        <p class="subtle_nu">Tight turn skating technique.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('K0P_gpW8Vmc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('gkY3WM3vgRs');"><img
                                    src="//img.youtube.com/vi/gkY3WM3vgRs/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-02-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Quick Starts</strong>

                        <p class="subtle_nu">Forward quick start technique.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('gkY3WM3vgRs')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('vLHPugl_aAM');"><img
                                    src="//img.youtube.com/vi/vLHPugl_aAM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-01-06"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Backward Quick Start Drills</strong>

                        <p class="subtle_nu">Practice drills for backward quick starts.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('vLHPugl_aAM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('P9rfpFENmpY');"><img
                                    src="//img.youtube.com/vi/P9rfpFENmpY/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-01-04"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Backward Quick Starts</strong>

                        <p class="subtle_nu">Technique for backward quick starts.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('P9rfpFENmpY')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('n6sVRkrFF68');"><img
                                    src="//img.youtube.com/vi/n6sVRkrFF68/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-12-08"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Shoot the Duck</strong>

                        <p class="subtle_nu">Balance, edgework, and leg strength development drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('n6sVRkrFF68')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('UU9a_9Hisyk');"><img
                                    src="//img.youtube.com/vi/UU9a_9Hisyk/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-11-17"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Heel to Heel Transitions</strong>

                        <p class="subtle_nu">Heel to heel transition from backward to forward.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('UU9a_9Hisyk')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('17561woJ7MI');"><img
                                    src="//img.youtube.com/vi/17561woJ7MI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-10-28"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defense Circle Transitions</strong>

                        <p class="subtle_nu">Drill to develop quickness, mobility, and agility.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('17561woJ7MI')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('7JAK88JSDSc');"><img
                                    src="//img.youtube.com/vi/7JAK88JSDSc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-10-19"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Star Drill</strong>

                        <p class="subtle_nu">Quick mobility, agility, and quick transition drill.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('7JAK88JSDSc')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('H4rifS5ug24');"><img
                                    src="//img.youtube.com/vi/H4rifS5ug24/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-10-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Forward Stride - In Depth</strong>

                        <p class="subtle_nu">Forward hockey stride broken down.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('H4rifS5ug24')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('0QYfKcWosp0');"><img
                                    src="//img.youtube.com/vi/0QYfKcWosp0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-09-30"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Crossovers - In Depth</strong>

                        <p class="subtle_nu">Technique and teaching points of a forward crossover.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('0QYfKcWosp0')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('SmxoWaqEXXw');"><img
                                    src="//img.youtube.com/vi/SmxoWaqEXXw/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-04-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Jumping Drill</strong>

                        <p class="subtle_nu">Jumping drill to develop balance and explosion.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('SmxoWaqEXXw')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('YtwM1T3_x6M');"><img
                                    src="//img.youtube.com/vi/YtwM1T3_x6M/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-03-31"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Powerskating / Edgework Circuit</strong>

                        <p class="subtle_nu">Quick power skating circuit for edgework development.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('YtwM1T3_x6M')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('iwRrJa4q5iM');"><img
                                    src="//img.youtube.com/vi/iwRrJa4q5iM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-12-02"/></a>

                    </td>
                    <td width="185" valign="top"><strong>One Foot Hops</strong>

                        <p class="subtle_nu">Drill to develop ankle strength and edge control.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('iwRrJa4q5iM')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('jevSo0oTw5k');"><img
                                    src="//img.youtube.com/vi/jevSo0oTw5k/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-11-25"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Half Pivots</strong>

                        <p class="subtle_nu">Focus on power generation during transitions.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('jevSo0oTw5k')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('y6ZIJPgc-vI');"><img
                                    src="//img.youtube.com/vi/y6ZIJPgc-vI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-10-29"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Skulling</strong>

                        <p class="subtle_nu">Drill to develop full stride extension and powerful pushes.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('y6ZIJPgc-vI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('txO9epbRXM0');"><img
                                    src="//img.youtube.com/vi/txO9epbRXM0/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-10-20"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Agility Skating Drills</strong>

                        <p class="subtle_nu">Stick jumping drills to develop footwork.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('txO9epbRXM0')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('lceAIbhF0KQ');"><img
                                    src="//img.youtube.com/vi/lceAIbhF0KQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2009-10-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Defense Escape Turns</strong>

                        <p class="subtle_nu">Quick drill to develop escapes and D turns.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('lceAIbhF0KQ')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Stickhandling
        </div>
        <div class="video-div">&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('8PLipEsHeJg');"><img
                                    src="//img.youtube.com/vi/8PLipEsHeJg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-01-23"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Expansion of Reach</strong>

                        <p class="subtle_nu">Drill working on expansion of reach using AttackPad</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('8PLipEsHeJg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('CPyviZ72PEg');"><img
                                    src="//img.youtube.com/vi/CPyviZ72PEg/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-01-16"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Body Movement Stickhandling</strong>

                        <p class="subtle_nu">Drill working on body and stick control</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('CPyviZ72PEg')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('x5SEqDEz_Gc');"><img
                                    src="//img.youtube.com/vi/x5SEqDEz_Gc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-01-09"/></a>
                    </td>
                    <td width="185" valign="top"><strong>Dribble & Pull</strong>
                        <p class="subtle_nu">Stickhandling drill using the AttackPad</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('x5SEqDEz_Gc')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('7JmQ-pd6BsQ');"><img
                                    src="//img.youtube.com/vi/7JmQ-pd6BsQ/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-05-17"/></a>
                    </td>
                    <td width="185" valign="top"><strong>Off Ice SH Part 3 of 3</strong>

                        <p class="subtle_nu">Vary your training with aids and form variations.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('7JmQ-pd6BsQ')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('GFhspWCLj80');"><img
                                    src="//img.youtube.com/vi/GFhspWCLj80/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-05-09"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Off Ice SH Part 2 of 3</strong>

                        <p class="subtle_nu">Off ice drills to develop your stickhandling skills.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('GFhspWCLj80')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('XNYejzWquYA');"><img
                                    src="//img.youtube.com/vi/XNYejzWquYA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-04-26"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Off Ice SH Part 1 of 3</strong>

                        <p class="subtle_nu">Technique overview and drills to develop your stickhandling skills.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('XNYejzWquYA')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('nUh4N4wZMG4');"><img
                                    src="//img.youtube.com/vi/nUh4N4wZMG4/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-03-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Toe Drags</strong>

                        <p class="subtle_nu">Basic toe drag stickhandling move.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('nUh4N4wZMG4')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('laRbJmZpAIM');"><img
                                    src="//img.youtube.com/vi/laRbJmZpAIM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-03-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>3 SH Warmup Drills Part 2</strong>

                        <p class="subtle_nu">Simple drills to implement while warming up.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('laRbJmZpAIM')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('cKKMnOVeW24');"><img
                                    src="//img.youtube.com/vi/cKKMnOVeW24/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2012-01-11"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Warmup Drills</strong>

                        <p class="subtle_nu">Simple warmup drills to improve stickhandling.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('cKKMnOVeW24')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('y1ue4oDR3_k');"><img
                                    src="//img.youtube.com/vi/y1ue4oDR3_k/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-08-07"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Puck Protection</strong>

                        <p class="subtle_nu">Concepts and breakdown of puck protection.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('y1ue4oDR3_k')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('wjFXwHWg8FI');"><img
                                    src="//img.youtube.com/vi/wjFXwHWg8FI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2011-07-13"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Quick Stickhandle &amp; Drive</strong>

                        <p class="subtle_nu">Skill drill to work on quick hands and net drive.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('wjFXwHWg8FI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('mZ12ii2pfk8');"><img
                                    src="//img.youtube.com/vi/mZ12ii2pfk8/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-12-01"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Stickhandling 101</strong>

                        <p class="subtle_nu">Basic stickhandling technique and teaching points.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('mZ12ii2pfk8')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('i7sMin5fIvI');"><img
                                    src="//img.youtube.com/vi/i7sMin5fIvI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-10-25"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Developing Stick Strength</strong>

                        <p class="subtle_nu">Training aid to use for developing stick strength.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('i7sMin5fIvI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('k27l2pnzDbc');"><img
                                    src="//img.youtube.com/vi/k27l2pnzDbc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-02-23"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Corner Create Space</strong>

                        <p class="subtle_nu">Drill to create space while protecting the puck.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('k27l2pnzDbc')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('jG_y4FVNwpM');"><img
                                    src="//img.youtube.com/vi/jG_y4FVNwpM/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2010-02-18"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Corner Escape</strong>

                        <p class="subtle_nu">Drill focusing on tight area puck protection and net drive.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('jG_y4FVNwpM')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>

        <div class="video-systems">
            Systems
        </div>
        <div class>&nbsp;
            <table class="more-video" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Ug9pRdUnFXA');"><img
                                    src="//img.youtube.com/vi/Ug9pRdUnFXA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-03-04"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Penalty Kill - Defensemen</strong>

                        <p class="subtle_nu">Tips for defensemen on the PK.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('Ug9pRdUnFXA')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('_D2gk9qg4DE');"><img
                                    src="//img.youtube.com/vi/_D2gk9qg4DE/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-02-25"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Penalty Kill - Forwards</strong>

                        <p class="subtle_nu">Tips for forwards on the PK.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('_D2gk9qg4DE')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('7jp4IjQRk0Q');"><img
                                    src="//img.youtube.com/vi/7jp4IjQRk0Q/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2014-01-01"/></a>

                    </td>
                    <td width="185" valign="top"><strong>Attacking the High Seam</strong>

                        <p class="subtle_nu">Tips for attacking the high seam off the half wall.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('7jp4IjQRk0Q')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('9SKlUhJQIuc');"><img
                                    src="//img.youtube.com/vi/9SKlUhJQIuc/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-10-30"/></a>

                    </td>
                    <td width="185" valign="top"><strong>NZ Forecheck Basics</strong>

                        <p class="subtle_nu">Neutral zone forecheck basics.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('9SKlUhJQIuc')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('6aXsrNrT5RI');"><img
                                    src="//img.youtube.com/vi/6aXsrNrT5RI/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-10-14"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D-Zone Coverage (Defensemen)</strong>

                        <p class="subtle_nu">D-Zone coverage overview for defensemen.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('6aXsrNrT5RI')">Watch
                                Video</a></p></td>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('E1BN-Om5i6Q');"><img
                                    src="//img.youtube.com/vi/E1BN-Om5i6Q/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-09-30"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D-Zone Coverage (Centers)</strong>

                        <p class="subtle_nu">D-Zone coverage overview for centers.</p>
                        <p align="center"><a class="underline" href="#top" onclick="changeVideo('E1BN-Om5i6Q')">Watch
                                Video</a></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" valign="top"><a class="underline" href="#top" onclick="changeVideo('Ikqt_iY4nSA');"><img
                                    src="//img.youtube.com/vi/Ikqt_iY4nSA/default.jpg" height="90" width="120"
                                    alt="Video Thumbnail" border="0" title="Added: 2013-09-23"/></a>

                    </td>
                    <td width="185" valign="top"><strong>D-Zone Coverage (Wings)</strong>

                        <p class="subtle_nu">D-Zone coverage overview for wingers.</p>
                        <p align="center"><a class="underline" class="underline" href="#top"
                                             onclick="changeVideo('Ikqt_iY4nSA')">Watch Video</a></p></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>

            </table>
        </div>
        <br/><br/>
        @include('includes.commercial')
        <br>
    </div>
</div>

