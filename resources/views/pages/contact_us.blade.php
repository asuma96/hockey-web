@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <!-- Content -->
        <h2>HockeyShare Support</h2>
        <p>We are committed to serving our customers as efficiently as possible. In addition to email and phone support,
            we also offer several self-help options to get your questions answered fast. Please see the list below for
            answers to commonly asked questions:</p>
        <ul>
            @if(Auth::check())
                <li><a href="/">My account is locked</a></li>
            @else
                <li><a href="/login/">My account is locked</a></li>
            @endif
            <li><a href="http://hockeyshare.com/login/forgot.php">I can't log in to my account</a></li>
            <li><a href="https://hockeyshare.groovehq.com/help_center" target="_blank">HockeyShare FAQ /
                    Knowledgebase</a></li>
        </ul>

        <h2>Contact HockeyShare</h2>

        <p>Couldn't find what you were looking for with the self-help options above? No problem, simply complete the
            form below and we will respond via email. Typical support requests are serviced within 24 hours on regular
            business days. If you are having difficulties with the below support form, you can contact support directly
            via email at <a
                    href="mailto:&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#64;&#104;&#111;&#99;&#107;&#101;&#121;&#115;&#104;&#97;&#114;&#101;&#46;&#99;&#111;&#109;">
                &#115;&#117;&#112;&#112;&#111;&#114;&#116;&#64;&#104;&#111;&#99;&#107;&#101;&#121;&#115;&#104;&#97;&#114;&#101;&#46;&#99;&#111;&#109;</a>.
        </p>

        <form method="post" action="">
            <ul class="form-style-1">
                <li><label>Full Name <span class="required2">*</span></label>
                    <input type="text" name="name" id="name" value="" class="field-long"
                           placeholder="First &amp; Last Name"/></li>
                <li><label>HockeyShare Username </label>
                    <input type="text" name="username" id="username" value="" class="field-long"
                           placeholder="Leave blank if you do not have one"/></li>
                <li>
                    <label>Email <span class="required2">*</span></label>
                    <input type="email" id="email" name="email" value="" class="field-long"/>
                </li>
                <li>
                    <label>Subject <span class="required2">*</span></label>
                    <select name="subject" id="subject" class="field-select field-long">
                        <option value="" selected="selected"> -- Select a Topic --</option>

                        <optgroup label="My Account">
                            <option value="Account Questions">Account Questions</option>
                            <option value="Login / Registration Issues">Login / Registration Issues</option>
                        </optgroup>

                        <optgroup label="Support Topics">
                            <option value="Advanced Coaching Platform">Advanced Coaching Platform (Drills / Practices)
                            </option>
                            <option value="Diagrammer">Drill Diagrammer</option>
                            <option value="10k Pucks">10k Pucks</option>
                            <option value="Team Management">Team Management</option>
                            <option value="Tournaments">Tournament Listings</option>
                            <option value="AttackPad">AttackPad</option>
                            <option value="Other"> - Other -</option>
                        </optgroup>
                    </select>
                </li>
                <li>
                    <label>Your Message <span class="required2">*</span></label>
                    <textarea name="question" id="question" class="field-long field-textarea"></textarea>
                </li>

                <li>
                    <label>Human Verification </label>
                    <div class="g-recaptcha" data-sitekey="6LezvcISAAAAABQlxts3FvDUSk6gT8rU8SZXgZdE">
                        <div class="contact_us_div">
                            <div>
                                <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LezvcISAAAAABQlxts3FvDUSk6gT8rU8SZXgZdE&amp;co=aHR0cHM6Ly93d3cuaG9ja2V5c2hhcmUuY29tOjQ0Mw..&amp;hl=en&amp;v=r20170515161201&amp;size=normal&amp;cb=6cxjqutpoxnz"
                                        title="recaptcha widget" width="304" height="78" frameborder="0" scrolling="no"
                                        name="undefined"></iframe>
                            </div>

                        </div>
                    </div>
                </li>

                <li>
                    <input type="submit" value="Submit" name="Submit"/>
                </li>
            </ul>
        </form>

        <h2>HockeyShare Phone Support</h2>
        <p>HockeyShare also offers telephone support for the following: </p>
        <ul>
            <li>Premium account support (also includes team &amp; association accounts)</li>
            <li>Professional account support (for professional, college, and junior teams only)</li>
            <li>Sales Inquiries</li>
        </ul>

        <p>*** Please note that we do not currently offer telephone support for help with the 10k Pucks contest. For
            help with the 10k Pucks contest or anything other than the items listed above, please use the contact form
            on this page. ***</p>
        <p><strong>HockeyShare Sales &amp; Support Phone Number: (262) 672-4126</strong></p>

        <p>In order for us to serve you more efficiently, when calling for technical support, please be sure to include
            the following information: </p>
        <ul>
            <li>Account holder full name (spelled out)</li>
            <li>Account Username</li>
            <li>Email address (spelled out)</li>
        </ul>
        @include('includes.commercial')
        <br>
    </div>
</div>

