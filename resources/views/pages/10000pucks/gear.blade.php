@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="logo_10k_container center_10k">
            <a href="http://www.avantlink.com/click.php?tt=cl&mi=11681&pw=189934&url=http%3A%2F%2Fwww.hockeyshot.com%2F"
               target="_blank"><img src="/img/1000x100_10k.jpg"
                                    class="center_10k" alt="Sponsored by HockeyShot"/></a>
        </div>

        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="https://www.instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="https://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>

        <div class="section1_10k">
            <h3>What type of equipement do you use?</h3>

            <p>Please complete the form below regarding the current equipmet you are using. </p>

            <form method="post" action="" name="playerForm" id="playerForm">

                <div class="topPad10_10k">
                    <div class="left_200_10k">Skates:</div>
                    <div class="float_right_10k">
                        <select name="skid" id="skid">
                            <option value="0" selected="selected">-- Select a Brand --</option>
                            <option value="101">Bauer</option>
                            <option value="100">CCM</option>
                            <option value="102">Easton</option>
                            <option value="103">Graf</option>
                            <option value="104">Reebok</option>
                            <option value="113">VH</option>
                            <option value="-1">OTHER</option>
                        </select>
                    </div>
                </div>

                <div class="topPad10_10k">
                    <div class="left_200_10k">Stick:</div>
                    <div class="float_right_10k">
                        <select name="stid" id="stid">
                            <option value="0" selected="selected">-- Select a Brand --</option>
                            <option value="101">Bauer</option>
                            <option value="100">CCM</option>
                            <option value="102">Easton</option>
                            <option value="103">Graf</option>
                            <option value="105">Miken</option>
                            <option value="106">Mission</option>
                            <option value="104">Reebok</option>
                            <option value="107">Sher-Wood</option>
                            <option value="108">STX</option>
                            <option value="109">Titan</option>
                            <option value="110">True</option>
                            <option value="111">Warrior</option>
                            <option value="112">Winwell</option>
                            <option value="-1">OTHER</option>
                        </select>
                    </div>
                </div>

                <div class="topPad10_10k">
                    <div class="left_200_10k">Gloves:</div>
                    <div class="float_right_10k">
                        <select name="gid" id="gid">
                            <option value="0" selected="selected">-- Select a Brand --</option>
                            <option value="101">Bauer</option>
                            <option value="100">CCM</option>
                            <option value="102">Easton</option>
                            <option value="103">Graf</option>
                            <option value="104">Reebok</option>
                            <option value="108">STX</option>
                            <option value="110">True</option>
                            <option value="-1">OTHER</option>
                        </select>
                    </div>
                </div>

                <div class="topPad10_10k">
                    <div class="left_200_10k">&nbsp;</div>
                    <div class="float_right_10k"><a href="#" onclick="validateForm()"><span
                                    class="medbutton_10k">Save</span></a></div>
                </div>
            </form>

            <br/>

        </div>

        <br>
        <br>
        @include('includes.commercial')
        <br>
    </div>
</div>
