@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="logo_10k_container center_10k">
            <a href="http://www.avantlink.com/click.php?tt=cl&mi=11681&pw=189934&url=http%3A%2F%2Fwww.hockeyshot.com%2F"
               target="_blank"><img src="/img/1000x100_10k.jpg" class="center_10k" alt="Sponsored by HockeyShot"/></a>
        </div>

        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="http://instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="http://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>
        @if (Auth::check())

            <br/>

            <div class="section1_10k">
                <h3>Create New Player Profile</h3>

                <form method="post" action="" id="playerForm">

                    <div class="topPad10_10k">
                        <div class="left_200_10k">Player First Name:</div>
                        <div class="float_right_10k"><input type="text" name="first" id="first" value=""/></div>
                    </div>

                    <div class="topPad10_10k">
                        <div class="left_200_10k">Player Last Name:</div>
                        <div class="float_right_10k"><input type="text" name="last" id="last" value=""/></div>
                    </div>

                    <div class="topPad10_10k">
                        <div class="left_200_10k">Gender:</div>
                        <div class="float_right_10k"><input type="radio" value="M" id="gender1" name="gender"
                                                            checked="checked"/> Male <input type="radio" value="F"
                                                                                            id="gender2" name="gender"/>
                            Female
                        </div>
                    </div>

                    <div class="topPad10_10k">
                        <div class="left_200_10k">Birth Year:</div>
                        <div class="float_right_10k">
                            <select name="birthyear" id="birthyear">
                                <option value="" selected="selected">-- Select a Year --</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                                <option value="2012">2012</option>
                                <option value="2011">2011</option>
                                <option value="2010">2010</option>
                                <option value="2009">2009</option>
                                <option value="2008">2008</option>
                                <option value="2007">2007</option>
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                            </select>
                        </div>
                    </div>

                    <div class="topPad10_10k">
                        <div class="left_200_10k">My Goal:</div>
                        <div class="float_right_10k"><input name="goal" type="text" id="goal" value="10000" size="6"/>
                            Shots <span class="subtle_nu">(do not enter commas or decimals)</span></div>
                    </div>

                    <div class="topPad10_10k">
                        <div class="left_200_10k">&nbsp;</div>
                        <div class="float_right_10k"><a href="#" onclick="validateForm()"><span class="medbutton_10k">Next</span></a>
                        </div>
                    </div>

                </form>

            </div>
            <br>
            <br>

        @else
            <h2 class="player_title">Please Log In or Register</h2>
            <p>The 10,000 Pucks Contest is free for players, teams, and associations. To get started in the contest, you
                must first register for a free account at HockeyShare.com. Please select one of the options below to
                continue.</p>
            <table width="100%" border="0" cellspacing="3" cellpadding="3">
                <tr>
                    <td width="50%" valign="top">
                        <div class="player_table">
                            <h3>Register</h3>
                            <p>To create a new <strong><u>FREE</u></strong> account, please use the link below. Please
                                note, you will be required to validate your email before you are able to activate your
                                account to use with the 10,000 Pucks Contest.</p>


                            <p align="center"><a class="underline red_text" href="/register/"
                                                >Register New
                                    Account</a></p>

                        </div>
                    </td>
                    <td valign="top">
                        <div class="player_page">
                            <h3>Log In</h3>
                            <p>If you already have an account at HockeyShare.com, please use the form below to log in -
                                if you cannot remember your password, please try using our <a class="underline"
                                                                                              href="http://hockeyshare.com/login/forgot.php">forgot
                                    password</a> feature.</p>

                            <form method="post" action="/login/">
                                <table width="100%" border="0" cellspacing="3" cellpadding="3">
                                    <tr>
                                        <td width="100"><label for="username">Username: </label></td>
                                        <td><input type="text" name="username" value="" maxlength="100" size="20"/></td>
                                    </tr>
                                    <tr>
                                        <td><label for="password">Password: </label></td>
                                        <td><input type="password" name="password" value="" maxlength="100" size="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label for="remember" class="subtle und-text">Remember Login: </label>
                                            <input type="checkbox" checked="checked" value="1" name="remember"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" name="Submit" value="Log In"/></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </td>
                </tr>
            </table>
        @endif
        <br>
        <br>
        @include('includes.commercial')
        <br>
    </div>
</div>
