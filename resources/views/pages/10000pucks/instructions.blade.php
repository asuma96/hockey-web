@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="logo_10k_container center_10k">
            <a href="http://www.avantlink.com/click.php?tt=cl&mi=11681&pw=189934&url=http%3A%2F%2Fwww.hockeyshot.com%2F"
               target="_blank"><img src="/img/1000x100_10k.jpg"
                                    class="center_10k" alt="Sponsored by HockeyShot"/></a>
        </div>

        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="http://instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="http://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>

        <div class="section1_10k" style="height: 853px">
            <h3 class="title-text">How to Participate (Players)</h3>
            <p>The contest begins June 1st and ends on August 31st. Follow the steps below to participate...</p>
            <div>
                <ol>
                    <li><a class="underline" href="http://hockeyshare.com/register">Register</a>&nbsp;with
                        HockeyShare.com (or&nbsp;<a class="underline" href="http://hockeyshare.com/login">log in</a>&nbsp;for
                        existing members)
                    </li>
                    <li>Fill out your&nbsp;<a class="underline"
                                              href="http://hockeyshare.com/10000pucks/player/start.php">contest
                            profile</a></li>
                    <li>Shoot your pucks...<a class="underline" href="http://hockeyshare.com/10000pucks/player/">log
                            your shots</a>!
                    </li>
                </ol>
            </div>

            <p align="center">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/9gpjwujeIa4" frameborder="0"
                        allowfullscreen></iframe>
            </p>

            <p>A couple notes to keep in mind.</p>
            <ol>
                <li>All participants must be 18 years old or&nbsp;<u>younger</u>&nbsp;at the time of entry in order to
                    be eligible for prize packages.
                </li>
                <li>Photos of the player along with the shooting area will be required to claim prizes.</li>
                <li>You must agree to the contest rules and regulations in order to participate.</li>
            </ol>
            <div align="center">&raquo;&nbsp;<a class="underline" href="rules.php"><strong>Official Contest
                        Rules</strong></a>&nbsp;&laquo;</div>
        </div>

        <div class="section3_10k">
            <h3>How to Track Your Team (Coaches)</h3><a name="team">&nbsp;</a>
            <ol>
                <li><a class="underline" href="http://hockeyshare.com/register">Register</a>&nbsp;with HockeyShare.com
                    (or&nbsp;<a class="underline" href="http://hockeyshare.com/login">log in</a>&nbsp;for existing
                    members)
                </li>
                <li>Click on <a class="underline" href="/10000pucks/team/">Team</a> on the 10,000 Pucks home page</li>
                <li>Enter your team's information</li>
                <li>Invite players by using the registration link on your team page</li>
            </ol>
            <p>Now you have your team created! When players register for the contest, they will be able to select your
                team on their profile page. Once they've selected your team and saved their profile, you will be able to
                see the player show up on your&nbsp;<a class="underline" href="http://hockeyshare.com/10000pucks/team/">dashboard</a>.
            </p>

            <p align="center">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/4sd6cFnTrLQ" frameborder="0"
                        allowfullscreen></iframe>
            </p>
        </div>

        <div class="section1_10k">
            <h3>How to Manage Your Entire Association</h3><a name="association">&nbsp;</a>
            <p>Want to set up multiple teams in the contest? Simply follow the steps below to create a new
                association.</p>
            <ol>
                <li><a class="underline" href="http://hockeyshare.com/register">Register</a>&nbsp;with HockeyShare.com
                    (or&nbsp;<a class="underline" href="http://hockeyshare.com/login">log in</a>&nbsp;for existing
                    members)
                </li>
                <li>Click on <a class="underline" href="/10000pucks/association/">Association</a> on the 10,000 Pucks
                    home page
                </li>
                <li>Create teams on your association dashboard by clicking on the &quot;Create New Team&quot; button
                </li>
                <li>Give coaches and/or team managers access to their teams by clicking on the &quot;Access&quot; link
                    by each team
                </li>
            </ol>

            <p align="center">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/Szol_HA8YqU" frameborder="0"
                        allowfullscreen></iframe>
            </p>
        </div>
        <div class="instructions_reklam">
            <br>
            <br>
            @include('includes.commercial')
        </div>
        <br>
    </div>
</div>

