
@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="logo_10k_container center_10k">
            <a href="http://www.avantlink.com/click.php?tt=cl&mi=11681&pw=189934&url=http%3A%2F%2Fwww.hockeyshot.com%2F"
               target="_blank"><img src="/img/1000x100_10k.jpg"
                                    class="center_10k" alt="Sponsored by HockeyShot"/></a>
        </div>

        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="https://www.instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="https://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>

        <div class="section1_10k">
            <h3>Find Your Team</h3>

            <p>If your team is using 10k Pucks, you can link your profile to the team to compete against your teammates
                and allow your coach to see your progress.</p>

            <form method="post" action="start2.php?id=23538">
                <div>Find Your Team: <input type="text" value="" name="search" placeholder="Team Name"/> <input
                            type="submit" name="Submit" value="Search" class="medbutton_10k"/></div>
            </form>

            <br/>

            <div class="right_10k"><a href="dashboard.php?id=23538" class="darkbutton_10k">Skip &raquo;</a></div>

        </div>
        <br>
        <br>
        @include('includes.commercial')
        <br>
    </div>
</div>
