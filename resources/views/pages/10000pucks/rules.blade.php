@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="logo_10k_container center_10k">
            <a href="http://www.avantlink.com/click.php?tt=cl&mi=11681&pw=189934&url=http%3A%2F%2Fwww.hockeyshot.com%2F"
               target="_blank"><img src="/img/1000x100_10k.jpg"
                                    class="center_10k" alt="Sponsored by HockeyShot"/></a>
        </div>

        <div class="nav_10k">
            <a href="http://hockeyshare.com/10000pucks/" class="nav_link_10k nav_divider_10k">10k Pucks Home</a>
            <a href="http://hockeyshare.com/10000pucks/player/" class="nav_link_10k nav_divider_10k">Players</a>
            <a href="http://hockeyshare.com/10000pucks/team/" class="nav_link_10k nav_divider_10k">Teams</a>
            <a href="http://hockeyshare.com/10000pucks/association/"
               class="nav_link_10k nav_divider_10k">Associations</a>
            <a href="http://hockeyshare.com/10000pucks/instructions.php"
               class="nav_link_10k help_10k nav_divider_10k"><i class="help_10k fa fa-life-ring"></i>&nbsp;&nbsp;&nbsp;Contest
                Help</a>
            <a href="https://www.instagram.com/kevinm2hockey/" target="_blank" class="nav_link_10k nav_divider_10k"><i
                        class="fa fa-instagram brown_10k"></i>&nbsp;&nbsp;&nbsp;Instagram</a>
            <a href="https://twitter.com/kevinm2hockey/" target="_blank" class="nav_link_10k"><i
                        class="fa fa-twitter blue_10k"></i>&nbsp;&nbsp;&nbsp;Twitter</a>
        </div>

        <div class="section1_10k">

            <div id="wrapperDivPopup">
                <table id="wrapperTablePopup" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td>
                            <div class="contentDivPopup">
                                <h3 class="title_text otst"> 10,000 Pucks Contest 2013 - Official Rules</h3>
                                <p class="title_text"><strong>NO PURCHASE OR PAYMENT OF ANY KIND IS NECESSARY TO ENTER
                                        OR WIN THIS CONTEST.
                                        A PURCHASE WILL NOT INCREASE YOUR CHANCES OF WINNING</strong>.</p>
                                <ol id="rules">
                                    <li><strong>CONTEST PERIOD/ENTRY DEADLINE:</strong> 10,000 Pucks - 1 Summer Contest
                                        (the &quot;<strong>Contest</strong>&quot;) begins at 12:01 a.m. (Eastern Time [&quot;<strong>ET</strong>&quot;])
                                        on June 1, 2017 and ends at 11:59pm (Eastern Time [&quot;ET&quot;]) on August
                                        31, 2017.
                                    </li>
                                    <li><strong>ELIGIBILITY:</strong> To enter the Contest you must 1) register for an
                                        account with HockeyShare.com 2) be 18 years of age or younger at the time of
                                        entry. Employees of HockeyShare LLC are not eligible to enter the contest <span
                                                class="style1">Winners must provide a photograph of themeselves shooting pucks in their primary shooting area in order to receive prizes</span>.
                                        Eligibility will be determined by HockeyShare LLC, in its sole discretion.
                                    </li>
                                    <li><strong>HOW TO ENTER: </strong></li>
                                    <p>A. First, ensure you comply with the above ELIGIBILITY criteria. Next, visit <a
                                                href="http://hockeyshare.com/10000pucks">hockeyshare.com/10000pucks/</a>
                                        (<a href="http://www.10000pucks.com"
                                            target="_blank">http://www.10000pucks.com</a>) (the
                                        &quot;<strong>website</strong>&quot;) to review entry information and more
                                        details on the Contest. Be sure to carefully review the Official Rules of the
                                        Contest (including all instructions on the submission of your photograph and the
                                        judging criterion). </p>
                                    <li><strong>GENERAL REQUIREMENTS FOR ENTRIES</strong><strong>:</strong></li>
                                    <p>A. Your shot log must be able to be verified by a parent/guardian. All winners of
                                        any prizes must be verified via parental signature on the prize claim form. </p>
                                    <p>B. Your photograph must not include a depiction of any person (whether in whole
                                        or in part) other than yourself i.e., the contestant. HockeyShare LLC reserves
                                        the right to verify the accuracy and legitimacy of your Entry Form and
                                        photograph, as well as your compliance with each of the provisions of these
                                        Official Rules. Photographs are only required for selected winners - although
                                        photographs from other entrants will be accepted under the terms of this
                                        agreement.</p>
                                    <p>C. Each contestant agrees, represents and warrants that use of submitted
                                        photograph submitted in this Contest is authorized by all persons having rights
                                        with respect to the photograph, and its use in this Contest will not violate the
                                        rights of any third parties. </p>
                                    <p>D. <strong>Limit of one Entry per contestant. </strong>If more than one Entry is
                                        received from the same person, then he/she will be disqualified. Only complete
                                        Entries will be accepted. If an Entry is incomplete or does not comply with the
                                        specifications and Official Rules described herein, or if the photograph
                                        submitted does not comply with the required specifications, then the Entry may
                                        be disqualified at HockeyShare LLC's sole discretion. Neither HockeyShare LLC
                                        nor the Contest Administrator will verify receipt of Entries. You should retain
                                        a copy of the photograph submitted for the Contest.</p>
                                    <li><strong>SELECTION OF WINNERS:</strong> The top three <strong>individual
                                            winners</strong> will be defined as the entrants logging the most shots via
                                        the website between the contest start and end dates. If an entry is determined
                                        to be using deceptive voting practices (defined as voting through means other
                                        than the website - including, but not limited to automated bots, automated
                                        scripts, illegal proxy use) the entry will be disqualified and will not be
                                        eligible for contest prizes.
                                    </li>
                                    <li>Tie breaking procedures will be as follows:
                                        <ol>
                                            <li>Most shots logged during the contest span (defined by the above contest
                                                beginning and ending date)
                                            </li>
                                            <li>Person who reached 10,000 logged shots (in the contest span) in the
                                                shortest amount of time (based on the contest starting date)
                                            </li>
                                            <li>Person who reached 7,500 logged shots (in the contest span) in the
                                                shortest amount of time (based on the contest starting date)
                                            </li>
                                            <li>Person who reached 5,000 logged shots (in the contest span) in the
                                                shortest amount of time (based on the contest starting date)
                                            </li>
                                            <li>Person recording the most BACKHAND shots</li>
                                            <li>Coin Flip</li>
                                        </ol>
                                    </li>
                                    <li>The winner will be awarded the '1st place package' consisting of "$300
                                        HockeyShot Gift Card". The entrant recording the second most shots at the end of
                                        the contest will receive the 'second prize package' consisting of "$200
                                        HockeyShot Gift Card". The entrant recording the third most shots will receive
                                        the 'third prize package' consisting of "$100 HockeyShot Gift Card". The entrant
                                        recording the fourth most shots will receive the 'fourth prize package'
                                        consisiting of "$50 HockeyShot Gift Card". The entrant recording the fifth most
                                        shots will receive the 'fifth prize package' consisting of "$25 HockeyShot Gift
                                        Card".
                                        <ol>
                                            <li>Note: Prize packages subject to change without notice. Prizes subject to
                                                availability of products.
                                            </li>
                                        </ol>
                                    </li>
                                    <li>The top <strong>team winner</strong> will be defined as the registered team
                                        whose users have (collectively) logged the most shots via the website between
                                        the contest start and end dates. If a team and/or users of the team are
                                        determined to be using deceptive voting practices (defined as voting through
                                        means other than the website - including, but not limited to automated bots,
                                        automated scripts, illegal proxy use) the entry will be disqualified and will
                                        not be eligible for contest prizes.
                                        <ol>
                                            <li>The winning team will be awarded a prize package consisting of a 90-day
                                                association subscription to HockeyShare.com's Drill Diagramming &amp;
                                                Practice Planning platform for up to 30 users. The prize package and
                                                association admin privelages will be granted to the administrator of the
                                                winning team.
                                            </li>

                                        </ol>
                                    </li>
                                    <li><strong>NOTIFICATION OF WINNERS:</strong> If you are chosen as one of the three
                                        winners, you will be notified via email. Notification will be sent to the email
                                        address used during the registration process. If a response is not received
                                        within 30 days of the notification, the entry may be disqualified and a new
                                        winner chosen. HockeyShare LLC is not under any obligation to distribute all of
                                        the above listed prize packages in the event of non-reply from chosen winners.
                                    </li>
                                    <li><strong>USE/OWNERSHIP:</strong> All Entries submitted become the property of
                                        HockeyShare LLC (whether disqualified or not) and will not be returned. Except
                                        to the extent prohibited by law, by participating in the Contest contestant
                                        agrees that i) HockeyShare LLC shall own the Entry and all photographs submitted
                                        by the contestant (including all rights embodied therein),ii) HockeyShare LLC
                                        and its designees may use, publish, and display all elements of such Entry
                                        (including but not limited to the contestant's photograph and other likeness,
                                        caption, nickname, name, address [city and state only], biographical
                                        information, statements and voice) in whole or in part, in any and all media
                                        either now known or not currently known, in perpetuity throughout the universe
                                        for all purposes (including but not limited to promotional and marketing
                                        purposes, and advertising, publicizing and promoting the HockeyShare LLC's
                                        products, and for use in connection with displays) without notification and
                                        without compensation of any kind to contestant or any third party. HockeyShare
                                        LLC reserves all rights, including without limitation, the right to reproduce,
                                        alter, amend, edit, publish, modify, crop and use each Entry in connection with
                                        commercials, advertisements and promotions related to the HockeyShare LLC, its
                                        associated artists, sales of HockeyShare LLC's products, the Contest and any
                                        other contests HockeyShare LLCed by HockeyShare LLC, and in connection with any
                                        displays, in any and all media, now or hereafter known, including but not
                                        limited to, television, theatrical advertisements, radio, Internet, newspapers,
                                        magazines and billboards.
                                    </li>
                                    <li><strong>CONTEST CONDITIONS:</strong> This Contest is subject to all applicable
                                        federal, state and local laws. By participating, each contestant agrees (i) to
                                        be bound by these Official Rules and the decisions of the HockeyShare LLC and
                                        the Contest Administrator, and waives any right to claim ambiguity in this
                                        Contest or these Official Rules, and (ii) to release, discharge, indemnify and
                                        hold harmless the Contest Entities, and their respective officers, directors,
                                        employees, shareholders, representatives and agents (collectively, the
                                        &quot;<strong>Released Parties</strong>&quot;), from and against any and all
                                        claims, damages or liabilities due to any injuries, damages or losses to any
                                        person (including death) or property of any kind resulting in whole or in part,
                                        directly or indirectly, from acceptance, possession, misuse or use of any prize
                                        or participation in any Contest-related activity or participation in this
                                        Contest, including any claims relating to use, misappropriation or disclosure of
                                        the Entries, photographs, captions or nicknames described herein. All decisions
                                        of the judges, HockeyShare LLC and the Contest Administrator are final in all
                                        matters relating to this Contest.
                                    </li>
                                    <li><strong>LIMITATIONS OF LIABILITY:</strong> The Released Parties are not
                                        responsible for lost, late, misdirected, undeliverable, illegible, unreadable,
                                        unviewable, garbled, corrupted, damaged, stolen or incomplete Entries. The
                                        Released Parties are also not responsible for system errors or failures, or
                                        faulty transmissions or other telecommunications or other types of malfunctions
                                        or interferences and/or for Entries or photographs not received resulting from
                                        any hardware or software failures of any kind, lost or unavailable network
                                        connections, Web site, Internet, or ISP availability, unauthorized human
                                        intervention, traffic congestion or failed, incomplete or garbled computer or
                                        telephone transmissions, typographical or system errors and failures, or faulty
                                        transmissions, or for any problems or technical malfunctions. HockeyShare LLC
                                        may disqualify a contestant if, in its sole discretion, it determines that said
                                        contestant is attempting to undermine the legitimate operation of the Contest by
                                        cheating, hacking, deception, or other unfair playing practices or intending to
                                        annoy, abuse, threaten or harass any other contestants or HockeyShare LLC
                                        representatives. If for any reason this Contest is not capable of running as
                                        planned, including, but not limited to, infection by computer virus, bugs,
                                        tampering, unauthorized intervention, fraud or any other causes beyond the
                                        reasonable control of HockeyShare LLC which corrupt or affect the
                                        administration, security, fairness, integrity or proper conduct of the Contest
                                        then HockeyShare LLC reserves the right at its sole discretion to cancel,
                                        terminate, modify or suspend the Contest and select winners by judging those
                                        eligible entries received up to the cancellation/suspension date. CAUTION: ANY
                                        ATTEMPT BY A CONTESTANT TO DELIBERATELY DAMAGE THE WEBSITE OR UNDERMINE THE
                                        LEGITIMATE OPERATION OF THE CONTEST MAY BE IN VIOLATION OF CRIMINAL AND CIVIL
                                        LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, HockeyShare LLC RESERVES THE RIGHT TO
                                        SEEK REMEDIES AND DAMAGES (INCLUDING ATTORNEY'S FEES) FROM ANY SUCH CONTESTANT
                                        TO THE FULLEST EXTENT OF THE LAW, INCLUDING CRIMINAL PROSECUTION.
                                    </li>
                                    <li><strong>RULES/WINNERS LIST</strong>: To view the Official Rules go to
                                        http://www.10000pucks.com or to receive a copy of the Official Rules or a list
                                        of the winners, send a self-addressed stamped envelope to: HockeyShare LLC ATTN:
                                        10,000 Pucks - 1 Summer Contest 5744 80th Street #908 Kenosha, WI 53142. Please
                                        specify &quot;rules&quot; or &quot;winners list&quot;. Otherwise, no written
                                        correspondence will be entered into except with potential winners.
                                    </li>
                                    <li><strong>HockeyShare LLC: </strong>7531 45th Ave, Kenosha, WI 53142 - Phone:
                                        (262) 672-4126.
                                    </li>
                                    <li><strong>CONTEST ADMINISTRATOR:</strong>7531 45th Ave, Kenosha, WI 53142 - Phone:
                                        (262) 672-4126.
                                    </li>
                                </ol>
                                <br/>
                                <hr/>
                                <br/>
                                <h3 class="title_text">PHOTOGRAPHER'S RELEASE FORM</h3>
                                <h4>THIS FORM MUST BE READ AND AGREED TO WHETHER THE PHOTO WAS PRODUCED BY THE
                                    CONTESTANT OR BY ANOTHER PERSON.</h4>
                                <p>As a condition of my entering the 10,000 Pucks - 1 Summer Contest (the &quot;<strong>Contest</strong>&quot;)
                                    and submitting a photograph (the &quot;<strong>Photo</strong>&quot;) in connection
                                    with the Contest, I hereby warrant, represent, acknowledge and agree as follows:</p>
                                <ol id="release">
                                    <li>I acknowledge and agree that once the Photo is submitted for the Contest, it
                                        will become the property of HockeyShare LLC and will not be returned.
                                    </li>
                                    <li>I hereby irrevocably grant to <strong>HockeyShare LLC,</strong> respective
                                        parent, affiliate and subsidiary companies, and advertising, fulfillment and
                                        promotion agencies (hereafter, collectively, the &quot;<strong>Contest
                                            Entities</strong>&quot;), the perpetual right and authority throughout the
                                        universe to use, publish, display, broadcast and reproduce the Photo (in whole
                                        or in part) in any and all media, now or hereafter known (including but not
                                        limited to television, radio, Internet, newspapers, magazines and billboards),
                                        for all purposes and without notification or compensation to me or any third
                                        party, including, without limitation, the right to reproduce, publish, publicly
                                        perform or use the Photo, in connection with web pages, commercials,
                                        advertisements and promotions related to the HockeyShare LLC, the sales of
                                        HockeyShare LLC's products, the Contest and subsequent contests held by
                                        HockeyShare LLC.
                                    </li>
                                    <li>I represent that I have full right and authority to enter into this agreement,
                                        and to grant the rights granted hereunder. I further represent that the consent
                                        of no other person, firm, corporation, labor organization or other entity is
                                        required to enable the Contest Entities to use the Photo, and that the use of
                                        the Photo in and in connection with this Contest and the Contest Entities'
                                        exercise of their rights hereunder does not, and will not, violate the rights of
                                        any third parties. I acknowledge and agree that the Contest Entities have the
                                        unlimited right throughout the universe to reproduce the Photo in full or to
                                        crop or otherwise edit or modify such Photo for reproduction at the Contest
                                        Entities' discretion, without review by me or payment of compensation to me or
                                        anyone else. Accordingly, I hereby waive all moral rights that I may assert with
                                        respect to the editing of such Photo. I acknowledge that nothing herein requires
                                        the Contest Entities to use such Photo, in, or in connection with, the Contest
                                        or for the advertising or marketing purposes specified above, and that all
                                        rights in and to such Photo shall be the sole and absolute property of the
                                        Contest Entities. If I should retain copies of the Photo, then I shall not (nor
                                        shall I permit others to) use, disclose and/or publish the Photo unless
                                        expressly permitted by HockeyShare LLC's prior written consent in each instance.
                                    </li>
                                    <li>On behalf of myself, my relatives, heirs, assignees, executors, administrators,
                                        successors and assigns, I hereby permanently, irrevocably and forever waive any
                                        and all claims, liabilities, losses and damages (including but not limited to
                                        court costs and attorneys' fees) now or in the future of any kind and nature
                                        whatsoever (collectively, &quot;<strong>Claims</strong>&quot;) against the
                                        Contest Entities, and each of their directors, officers, employees, agents and
                                        representatives, and their predecessors and successors in interest, assignees,
                                        licensees and designees (collectively, the &quot;<strong>Released
                                            Parties</strong>&quot;) relating in any way to: (i) the Photo or the
                                        Contest; and/or (ii) any personal injury, property damage, or any other injury,
                                        damage or loss resulting from the Photo or Contest, or the exploitation of the
                                        rights granted herein; and I forever release and discharge the Released Parties
                                        from any and all liability with respect thereto. I further agree that I will
                                        indemnify and hold the Released Parties harmless from and against all Claims
                                        arising out of, connected with or resulting from my breach of this agreement. I
                                        understand that HockeyShare LLC and those whom it may authorize are not
                                        responsible for unauthorized duplication/use of the Photo by third parties.
                                    </li>
                                    <li>I further acknowledge that this document, together with the Official Rules of
                                        the Contest, constitutes the entire agreement and complete understanding of the
                                        parties, and that no oral or prior written agreements shall be deemed a part of
                                        or modification of this agreement. This agreement cannot be modified except by a
                                        written instrument signed by an authorized representative of HockeyShare LLC.
                                    </li>
                                    <li>This release shall be governed by the laws of the State of Wisconsin without
                                        reference to the conflicts of law provisions thereof. I hereby consent to the
                                        exclusive jurisdiction of the federal, state, and local courts located within
                                        Illinois. Invalidity, illegality or unenforceability of any provision or any
                                        part of any provision of this agreement shall not affect or impair the validity,
                                        legality or enforceability of any other provision or any part of any other
                                        provision of this agreement.
                                    </li>
                                </ol>
                                <p>I hereby certify and represent that I have read the foregoing and fully understand
                                    the meaning and effect thereof, and intending to be legally bound, I hereby accept
                                    all of the terms and conditions of this photographer's release.</p>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="margin-top: 60px;">
            @include('includes.commercial')
        </div>
        <br>
    </div>
</div>

