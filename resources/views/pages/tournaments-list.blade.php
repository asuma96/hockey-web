@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <h3 class="title-text">List Your Hockey Tournament</h3>
        <p>List your tournament and get direct visibility with our over <b>20,000 registered users</b> plus hundreds
            every month who browse our site. Regular listings are free of charge! Featured listings start at just $5.
        </p>
        <br/>
        <br>
        <div class="tournaments_div">
            <p class="txt_2">*required fields</p>
            <br/>
            <form action="" method="post" enctype="multipart/form-data" name="form1">
                <table class="tournaments-table" width="95%" border="0" cellspacing="3" cellpadding="3">
                    <tr>
                        <td valign="top">Listing Type:*</td>
                        <td>
                            <label>
                                <select name="listing_type" id="listing_type">
                                    <option value="0" selected>Regular Listing ($0.00)</option>
                                    <option value="1">Featured (+$5.00)</option>
                                </select>
                            </label></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="125" valign="top">Title:*</td>
                        <td><label>
                                <input name="title" type="text" id="title" value="" size="35" maxlength="100">
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">Start Date:*</td>
                        <td><label>
                                <select name="start_month" id="start_month"
                                        onchange="document.form1.end_month.selectedIndex=document.form1.start_month.selectedIndex">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05" selected="selected">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select></label>
                            <select name="start_day" id="start_day"
                                    onchange="document.form1.end_day.selectedIndex=document.form1.start_day.selectedIndex">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10" selected="selected">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="start_year" id="start_year"
                                    onchange="document.form1.end_year.selectedIndex=document.form1.start_year.selectedIndex">
                                <option value="2017" selected="selected">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>

                            </select></td>
                    </tr>
                    <tr>
                        <td valign="top">End Date:*</td>
                        <td><label>
                                <select name="end_month" id="end_month">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05" selected="selected">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </label>
                            <select name="end_day" id="end_day">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10" selected="selected">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="end_year" id="end_year">
                                <option value="2017" selected="selected">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">City*</td>
                        <td><label>
                                <input name="city" type="text" id="city" maxlength="100" value="">
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">State/Prov.*</td>
                        <td><select name="state">
                                <option value="" selected="selected">Select a State or Province</option>
                                <option value="AB">Alberta</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="BC">British Columbia</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="MB">Manitoba</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NB">New Brunswick</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NL">Newfoundland and Labrador</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="NT">Northwest Territories</option>
                                <option value="NS">Nova Scotia</option>
                                <option value="NU">Nunavut</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="ON">Ontario</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="PE">Prince Edward Island</option>
                                <option value="QC">Quebec</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="SK">Saskatchewan</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                                <option value="YT">Yukon</option>
                                <option value="OTHER">OTHER - NON-US/CAN</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td valign="top">Zip/Postal:</td>
                        <td><input name="zip" type="text" id="zip" value="" size="12" maxlength="12"></td>
                    </tr>
                    <tr>
                        <td valign="top">Country:*</td>
                        <td><label>
                                <select name="country" id="country">
                                    <option value="US" selected="selected">United States</option>
                                    <option value="CA">Canada</option>
                                    <option value="OTHER">Other</option>
                                </select>
                            </label></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">Description:*</td>
                        <td>
                            <textarea name="description" id="description" cols="55" rows="10"></textarea>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">Contact Info:*</td>
                        <td><label>
                                <input type="text" name="contactinfo" id="contactinfo" value="">
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">Website:</td>
                        <td><label>
                                <input type="text" name="website" id="website" value="">
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">Phone:</td>
                        <td><label>
                                <input name="phone" type="text" id="phone" size="25" maxlength="25" value="">
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">Email:</td>
                        <td><input name="email" type="text" id="email" size="55" maxlength="200" value=""/></td>
                    </tr>
                    <tr>
                        <td valign="top">Logo/Photo:<br/>
                            (<strong>+ $1.00</strong>)
                        </td>
                        <td><label>
                                <input type="file" name="image" id="image"></label>
                            (optional)<br/>
                            - <em>Must be a jpg file - 250 max width, 250 max height</em></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">Type:</td>
                        <td><label>
                                <select name="type" id="type">
                                    <option value="Y" selected="selected">Youth</option>
                                    <option value="A">Adult</option>
                                    <option value="B">Youth &amp; Adult</option>
                                </select>
                            </label></td>
                    </tr>
                    <tr>
                        <td valign="top">Age Divisions:*</td>
                        <td><label>
                                <input name="divisions" type="text" id="divisions" value="" size="55" maxlength="255"/>
                                <br/>
                                (ex: 2001, 2000, 1999, 1998, Peewee, Bantam, Midget)</label></td>
                    </tr>
                    <tr>
                        <td valign="top">Play Levels:*</td>
                        <td><label>
                                <input name="levels" type="text" id="levels" value="" size="55" maxlength="255"/>
                                <br/>
                                (ex: AAA, AA, A, B, C, D, House, Recreation)</label></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" bgcolor="#000000">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">&nbsp;</td>
                        <td><label>
                                <input class="margin_tournaments" type="submit" name="Submit" id="Submit"
                                       value="Submit">
                            </label></td>
                    </tr>
                </table>
            </form>
            <p>&nbsp;</p>
        </div>
        <br/>
        @include('includes.commercial')
        <br>
    </div>
</div>
