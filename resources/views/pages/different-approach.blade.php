@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div align="left" class="page_content_stretch_top" style="margin-bottom:0px;"><img
                    src="/img/ada_logo.jpg" alt="A Different Approach"
                    height="77" width="1000" border="0"/></div>

        <h1>A Different Approach - Re-think Your Coaching Process</h1>

        <table width="100%" border="0">
            <tr>
                <td width="600" valign="top">
                    <iframe src="//player.vimeo.com/video/97884005?title=0&amp;byline=0&amp;portrait=0&amp;color=801c1c"
                            width="600" height="339" frameborder="0" webkitallowfullscreen mozallowfullscreen
                            allowfullscreen></iframe>
                </td>
                @if(Auth::check())
                    <td align="center">
                        <a href="/training/different-approach/members.php"><img src="/img/ada-starttrial.jpg"
                                                                                width="300" height="102"
                                                                                alt="Start Your Free Trial"><br>
                        </a>
                        <p class="subtle_nu"><a href="/training/different-approach/members.php">Ready to purchase the
                                program? </a><a href="http://hockeyshare.com/training/different-approach/purchase.php">Click
                                here</a></p>

                    </td>
                @else
                    <td align="center">
                        <img src="/img/ada-loginregister.jpg"
                             width="300" height="155" alt="Login or Register"/><br/>
                        <p class="subtle_nu">Already have a HockeyShare account? <a
                                    href="http://hockeyshare.com/login/">Log in</a><br/>Need a HockeyShare account
                            (free)? <a href="http://hockeyshare.com/register/">Register</a></p>
                    </td>
                @endif
            </tr>
        </table>
        <h3>About the Program</h3>
        <p><strong><em>A Different Approach</em></strong> is designed to give you a fresh perspective on your season. In
            this program we focus on the elements of planning and building with intention. Each chapter contains a video
            segment describing in detail the topic of the week and offers application questions to get the most out of
            the series. The program is broken down into 18 chapters (plus two bonus chapters) and will be delivered to
            you once a week. This delivery method will help you stay focused on the process and present the content in a
            manageable manner. Over the course of 18 weeks, you will see your insight into your team as well as your
            coaching ability reach new heights. If you are a coach looking to do the extra work to set yourself apart,
            this is the program for you. Get started today with a free 2-week trial.</p>
        <p class="different_p">"<em>I
                have known and worked with Eric Hoffberg for over 10 years. His approach to building high performance
                team cultures works great in the world of hockey just the way it would in any realm that cares about
                getting the best you can out of your people. His concepts are creative yet simple and actually make a
                difference. As coaches we all need good reminders and good ideas for how to put an emphasis on
                strengthening self-leadership skills so that we can build collective mental toughness and for how to get
                every team member to take ownership of the way they show up on the ice by first taking ownership of the
                way they are thinking</em>."<br/>
            <span class="different_span">Barry Trotz - Head Coach of Washington Capitals</span>
        </p>

        <h3>About the Authors</h3>
        <p>Eric Hoffberg and Kevin Muller both share a similar passion for hockey and leadership development. The two
            teamed together in late 2013 to develop <strong><em>A Different Approach</em></strong> - an exciting and
            unique combination of Eric's mental toughness training techniques coupled with Kevin's hands-on knowledge of
            the current state of hockey.</p>

        <table width="100%" border="0" cellspacing="3" cellpadding="3">
            <tr>
                <td width="50%" valign="top">
                    <h4>Eric Hoffberg</h4>
                    <p><img src="/img/HoffSm.jpg" alt="Eric Hoffberg" height="176" width="172" border="0" align="left"
                            style="padding:5px;"/>Eric Hoffberg spent 16 years coaching hockey, college, then pro.
                        Originally from Rochester NY, Hoffberg was the head coach at RIT all through the 90’s. Today,
                        Hoffberg works as both a Corporate Coach for leaders and executives across the country that are
                        looking to build High Performance Cultures and as a Mental Toughness Trainer for athletes that
                        are looking for a greater understanding of how to focus under pressure. He is the author of two
                        great books for training attitude and mindset.</p></td>
                <td valign="top">
                    <h4>Kevin Muller</h4>
                    <p><img src="/img/camps_kevinmuller.jpg" alt="Kevin Muller" height="176" width="172" border="0"
                            align="left" style="padding:5px;"/>Kevin is founder and owner of HockeyShare.com - a world
                        renowned resource for hockey coaches. In addition to overseeing HockeyShare, Kevin currently
                        coaches AAA hockey in the States and has traveled the US and Canada providing high-quality skill
                        instruction focusing on specialized skating and shooting techniques. Kevin shares a passion for
                        developing winning cultures within teams while using cutting-edge technology to enhance and
                        accelerate the training process.</p>

                </td>
            </tr>
            <tr>
                <td valign="top" align="center"><a href="http://www.erichoffberg.com" target="_blank">Website</a> | <a
                            href="http://twitter.com/EricHoffberg" target="_blank">Twitter</a></td>
                <td valign="top" align="center"><a href="http://hockeyshare.com/" target="_blank">Website</a> | <a
                            href="http://twitter.com/hockeyshare" target="_blank">Twitter</a></td>
            </tr>
        </table>
        @include('includes.commercial')
        <br>
    </div>
</div>

