@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="attackpad">
            <h2>AttackPad Hockey Training System</h2>
            <table width="100%" border="0" cellspacing="3" cellpadding="3">
                <tr>
                    <td valign="top" width="405" align="center"><img
                                src="/img/apts_splash.jpg" height="183"
                                width="400" alt="AttackPad Hockey Training System" border="0"/></td>
                    <td valign="top" class="attackpad-top">
                        <div class="attackpad-loged">
                            LOG IN TO REDEEM YOUR ACCESS CODE
                        </div>
                        <p>In order to redeem your access code, you must have an account at HockeyShare.com. If you
                            already
                            have an account, you may log in below. Don't have an account yet? <a
                                    href="http://hockeyshare.com/register/">Sign up for <strong><u>FREE</u></strong></a>.
                        </p>

                        <form method="post" action="http://hockeyshare.com/login/">
                            <table width="100%" border="0">
                                <tr>
                                    <td>Username:</td>
                                    <td><input type="text" class="hs_text required" name="username" id="username"
                                               value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password:</td>
                                    <td><input type="password" class="hs_text required" name="password" id="password"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><input type="submit" name="Submit" value="Log In"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                href="http://hockeyshare.com/login/forgot.php">Forgot Password</a></td>
                                </tr>
                            </table>
                            <input type="hidden" name="redirect" value="http://hockeyshare.com/ttp/attackpad/"/>
                        </form>
                    </td>
                </tr>
            </table>
            <br/>
            <table border="0" cellpadding="3" cellspacing="3" width="100%">
                <tr>
                    <td valign="top">
                        <p class="p_attackpad">WHAT'S INCLUDED</p>
                        <p>The Attack Pad Hockey Training System is a revolutionary approach to hockey training
                            products.
                            The system not only includes the physical product, but also exclusive online training and
                            results capturing resources.</p>
                        <ul>
                            <li>60 minute video training series demonstrating skills and drills to try at home</li>
                            <li>Access to HockeyShare's Online Locker Room to track your progress and compare your
                                results
                                against others
                            </li>
                            <li>Downloadable worksheets for each week of the program</li>
                            <li>Downloadable maintenance &amp; care documents</li>
                            <li>Ability to submit your own videos showing off your skill</li>
                        </ul>
                        <p align="center"><a
                                    href="http://www.hockeyshot.com/AttackPad-Hockey-Training-System-p/stickhandling-aid-025.htm?Click=40243"
                                    target="_blank" class="p_attackpad_one color_attack">Order Yours Today!</a></p>
                    </td>
                    <td width="725" align="center" valign="top">
                        <div align="center">
                            <iframe src="https://player.vimeo.com/video/201691670" width="" height="" frameborder="0"
                                    webkitallowfullscreen mozallowfullscreen allowfullscreen id="player1"></iframe>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

