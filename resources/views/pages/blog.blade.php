@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a class="p_blog" href="http://feeds.feedburner.com/HockeyshareBlog"
                                         rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1371 post type-post status-publish format-standard hentry category-comments-thoughts"
                         id="post-1371">

                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/"
                               rel="bookmark">The Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a></h2>
                        <div class="margin_blog">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="table-blog blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Imagine your team about to take the ice in a championship game.</p>
                            <p>Would you feel stressed as a coach?</p>
                            <p>I know I’d definitely be feeling some nervous anticipation! Not as much, though, as I
                                sometimes feel dealing with try-outs!<span class="tve_image_frame"><br/>
</span></p>
                            <p>Ah, hockey try-outs. A necessary part of your coaching position but not always the most
                                fun. How many of you out there cringe a little bit when you think about having to cut
                                kids and watching them cry, dealing with angry parents (who may or may not even
                                understand the game of hockey) complaining about your decisions, feeling the pressure of
                                politics from your club or organization?</p>
                            <p>There are a lot of reason most hockey coaches don’t love try-outs!</p>
                            <p>Well, some of these problems are never going to go away entirely, but there are
                                absolutely some best-practices and some considerations you can make as a coach to make
                                your life 100x easier come try-outs next season.</p>
                            <p>Let’s dig in and find out what you can do to make your next set of try-outs a much more
                                enjoyable, stress-free experience and get you the best possible roster for your
                                team!</p>
                            <p><span class="bold_text"><strong><a href="http://www.winsmarter.com/go-hockeyshare-com"
                                                                  target="_blank">Heck, we’ll even give you the three forms you need to make your next try-out the smoothest, most stress-free try-out you’ve ever run!</a></strong></span>
                            </p>
                            <p>Before we get to that though, let’s give you a quick outline of what you need to be
                                thinking about to make your life easier!</p>
                            <h3 class="title-text">Planning &#8211; Before the Try-Outs<span
                                        class="tve_image_frame"><br/>
</span></h3>
                            <p style="font-weight: bold">Be prepared</p>
                            <p>Alexander Graham Bell said it all. The more prepared you are the better the results will
                                be. So let’s start at the beginning:</p>
                            <ul class="thrv_wrapper" style="margin-bottom: 15px !important;">
                                <li class="blog_li_one"><b>Always plan your sessions in advance.</b>
                                    How many sessions will you have? How much ice time for each? Have you factored in
                                    pre- and post-session meetings, warm-ups and water breaks? Answer these questions
                                    and use them to develop a written plan for tryouts &#8211; the specific drills and
                                    timing you want and make sure everyone involved understands the set-up and sequence
                                    of events &#8211; this is especially important if you won’t be on the ice during the
                                    try-outs, but we’ll talk more about that later. Not only is this one of the most
                                    important things you can do to reduce your stress, but the better your preparation
                                    the more professional you appear to players and parents and the more credibility you
                                    gain. Fear not, though, you’ve come to the right place! HockeyShare has some <a
                                            href="http://hockeyshare.com/drills/learn_more.php" target="_blank">excellent
                                        resources</a> to help you plan effective try-out session on the ice.
                                </li>
                                <li class="blog_li_one"><b>Find colleagues to help you during
                                        tryouts.</b> As hard as we might try, we still only have one set of eyes and one
                                    set of hands. If you want reduce your stress-level, lightening your load with more
                                    eyes and more hands is an absolute MUST! In a perfect world, you’re gonna want some
                                    help setting up and running drills on the ice, and you’re definitely gonna want some
                                    help sitting in the stands evaluating and taking notes. I’d suggest using those with
                                    more experience and those who are more trusted as your off-ice evaluators and other
                                    volunteers to help you set up and manage drills on the ice. Some best practices here
                                    are:
                                </li>
                                <li class="lil">
                                    <b>Avoid the use of any “parent-coaches”</b> or anyone with a connection to any of
                                    the kids trying out &#8211; particularly in the role of an evaluator! Hey we all
                                    love our kids and all come with our own inherent biases &#8211; let’s do our best to
                                    not let it factor into our decision making and to maintain a perception of fairness
                                    for all kids involved!
                                </li>
                                <li class="lil">
                                    If possible, try to <b>get some help evaluating </b>from some coaches who have never
                                    coached any of the kids before. Same reason &#8211; if we can avoid it, let’s not
                                    let prior biases influence putting the best possible team on the ice!
                                </li>
                                <li class="lil_blog" style="margin-left: 50px!important;">
                                    <b>Create an Evaluation Form.</b> So now that you’ve got some friends helping you
                                    evaluate, let’s make their lives easier and your life easier. By coming up with an
                                    evaluation form, you’re letting your staff know exactly and specifically what you
                                    want them looking for while watching each kid. You’re also getting consistent
                                    criteria for you to use in your decision making process. Let’s not make things
                                    harder by having to compare apples to oranges!
                                </li>
                            </ul>
                            <div class="blog_margin">
                            <p class="p_blog_two">Coaches can always opt to take
                                their own notes instead of using your exact form, but ask them to at least use the form
                                as a guide.</p>
                            <p class="p_blog_two">On your form, you can include
                                things like:</p>
                            <ul class="thrv_wrapper li_blog_six">
                                <li>Speed</li>
                                <li>Skating transitions (forward-to-backward, etc.)</li>
                                <li>Giving/receiving passes</li>
                                <li>Shooting</li>
                                <li>“Hockey sense&#8221;</li>
                                <li>Any other “non-skill” characteristics you&#8217;re looking for. Some
                                    examples:
                                </li>
                                <li class="li_blog_class">Leadership</li>
                                <li class="li_blog_class">Hustle &#8211; full effort before,
                                    during and after every drill
                                </li>
                                <li class="li_blog_class">Physicality</li>
                                <li class="li_blog_class">Focus</li>
                                <li class="li_blog_class">Accountability (remembering all their
                                    equipment, being on time, etc.)
                                </li>
                                <li class="li_blog_class">Resilience (ability to recover from
                                    mistakes, not pouting or throwing a tantrum, etc.)
                                </li>
                                <li class="li_blog_class">Coachability (ability to take
                                    coaching or criticism)
                                </li>
                                <li>Some space for their own general comments</li>
                            </ul>
                            <div class="thrv_paste_content thrv_wrapper blog_ul_style">
                                <ul>
                                    <li class="li_blog_six lil_blog"><strong>Consider
                                            seeking out feedback from prior coaches of kids you don’t already
                                            know.</strong> There are definitely some pros and cons to this so we’ll let
                                        you make your own decision here, but some things to consider:
                                    </li>
                                </ul>
                                <div class="thrv_paste_content thrv_wrapper blog_lo">
                                    <ul class="thrv_wrapper">
                                        <li class="li_blog_one">Pros
                                            <ul>
                                                <li class="li_blog_two_1">learn a bit about
                                                    players you don’t know
                                                </li>
                                                <li class="li_blog_three">
                                                    hear about their attitude and team orientation
                                                </li>
                                                <li class="li_blog_five">find out how much the
                                                    actually scored in games vs. what you see in tryouts
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="thrv_wrapper">
                                        <li class=" blog_ul_style">Cons
                                            <ul>
                                                <li class="li_blog">
                                                    might give you unfair preconceived notion about some players &#8211;
                                                    be careful here
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            </div>
                            <h3 class="title-text">Get the Info Out</h3>
                            <p>Like most aspects of coaching, communication is key here. Keeping your players and
                                parents well-informed before try-outs even start is one of the best things you can
                                possibly do to make the process smoother for everyone. Likewise, making sure your
                                coaches and evaluators know the game plan ahead of time helps everything move along
                                well.</p>
                            <p><b>The single best suggestion I can give you</b> to keep players and parents informed is
                                to create a flyer that’s distributed to them ahead of time with all the info they need
                                about the process. Include things like the schedule and location of each session, the
                                criteria upon which you’re evaluating the kids, best practices for parents, and tips and
                                suggestions for players to have the best possible try-out they can.</p>
                            <p>The other <b>huge benefit</b> of this form that can’t be overstated is the perception of
                                organization and professionalism it presents to everyone involved in your try-out
                                process. It’s important to understand the psychology here &#8211; you’re the
                                professional, you’re presenting yourself accordingly and that’s going to make it a lot
                                easier for players and parents to accept your decisions later in the process.</p>
                            <p><b><a href="http://www.winsmarter.com/go-hockeyshare-com" target="_blank">This is so
                                        important, we couldn’t possibly leave you hanging! Get the exact form we use in
                                        your inbox right now!</a> </b></p>
                            <p>Now that the players and parents are squared away. Let’s get the coaches and evaluators
                                ready to go as well. Get your Evaluation Forms out to your staff a day or two before the
                                try-outs start so they have a chance to check out what you’re looking for and ask any
                                questions they might have. Same goes for your try-out plans &#8211; the specific drills
                                and timing you want &#8211; to make sure all coaches are on the same page.</p>
                            <h3 class="title-text">Try-Out Days</h3>
                            <h4>Logistical Considerations</h4>
                            <p>Alright, it’s try-out day! Here are some things to consider to get things moving in a
                                good direction:</p>
                            <ul class="thrv_wrapper blog_ul_style">
                                <li class="blog_li_one"><b>Arrive early. </b>No need for comment here!
                                </li>
                                <li class="blog_li_one"><b>Quick meeting.</b> At beginning of each
                                    tryout session, meet face-to-face with the players, let them know the structure of
                                    that day’s session, remind them what you’re looking for and wish everyone good luck.
                                </li>
                                <li class="blog_li_one"><b>Avoid parent conversations</b>. Do your
                                    best to steer clear of any conversations with parents while you’re at the try-outs,
                                    even if you know them, to avoid any perceptions of favoritism.
                                </li>
                                <li class="blog_li_one"><b>On the ice or in the stands? </b>This has
                                    been a debate amongst hockey coaches for years! The bottom line is your decision
                                    about where to position yourself during try-outs is going to depend on a lot
                                    factors: your experience, your coaching staff and your comfort level with them, the
                                    level of the players, etc. Think about the advantages of each option and choose what
                                    you think is best! Here are some things to think about:
                                </li>
                                <li class="blog_li"><b>For multiple
                                        session try-outs</b> consider being on ice initially and then in stands for at
                                    least the last session.
                                </li>
                                <li class="blog_li"><b>If it’s a
                                        single-session try-out</b> consider being in the stands after giving your
                                    initial introduction on the ice. Make sure your coaching staff knows the exact
                                    drills and timing you want to see &#8211; specifics are important!
                                </li>
                                <li class="blog_li"><b>Try to
                                        observe at least the final tryout session from the stands</b> (preferably where
                                    no one can disturb you!). At this point there’s probably only a few kids you really
                                    need to watch (the ones on the bubble) and giving yourself a different perspective
                                    can be helpful.
                                </li>
                                <li class="blog_li_one"><b>Keep the evaluators on their own. </b>Evaluators
                                    should sit away from all parents for obvious reasons. If you’re really feeling
                                    crazy, consider a closed try-out with no parents watching at all if your club or
                                    league allows. Just remember, that might cause more problems than it prevents
                                    &#8211; consider all your options!
                                </li>
                                <li class="blog_li_one"><b>Don’t “pre-label”.</b> Make sure you’re not
                                    deciding the fate of any kids at try-outs before they actually try-out! Treat every
                                    kid the same to preserve the perception of fairness and impartiality. For example,
                                    have coaches demonstrate drills rather than asking a kid who was on the team last
                                    year to show it, and potentially give an impression of favoritism. Perception is
                                    reality for most players and parents!
                                </li>
                            </ul>
                            <h3 class="title-text">On the Ice<span class="tve_image_frame"><br/>
</span></h3>
                            <p>There’s no right or wrong way to run your try-outs on the ice and obviously there are a
                                lot of considerations that go into how you structure things: age, ability level,
                                coaching resources, number of kids, etc. Trust your hockey experience and instincts to
                                organize your try-out the best way you can. Hey, you’re the coach for a reason!</p>
                            <p>Here are some things for you to consider:</p>
                            <p><b>Warm-ups<br/>
                                </b>Obviously if time is a constraining factor, you want to get down to business and
                                warming up is probably low on the priority list for many coaches during try-outs. Think
                                about this though: not only is a good warm-up important to allow kids to perform at
                                their best (isn’t that what we’re looking for here?), but watching who takes their
                                warm-up seriously and who just goes through the motions can start to tell you a lot
                                about kids’ attitudes.</p>
                            <p><b>Skill Drills</b><br/>
                                Skill drills are always an important component of try-outs since they can give you a
                                good idea of where your kids all stack up skill-wise, as well as further insight into
                                each kid’s attitude towards doing skill work. As a coach, you should be careful to
                                choose drills that are developmentally appropriate for the level of the kids trying out.
                            </p>
                            <p>Keep your life simple! Think about choosing drills that allow you to evaluate single,
                                specific criteria rather than drills where you’re trying to judge several different
                                things all at the same time. For example, drills that specifically evaluate things like
                                transitioning from forwards to backwards skating, puck handling and giving and receiving
                                passes, might be more accurately evaluated if they were all part of their own quicker
                                drills.</p>
                            <p><b>Competitive Drills</b><br/>
                                Including one-on-one and small group competitive drills is another great way to get a
                                better idea of where kids measure up against each other and see how well they compete.
                                One-on-one’s and two-on-one’s can help a lot to evaluate each kid’s ability to move the
                                puck offensively and play defense. These small group set-ups also allow you to easily
                                manipulate the match-ups on each repetition as you see fit to get a sense of where
                                certain kids stack up against each other.</p>
                            <p><b>Scrimmages</b><br/>
                                In addition to the standard full team scrimmages, think about spending some time doing
                                full ice three-on-three’s. This spreads out the game and gives more space for you to
                                carefully assess each player’s skills as well as their &#8220;hockey sense,” creativity,
                                positioning, conditioning, ability to beat guys one-on-one, how much hustle they show on
                                defensively and of course their overall attitude.</p>
                            <p>Full team scrimmages are also an important tool in your arsenal. Continue to change up
                                the lines frequently to get different looks and vary the competition, giving you a
                                fuller picture of each kid. Remember that you’re not just observing their skills here,
                                but looking for their hockey sense, as well &#8211; their ability to read the situation,
                                see the big picture, be in the right spot, anticipate the puck, etc. Creativity, ability
                                to read the play and continuous movement are also good things to watch for in these
                                scrimmages that skill work alone won’t show you.</p>
                            <p><b>Goalie Considerations</b><br/>
                                Goalies can be a unique animal when it comes to try-outs so it’s important to think
                                about the best way to handle them ahead of time. Depending on how many goalies you have
                                trying out and on your ice-time limitations, you might consider having a goalie-only
                                try-out session sometime before the main try-outs. That allows you to initially screen
                                out anyone whose skills are not yet at the right level before your actual try-outs. That
                                can be important, as having too many goalies at your regular try-outs can make it hard
                                to evaluate each of them.</p>
                            <p>Because goalies are unique, another thing to think about might be assigning one coach to
                                be a goalie-only evaluator. In addition, when assessing goalies try to commit at least
                                some of your ice time to “goalie friendly” drills. For example, having multiple players
                                line up pucks and shoot rapid fire at a goalie, might test his reaction time, but is not
                                a very game-realistic scenario! Obviously, in addition to these drills, watching them in
                                scrimmage situations is also important.</p>
                            <h3 class="title-text">Choosing Your Team<span class="tve_image_frame"><br/>
</span></h3>
                            <p>This is real life. Deciding on your roster is usually a bit more complicated than we’d
                                like it to be. So let’s quickly touch on a few things to think about to help you out
                                here!</p>
                            <h3 class="title-text">Take Advantage of Your Evaluators</h3>
                            <p>Hey, they’re here for a reason! At the end of each session, block out some time to sit
                                down with your evaluators and compare notes. Be prepared for different people to have
                                different opinions about the same kid, though. Invariably, different coaches will notice
                                different things, and that’s ok! In fact, that’s the whole reason you asked for help!
                                It’s not possible to see everything yourself. The important thing here is, if they’re
                                seeing something different than you, figure out why.</p>
                            <p>If you’re looking to simplify things, another great tactic is to ask your evaluators to
                                rank all the kids trying out, with each kid on either an offensive or defensive list.
                                This can be a quick way to confirm or to question what your thoughts were and can be an
                                easy jumping off point for the conversation where there are discrepancies.</p>
                            <p>Another useful tactic is to consider having someone keep track of how many goals each kid
                                scores over the course of the try-outs in both drills and scrimmages. This can be a good
                                way to help identify the kids who just have a better knack for finding the net.</p>
                            <h3 class="title-text">Narrow Your Focus</h3>
                            <p>Towards the end of your try-outs, there will likely be several kids you already know will
                                make the team and several kids who will not. So let’s take advantage of that and make
                                our lives a bit easier. Now’s the time when you can selectively ignore those kids while
                                watching practice, freeing you up to pay closer attention to kids on the bubble. This
                                number should hopefully decrease as the try-outs progress.</p>
                            <p>If you have several sessions in which to run your try-outs, you can also consider cutting
                                the kids you know won’t make it at the end of each session and bring back fewer kids for
                                the subsequent sessions. This lets you watch the kids on the bubble a little more
                                carefully and helps you avoid the situation where a &#8220;bubble kid” maybe looks
                                better than he really is due to match-ups against some of the less-skilled kids in the
                                earlier sessions.</p>
                            <p>Skills aside, you might also think about what each kid would contribute to the group or
                                team dynamic. This could be the difference maker for some of these players and the best
                                fit for your team is not necessarily the biggest, fastest or strongest kid.</p>
                            <h3 class="title-text">Do the Parents Make the Team?</h3>
                            <p class="blog_p">One last thing you might think about is: How are
                                a kid’s parents going to be to deal with? Hate to say it, but this has to be a real
                                thought for coaches in this day and age.</p>
                            <ul class="thrv_wrapper"
                            >
                                <li>Are they high maintenance?</li>
                                <li>Do they bad mouth other players, teams, clubs or coaches they’ve been
                                    involved with?
                                </li>
                            </ul>
                            <p>These are very real factors that can not only contribute to your enjoyment of the season
                                but to the cohesiveness of your team as a whole!</p>
                            <h3 class="title-text">Letting them Know<span class="tve_image_frame"><br/>
</span></h3>
                            <p>No coach enjoys telling a kid they didn’t make your team, but it’s a part of the process
                                we can’t get rid of. The way you handle cuts as a coach is so important for several
                                reasons. First, fair or not, it’s a way that many gauge your professionalism and your
                                compassion for the kids you coach. It can have a big influence on how people view both
                                you and your organization, team or club. Let’s take a quick look at a few ways to make
                                it easier, less-stressful and more painless for everyone involved!</p>
                            <h3 class="title-text">The Sealed Envelope Technique</h3>
                            <p>Once again, there’s no right or wrong way to do you cuts, but this is one method that has
                                been effective. (Gotta give credit to HockeyShare’s own Coach Kevin Muller for this
                                one!)</p>
                            <p>After the final tryout, each player receives a sealed envelope with their name on it. The
                                envelope will either have a &#8220;congratulations&#8221; letter or a &#8220;thanks for
                                trying out&#8221; letter. The players receive the envelope AFTER they’ve showered and
                                are ready to leave. Players are instructed NOT to open the letters until they get to
                                their cars (parents are informed the same thing). This way, if they&#8217;re cut, they&#8217;re
                                already in the car and can leave quietly without embarrassment and it also decreases the
                                likelihood of an angry parent coming back into the rink.</p>
                            <p>Players selected are asked to come back inside for a meeting.</p>
                            <h3 class="title-text">The 24-Hour Rule</h3>
                            <p>The day after the try-outs end, post the selected roster online. Let all parents know
                                that any conversations about your decisions will not be held until after the roster is
                                posted. Emotions can be high for certain parents immediately after the letters are
                                distributed and that might not lend itself to productive discussion. This period will
                                hopefully allow those who are unhappy some time to cool off and have a productive
                                discussion if warranted.</p>
                            <p>Another good suggestion is to wait until after 9 PM the day after try-outs to post your
                                roster, to further minimize the likelihood of getting any calls until the following
                                day.</p>
                            <h3 class="title-text">Giving Feedback</h3>
                            <p>It’s important to let each kid get some feedback as to where they need to improve. I’ve
                                always felt that if we’re going to cut a kid, we have a responsibility to let him or her
                                know what they can do to improve for next season and give them some hope. After all, as
                                ambassadors of the sport, we don’t ever want our try-out to be the reason a kid
                                quits!</p>
                            <p>As much as we might not want to deal with unhappy parents, giving the kids some feedback
                                is just the right thing to do. One suggestion is to set aside an afternoon in the coming
                                days when the team is not practicing to have 15 minute meetings with any player or
                                parent interested in receiving feedback. It could also be done over email or phone, but
                                is much more impactful for the player when done face-to-face. Always start by telling
                                the player what they did well and then transition into where they need to improve.</p>
                            <p>Obviously, we need to be prepared for those who are critical of your decisions. The fact
                                is you’re never going to be able to please everyone. If you’re the coach, it’s your team
                                and your decisions but there will likely be a few parents, board members or friends who
                                are unhappy and make it known that they disagree with your decisions, and that’s ok. In
                                these meetings, always make sure to be a good listener, first and foremost. If a parent
                                or player is talking, sit quietly and listen. Don’t cut them off, don’t interrupt and
                                let them finish completely. If needed, take notes so you can respond to all the points
                                once they are done talking. Many times parents just want to be able to say their peace,
                                and by letting them go until they&#8217;re completely finished, you’re allowing them to
                                get that satisfaction. Even if they still disagree with you, they’ll never be able to
                                say that you didn’t listen to them!</p>
                            <h3 class="title-text">Putting It All Together</h3>
                            <p>Coaching hockey is fun, rewarding and challenging all at the same time. You as coaches
                                are in the unique position to share your passion and knowledge with the next generation
                                of players and coaches and help create great young men and women in the process.</p>
                            <p>Let’s be honest, it sucks to have an otherwise AWESOME experience become a stressful,
                                frustrating headache because of try-outs.</p>
                            <p>Now, imagine a situation where your try-outs run smoothly, your decision-making process
                                is easier than ever and parent complaints are at an all-time minimum.</p>
                            <p>How much more would you enjoy your coaching position? How much more quickly could you
                                just get to work doing what you do best &#8211; coaching and developing your team?</p>
                            <p>Hopefully this guide has given you some great ideas to get there, but we want to make
                                this system bulletproof for you by giving you the materials you need to solve these
                                problems now!</p>
                            <p><b>So&#8230; as a special offer only for HockeyShare readers, we’ll send you our premium
                                    <span style="text-decoration: underline;"><span class="underline_text">Try-Out Survival Kit</span></span>
                                    for <span style="color: #e60e0e;">FREE</span>. Just tell us where to send it.</b>
                            </p>
                            <hr/>
                            <p><i><span style="font-weight: 400;"><br/>
Pete Jacobson created WinSmarter to help coaches with the biggest frustrations we all sometimes struggle with: things like dealing with difficult parents, motivating your players, recruiting more kids into your program, fundraising, increasing participation in off-season activities and much more. Get started right now with the </span></i><a
                                        href="http://www.winsmarter.com/go-hockeyshare-com"><b><i>Hockey Coaches’
                                            Try-Out Survival Kit</i></b></a><i><span style="font-weight: 400;">.</span></i>
                            </p>
                            <br/>
                        </div>

                        <div class="feedback">
                        </div>
                    </div>
                    <br/>
                    <p>&nbsp;</p>

                    <div class="post-1366 post type-post status-publish format-standard hentry category-comments-thoughts tag-off-season tag-spring-hockey tag-summer-hockey"
                         id="post-1366">
                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/"
                               rel="bookmark">Tips for getting the most out of your spring training</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">11</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/off-season/" rel="tag">off season</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/spring-hockey/"
                                                    rel="tag">spring hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/summer-hockey/"
                                                    rel="tag">summer hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Spring is an interesting time of year for the hockey world. There is seemingly no end to
                                the options available for the competitive hockey player in the &#8220;off&#8221; season
                                &#8211; if you are offering a training option, here are some tips (from a coaching
                                perspective) to make sure you&#8217;re serving your athletes effectively.</p>
                            <ul>
                                <li><strong>Tempo</strong>: If you are expecting your athletes to compete at the same
                                    level they did over the previous 6 months, you&#8217;ll likely be disappointed.
                                    There is nothing wrong with expecting effort, but understand the early phases of the
                                    off season need to be about <strong>lower tempo, higher detail</strong>. Get
                                    in-depth with whatever skill you&#8217;re looking to teach and make sure you&#8217;re
                                    armed with the proper technical knowledge to effectively communicate the key
                                    components.
                                </li>
                                <li><strong>Skating</strong>: It becomes tempting as a coach to overlook incorporating
                                    skating in to each session. It isn&#8217;t always exciting, it&#8217;s not a
                                    player-favorite, it&#8217;s an extremely intricate art, and you have to make sure
                                    your goalies are still getting beneficial training. The bottom line is there is not
                                    a single skater on your ice who does not need to work on their skating. Players and
                                    coaches who truly embrace skill development understand this is an extremely
                                    important topic.
                                </li>
                                <li><strong>Games</strong>: Off-season games don&#8217;t make your players better, in
                                    fact they usually just create bad habits &#8211; habits you will need to spend time
                                    fixing later. Most spring / summer games are simply glorified rat hockey. This isn&#8217;t
                                    to say you can&#8217;t play any games over the off-season, simply use them sparingly
                                    and in an amount appropriate to the level you are coaching. The more competitive the
                                    team, the fewer games necessary.
                                </li>
                                <li><strong>Off Ice Training</strong>: If you are involved with a competitive team,
                                    age-appropriate off ice training is a must. Look for qualified professionals to
                                    train your players or at minimum oversee the program design. While most coaches are
                                    well-meaning, the likelihood of injury skyrockets when the person running the
                                    workout is unqualified. A proper off ice training program should include measures to
                                    correct specific pattern overloads created by our sport.
                                </li>
                                <li><strong>Planning</strong>: Many coaches view the spring and summer as a time to
                                    &#8220;wing&#8221; it when they hit the ice. While many coaches can &#8220;wing&#8221;
                                    the drills being run, the concept being taught should not. Plan the skills you want
                                    to cover and build a skill progression. This way even if you don&#8217;t have time
                                    to put together the specific practice plan, you will still be able to teach the
                                    concepts in the correct sequences. It is always best to have the full practice plan
                                    created and saved for future reference.
                                </li>
                            </ul>
                            <p>If you&#8217;re looking for some training ideas, have a look at our <a
                                        href="http://hockeyshare.com/video/">video section</a> with over 100 skill
                                videos and our <a href="http://hockeyshare.com/drills/">free hockey drill
                                    library</a> featuring over 1,100 drills. Best of luck this off season!</p>
                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="post-1356 post type-post status-publish format-standard hentry category-a-different-approach"
                         id="post-1356">

                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/"
                               rel="bookmark">Coaching Leadership Self-Awareness Questions</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">19</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/"
                                                rel="category tag">A Different Approach</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Eric Hoffberg (&#8220;Hoff&#8221;) is a brilliant mind when it comes to leadership and
                                mental toughness. He spends his days training professional athletes and coaches as well
                                as top corporate executives &#8211; but one of his true passions is hockey. Hoff
                                recently shared a great document for coaches focused around raising self-awareness
                                around the leadership and attitude. Every coach who is serious about creating a winning
                                culture should give this document a serious look.</p>
                            <p><a href="http://hockeyshare.com/files/Hoffberg-20151118.pdf" target="_blank">Download
                                    Hoff&#8217;s Self-Awareness Questions (PDF)</a></p>
                            <p><strong>A little background on &#8220;Hoff&#8221;:</strong></p>
                            <p>Eric Hoffberg spent 16 years coaching hockey, college, then pro. Originally from
                                Rochester NY, Hoffberg was the head coach at RIT all through the 90’s. Today, Hoffberg
                                works as both a Corporate Coach for leaders and executives across the country that are
                                looking to build High Performance Cultures and as a Mental Toughness Trainer for
                                athletes that are looking for a greater understanding of how to focus under pressure. He
                                is the author of two great books (<a
                                        href="http://erichoffberg.com/index.php/products-for-sale/125-think-strong-reminders"
                                        target="_blank">Think Strong Reminders</a> and <a
                                        href="http://erichoffberg.com/index.php/products-for-sale/120-think-strong-for-athletes"
                                        target="_blank">Think Strong for Athletes</a>) for training attitude and
                                mindset. Learn more about Hoff at <a href="http://www.erichoffberg.com" target="_blank">www.erichoffberg.com</a>.
                            </p>
                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class=" post-1334 post type-post status-publish format-standard hentry category-cool-links tag-3v2 tag-offensive-attack tag-rush"
                         id="post-1334">

                        <h2><a class="title-blog" href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/"
                               rel="bookmark">3
                                on 2 Rushes / Plays</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">6</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/cool-links/"
                                                rel="category tag">Cool Links</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/3v2/" rel="tag">3v2</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/offensive-attack/"
                                                    rel="tag">offensive attack</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/rush/"
                                                    rel="tag">rush</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>A 3v2 rush is a great opportunity to generate offensive chances &#8211; if executed
                                correctly. Here&#8217;s an article explaining several ways to approach a 3v2 rush. The
                                article includes several still shots and game videos.</p>
                            <p><a class="title-blog" href="http://thecommittedindian.com/breaking-3-2-plays/"
                                  target="_blank">Click Here to
                                    Read Article</a></p>

                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="post-1321 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-penalty-kill tag-special-teams tag-video"
                         id="post-1321">

                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/"
                               rel="bookmark">Penalty Killing Tips &#038; Tricks</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">12</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/penalty-kill/" rel="tag">penalty kill</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/special-teams/"
                                                    rel="tag">special teams</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>HockeyShare has put together two videos illustrating tips &amp; tricks for teaching the
                                penalty kill for both forwards and defensemen. The videos cover keys to look for during
                                coverage as well as technique and positional topics.</p>
                            <p><span class="youtube"><iframe title="YouTube video player" class="youtube-player"
                                                             type="text/html" width="640" height="360"
                                                             src="//www.youtube.com/embed/_D2gk9qg4DE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                             frameborder="0" allowfullscreen></iframe></span></p>
                            <p><span class="youtube"><iframe title="YouTube video player" class="youtube-player"
                                                             type="text/html" width="640" height="360"
                                                             src="//www.youtube.com/embed/Ug9pRdUnFXA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                             frameborder="0" allowfullscreen></iframe></span></p>

                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="post-1318 post type-post status-publish format-standard hentry category-a-different-approach"
                         id="post-1318">
                        <h2>
                            <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/"
                               rel="bookmark">High-Quality Year-End Meetings</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">4</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/"
                                                rel="category tag">A Different Approach</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Several months ago, a friend of mine introduced me to <a title="Visit Eric's Website"
                                                                                        href="http://www.erichoffberg.com"
                                                                                        target="_blank">Eric
                                    Hoffberg</a>. Eric trains many elite-level (pro, college, junior, etc) hockey
                                athletes and a lot of corporate clients in the art of mental toughness. Through many
                                conversations, Eric and I quickly realized how much we had in common in terms of our
                                views on not only the game but also on mental training, and began immediately working on
                                creating content together. We are excited to announce a June 2014 launch of our first
                                product together titled: <strong><em>A Different Approach</em></strong>. The program is
                                all about how to build and plan your season with intention, and we know it will give you
                                some great ideas and thinking points.</p>
                            <p>In the meantime, we both wanted to offer some thoughts and provide a resource for coaches
                                to begin thinking differently about their team right away. Given the timing, we felt
                                like a quick one-page download focused on year-end meetings would be relevant. The
                                year-end meeting can pave the path to success for the next year and when used
                                effectively can encourage athletes to invest time in their development during the
                                off-season. Below is a link to a <span style="text-decoration: underline;">free</span>
                                PDF download of some key ideas on how to hold high-quality year-end meetings. We hope
                                you find the content useful and valuable:</p>
                            <p><strong><a title="Download Now"
                                          href="https://hockeyshare.s3.amazonaws.com/year-end-meetings.pdf"
                                          target="_blank">Download the FREE PDF on Holding High-Quality Year-End
                                        Meetings</a></strong></p>
                            <p><img class="alignnone"
                                    src="https://hockeyshare-newsletters.s3.amazonaws.com/20140304_ada.jpg"
                                    alt="A Different Approach" width="170" height="154"/></p>
                            <p>Stay tuned to www.hockeyshare.com for the latest on <strong><em>A Different Approach</em></strong>.
                                For more on Eric Hoffberg, you can visit his site at <a title="Visit Eric's Website"
                                                                                        href="http://www.erichoffberg.com"
                                                                                        target="_blank">www.erichoffberg.com</a>
                                or follow him on Twitter <a title="@EricHoffberg"
                                                            href="https://twitter.com/EricHoffberg"
                                                            target="_blank">@erichoffberg</a></p>

                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1316 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-video"
                         id="post-1316">
                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/"
                               rel="bookmark">Bruins Illustrate Importance of Driving the Net &#038; Net Front
                                Presence</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The Boston Bruins put on a clinic on the importance of net drives and net front presence
                                in their 6-3 win over the NY Rangers on Sunday, March 2nd, 2014. Also notice the puck
                                support when entering the zone &#8211; great examples:</p>
                            <p>Boston &#8211; Goal #1: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-221-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-221-h</a>
                                Quality puck dump, aggressive 1st man, solid puck support, net drive</p>
                            <p>Boston &#8211; Goal #2: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-401-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-401-h</a>
                                Good rush / puck support, net drive, patience with puck on entry, D activating in rush
                            </p>
                            <p>Boston &#8211; Goal #3: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-419-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-419-h</a>
                                Quality puck movement, movement away from puck (players making themselves available for
                                the puck carrier), net front presence</p>
                            <p>Boston &#8211; Goal #4: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-632-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-632-h</a>
                                Great patience entering the zone, good puck protection, net drive</p>
                            <p>Boston &#8211; Goal #5: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-643-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-643-h</a>
                                Aggressive puck pursuit w/ good containment creates turnover, wide drive to allow
                                support, net drive</p>
                            <p>Boston &#8211; Goal #6: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-865-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-865-h</a>
                                Good puck movement in zone, net front presence for tip</p>

                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1308 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2hockey tag-video"
                         id="post-1308">

                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/"
                               rel="bookmark">Attacking the High Seam (Video &#038; Examples)</a></h2>
                        <div class="blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2hockey/"
                                                    rel="tag">m2hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We have gotten a lot of great feedback from users on our video on attacking the high seam
                                off the half-wall. Lots of people wrote us asking to see it in action, so we provided a
                                series of links to additional videos showing the use of the seam to either create a goal
                                or set one up. In case you missed our video &#8211; we have embedded it below. The
                                example video links are located below the video.</p>
                            <p class="center"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/7jp4IjQRk0Q?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p><strong>Examples of this seam in action:</strong></p>
                            <p>Basic Example: <a href="http://video.nhl.com/videocenter/console?id=2013020618-325-h">http://video.nhl.com/videocenter/console?id=2013020618-325-h</a>
                                (watch how the D pulls up at the hash marks)</p>
                            <p>Power Play Example: <a href="http://video.nhl.com/videocenter/?id=2013020603-660-h">http://video.nhl.com/videocenter/?id=2013020603-660-h</a>
                            </p>
                            <p>Different Angle of Attack on the Same Seam: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020616-342-h">http://video.nhl.com/videocenter/console?id=2013020616-342-h</a>
                            </p>
                            <p>Seam Off a Faceoff: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020616-350-h">http://video.nhl.com/videocenter/console?id=2013020616-350-h</a>
                            </p>
                            <p>Seam Exploited Off a Contained Rush: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020613-635-h">http://video.nhl.com/videocenter/console?id=2013020613-635-h</a>
                            </p>
                            <p>Seam w/ Pass: <a href="http://video.nhl.com/videocenter/console?id=2013020615-261-h">http://video.nhl.com/videocenter/console?id=2013020615-261-h</a>
                                (Pause the video 4 seconds in &#8211; notice how the strong-side winger has turned his
                                body and committed in to the puck carrier, opening up the point option)</p>
                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="post-1302 post type-post status-publish format-standard hentry category-10000-pucks tag-10k-pucks tag-contest"
                         id="post-1302">

                        <h2><a class="title-blog"
                               href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/"
                               rel="bookmark">10,000 Pucks Contest &#8211; 2013</a></h2>
                        <div class="class= blog_div_one">
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10k-pucks/"
                                                    rel="tag">10k pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The 7th Annual 10,000 Pucks Contest is about to begin. Last year we had over 5.4 million
                                shots logged world-wide by over 1,400 players. This year&#8217;s contest looks to be
                                bigger than ever. Thanks to our prize sponsor for the contest <a
                                        href="http://www.hockeyshot.com/?Click=40243" target="_blank">HockeyShot.com</a>.
                            </p>
                            <p class="center"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/Pur6IazNxWs?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <h3 class="title-text" class="center"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/10000pucks/"><span
                                                    style="color: #ff0000;">Get started today</span></a></strong></span>
                            </h3>

                            <br/>
                        </div>
                        <div class="feedback">
                        </div>
                    </div>


                    <p>&nbsp;</p>
                    <div class="post-1298 post type-post status-publish format-standard hentry category-comments-thoughts tag-audio tag-podcast tag-summer-training"
                         id="post-1298">
                        <h2>
                            <a class="title-blog"
                               href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/"
                               rel="bookmark">Kevin from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a></h2>
                        <div>
                            <table class="blog_text" width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="blog_table">
                                            <tr>
                                                <td align="center" class="blog_td">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="blog_td_one">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/audio/"
                                                    rel="tag">audio</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/podcast/"
                                                    rel="tag">podcast</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/summer-training/"
                                                    rel="tag">summer training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Kevin from HockeyShare.com was recently featured on a podcast from Weiss Tech Hockey.
                                Founder of Weiss Tech Hockey, Jeremy Weiss, and Kevin Muller discuss in-depth strategies
                                for the off-season from both a player and coach standpoint.</p>
                            <p><a href="http://weisstechhockey.com/podcast/?p=40" target="_blank">Podcast Page at Weiss
                                    Tech Hockey</a> | <a
                                        href="https://itunes.apple.com/us/podcast/weiss-tech-hockey-cast/id596531440"
                                        target="_blank">Weiss Tech Hockey Podcast on iTunes</a></p>
                            <br/>

                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <a style="font-size: 12px" href="http://hockeyshare.com/blog/page/2/">Older Posts &raquo;</a>

                </td>
                <td valign="top" width="250">
                    <div id="mysidebar" class="sidebar_blog">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span
                                        class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text blog_search" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input class="blog_search" type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <!-- Footer -->
    </div>
</div>

