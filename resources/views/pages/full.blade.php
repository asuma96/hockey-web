@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="full">

                        <h2>Compare Features</h2>
                        <table width="100%" border="0" cellspacing="0" id="table_a">
                            <tbody>
                            <tr class="even">
                                <th>&nbsp;</th>
                                <th width="140" class="drills_compare_std drills_compare_top">Free</th>
                                <th width="140" class="drills_compare_prem drills_compare_top">Premium</th>
                                <th width="200" class="drills_compare_assn drills_compare_top">
                                    <strong>Team/Association</strong></th>
                                <th width="20" class="drills_compare_assn drills_compare_top">&nbsp;</th>
                            </tr>

                            <tr class="odd">
                                <td class="drills_compare_feature drills_compare_bottom">&nbsp;</td>
                                <td class=" center drills_compare_std drills_compare_bottom"
                                    style="padding:10px;"><a href=" http://hockeyshare.com/upgrade/acp.php">upgrade/acp.php</a>
                                </td>
                                <td class=" center drills_compare_prem drills_compare_bottom"><a
                                            href=" http://hockeyshare.com/login" class="pbutton full_a_input">Sign
                                        Up</a></td>
                                <td class=" center drills_compare_assn drills_compare_bottom"
                                    style="padding:10px;"><span class="drills_compare_prem drills_compare_bottom"><a
                                                href=" http://hockeyshare.com/upgrade/acp_assn.php"
                                                class="pbutton full_a_input">Sign Up</a></span>
                                </td>
                                <td class=" center drills_compare_assn drills_compare_bottom"
                                    style="padding:10px;">&nbsp;</td>
                            </tr>


                            <tr class="even">
                                <td colspan="5"
                                    class="full_td">Drill
                                    Features
                                </td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature ">Access Your Drills from Multiple Computers</td>
                                <td class=" center drills_compare_std "><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn "><img src="/img/check.png"
                                                                              height="16" width="16" alt="Yes"
                                                                              border="0"></td>
                                <td class=" center drills_compare_assn ">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature ">Diagram Drills Online</td>
                                <td class=" center drills_compare_std "><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn "><img src="/img/check.png"
                                                                              height="16" width="16" alt="Yes"
                                                                              border="0"></td>
                                <td class=" center drills_compare_assn ">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Animate Your Drills</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Number of Drills Allowed</td>
                                <td class=" center drills_compare_std">15</td>
                                <td class=" center drills_compare_prem">Unlimited</td>
                                <td class=" center drills_compare_assn">Unlimited</td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Edit Drills</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Export Drill as JPG</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Share Drills on HockeyShare.com</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Hide Drills from Public</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Export Drills as High-Resolution JPG</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Export Drills as PDF</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Create Custom Drill Categories</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Assign Multiple Categories to Drills</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Create Multiple Diagrams for Drills (Up to 5)</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Email Drills</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Branding Free Diagrams <span class="subtle_nu">No HockeyShare Logo</span>
                                </td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">View Drill Diagram on Mouse Over</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Share Drill PDF w/ Your Team (Password Protected)
                                </td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>

                            <tr class="even">
                                <td colspan="5"
                                    class="full_td_3">Practice
                                    Planner Features
                                </td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Access Your Plans from Multiple Computers</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Create Practice Plans</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Number of Practice Plans Allowed</td>
                                <td class=" center drills_compare_std">5</td>
                                <td class=" center drills_compare_prem">Unlimited</td>
                                <td class=" center drills_compare_assn">Unlimited</td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Print Practice Plans</td>
                                <td class=" center drills_compare_std"><img src="/img/check.png"
                                                                            height="16" width="16" alt="Yes"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>

                            <tr class="odd">
                                <td class="drills_compare_feature">Email Practice Plans</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Export Practice Plans as PDF</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td align="center"><img src="/img/check.png" height="16" width="16"
                                                        alt="Yes" border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Share Plan PDF w/ Your Team (Password Protected)</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature">Add Practice Plan to Practice Schedule (<span
                                            style="font-weight:bold;">more info</span>)
                                </td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td colspan="5"
                                    class="full_td_3">
                                    Association Features
                                </td>
                            </tr>

                            <tr class="even">
                                <td class="drills_compare_feature">Custom Logo Support</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem">Add-On</td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Share Plans &amp; Drills w/ Association Members</td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/no.png"
                                                                             height="16" width="16" alt="No"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>

                            <tr class="even">
                                <td class="drills_compare_feature">View Plans &amp; Drills from Other Association
                                    Members
                                </td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/no.png"
                                                                             height="16" width="16" alt="No"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>

                            <tr class="odd">
                                <td class="drills_compare_feature">Copy Plans &amp; Drills from Other Association
                                    Members
                                </td>
                                <td class=" center drills_compare_std"><img src="/img/no.png"
                                                                            height="16" width="16" alt="No"
                                                                            border="0"></td>
                                <td class=" center drills_compare_prem"><img src="/img/no.png"
                                                                             height="16" width="16" alt="No"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn"><img src="/img/check.png"
                                                                             height="16" width="16" alt="Yes"
                                                                             border="0"></td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>

                            <tr class="even">
                                <td colspan="5"
                                    class="full_td_2">Pricing
                                </td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature">Monthly</td>
                                <td class=" center drills_compare_std">FREE</td>
                                <td class=" center drills_compare_prem">$
                                    8.00
                                </td>
                                <td class=" center drills_compare_assn">-</td>
                                <td class=" center drills_compare_assn">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature ">3/mn</td>
                                <td class=" center drills_compare_std ">FREE</td>
                                <td class=" center drills_compare_prem ">$22.00 <br>
                                    <span class="subtle_nu" style="color:#B52222;">(Save 8%)</span></td>
                                <td class=" center drills_compare_assn ">-</td>
                                <td class=" center drills_compare_assn ">&nbsp;</td>
                            </tr>
                            <tr class="odd">
                                <td class="drills_compare_feature ">6/mn</td>
                                <td class=" center drills_compare_std ">FREE</td>
                                <td class=" center drills_compare_prem ">$40.00 <br>
                                    <span class="subtle_nu" style="color:#B52222;">(Save 16%)</span></td>
                                <td class=" center drills_compare_assn ">-</td>
                                <td class=" center drills_compare_assn ">&nbsp;</td>
                            </tr>
                            <tr class="even">
                                <td class="drills_compare_feature ">Yearly</td>
                                <td class=" center drills_compare_std ">FREE</td>
                                <td class=" center drills_compare_prem ">$
                                    75.00 <br>
                                    <span class="subtle_nu" style="color:#B52222;">(Save 21%)</span></td>
                                <td class=" center drills_compare_assn ">As low as $20/user/yr<span
                                            class="subtle_nu"></span></td>
                                <td class=" center drills_compare_assn ">&nbsp;</td>
                            </tr>


                            <tr class="odd">
                                <td class="drills_compare_feature drills_compare_bottom">&nbsp;</td>
                                <td class=" center drills_compare_std drills_compare_bottom"
                                    style="padding:10px;"><a href=" http://hockeyshare.com/upgrade/acp.php">upgrade/acp.php</a>
                                </td>
                                <td class=" center drills_compare_prem drills_compare_bottom"><a
                                            href=" http://hockeyshare.com/login" class="pbutton full_a_input">Sign
                                        Up</a></td>
                                <td class=" center drills_compare_assn drills_compare_bottom"
                                    style="padding:10px;"><span class="drills_compare_prem drills_compare_bottom"><a
                                                href=" http://hockeyshare.com/upgrade/acp_assn.php"
                                                class="pbutton full_a_input">Sign Up</a></span>
                                </td>
                                <td class=" center drills_compare_assn drills_compare_bottom"
                                    style="padding:10px;">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        @include('includes.commercial')
        <br>
    </div>
</div>

