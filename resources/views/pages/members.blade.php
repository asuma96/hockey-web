@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content members">
        <br/>
        <div align="left" class="page_content_stretch_top" class="members_div"><img
                    src="/img/ada_logo.jpg" alt="A Different Approach" height="77" width="1000" border="0"/>
        </div>
        <table width="100%">
            <tr>
                <td valign="top">
                    <h3>Table of Contents</h3>

                    <p><a href="members.php?" class="subtle">Introduction / Home</a></p>

                    <p><a href="members.php?ch=1&" class="subtle">Chapter 1: Save Everything</a></p>
                    <p><a href="members.php?ch=2&" class="subtle">Chapter 2: Why Are You Coaching This Season?</a></p>
                    <p class="subtle_nu">Chapter 3: Plan Your Culture</p>
                    <p class="subtle_nu">Chapter 3: Controllables (Bonus)</p>
                    <p class="subtle_nu">Chapter 4: Plan the System</p>
                    <p class="subtle_nu">Chapter 5: Plan the Training</p>
                    <p class="subtle_nu">Chapter 6: Great Drill Concepts</p>
                    <p class="subtle_nu">Chapter 7: Study Your Team - Get Intrigued</p>
                    <p class="subtle_nu">Chapter 8: Launch Your Style of Influence</p>
                    <p class="subtle_nu">Chapter 9: Building Great Habits</p>
                    <p class="subtle_nu">Chapter 10: Team Building Ideas</p>
                    <p class="subtle_nu">Chapter 11: Build a Sense of Community</p>
                    <p class="subtle_nu">Chapter 12: Build Competitive Power</p>
                    <p class="subtle_nu">Chapter 13: Create a Customized Assessment Tool</p>
                    <p class="subtle_nu">Chapter 14: Team Meetings</p>
                    <p class="subtle_nu">Chapter 15: Player Meetings</p>
                    <p class="subtle_nu">Chapter 16: Get With the Times</p>
                    <p class="subtle_nu">Chapter 17: Train the Total Athlete</p>
                    <p class="subtle_nu">Chapter 18: Stop & Reflect</p>
                </td>
                <td valign="top" width="700">
                    <p align="right"><a href="http://hockeyshare.com/training/different-approach/purchase.php?"
                                        class="rpbutton">Purchase Program</a></p>
                    <h3>Free Trial</h3>
                    <p>We hope you're enjoying your free trial! Your trial entitles you to 2 free weeks of content.
                        Ready to purchase the full program? Click the button below to get started!</p>
                    <p><a href="http://hockeyshare.com/training/different-approach/purchase.php?" class="rpbutton">Purchase
                            Program for Only $99.99</a></p>
                    <p>&nbsp;</p>
                    <h3>How the Program Works</h3>
                    <p>Each week you'll receive access to: </p>
                    <ul>
                        <li>Video Discussion of the Chapter Content</li>
                        <li>Homework Tasks / Questions</li>
                        <li>Additional Resources / Link (If Available for the Chapter)</li>
                        <li>Access to Bonus Content (If Available for the Chapter)</li>
                    </ul>
                    <p>The program is designed to help you transform your season one week at a time. We deliver the
                        content to you once per week to help make the volume of content managable within a busy
                        schedule. Our goal is to provide you with thought-provoking content along with action items to
                        help you implement the ideas discussed. We know if you spend time each week on the content and
                        implementation, you will see a profound affect on your team.</p>

                    <h3>What You'll Need Each Week</h3>
                    <ul>
                        <li>Notebook (Journal) &amp; Pen</li>
                        <li>About 30 minutes to watch the video content</li>
                        <li>15-20 minutes to complete the homework</li>
                        <li>An open mind and the ability to be realistic in your self-evaluation</li>
                    </ul>
                </td>
            </tr>
        </table>
        @include('includes.commercial')
    </div>
</div>