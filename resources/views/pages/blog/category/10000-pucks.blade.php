@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">

                    <div class="post-1302 post type-post status-publish format-standard hentry category-10000-pucks tag-10k-pucks tag-contest"
                         id="post-1302">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/"
                               rel="bookmark">10,000 Pucks Contest &#8211; 2013</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10k-pucks/" rel="tag">10k pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The 7th Annual 10,000 Pucks Contest is about to begin. Last year we had over 5.4 million
                                shots logged world-wide by over 1,400 players. This year&#8217;s contest looks to be
                                bigger than ever. Thanks to our prize sponsor for the contest <a
                                        href="http://www.hockeyshot.com/?Click=40243" target="_blank">HockeyShot.com</a>.
                            </p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/Pur6IazNxWs?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <h3 style="text-align: center;"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/10000pucks/"><span
                                                    style="color: #ff0000;">Get started today</span></a></strong></span>
                            </h3>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1191 post type-post status-publish format-standard hentry category-10000-pucks category-hockey-instructional-video tag-10000-pucks tag-off-ice-shooting tag-shooting tag-video"
                         id="post-1191">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/off-ice-shooting-games/"
                               rel="bookmark">Off Ice Shooting Games</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">11</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-shooting/"
                                                    rel="tag">off ice shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/"
                                                    rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>With the 10,000 Pucks Contest already logging in over 2.5 million shots, we thought it
                                would be good to post a video covering some games you can play while you&#8217;re
                                shooting your pucks to keep your training fresh!</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/yJXRsS66jS0?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Ultimate Goalie Link: <a
                                        href="http://hockeyshot.hockeyshare.com/ug" target="_blank">hockeyshot.hockeyshare.com/ug</a>
                            </p>
                            <p style="text-align: left;">HockeyShot Trick Shot Challenge: <a
                                        href="http://hockeyshare.com/trickshot" target="_blank">hockeyshare.com/trickshot</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1179 post type-post status-publish format-standard hentry category-10000-pucks tag-10kpucks tag-10000-pucks tag-contest"
                         id="post-1179">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2012/"
                               rel="bookmark">10,000 Pucks Contest &#8211; 2012</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">31</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10kpucks/"
                                                    rel="tag">#10kpucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The
                                <a href="http://hockeyshare.com/10000pucks?utm_source=hockeyshare+llc+list&amp;utm_campaign=d42e504e83-10kpucks_2012_email&amp;utm_medium=email">10,000
                                    Pucks Contest</a> is a free contest dedicated to helping youth hockey players set
                                and reach their goals while becoming a better shooter in the process. The premise is
                                simple: set a goal of how many pucks you want to shoot this summer (Jun 1 &#8211; Aug
                                31) &#8211; then track your shots through the <a
                                        href="http://hockeyshare.com/10000pucks/?utm_source=hockeyshare+llc+list&amp;utm_campaign=d42e504e83-10kpucks_2012_email&amp;utm_medium=email">10,000
                                    Pucks website</a> to measure your progress. The website is designed to allow coaches
                                and associations to track the progress of their players in the off-season. Associations
                                and coaches can set up teams in the contest &#8211; players can then join the teams,
                                giving coaches and teammates access to their progress, thus creating accountability
                                toward reaching their goals. The contest is free to use for players, coaches, and
                                associations.</p>
                            <p>This year we&#8217;ve made some big improvements to the system. Here are a couple of the
                                key upgrades you&#8217;ll find this year:</p>
                            <ol>
                                <li>Track multiple players from the same login</li>
                                <li>iPad friendly interface (graphs &amp; charts now load on most tablets / mobile
                                    devices)
                                </li>
                                <li>Coaches: manage access to your team</li>
                                <li>Associations: create all your teams in one place and manage access by team</li>
                                <li>Account setup instructional videos</li>
                            </ol>
                            <p>Get Started Today by Going to: <a href="http://hockeyshare.com/10000pucks">www.hockeyshare.com/10000pucks</a>
                            </p>
                            <p>Account Set Up Instructions: <a
                                        href="http://hockeyshare.com/10000pucks/instructions.php">www.hockeyshare.com/10000pucks/instructions.php</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1035 post type-post status-publish format-standard hentry category-10000-pucks tag-10000-pucks tag-contest tag-results"
                         id="post-1035">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-winners-2011/"
                               rel="bookmark">10,000 Pucks Winners &#8211; 2011</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">20</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/results/"
                                                    rel="tag">results</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As another great 10,000 Pucks Contest comes to a close, it&#8217;s time to recap this
                                year&#8217;s event. With over 800 participants from around the world and 3.5 million
                                shots recorded, this was the biggest (and best) contest to date!</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/Fsfcuxs3pCA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p>I would like to congratulate all the participants who took the time to focus in on their
                                goals and dedicated themselves to improving their game. Whether your goal was 1,000 or
                                20,000 &#8211; there is much to be said about setting a goal and putting in the effort
                                to achieve it. By applying these principles on and off the ice, you will be successful
                                in life!</p>
                            <h3><strong><span
                                            style="text-decoration: underline;">Final Contest Statistics:</span></strong>
                            </h3>
                            <p><strong>Total Participants:</strong> 888<br/>
                                <strong>Total Shots Logged: </strong>3,541,210</p>
                            <h3><strong><span style="text-decoration: underline;">Top Shooters:</span></strong></h3>
                            <p>1st Place :: Michael Thoresen &#8211; 50,150 pucks<br/>
                                2nd Place :: CBRSkinner &#8211; 50,115 pucks<br/>
                                3rd Place :: TeamV &#8211; 41,210 pucks<br/>
                                4th Place :: Noahhockey &#8211; 31,844 pucks<br/>
                                5th Place :: chrisv99 &#8211; 30,000 pucks</p>
                            <h3><span style="text-decoration: underline;"><strong>Top Shooting Teams:</strong></span>
                            </h3>
                            <p>1st Place :: Milwaukee Jr Admirals 2001 &#8211; 189,083 pucks<br/>
                                2nd Place :: AAHA #1 &#8211; 178,852 pucks<br/>
                                3rd Place :: 2001 Green Bay Jr Gamblers &#8211; 174,810<br/>
                                4th Place :: Milwaukee Jr Admirals 1998 &#8211; 163,947<br/>
                                5th Place :: Milwaukee Jr Admirals Girls U14 &#8211; 159,176</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-958 post type-post status-publish format-standard hentry category-10000-pucks category-hockey-instructional-video tag-10kpucks tag-10000-pucks tag-hockey-instructional-video tag-video"
                         id="post-958">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/backhand-off-ice-shooting-tips-video/"
                               rel="bookmark">Backhand Off Ice Shooting Tips [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10kpucks/"
                                                    rel="tag">#10kpucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>With over 200,000 shots already logged in the 10,000 Pucks Contest, we wanted to continue
                                to offer up tips for the players shooting pucks off ice. This video covers some simple
                                tips for working on your backhand shot off the ice.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/UXFPcTrNsQI?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-689 post type-post status-publish format-standard hentry category-10000-pucks category-hockeyshare-contests tag-10000-pucks tag-contests tag-hockeyshare"
                         id="post-689">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/"
                               rel="bookmark">10,000 Pucks &#8211; Final Results</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">13</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contests/"
                                                    rel="tag">contests</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The results are in for the 10,000 Pucks 2010 contest. In this year&#8217;s contest, we
                                had 527 players logging shots from around the world. The total shot tally for this
                                season was an impressive: 1,922,853. Not only did we surpass our goal of seeing 1
                                million shots logged, we almost broke the 2 million mark! Now, the moment everyone&#8217;s
                                been waiting for&#8230;.the player results.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-661 post type-post status-publish format-standard hentry category-10000-pucks tag-10000-pucks tag-hockeyshare"
                         id="post-661">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-update/" rel="bookmark">10,000
                                Pucks Update</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Well, it&#8217;s official &#8211; this year&#8217;s contest topped the charts with over
                                1.2 million shots logged from around the world! Wow &#8211; I would have never guessed a
                                few years back when we started running this it would ever get this big! Great to see
                                everyone working so hard to reach their goals&#8230;.which is the true purpose of this
                                contest.</p>
                            <p><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-update/#more-661"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-645 post type-post status-publish format-standard hentry category-10000-pucks tag-10000-pucks tag-hockeyshare"
                         id="post-645">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-update/"
                               rel="bookmark">10,000 Pucks Contest Update</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As the first month of the contest draws to a close, we&#8217;ve seen a record 740,000 +
                                shots logged in the contest from over 480 participants across the world (yes, we even
                                have some over-seas participants). It&#8217;s great to see everyone so dedicated to
                                improving themselves.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-update/#more-645"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-121 post type-post status-publish format-standard hentry category-10000-pucks"
                         id="post-121">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-winners/" rel="bookmark">10,000
                                Pucks Winners</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">5</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The winners of the 10,000 Pucks Contest can download the Prize Claim Form by using the
                                link sent to you via email.</p>
                            <p>All the users reaching 10,000 shots during the contest are encouraged to send in their
                                prize claim forms to receive a special gift from HockeyShare.com!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                </td>
                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

