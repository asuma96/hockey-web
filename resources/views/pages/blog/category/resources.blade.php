@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-818 post type-post status-publish format-standard hentry category-resources tag-espn tag-link"
                         id="post-818">

                        <h2>
                            <a href="http://hockeyshare.com/blog/resources/espn-article-on-producing-elite-us-players/"
                               rel="bookmark">ESPN Article on Producing Elite US Players</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/espn/" rel="tag">espn</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/link/" rel="tag">link</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Earlier this morning, <a href="http://coachnielsen.wordpress.com/" target="_blank">Coach
                                    Neilsen</a> posted a link to a great article on ESPN by John Buccigross about
                                producing elite US players. It&#8217;s definitely a good read:</p>
                            <p>
                                <a href="http://espn.go.com/nhl/notebook/_/page/buccigross_101129/producing-elite-us-players-starts-bottom?ex_cid=MyESPNToday_TopStory"
                                   target="_blank">http://espn.go.com/nhl/notebook/_/page/buccigross_101129/producing-elite-us-players-starts-bottom?ex_cid=MyESPNToday_TopStory</a>
                            </p>
                            <p>While you&#8217;re checking out links, head over to Coach Neilsen&#8217;s website and
                                enjoy some of his content!</p>
                            <p><a href="http://coachnielsen.wordpress.com/" target="_blank">http://coachnielsen.wordpress.com/</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-693 post type-post status-publish format-standard hentry category-resources tag-scouts tag-tryouts tag-youth-hockey-coaching"
                         id="post-693">

                        <h2><a href="http://hockeyshare.com/blog/resources/qualities-coaches-scouts-look-for/"
                               rel="bookmark">Qualities Coaches &#038; Scouts Look For</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">15</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/scouts/"
                                                    rel="tag">scouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here&#8217;s an article from Minnesota Hockey outlining some key points coaches and
                                scouts look for in hockey players:</p>
                            <p><a href="http://go.madmimi.com/redirects/7de2629d8ce2ea5be4755ab310d9ddf7?pa=1814571722">Download
                                    the PDF from Minnesota Hockey</a></p>
                            <p>Certainly some great points for players to take into their tryouts!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-637 post type-post status-publish format-standard hentry category-resources tag-minnesota-hockey tag-player-development tag-youth-hockey-coaching"
                         id="post-637">

                        <h2><a href="http://hockeyshare.com/blog/resources/coaching-to-develop-players/"
                               rel="bookmark">Coaching to Develop Players</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/minnesota-hockey/"
                                                    rel="tag">minnesota hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/player-development/"
                                                    rel="tag">player development</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Minnesota Hockey puts out a great newsletter every month with lots of great resources. In
                                the most recent article, they put out a brief article on Coaching to Develop Players,
                                and the importance of stressing fundamentals. I wanted to share the link with
                                everyone:</p>
                            <p>
                                <a href="http://assets.ngin.com/attachments/document/0005/2292/Coaching_to_Develop_Players.pdf"
                                   target="_blank">Minnesota Hockey&#8217;s &#8220;Coaching to Develop Players&#8221;
                                    (PDF)</a></p>
                            <p>Let me know what you think about this article by leaving a comment below.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-561 post type-post status-publish format-standard hentry category-resources tag-bench-management tag-hockey-coaching tag-hockeyshare-download"
                         id="post-561">

                        <h2><a href="http://hockeyshare.com/blog/resources/bench-management-lineu-card-download/"
                               rel="bookmark">Bench Management &#8211; Lineup Card [Download]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">24</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/bench-management/"
                                                    rel="tag">bench management</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-download/"
                                                    rel="tag">hockeyshare download</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Managing the bench during game situations can be difficult at best. Players get hurt,
                                equipment fails, penalties are called &#8211; you name it, it changes your game plan.
                                Good coaches are able to react and make decisions quickly. One of the tools I&#8217;ve
                                found most useful over the years has been the lineup card. This allows me to quickly
                                glance down and know my first choice for most situations. It is also extremely useful if
                                you&#8217;re in a game where line match-ups are important. I&#8217;ve provided a PDF
                                download of the template I use for my lineup cards. It includes 4 offensive and
                                defensive lines, 2 goaltenders, 2 power play units, 2 penalty kill units, two groups for
                                end of the game play (pulling goalie, or defending a one-goal lead), and areas for notes
                                and diagrams. I hope you find this useful in your coaching!</p>
                            <p><a href="http://www.mediafire.com/?yywmydiyt5y" target="_blank">» Download the
                                    HockeyShare Lineup Card (PDF)</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-192 post type-post status-publish format-standard hentry category-resources tag-don-lucia tag-hockey-coaching tag-lets-play-hockey tag-magic-hockey-helmet tag-youth-hockey-coach"
                         id="post-192">

                        <h2><a href="http://hockeyshare.com/blog/resources/5-great-hockey-resources/"
                               rel="bookmark">5 Great Hockey Resources</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/don-lucia/" rel="tag">don lucia</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/lets-play-hockey/"
                                                    rel="tag">let's play hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/magic-hockey-helmet/"
                                                    rel="tag">magic hockey helmet</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coach/"
                                                    rel="tag">youth hockey coach</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p style="text-align: left;">Don Lucia speaking on coaches and parents in youth hockey.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/eFsF0Z9EKDg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">The Magic Hockey Helmet &#8211; great insight from the mind of
                                a kid!</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/rWjBvcfhRX0?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Great tips for players of all positions from Let&#8217;s Play
                                Hockey:</p>
                            <p style="text-align: left;"><a href="http://www.letsplayhockey.com/GoldenRulesForwards.pdf"
                                                            target="_blank">Golden Rules for Forwards<br/>
                                </a><a href="http://www.letsplayhockey.com/GoldenRulesDefensemen.pdf" target="_blank">Golden
                                    Rules for Defensemen<br/>
                                </a><a href="http://www.letsplayhockey.com/GoldenRulesGoaltenders.pdf" target="_blank">Golden
                                    Rules for Goalies</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-172 post type-post status-publish format-standard hentry category-resources tag-hockey-coaching-forms tag-hockey-stat-tracking tag-hockey-stat-tracking-forms"
                         id="post-172">

                        <h2><a href="http://hockeyshare.com/blog/resources/stat-tracking-forms/" rel="bookmark">Stat
                                Tracking Forms</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching-forms/"
                                                    rel="tag">hockey coaching forms</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-stat-tracking/"
                                                    rel="tag">hockey stat tracking</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-stat-tracking-forms/"
                                                    rel="tag">hockey stat tracking forms</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Keeping statistics for your season can prove to be a daunting task. Lots of parents are
                                hesitant to take on the role, and finding the best way to track them can be a pain. Over
                                the past several seasons, we have come up with some formats that seem to work very well.
                                Below are download links to some of the blank forms we&#8217;ve found useful.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/HockeyShare_Stat_Tracker.pdf">HockeyShare
                                    Stat Tracker (PDF)<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/HockeyShare_Stat_Tracker.xls">HockeyShare
                                    Stat Tracker (XLS)</a></p>
                            <p>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/HockeyShare_Stat_Tracker-FO.pdf">HockeyShare
                                    Face Off Tracker (PDF)</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/HockeyShare_Stat_Tracker-PM.pdf">HockeyShare
                                    Plus/Minus Tracker (PDF)</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/HockeyShare-ShotTracker.pdf">HockeyShare
                                    Shot Tracker (PDF)</a></p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/Game-Stat-Sheet.pdf">Game
                                    Stat Sheet (PDF)</a> [Contributed by forum user Blue Chicago]</p>
                            <p>If you have forms you&#8217;d like to share, please send me an email!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

