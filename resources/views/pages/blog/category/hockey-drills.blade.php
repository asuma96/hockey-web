@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1254 post type-post status-publish format-standard hentry category-hockey-drills tag-hockey-drills"
                         id="post-1254">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-drills/drill-shootout-winning-drill-back-passing/"
                               rel="bookmark">Drill Shootout Winning Drill: Back-Passing</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">27</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The winner of the Drill Shootout from last week is the Back-Passing drill. Below is a
                                link to the drill page, which includes an <strong>animation</strong> of the drill.</p>
                            <p>Back-Passing Hockey Drill: <a
                                        href="http://hockeyshare.com/drills/drill.php?id=59606">www.hockeyshare.com/drills/drill.php?id=59606</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1250 post type-post status-publish format-standard hentry category-hockey-drills tag-drill-shootout tag-hockey-drills"
                         id="post-1250">
                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-drills/drill-shootout-winning-drill-3-2-washington/"
                               rel="bookmark">Drill Shootout Winning Drill: 3-2 Washington</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">22</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drill-shootout/"
                                                    rel="tag">drill shootout</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The first winner of the Drill Shootout is the 3-2 Washington drill. Below is a link to
                                the drill page, which includes an <strong>animation</strong> of the drill.</p>
                            <p><strong>3-2 Washington Hockey Drill: <a
                                            href="http://hockeyshare.com/drills/drill.php?id=59604">http://hockeyshare.com/drills/drill.php?id=59604</a></strong>
                            </p>
                            <p>&nbsp;</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1245 post type-post status-publish format-standard hentry category-hockey-drills tag-hockey-drills tag-video"
                         id="post-1245">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/criss-cross-tip-drill/"
                               rel="bookmark">Criss Cross Tip Drill</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here&#8217;s a great drill to work on tipping shots and generating offensive attack to
                                the net. This drill page includes a drill diagram and also an animation: <a
                                        href="http://hockeyshare.com/drills/drill.php?id=216">View the Drill
                                    Page</a></p>
                            <p><span class="youtube"><iframe title="YouTube video player" class="youtube-player"
                                                             type="text/html" width="640" height="360"
                                                             src="//www.youtube.com/embed/kBHbLCu2HfI?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                             frameborder="0" allowfullscreen></iframe></span></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1120 post type-post status-publish format-standard hentry category-hockey-drills tag-2-on-1 tag-drills tag-timing tag-video"
                         id="post-1120">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/posting-2-on-1-timing-drill/"
                               rel="bookmark">Posting 2 on 1 Timing Drill</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">10</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/2-on-1/"
                                                    rel="tag">2 on 1</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/drills/"
                                                    rel="tag">drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/timing/"
                                                    rel="tag">timing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This intermediate drill focuses on quick passing, defensive regroup puck movement and
                                timing. This drill requires relatively advanced passing abilities and good timing
                                abilities to execute effectively. I would also recommend having at least 9 forwards and
                                6 defensemen to run this drill.</p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/drill.php?id=22943">View Drill Page to
                                    Use in Practice Plans</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1062 post type-post status-publish format-standard hentry category-hockey-drills tag-drills tag-passing tag-video"
                         id="post-1062">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/4-corner-passing-stretch-pass/"
                               rel="bookmark">4 Corner Passing &#8211; Stretch Pass</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drills/"
                                                    rel="tag">drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/passing/" rel="tag">passing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Lately I&#8217;ve been receiving a lot of requests for some more advanced drills. This
                                drill is a variation on one of my favorite drills (<a
                                        href="http://hockeyshare.com/drills/drill.php?id=2">4 Corner Passing</a>).
                                The variation focuses primarily on stretch passes and timing. I would recommend this
                                drill for Bantams or older, as some of the younger teams will not be able to move the
                                puck quick enough or execute the cross-body pass effectively to keep the timing of the
                                drill. This video walks through the drill and explains some key portions. The entire
                                drill was drawn using the <a href="http://hockeyshare.com/drills/learn_more.php">HockeyShare
                                    Drill Diagrammer</a>.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/M1iV0f11FoY?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/drill.php?id=14899">View the drill
                                    page</a> | <a href="http://hockeyshare.com/drills/learn_more.php">Draw Drills
                                    &amp; Create Practice Plans</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1028 post type-post status-publish format-standard hentry category-hockey-drills tag-drills tag-flow tag-passing tag-timing tag-video"
                         id="post-1028">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/nj-flow-drill-3-man-drill-video/"
                               rel="bookmark">NJ Flow Drill &#8211; 3 Man Variation Drill Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">5</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drills/"
                                                    rel="tag">drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/flow/" rel="tag">flow</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/passing/" rel="tag">passing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/timing/"
                                                    rel="tag">timing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This video walks throught a variation of the NJ Flow Drill focusing on timing, one-touch,
                                and stretch passes. This drill is recommended for Peewee and up, and can be modified to
                                run from both ends simultaneously for more advanced groups.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/_nWx9E3Davs?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark&amp;feature=channel_video_title"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/learn_more.php"><span
                                            style="color: #ff0000;"><strong>This drill was created using the HockeyShare Drill Diagramming platform.</strong></span></a>
                            </p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/drill.php?id=6104">View the Drill Page
                                    for the NJ Flow (3-Man Variation)</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-991 post type-post status-publish format-standard hentry category-hockey-drills tag-coach-nielsen tag-link"
                         id="post-991">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/cycling-the-puck-by-coach-nielsen/"
                               rel="bookmark">Cycling the Puck by Coach Nielsen</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/coach-nielsen/"
                                                    rel="tag">coach nielsen</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/link/" rel="tag">link</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Friend and fellow blogger Coach Nielsen has posted a great progression of cycling drills
                                over on his blog. This post is definitely worth a read if you&#8217;re trying to teach
                                cycling to your group:</p>
                            <p>
                                <a href="http://coachnielsen.wordpress.com/2011/07/24/cycling-the-puck-for-offensive-success/">http://coachnielsen.wordpress.com/2011/07/24/cycling-the-puck-for-offensive-success/</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-836 post type-post status-publish format-standard hentry category-hockey-drills category-practice-plans tag-grassroots tag-youth-hockey-coaching"
                         id="post-836">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/3-resources-for-grassroots-hockey/"
                               rel="bookmark">3 Resources for Grassroots Hockey</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/grassroots/" rel="tag">grassroots</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are three great resources for those involved in grassroots hockey:</p>
                            <ol>
                                <li>
                                    <a href="http://www.usahockey.com//Template_Usahockey.aspx?NAV=CO_07_05&amp;ID=275670"
                                       target="_blank">USA Hockey: 6U, 8U, &amp; 10U Practice Plans and Program
                                        Information</a></li>
                                <li><a href="http://hockeycanada.com/index.php/ci_id/63693/la_id/1.htm" target="_blank">Hockey
                                        Canada: 6U, 8U, 10U, 12U, 14U, Midget, &amp; Goalie Practice Plans, Drills, and
                                        Program Information</a></li>
                                <li><a href="http://www.mediafire.com/?nbzw1njmzgm" target="_blank">M2 Hockey&#8217;s
                                        Beginner Station Manual</a></li>
                            </ol>
                            <p>Enjoy the resources!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-832 post type-post status-publish format-standard hentry category-hockey-drills category-hockey-instructional-video tag-dryland tag-hockey-instructional-video tag-off-ice-training"
                         id="post-832">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/"
                               rel="bookmark">Agility Ladder Drills for On-Ice Quickness</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">14</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We put together a new video featuring three of our favorite off-ice agility ladder drills
                                to help players develop their quickness and foot speed.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/#more-832"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-829 post type-post status-publish format-standard hentry category-hockey-drills category-hockey-instructional-video tag-hockey-instructional-video tag-power-skating tag-video"
                         id="post-829">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/shoot-the-duck-video/"
                               rel="bookmark">Shoot the Duck &#8211; Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/"
                                                    rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Shoot the Duck is a hockey power skating drill that works on balance, edgework, and leg
                                strength. In this video, we show you forward and backward progressions for teaching.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/shoot-the-duck-video/#more-829"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/category/hockey-drills/page/2/">Older Posts &raquo;</a>
                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

