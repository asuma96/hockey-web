@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog img_margin">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">

                    <div class="post-1040 post type-post status-publish format-standard hentry category-hockey-systems tag-neutral-zone tag-regroup tag-systems"
                         id="post-1040">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-systems/basic-neutral-zone-regroup-options/"
                               rel="bookmark">Basic Neutral Zone Regroup Options</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">22</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/neutral-zone/" rel="tag">neutral zone</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/regroup/" rel="tag">regroup</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/systems/" rel="tag">systems</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The ability for a team to properly execute a neutral zone regroup can mean the difference
                                between generating a scoring opportunity and giving up a scoring opportunity. In this
                                video, we cover four basic neutral zone regroup tactics to help your team transition
                                from the neutral zone to the offensive zone.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/J1jymsyBzQE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/drill.php?id=7923">View the Drill Page
                                    for these Neutral Zone Regroup Options</a></p>
                            <p style="text-align: left;"><strong>Option #1</strong>: Post Up &#8211; Wings post up just
                                inside the blue line along the wall for quick outlet options. Center curls strong-side
                                for a secondary pass. This is a good option for less experienced teams, or teams with
                                defensemen who don&#8217;t have strong puck-movement skills and ice vision.</p>
                            <p style="text-align: left;"><strong>Option #2</strong>: Double Curl &#8211; The strong-side
                                wing curls to the middle of the ice while the center curls toward the strong-side wall.
                                The weak-side winger can post up for a tertiary outlet option. This option creates more
                                offensive movement through the neutral zone, so defensemen need to have solid passing
                                abilities, as they&#8217;re attempting to hit a cutting player instead of one at a
                                stand-still.</p>
                            <p style="text-align: left;"><strong>Option #3</strong>: Weak-Side Stretch &#8211; The
                                strong-side wing posts-up, center curls strong-side. The weak-side wing starts up ice,
                                then cuts back across the far blue line looking for a stretch pass outlet. This option
                                requires defensemen with strong ice vision and passing abilities.</p>
                            <p style="text-align: left;"><strong>Option #4</strong>: Strong-Side Stretch &#8211; The
                                weak-side wing posts-up, center curls strong-side. The strong-side wing starts up ice,
                                then cuts back across the far blue line looking for a stretch pass outlet. This option
                                requires defensemen with strong ice vision and passing abilities.</p>

                            <br/>

                        </div>
                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-649 post type-post status-publish format-standard hentry category-hockey-systems tag-coaching tag-special-teams tag-systems"
                         id="post-649">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-systems/penalty-kill-handout-user-submission/"
                               rel="bookmark">Penalty Kill Handout (User Submission)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">6</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/coaching/" rel="tag">coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/special-teams/"
                                                    rel="tag">special teams</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/systems/" rel="tag">systems</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Harry from Kingston was kind enough to share a recent penalty killing handout he
                                distributed to his Peewee team. Click the link below to download the PDF version of his
                                handout:</p>
                            <p><a href="http://www.mediafire.com/?mzwdnxj2tdz">Download Harry from Kingston&#8217;s PK
                                    Handout (PDF)</a></p>
                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-634 post type-post status-publish format-standard hentry category-hockey-systems tag-hockey-coaching tag-hockey-systesm tag-youth-hockey-coaching"
                         id="post-634">

                        <h2><a href="http://hockeyshare.com/blog/hockey-systems/simple-penalty-kill-forecheck/"
                               rel="bookmark">Simple Penalty Kill Forecheck</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">29</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-systesm/"
                                                    rel="tag">hockey systesm</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Killing off a penalty can be one of the most critical turning points of a game. Your team
                                finally ices the puck, and you get a fresh set of legs on the ice to go pressure the
                                opposing team while they&#8217;re setting up their breakout&#8230;.now what? If you&#8217;re
                                dealing with older players, it is important your players know their responsibilities and
                                the lanes they&#8217;re defending.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-systems/simple-penalty-kill-forecheck/#more-634"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-477 post type-post status-publish format-standard hentry category-hockey-systems tag-forecheck tag-hockey-systems tag-offensive-zone-pressure"
                         id="post-477">

                        <h2><a href="http://hockeyshare.com/blog/hockey-systems/2-1-2-forecheck/" rel="bookmark">2-1-2
                                Forecheck</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/forecheck/" rel="tag">forecheck</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-systems/"
                                                    rel="tag">Hockey Systems</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/offensive-zone-pressure/"
                                                    rel="tag">offensive zone pressure</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Forechecking efficiently is a must for any successful team today. Without an organized
                                structure for attacking and regaining control of the puck, the team is vulnerable to
                                quick breakouts, and will assuredly not be in proper position to play defense when the
                                other team begins heading up ice.</p>
                            <p>Before getting into the specifics of the 2-1-2 forecheck system, it is important to
                                understand what type of play a forecheck is exactly. Many coaches believe the act of
                                forechecking is an offensive tactic. I don&#8217;t believe this is an accurate
                                description. While the forechecking described here outlines the pressure in the
                                offensive zone, the key lies in the possession of the puck. I believe there are only
                                three distinct scenarios regarding possession: 1) offense, 2) defense, 3) transition. In
                                order for a player to be on &#8220;offense&#8221;, his/her team MUST have control of the
                                puck. If the team does not have control of the puck, they are either in the transition
                                phase or defense stage. The transition stage occurs during turn-overs while players
                                shift from defense mode to offense mode (or vice-versa). A good rule of thumb is: if you
                                don&#8217;t have possession of the puck, you&#8217;re playing defense.</p>
                            <p>An offensive zone forechecking system is a defensive tactic to regain control of the
                                puck. A well executed forechecking system will allow players to make quick transitions
                                from defense to offense to create scoring opportunities. Most often an offensive zone
                                forecheck will occur when the puck is dumped in. With that understanding of how to
                                approach the pressure, let&#8217;s take a look at the 2-1-2.</p>
                            <p>The 2-1-2 is an aggressive-style forechecking system designed to place pressure on the
                                puck carrier. Its name comes from the pressure-style being used where the first two
                                players aggressively pursue the puck, one player stays slightly higher for middle
                                support, and the defense hold their regular positions on the blue line. Here is a
                                diagram of a basic 2-1-2 setup:</p>
                            <p style="text-align: center;"><a
                                        href="http://www.mediafire.com/imageview.php?quickkey=oiymwnwoziw&amp;thumb=5"
                                        target="_blank"><img
                                            src="/img/imagess.png"
                                            alt="Unlimited Free Image and File Hosting at MediaFire" border="0"/></a>
                            </p>
                            <p style="text-align: left;">In this basic scenario, players 1 &amp; 2 (in blue) are the
                                first two to the puck, player 3 remains in the middle high-slot, and the defense
                                (players 4 &amp; 5) play their typical point positions. Below is an outline of the each
                                forechecking player&#8217;s responsibility:</p>
                            <p style="text-align: left;"><strong>Player 1</strong>: The first player on the puck carrier
                                should take the body (age permitting) to create a separation of the puck and the
                                defending player. This player must have an active stick and actively take away passing
                                lanes while pursuing the puck carrier.</p>
                            <p style="text-align: left;"><strong>Player 2</strong>: The second player should ensure
                                there is no D-to-D pass option for the puck carrier. Once player 1 has successfully
                                taken the body and separated the defending player from the puck, player 2 should look to
                                pick up the loose puck.</p>
                            <p style="text-align: left;"><em>Note</em>: Player 1 can also share responsibility in
                                eliminating the D-to-D outlet pass option depending on where he/she is coming from on
                                the ice. With a 2-1-2 forecheck, the idea is to force the play to one side of the ice
                                and not allow puck movement to the weak-side.</p>
                            <p style="text-align: left;"><strong>Player 3</strong>: Player 3 plays a read-and-support
                                role, staying in the middle of the ice in the high-slot. If defensive player 2 is able
                                to make a pass to defensive player 4, player 3 would pressure player 4 to create a
                                turn-over. If the puck is broken out, player 3 is in good position to backcheck.</p>
                            <p style="text-align: left;"><strong>Players 4 &amp; 5</strong>: The defensemen should play
                                the point play as normal, reading the play as the defending player controls the puck and
                                attacking players are attempting to force the play to one side of the ice.</p>
                            <p style="text-align: left;">The 2-1-2 system is great to run when the opposing team has
                                defensemen who are confident in skating the puck up the ice. Applying this type of
                                pressure will take away their time and space to handle the puck. It also works well when
                                opposing teams have slow defenders. Slow defensemen can be exploited by applying
                                aggressive pressure. It is critical to have the first two players keep their feet moving
                                through the entire process. When forecheckers hesitate using a 2-1-2, a good defense
                                pair on the opposing team will use the extra second or two to find an outlet pass
                                &#8211; usually D-to-D. Skate hard all the way through the forecheck.</p>
                            <p style="text-align: left;">This article outlines one way to apply pressure, there are
                                countless minor differences between implementations. Below are some animations to show
                                examples of ways to apply this forecheck and rotations if the other team completes a
                                breakout pass.</p>
                            <div id="attachment_1217" style="width: 299px" class="wp-caption aligncenter"><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2.gif"><img
                                            class="size-full wp-image-1217" title="Basic 2-1-2 Forecheck w/ Rotation"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2.gif"
                                            alt="Basic 2-1-2 Forecheck w/ Rotation" width="289" height="361"
                                            srcset="/img/2_1_2.gif 289w, /img/2_1_2-240x300.gif 240w"
                                            sizes="(max-width: 289px) 100vw, 289px"/></a>
                                <p class="wp-caption-text">Basic 2-1-2 Forecheck w/ Rotation</p></div>
                            <div id="attachment_1218" style="width: 299px" class="wp-caption aligncenter"><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2_-_opt_1.gif"><img
                                            class="size-full wp-image-1218"
                                            title="2-1-2 Forecheck - Puck Behind the Net "
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2_-_opt_1.gif"
                                            alt="2-1-2 Forecheck - Puck Behind the Net" width="289" height="361"
                                            srcset="/img/2_1_2_-_opt_1.gif 289w, /img/2_1_2_-_opt_1-240x300.gif 240w"
                                            sizes="(max-width: 289px) 100vw, 289px"/></a>
                                <p class="wp-caption-text">2-1-2 Forecheck &#8211; Puck Behind the Net</p></div>
                            <div id="attachment_1219" style="width: 299px" class="wp-caption aligncenter"><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2_-_opt_2.gif"><img
                                            class="size-full wp-image-1219"
                                            title="2-1-2 Cross-Corner Dump-In w/ Wrap Around"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2010/03/2_1_2_-_opt_2.gif"
                                            alt="2-1-2 Cross-Corner Dump-In w/ Wrap Around" width="289" height="361"
                                            srcset="/img/2_1_2_-_opt_2.gif 289w, /img/2_1_2_-_opt_2-240x300.gif 240w"
                                            sizes="(max-width: 289px) 100vw, 289px"/></a>
                                <p class="wp-caption-text">2-1-2 Cross-Corner Dump-In w/ Wrap Around</p></div>

                            <br/>
                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-334 post type-post status-publish format-standard hentry category-hockey-systems tag-defensive-zone-coverage tag-hockey-systems"
                         id="post-334">

                        <h2><a href="http://hockeyshare.com/blog/hockey-systems/box-plus-one-d-zone-coverage/"
                               rel="bookmark">Box Plus One &#8211; D-Zone Coverage</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Feb</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">11</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/defensive-zone-coverage/"
                                                    rel="tag">defensive zone coverage</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-systems/"
                                                    rel="tag">Hockey Systems</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>There have been a lot of questions popping up on the <a
                                        href="http://hockeyshare.com/forums/forumdisplay.php?f=4">Message Board</a>
                                regarding defensive zone coverage. Coaches have several different options when deciding
                                on a defensive zone coverage strategy/system. The &#8220;Box Plus One&#8221; system has
                                become increasingly popular because of its ability to force the play to the outside of
                                the ice. The primary purpose of any d-zone coverage is to minimize the number of
                                opportunities from the &#8220;home base&#8221; area of the rink (see diagram below) and
                                force the play into the &#8220;soft&#8221; areas of the ice. Soft areas are parts of the
                                ice where far fewer goals are scored.</p>
                            <div id="attachment_335" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/box-plus-one-home-base.jpg"><img
                                            class="size-full wp-image-335"
                                            title="Box Plus One - Hockey Defensive Zone Coverage"
                                            src="/img/box-plus-one-home-base.jpg"
                                            alt="Box Plus One - Hockey Defensive Zone Coverage" width="298"
                                            height="279"/></a>
                                <p class="wp-caption-text">Red is &quot;Home Base&quot;, Blue is &quot;Soft
                                    Area&quot;</p></div>
                            <p>In order to effectively run the Box Plus One, coaches must have a good grasp of the area
                                each player is responsible to cover. Starting with the puck in the corner, the diagram
                                below shows basic areas of responsibility.</p>
                            <p>
                                <a href="http://hockeyshare.com/img/box-plus-one-responsibilities.jpg"><img
                                            class="alignnone size-full wp-image-336"
                                            title="Box Plus One Responsibilities"
                                            src="/img/box-plus-one-responsibilities.jpg"
                                            alt="Box Plus One Responsibilities" width="298" height="279"/></a></p>
                            <p>The LW is responsible for the general areas shown in light-blue. RW is responsible for
                                areas shown in tan. RD is responsible for areas shown in light-green. LD is responsible
                                for the areas shown in orange. Both LD and RD are jointly responsible for the area
                                directly in front and behind the net. The C is jointly responsible (with LD and RD) to
                                cover the entire area of both defensemen&#8217;s responsibility.</p>
                            <p>Along with the basic coverage areas shown in the diagram are arrows indicating which way
                                the player&#8217;s feet should be facing during the play. Too often coaches leave this
                                important concept out of their defensive zone instruction. The direction a player has
                                his/her feet facing can be the difference between making a play and giving up a goal. As
                                a rule-of-thumb, players in the defensive zone should not have their feet facing the
                                net. The should be facing up ice, or no more than parallel to the goal line. This simple
                                concept allows players to see and read the ice much easier, giving them a better chance
                                of finding their responsibilities.</p>
                            <p>The diagrams below outline each player&#8217;s individual line of sight in the above
                                scenario. Knowing where to look is crucial in properly executing the coverage.</p>
                            <div id="attachment_337" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/left-wing.jpg"><img
                                            class="size-full wp-image-337" title="Left Wing Line of Sight"
                                            src="/img/left-wing.jpg"
                                            alt="Left Wing Line of Sight" width="298" height="279"/></a>
                                <p class="wp-caption-text">Left Wing Line of Sight</p></div>
                            <div id="attachment_338" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/right-wing.jpg"><img
                                            class="size-full wp-image-338" title="Right Wing Line of Sight"
                                            src="/img/right-wing.jpg"
                                            alt="Right Wing Line of Sight" width="298" height="279"/></a>
                                <p class="wp-caption-text">Right Wing Line of Sight</p></div>
                            <div id="attachment_339" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/right-d.jpg"><img
                                            class="size-full wp-image-339" title="Right Defense Line of Sight"
                                            src="/img/right-d.jpg"
                                            alt="Right Defense Line of Sight" width="298" height="279"/></a>
                                <p class="wp-caption-text">Right Defense Line of Sight</p></div>
                            <div id="attachment_340" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/center.jpg"><img
                                            class="size-full wp-image-340" title="Center Line of Sight"
                                            src="/img/center.jpg" alt="Center Line of Sight"
                                            width="298" height="279"/></a>
                                <p class="wp-caption-text">Center Line of Sight</p></div>
                            <div id="attachment_341" style="width: 308px" class="wp-caption alignnone"><a
                                        href="http://hockeyshare.com/img/left-d.jpg"><img
                                            class="size-full wp-image-341" title="Left D Line of Sight"
                                            src="/img/left-d.jpg" alt="Left D Line of Sight"
                                            width="298" height="279"/></a>
                                <p class="wp-caption-text">Left D Line of Sight</p></div>
                            <p><em>Note: The darker-orange areas in the Left D&#8217;s line of sight diagram represent
                                    areas LD must constantly be checking.</em></p>
                            <p>In order to more fully understand the coverage, a coach must understand the rotations and
                                shifts in responsibility when the puck changes location. There are three main rotational
                                scenarios.</p>
                            <p><strong>Scenario #1 &#8211; Puck is passed to the point</strong></p>
                            <p>
                                <a href="http://hockeyshare.com/img/box-plus-one-point.jpg"><img
                                            class="alignnone size-full wp-image-342" title="Box Plus One - Point Pass"
                                            src="/img/box-plus-one-point.jpg"
                                            alt="Box Plus One - Point Pass" width="298" height="279"/></a></p>
                            <p>In this scenario, each player&#8217;s responsibility is highlighted.</p>
                            <p>RW should attack LD from the inside-out. This prevents LD from getting a better angle
                                shot and also leaves fewer points of escape by utilizing the blue line and boards. RW
                                should finish his/her check when pressuring the point.</p>
                            <p>LW is responsible for the RD, but should remain in the high-slot to help out with loose
                                pucks or anyone who may be open. If RW pressures LD properly, the pass from LD to RD
                                should not be an option.</p>
                            <p>RD and C are responsible for staying on the defensive side of their respective player.
                                Defensive side simply means having your body between the player you&#8217;re defending
                                and the net.</p>
                            <p>LD is responsible for tying up his/her man in front if a shot comes, as well as
                                preventing the player from getting body position on any rebounds. LD should stay on the
                                defensive side of the opponent at all times.</p>
                            <p><strong>Scenario #2 &#8211; Puck is passed behind the net</strong></p>
                            <p>
                                <a href="http://hockeyshare.com/img/box-plus-one-behind-net.jpg"><img
                                            class="alignnone size-full wp-image-343"
                                            title="Box Plus One - Behind the Net Rotation"
                                            src="/img/box-plus-one-behind-net.jpg"
                                            alt="Box Plus One - Behind the Net Rotation" width="298" height="279"/></a>
                            </p>
                            <p>Individual player responsibilities are again highlighted along with basic rotation
                                directions.</p>
                            <p>C should pressure the new puck carrier. Ideally, C will be able to take the body and pin
                                the puck carrier against the wall creating a turn over. C should try to force the puck
                                carrier out to one side of the net and not allow cut-backs. Cut-backs often create
                                coverage confusion and lead to scoring opportunities.</p>
                            <p>RD remains responsible for the previous puck carrier. It is crucial RD wins the race back
                                to the front of the net and remains on d-side of his/her player.</p>
                            <p>LD remains responsible for the opposing player in front of the net. LD can not allow
                                his/her feet to face the end-boards, as this will allow the opposing forward to get open
                                in front of the net without LD knowing exactly where the opponent is skating. LD should
                                keep his/her feet facing up ice until the puck carrier reaches the far post, at which
                                point, LD can open facing the sideboards allowing clear vision of both the puck carrier
                                and responsibility in front of the net.</p>
                            <p>LW remains in the high slot for additional support and remains responsible for the
                                opposing RD.</p>
                            <p>RW shifts to the high slot for additional support. RW remains responsible for the
                                opposing LD and should not turn his/her back to the coverage.</p>
                            <p><strong>Alternate Rotation</strong></p>
                            <p>
                                <a href="http://hockeyshare.com/img/box-plus-one-behind-net-alt.jpg"><img
                                            class="alignnone size-full wp-image-344"
                                            title="Box Plus One - Behind the Net Alternate Rotation"
                                            src="/img/box-plus-one-behind-net-alt.jpg"
                                            alt="Box Plus One - Behind the Net Alternate Rotation" width="298"
                                            height="279"/></a></p>
                            <p>An alternate to the scenario #2 rotation is to allow the LD to pressure the new puck
                                carrier behind the net and have C fill LD&#8217;s previous responsibility in front of
                                the net. This rotation can be useful when the puck carrier has a step or two on the C.
                                In order for this rotation to work properly, C and LD need to communicate to ensure only
                                one player is pressuring the puck. LD should not leave until C has picked up the man in
                                front of the net. If LD leaves too early, the opposing forward will be left open in
                                front of the net until C is able to pick him/her up.</p>
                            <p><strong>Scenario #3 &#8211; Puck is moved to the far corner</strong></p>
                            <p>
                                <a href="http://hockeyshare.com/img/box-plus-one-corner-rotation.jpg"><img
                                            class="alignnone size-full wp-image-345"
                                            title="Box Plus One - Corner Rotation"
                                            src="/img/box-plus-one-corner-rotation.jpg"
                                            alt="Box Plus One - Corner Rotation" width="298" height="279"/></a></p>
                            <p>In scenarios where the puck shifts from one corner to the other, players must quickly and
                                efficiently rotate to avoid any gaps in coverage.</p>
                            <p>LD becomes strong-side defenseman and should pressure the puck carrier assuming he/she is
                                closer to the puck than C (as shown in this diagram).</p>
                            <p>C should rotate to the other side of the net for support &#8211; traveling through the
                                front of the net to block passing lanes and cover anyone who may be in the high slot
                                during the rotation.</p>
                            <p>RD returns to the front of the net. RD must win the race back to the front.</p>
                            <p>RW shifts into the high-slot for weak-side support.</p>
                            <p>LW rotates just above the dot on the far circle, staying in between RD and the net.</p>
                            <p><strong>Final Tips: </strong></p>
                            <p>1) Always keep your stick on the ice and in the most dangerous passing lane. This simple
                                act can prevent countless scoring opportunities. The most dangerous passing lane is most
                                often the middle of the ice.</p>
                            <p>2) Proper communication is key. In a game, there will be times of confusion. Proper
                                communication amongst the players will allow responsibility shifts without creating gaps
                                in coverage. Coverage gaps equal scoring opportunities!</p>
                            <p>3) Be aggressive on the puck carrier. The more time you give an opponent time to set up
                                and make plays, the more likely it becomes someone will miss a coverage.</p>
                            <p>4) Centers should be treated like defensemen and be allowed to pursue the puck carrier in
                                the corner if he/she can get there before a defenseman.</p>
                            <p>5) The first person pressuring the puck should look to take the body and separate the man
                                from the puck. When separation has occurred, the support player (Center in the diagrams
                                above) should quickly move in to gain possession of the puck.</p>
                            <p>6) Have your head on a swivel. In the defensive zone, players must consistently look
                                around to identify gaps in coverage (open players). Players cannot get caught watching
                                the puck and losing track of their responsibility.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

