@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1019 post type-post status-publish format-standard hentry category-comments-thoughts category-hockey-tips tag-early-season tag-team-activities tag-team-building tag-team-chemistry"
                         id="post-1019">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/10-early-season-team-building-ideas/"
                               rel="bookmark">10 Early Season Team Building Ideas</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/early-season/"
                                                    rel="tag">early season</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-activities/" rel="tag">team activities</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-building/" rel="tag">team building</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-chemistry/" rel="tag">team chemistry</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Getting a team to gel together can be a big task if you&#8217;ve got a lot of new players
                                on your
                                team. Below is a list of ten ideas to improve your team&#8217;s chemistry early in the
                                season.</p>
                            <ol>
                                <li><strong>Early Season Tournament/Road Trip</strong> &#8211; ideally, pick a
                                    tournament where you&#8217;re
                                    out of your home town and parents/players must stay in a hotel. This lets players
                                    get to know
                                    each other away from the rink setting, and gives parents time to socialize in the
                                    evenings. If
                                    you&#8217;re in a hotel and have time between games, try planning a team &#8220;pot-luck&#8221;
                                    lunch/dinner where players are required to attend as opposed to everyone heading
                                    their own
                                    direction for meals.
                                </li>
                                <li><strong>Ropes Courses</strong> &#8211; a ropes course will force players to work
                                    together as a
                                    team to achieve a common goal &#8211; just like in the season. It will also force
                                    some players
                                    to address their fears (especially if you&#8217;re doing a high ropes course) and
                                    get support
                                    from their teammates.
                                </li>
                                <li><strong>Team Building Activities</strong> &#8211; choose a day and location away
                                    from the rink
                                    and plan group challenges (mental as well as physical). Activities that force
                                    players to
                                    communicate and interact are excellent in establishing trust among teammates. For
                                    some ideas on
                                    activities, <a
                                            href="http://hockeyshare.com/blog/uncategorized/team-building-resources/">check
                                        out our blog post on Team Building Resources</a>.
                                </li>
                                <li><strong>Team Cook Out</strong> &#8211; this can be done at the rink, or if a parent
                                    is kind
                                    enough to open their home, at a family&#8217;s house. Ideally there would be an
                                    activity the
                                    players can do (pool, ping pong, swimming, etc.) which will focus them to one area.
                                    Avoid
                                    allowing video games to be the central focus, as the amount of communication and
                                    group
                                    interaction is severely lessened.
                                </li>
                                <li><strong>Change Locker Room Seats</strong> &#8211; players love to get into a routine
                                    and sit
                                    next to their buddies in the locker room. This can be okay as the season progresses,
                                    but if you&#8217;ve
                                    got a team with a lot of new skaters, forcing players to sit in different locations
                                    will cause
                                    them to talk with and get to know people outside their small clique.
                                </li>
                                <li><strong>Paint Balling</strong> &#8211; not every team will have access to this, but
                                    teams that
                                    do will find that their players will enjoy the competition and have a great time
                                    being together
                                    away from the rink. You could also plan for a team cookout after the paint balling
                                    event!
                                </li>
                                <li><strong>Team Workout</strong> &#8211; you see this in the NHL quite a bit &#8211;
                                    players and
                                    coaches will do team runs, bike rides, canoeing, etc. Although it may not be quite
                                    as much fun
                                    as some of the other activities listed above, you&#8217;ll be getting the group
                                    together and
                                    also helping their overall conditioning.
                                </li>
                                <li><strong>Mix Lines / D Partners</strong> &#8211; early in the season, forcing players
                                    to play
                                    with skaters other than the one or two players they&#8217;re used to will not only
                                    get players
                                    to work together and communicate, but will prepare older players for future tryout
                                    camps where
                                    they&#8217;ll be playing with skaters they&#8217;ve never played with before.
                                </li>
                                <li><strong>Team Video</strong> &#8211; have some fun with this one &#8211; especially
                                    early in the
                                    season. Instead of doing game tape review or something expected, have some fun and
                                    watch an
                                    entertaining video or movie. Maybe even get some pizzas for the players (without
                                    telling them).
                                    For older groups, <a
                                            href="http://totalhockey.com/Product.aspx?itm_id=1946&amp;div_id=2">The
                                        Tournament</a> is a great choice. For younger groups, Miracle may be a better
                                    idea.
                                </li>
                                <li><strong>Personal Information</strong> &#8211; before or after a practice, hold a
                                    team gathering
                                    in the locker room and have players get up and introduce themselves one-by-one. It
                                    is also
                                    helpful to have 3 or 4 questions they need to answer while it is their turn. Simple
                                    questions
                                    like the following tend to work well: favorite hockey team, one thing we didn&#8217;t
                                    know about
                                    you, home town, etc.
                                </li>
                            </ol>
                            <div>Do you have another idea to add to the list? Leave a comment below to contribute! Good
                                luck this
                                season!
                            </div>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-800 post type-post status-publish format-standard hentry category-hockey-tips tag-power-skating tag-technique"
                         id="post-800">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/heel-to-heel-broken-down/" rel="bookmark">Heel
                                to
                                Heel &#8211; Broken Down</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">22</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/" rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/technique/"
                                                    rel="tag">technique</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Last week we got lots of great comments on the <a
                                        href="http://hockeyshare.com/blog/m2-hockey/heel-to-heel-transitions-video/">Heel
                                    to
                                    Heel transition video</a>. Several people wrote in asking if we could do a slow
                                motion / still
                                frame breakdown of the technique. In this post, I&#8217;ve taken several still shots and
                                included a
                                slow motion video of a heel to heel transition &#8211; as well as included key points
                                for various
                                stages of the transition.</p>
                            <p><a href="http://hockeyshare.com/blog/hockey-tips/heel-to-heel-broken-down/#more-800"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-785 post type-post status-publish format-standard hentry category-hockey-tips tag-offensive-attack tag-youth-hockey-coaching tag-zone-entry"
                         id="post-785">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/zone-entry-board-passes/" rel="bookmark">Zone
                                Entry &#8211; Board Passes</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">12</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/offensive-attack/" rel="tag">offensive attack</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/zone-entry/" rel="tag">zone entry</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Getting the puck into the zone can be the difference between creating a scoring
                                opportunity and
                                giving one up. Using a simple board pass play can be one of the most efficient means of
                                entering the
                                offensive zone. In this post, we&#8217;ll take a look at two different examples of a
                                board pass as
                                well as a video example from an NHL game.</p>
                            <p><a href="http://hockeyshare.com/blog/hockey-tips/zone-entry-board-passes/#more-785"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-734 post type-post status-publish format-standard hentry category-hockey-tips category-hockey-instructional-video tag-hockey-instructional-video tag-power-skating tag-video tag-youth-hockey-coaching"
                         id="post-734">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/forward-stride-in-depth-video/"
                               rel="bookmark">Forward Stride &#8211; In Depth [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Oct</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">15</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/" rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We&#8217;re back with a video this week breaking down another essential skating aspect of
                                the game.
                                This week&#8217;s video focuses on the forward stride. Becoming an efficient skater with
                                proper
                                technique is essential to being the fastest skater you can become.</p>
                            <p style="text-align: center;"><a
                                        href="http://hockeyshare.com/blog/hockey-instructional-video/forward-stride-in-depth-video/#more-734"
                                        class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-717 post type-post status-publish format-standard hentry category-hockey-tips category-hockey-instructional-video tag-hockey-coaching tag-hockey-crossovers tag-power-skating tag-youth-hockey-coaching"
                         id="post-717">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/crossovers-in-depth-video/"
                               rel="bookmark">Crossovers &#8211; In Depth [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/" rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-crossovers/" rel="tag">hockey crossovers</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/" rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The hockey crossover is an essential part of any hockey player&#8217;s skating arsenal.
                                In this
                                video, we break down the crossover into easy teaching points and give ideas on how to
                                coach your
                                players into using better technique.</p>
                            <p style="text-align: left;"><a
                                        href="http://hockeyshare.com/blog/hockey-instructional-video/crossovers-in-depth-video/#more-717"
                                        class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-713 post type-post status-publish format-standard hentry category-hockey-tips tag-divide-ice tag-split-ice tag-youth-hockey-coaching"
                         id="post-713">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/creative-ways-to-divid-the-ice/"
                               rel="bookmark">Creative
                                Ways to Divide the Ice</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">28</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/divide-ice/" rel="tag">divide ice</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/split-ice/" rel="tag">split ice</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>There&#8217;s no doubt, one of the easiest ways to keep our game affordable is to
                                increase the number
                                of skaters on the ice at any given time. The major governing bodies are pushing to get
                                more kids on
                                the ice simultaneously to make the most out of costly ice time. Below I&#8217;ve
                                included some
                                different ways of splitting up the ice I have found to be effective over the years. In
                                my opinion,
                                the key to deciding how to split your ice depends on the goals of your practice session
                                and how many
                                skaters you have on the ice.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-tips/creative-ways-to-divid-the-ice/#more-713"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-709 post type-post status-publish format-standard hentry category-hockey-tips tag-creativity tag-lateral-movement tag-ludek-bukac tag-minnesota-hockey"
                         id="post-709">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/lateral-movement-creativity/"
                               rel="bookmark">Lateral
                                Movement &#038; Creativity</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/creativity/" rel="tag">creativity</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/lateral-movement/" rel="tag">lateral movement</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/ludek-bukac/"
                                                    rel="tag">ludek bukac</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/minnesota-hockey/" rel="tag">minnesota hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Minnesota Hockey posted a video with Ludek Bukac about creativity and lateral movement.
                                Many of you
                                may recognize the name from the <a
                                        href="http://www.amazon.com/gp/product/B001G3S8SQ?ie=UTF8&amp;tag=kevimull-20&amp;linkCode=as2&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B001G3S8SQ"
                                        target="_blank">Variable Goal Training DVD</a> Bukac has put out. The concept is
                                to use
                                extra nets on the ice to force skaters to move laterally and be creative in small areas.
                                If you aren&#8217;t
                                familiar with this type of training, the video below is definitely worth a look.</p>
                            <div>
                                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="351" height="263"
                                        codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
                                    <param name="allowfullscreen" value="true"/>
                                    <param name="allowscriptaccess" value="always"/>
                                    <param name="src"
                                           value="http://vimeo.com/moogaloop.swf?clip_id=14034401&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=0&amp;show_portrait=0&amp;color=ffffff&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"/>
                                    <embed type="application/x-shockwave-flash" width="351" height="263"
                                           src="http://vimeo.com/moogaloop.swf?clip_id=14034401&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=0&amp;show_portrait=0&amp;color=ffffff&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"
                                           allowscriptaccess="always" allowfullscreen="true"></embed>
                                </object>
                            </div>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-631 post type-post status-publish format-standard hentry category-hockey-tips tag-over-speed tag-skating tag-skill-development tag-training"
                         id="post-631">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/over-speed-training/" rel="bookmark">Over-Speed
                                Training</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">20</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/over-speed/" rel="tag">over-speed</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/skating/"
                                                    rel="tag">skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/skill-development/" rel="tag">skill development</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/training/"
                                                    rel="tag">training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Over-speed training is a buzz term often associated having &#8220;quick feet&#8221;
                                during a drill.
                                Simply put, over-speed training involves the execution of a skill at a higher tempo than
                                the player
                                is comfortable with. Most players go on the ice and &#8220;practice&#8221; their skills
                                at a pace
                                they&#8217;re comfortable with &#8211; this &#8220;comfort zone&#8221; is one of the
                                biggest
                                obstacles when it comes to developing as an athlete and player. By staying within the
                                player&#8217;s
                                comfort zone, there is a much slower progression of skills. Over-speed training is a
                                critical
                                component of proper training, but must be used appropriately. Properly phased over-speed
                                training
                                should look something like the following:</p>
                            <p>1) Learn the basics of the skill you&#8217;re performing: become proficient (technically)
                                in the
                                skill you&#8217;re working on. This is the part where you spend hours honing your craft.
                                Don&#8217;t
                                overlook this phase, because poor techniques at the beginning will lead to poor
                                techniques
                                long-term. Develop good habits, and practice the fundamentals.</p>
                            <p>2) Perform the skill at a pace outside your comfort zone: increase the tempo of your
                                performance
                                without a puck. Begin executing the drills at a pace where you&#8217;re outside your
                                comfort zone.
                                Falling in this phase is OK &#8211; players must understand falling is part of their
                                progression in
                                this phase. By spending a lot of time training at the increased tempo, players will
                                eventually learn
                                to control the bodies and skates to they&#8217;re able to execute the skill naturally at
                                a higher
                                pace.</p>
                            <p>3) Perform the skill with a puck at your new level: adding the puck adds in additional
                                challenges. As
                                players begin to become comfortable performing the techniques, adding the puck will
                                force them to
                                adjust other aspects of their skill to adapt to the new pace. Emphasize to players it is
                                OK to lose
                                the puck or fall in this phase.</p>
                            <p>By consistently forcing players outside their comfort zone, their speed and level of play
                                will
                                continue to climb. I would recommend making over-speed training part of every practice
                                &#8211; you
                                will be amazed at how much it will help your players improve!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-609 post type-post status-publish format-standard hentry category-hockey-tips tag-power-skating tag-tight-turns tag-user-email"
                         id="post-609">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/user-email-tight-turns/" rel="bookmark">User
                                Email: Tight Turns</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/" rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tight-turns/"
                                                    rel="tag">tight turns</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/user-email/" rel="tag">user email</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I recently received the following email:</p>
                            <p><em><span style="color: #003366;">Hi Kevin</span></em></p>
                            <p><span style="color: #000080;"><em>Love the site, a great resource for new coaches like myself! </em></span>
                            </p>
                            <p><em><span style="color: #003366;">I would like your advice on teaching tight turns to under 10&#8217;s?</span></em>
                            </p>
                            <p><span style="color: #000080;"><em>Many Thanks<br/>
Lee</em></span></p>
                            <p><a href="http://hockeyshare.com/blog/hockey-tips/user-email-tight-turns/#more-609"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-587 post type-post status-publish format-standard hentry category-hockey-tips tag-defensemen tag-gap-control"
                         id="post-587">

                        <h2><a href="http://hockeyshare.com/blog/hockey-tips/gap-control-basics/" rel="bookmark">Gap
                                Control
                                Basics</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey
                                            Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/defensemen/" rel="tag">defensemen</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/gap-control/"
                                                    rel="tag">gap control</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Gap control is one of the most important skills/concepts for defensemen to learn. Simply
                                stated, a
                                defenseman&#8217;s gap is the distance between the puck carrying forward and the
                                defender. Making a
                                play at the right time, and knowing how and when to make the commitment can be the
                                difference
                                between a defender making a great play or getting beat. The basics to setting a proper
                                gap can be
                                broken down into three main points:</p>
                            <ol>
                                <li>Required skating skills</li>
                                <li>Ability to react to the rush</li>
                                <li>Rule of thumb</li>
                            </ol>
                            <p><a href="http://hockeyshare.com/blog/hockey-tips/gap-control-basics/#more-587"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/category/hockey-tips/page/2/">Older Posts &raquo;</a>
                <td valign="top" width="250">

                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

