@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-836 post type-post status-publish format-standard hentry category-hockey-drills category-practice-plans tag-grassroots tag-youth-hockey-coaching"
                         id="post-836">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/3-resources-for-grassroots-hockey/"
                               rel="bookmark">3 Resources for Grassroots Hockey</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/grassroots/" rel="tag">grassroots</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are three great resources for those involved in grassroots hockey:</p>
                            <ol>
                                <li>
                                    <a href="http://www.usahockey.com//Template_Usahockey.aspx?NAV=CO_07_05&amp;ID=275670"
                                       target="_blank">USA Hockey: 6U, 8U, &amp; 10U Practice Plans and Program
                                        Information</a></li>
                                <li><a href="http://hockeycanada.com/index.php/ci_id/63693/la_id/1.htm" target="_blank">Hockey
                                        Canada: 6U, 8U, 10U, 12U, 14U, Midget, &amp; Goalie Practice Plans, Drills, and
                                        Program Information</a></li>
                                <li><a href="http://www.mediafire.com/?nbzw1njmzgm" target="_blank">M2 Hockey&#8217;s
                                        Beginner Station Manual</a></li>
                            </ol>
                            <p>Enjoy the resources!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-552 post type-post status-publish format-standard hentry category-practice-plans tag-body-checking tag-youth-hockey-coaching"
                         id="post-552">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/checking-clinic-practice-plan/"
                               rel="bookmark">Checking Clinic Practice Plan</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">19</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/body-checking/"
                                                    rel="tag">body checking</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Harry Guittard was kind enough to share a practice plan he used for a body checking
                                clinic recently. Much of the content is based off of Hockey Canada&#8217;s
                                recommendations, but it&#8217;s great to see how different coaches put this content
                                together and present it. Click one of the the links below to download the entire
                                practice plan.</p>
                            <p style="text-align: center;"><a href="http://www.mediafire.com/?2qhtlizvfju"
                                                              target="_blank">PDF Version</a> | <a
                                        href="http://www.mediafire.com/?n4ymyndnh1w">MS Word Version</a></p>
                            <p style="text-align: left;">Thanks for sharing Harry!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-425 post type-post status-publish format-standard hentry category-practice-plans tag-hockey-coaching tag-hockey-practice tag-hockey-practice-plan tag-youth-hockey-coaching"
                         id="post-425">

                        <h2>
                            <a href="http://hockeyshare.com/blog/practice-plans/february-march-2010-practice-plans/"
                               rel="bookmark">February / March 2010 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">12</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-practice/"
                                                    rel="tag">hockey practice</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-practice-plan/"
                                                    rel="tag">hockey practice plan</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Well, now that the season has come to a close, this is the final posting for my
                                full-season of practice plans. I hope you&#8217;ve enjoyed the entries and have found
                                some value in them. If you have questions about any of the practices, don&#8217;t
                                hesitate to drop me a line.</p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/020110-Dryland.pdf">02/01/10
                                    &#8211; Dryland Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/020110.pdf">02/01/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/020910.pdf">02/09/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/021110.pdf">02/11/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/021610-Dryland.pdf">02/16/10
                                    &#8211; Dryland Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/021610.pdf">02/16/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/021810.pdf">02/18/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022310-Dryland.pdf">02/23/10
                                    &#8211; Dryland Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022310.pdf">02/23/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022510.pdf">02/25/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022710-Dryland.pdf">02/27/10
                                    &#8211; Dryland Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022710-PM-DEFENSE.pdf">02/27/10
                                    &#8211; Puckmasters Practice Plan (Defensemen)<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022710-PM-OFFENSE.pdf">02/27/10
                                    &#8211; Puckmasters Practice Plan (Forwards &amp; Goalies)<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/022810-PM.pdf">02/28/10
                                    &#8211; Puckmasters Practice Plan (All Players)<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/030210.pdf">03/02/10
                                    &#8211; Practice Plan<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/03/034010.pdf">03/04/10
                                    &#8211; Practice Plan</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-274 post type-post status-publish format-standard hentry category-practice-plans tag-hockey-drills tag-hockey-practice-plans"
                         id="post-274">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/january-2010-practice-plans/"
                               rel="bookmark">January 2010 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">29</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-practice-plans/"
                                                    rel="tag">hockey practice plans</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are my practice plans for January 2009.</p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/010610.pdf">Practice
                                    Plan for 01/06/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/010910.pdf">Practice
                                    Plan for 01/09/10<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011110.pdf">Practice
                                    Plan for 01/11/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011210.pdf"></a><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011210-Dryland.pdf">Dryland
                                    Plan for 01/12/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011210.pdf">Practice
                                    Plan for 01/12/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011410.pdf">Practice
                                    Plan for 01/14/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011710.pdf">Practice
                                    Plan for 01/17/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011910-Dryland.pdf">Dryland
                                    Plan for 01/19/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/011910.pdf">Practice
                                    Plan for 01/19/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012110-Dryland.pdf">Dryland
                                    Plan for 01/21/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012110.pdf">Practice
                                    Plan for 01/21/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012610-Dryland.pdf">Dryland
                                    Plan for 01/26/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012610.pdf">Practice
                                    Plan for 01/26/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012710.pdf">Practice
                                    Plan for 01/27/10</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/012810.pdf">Practice
                                    Plan for 01/28/10</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-209 post type-post status-publish format-standard hentry category-practice-plans tag-hockey-drills tag-hockey-practice-plans tag-hockey-practices"
                         id="post-209">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/december-2009-practice-plans/"
                               rel="bookmark">December 2009 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-practice-plans/"
                                                    rel="tag">hockey practice plans</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-practices/"
                                                    rel="tag">hockey practices</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are my practice plans for December, 2009.</p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/120209.pdf">Practice
                                    Plan for 12/02/09</a><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/120309-Dryland.pdf"><br/>
                                    Dryland Plan for 12/03/09<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/120309.pdf">Practice
                                    Plan for 12/03/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/121009-Dryland.pdf">Dryland
                                    Plan for 12/10/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/121009.pdf">Practcie
                                    Plan for 12/10/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/121409.pdf">Practice
                                    Plan for 12/14/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/121509.pdf">Practice
                                    Plan for 12/15/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/121709.pdf">Practice
                                    Plan for 12/17/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/122209.pdf">Practice
                                    Plan for 12/22/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/122309-Dryland.pdf">Dryland
                                    Plan for 12/23/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/122309.pdf">Practice
                                    Plan for 12/23/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/122909.pdf">Practice
                                    Plan for 12/29/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2010/01/123009.pdf">Practice
                                    Plan for 12/30/09</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-22 post type-post status-publish format-standard hentry category-practice-plans"
                         id="post-22">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/practice-plans-102609-103009/"
                               rel="bookmark">October 2009 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are the PDF downloadable practice plans for all my practices in October, 2009.</p>
                            <p><a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/100109.pdf'>Practice
                                    Plan for 10/01/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/100609.pdf'>Practice
                                    Plan for 10/06/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/100609-Dryland.pdf'>Dryland
                                    Plan for 10/06/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/100709.pdf'>Practice
                                    Plan for 10/07/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/100809.pdf'>Practice
                                    Plan for 10/08/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/101309.pdf'>Practice
                                    Plan for 10/13/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/101309-dryland.pdf'>Dryland
                                    Plan for 10/13/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/101509.pdf'>Practice
                                    Plan for 10/15/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102009.pdf'>Practice
                                    Plan for 10/20/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102109.pdf'>Practice
                                    Plan for 10/21/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102209.pdf'>Practice
                                    Plan for 10/22/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102709.pdf'>Practice
                                    Plan for 10/27/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102909.pdf'>Practice
                                    Plan for 10/29/09</a><br/>
                                <a href='http://hockeyshare.com/blog/wp-content/uploads/2009/11/102909-Dryland.pdf'>Dryland
                                    Plan for 10/29/09</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-45 post type-post status-publish format-standard hentry category-practice-plans tag-hockey-coaching tag-practice-plans"
                         id="post-45">

                        <h2><a href="http://hockeyshare.com/blog/practice-plans/september-2009-practice-plans/"
                               rel="bookmark">September 2009 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Oct</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/practice-plans/"
                                                rel="category tag">Practice Plans</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/practice-plans/"
                                                    rel="tag">Practice Plans</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are all my tune-up, tryout, and practice plans for September,</p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091009.pdf"></a><a
                                        href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/090809.pdf">Tune-Up
                                    Practice Plan for 09/08/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091009.pdf">Tune-Up
                                    Practice Plan for 09/10/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091209.pdf">Tune-Up
                                    Practice Plan for 09/12/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091409.pdf">Tune-Up
                                    Practice Plan for 09/14/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091509.pdf">Tune-Up
                                    Practice Plan for 09/15/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091609.pdf">Tryout
                                    Practice Plan for 09/16/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091709.pdf">Tryout
                                    Practice Plan for 09/17/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/091909.pdf">Tryout
                                    Practice Plan for 09/19/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/092209.pdf">Practice
                                    Plan for 09/22/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/092309.pdf">Practice
                                    Plan for 09/23/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/092409-web.pdf">Practice
                                    Plan for 09/24/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/092909-web.pdf">Practice
                                    Plan for 09/29/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/11/092909-Dryland.pdf">Dryland
                                    Plan for 09/29/09</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

