@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-945 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-10000-pucks tag-contest"
                         id="post-945">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/10000-pucks-begins-today/"
                               rel="bookmark">10,000 Pucks Begins Today!</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Today (June 1st, 2011) marks the beginning of the 10,000 Pucks Contest for 2011. If you&#8217;re
                                unfamiliar with 10,000 Pucks, here&#8217;s the quick overview:</p>
                            <p><strong>Players</strong>: Set your goal for how many pucks you want to shoot over the
                                summer, log your shots online, win prizes if you&#8217;re one of the top shooters.</p>
                            <p><strong>Coaches</strong>: Set up your team, send them the link, track their progress
                                through the summer.</p>
                            <p>The contest remains free for coaches and players to enter. <span style="color: #ff0000;"><strong>To get started today, visit</strong></span>
                                <a href="http://hockeyshare.com/10000pucks">www.hockeyshare.com/10000pucks</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-941 post type-post status-publish format-standard hentry category-hockeyshare-contests"
                         id="post-941">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/10000-pucks-2011/"
                               rel="bookmark">10,000 Pucks &#8211; 2011</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The 10,000 Pucks Contest is back for the 5th season. In 2010, we had nearly 2 million
                                shots logged by more than 500 players from across the world. If you aren&#8217;t
                                familiar with the 10,000 Pucks Contest, here&#8217;s the quick overview:</p>
                            <p><span style="color: #000080;">The 10,000 Pucks Contest is designed for players to set personal goals and achieve them. Individual players can join the contest and set their own goals for number of pucks from June 1st &#8211; August 31st.  Our tracking system allows players to enter how many shots they&#8217;ve taken and track their progress.  It even tells them how many pucks they need to shoot each day to reach their goal! </span>
                            </p>
                            <p><span style="color: #000080;">Coaches can also leverage our contest platform to create accountability through the summer months. Coaches can create teams and direct players to the contest page.  When the players sign up, they can select their team, which allows coaches to view their progress.  We had man teams from the US &amp; Canada using our platform in 2010 to make their teams better. </span>
                            </p>
                            <p><strong>What&#8217;s new for the 2011 contest?</strong></p>
                            <ul>
                                <li>International Competition &#8211; Once the contest begins, users will be able to see
                                    how many shots have been logged from the United States, Canada, and Internationally
                                </li>
                                <li>Photo Upload &#8211; Upload your photos of you shooting for the contest for everyone
                                    to see
                                </li>
                                <li>Team Prize Package &#8211; The team logging the most shots in the contest will be
                                    eligible to receive a prize package
                                </li>
                                <li>Trick Shot Contest &#8211; Upload your best &#8220;trick shot&#8221; to YouTube and
                                    submit it from your profile to be entered in a chance to win another prize package
                                </li>
                            </ul>
                            <p><strong>The contest runs from June 1st, 2011 &#8211; August 31st, 2011. </strong></p>
                            <p>
                                <span style="color: #ff0000;"><strong>To get started and set your account up today, visit</strong></span>:
                                <a title="10,000 Pucks" href="http://hockeyshare.com/10000pucks/">www.hockeyshare.com/10000pucks/</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-689 post type-post status-publish format-standard hentry category-10000-pucks category-hockeyshare-contests tag-10000-pucks tag-contests tag-hockeyshare"
                         id="post-689">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/"
                               rel="bookmark">10,000 Pucks &#8211; Final Results</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">13</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contests/"
                                                    rel="tag">contests</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The results are in for the 10,000 Pucks 2010 contest. In this year&#8217;s contest, we
                                had 527 players logging shots from around the world. The total shot tally for this
                                season was an impressive: 1,922,853. Not only did we surpass our goal of seeing 1
                                million shots logged, we almost broke the 2 million mark! Now, the moment everyone&#8217;s
                                been waiting for&#8230;.the player results.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/#more-689"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-616 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-10000-pucks tag-hockeyshare"
                         id="post-616">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/10000-pucks-2010/"
                               rel="bookmark">10,000 Pucks &#8211; 2010</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>HockeyShare is excited to announce the launch of the 2010 &#8211; 10,000 Pucks, 1 Summer
                                Contest. This will be the 4th year of the contest, and we&#8217;re excited to see it
                                continue to grow. Last year we had over 500 participants from across the US &amp; Canada
                                shooting pucks.</p>
                            <p><a href="http://srv.ezinedirector.net/?n=3774676&amp;s=93138682" target="_blank">Visit
                                    the 10,000 Pucks Page</a></p>
                            <p><strong>Coaches: </strong>you can have your team compete and track their progress. Just
                                create your team, and have your players register for the contest (they can select their
                                team when they signup).</p>
                            <p><strong>Players:</strong> start logging your shots beginning June 1st.</p>
                            <p><a href="http://srv.ezinedirector.net/?n=3774677&amp;s=93138682" target="_blank">www.hockeyshare.com/10000pucks/</a>
                            </p>
                            <p>Skate hard &amp; keep your head up! See you around the rinks.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-471 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-10000-pucks"
                         id="post-471">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-contests/10000-pucks-contest-winners-2009/"
                               rel="bookmark">10,000 Pucks Contest Winners &#8211; 2009</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <div>With the last of the prizes having been shipped out for the 2009 10,000 Pucks Contest,
                                I thought it would be a good time to showcase some of the winners from last year&#8217;s
                                contest. We&#8217;re looking to make the 2010 contest bigger and better. We&#8217;re
                                always welcome to hear new ideas for the program. If you&#8217;ve got suggestions or
                                sponsorship ideas for the 2010 contest, please let me know at kevin@hockeyshare.com
                            </div>
                            <p>&nbsp;</p>
                            <div align="center">
                                <object id="vp1doKHk" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="432"
                                        height="240"
                                        codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
                                    <param name="allowFullScreen" value="true"/>
                                    <param name="allowscriptaccess" value="always"/>
                                    <param name="src"
                                           value="http://static.animoto.com/swf/w.swf?w=swf/vp1&amp;e=1269545473&amp;f=doKHksoxlPkNrYI9lDTiyQ&amp;d=116&amp;m=a&amp;r=w&amp;i=m&amp;options="/>
                                    <param name="allowfullscreen" value="true"/>
                                    <embed id="vp1doKHk" type="application/x-shockwave-flash" width="432" height="240"
                                           src="http://static.animoto.com/swf/w.swf?w=swf/vp1&amp;e=1269545473&amp;f=doKHksoxlPkNrYI9lDTiyQ&amp;d=116&amp;m=a&amp;r=w&amp;i=m&amp;options="
                                           allowscriptaccess="always" allowfullscreen="true"></embed>
                                </object>
                            </div>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-397 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-hockey-contest tag-hockeyshare-contest"
                         id="post-397">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/hockey-is-contest-winners/"
                               rel="bookmark">Hockey Is&#8230; &#8211; Contest Winners</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-contest/"
                                                    rel="tag">hockey contest</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-contest/"
                                                    rel="tag">hockeyshare contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The winners of the &#8220;Hockey Is&#8230;&#8221; contest are as follows:</p>
                            <p><strong>1) Ted Miskolczi Sr</strong> &#8211; Hockey is knowing that you are going to the
                                arena to teach kids a game that could alter their life in some way, also knowing that
                                they are with friends and family but most of all at the end of the day hockey is when
                                the kids come up to you with a big smile and says I had fun today coach.</p>
                            <p><strong>2) JIM</strong> &#8211; hockey is filling a void in your life that only you,
                                yourself can place the value on. The game by nature is an ever changing enviroment of
                                recognizable patterns , yet it lends itself well to those individuals that bore easily
                                and seldom tire. It is the freedom to be outside of your present life if only for a
                                brief moment in time. It is a dream come to life , in a childs heart. A heart still
                                young no matter what the age.</p>
                            <p><strong>3) Thomas Guarino</strong> -Hockey is very important to me. Hockey is me! I love
                                to play it. It is something that I know is good for my body and my mind.I have learned
                                alot of things by playing hockey. Not only have I learned how to play the game of hockey
                                but I have also learned many lessons.</p>
                            <p>I have learned that to be a “great hockey player” I must be a team player and not just
                                the leading goal scorer. I know that my teammates have alot of respect for me because I
                                do what is expected of me from my team and from my coach. It makes me feel good when I
                                choose to pass to the lead man to score instead of “hogging” the puck just to prove that
                                I can score. I know that I need to play for my team and not just for myself.</p>
                            <p>My life is good because of what hockey has taught me. I play my hardest and I play as a
                                part of a whole team. I know that I am well liked because of this. This is how I try to
                                live my life outside of the rink, in school, and at home. I pitch in to help out at home
                                and I am always there for my friends if they need me. Whether I am helping out as a part
                                of my family, as part of my group of friends, or as a part of my hockey team, I know
                                that I always need to work well with others.</p>
                            <p>I learned that hard work on the ice is like hard work at school. When I play hard on the
                                ice, I feel better at the end of the game whether we win or lose. When I work hard at
                                school I know that I am learning alot of good things for when I am older and go to
                                college. If I put all my best effort into my work I know that I will get a pretty good
                                shot of getting something good out of it.</p>
                            <p>In the future, I believe that I will be whatever I want to be because of what hockey has
                                taught me. I know that I have to work hard to get things done. Hockey made me see that I
                                have to trust people, be generous, and always help people who need it without expecting
                                anything back. Hockey has taught me lessons that have made me a good person and these
                                things that I have learned will let me do anything I want in my life.</p>
                            <p><strong>4) Rick Vaile</strong> &#8211; hockeys is a game of fun and skill where everyone
                                involved learns something new just about everyday they are at the rink. Hockey is
                                spending time with the family and fFriends. Hockey is being there when they need help,
                                to see all the smiles when something great happens. or console when something not so
                                good happens. Hockey is teaching and learning respect for the game on the ice as well as
                                taking that respect and applying it to off ice life. Hockey is a great way to teach and
                                learn life lessons that will arise when we get older.Hockey is a way to stay in shape
                                and stay active. Hockey is plain and simply the best game in the world with some of the
                                best people and role models I have ever met. Without Hockey my life would have never
                                been as great as it has been. Thank You Hockey and to all the people who have help make
                                me who I am today.</p>
                            <p><strong>5) Gary</strong> &#8211; Hockey is the ultimate combination of teamwork,
                                artistry, discipline, creativity, athleticism and passion!</p>
                            <p>We were originally going to do only 3 winners, but both Brett and I thought we could go
                                with more. Winners #4 and #5 will both receive a copy of &#8220;In Pursuit of
                                Excellence.&#8221; This is a great motivational book with lots of ideas to apply to
                                sports and life.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-204 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-hockey-contest tag-hockey-drills tag-hockeyshare-contest"
                         id="post-204">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/drill-submission-contest/"
                               rel="bookmark">Drill Submission Contest</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-contest/"
                                                    rel="tag">hockey contest</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-drills/"
                                                    rel="tag">Hockey Drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-contest/"
                                                    rel="tag">hockeyshare contest</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <div id="_mcePaste">Got Drills? We&#8217;ve been adding new drills, and want to encourage
                                user submissions to see what others around the world have in their drill banks! From now
                                until January 31st, 2010, if you submit 3 drills with diagrams to us, you&#8217;ll be
                                entered for a chance to win a great coaching prize pack of: <strong>In Pursuit of
                                    Excellence</strong> (book), <strong>Mitch Korn&#8217;s Goaltending Manual</strong>
                                (CD-ROM), and <strong>Improve Your Skating at Home</strong> (DVD). There are 3 easy ways
                                you can submit drills to us:
                            </div>
                            <p>1) Online Form<br/>
                                2) Email: kevin@hockeyshare.com<br/>
                                3) Fax: (866) 381-8260</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <!-- begin sidebar -->
                <td valign="top" width="250">

                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

