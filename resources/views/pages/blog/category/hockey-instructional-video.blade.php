@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1321 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-penalty-kill tag-special-teams tag-video"
                         id="post-1321">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/"
                               rel="bookmark">Penalty Killing Tips &#038; Tricks</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">12</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/penalty-kill/" rel="tag">penalty kill</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/special-teams/"
                                                    rel="tag">special teams</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>HockeyShare has put together two videos illustrating tips &amp; tricks for teaching the
                                penalty kill for both forwards and defensemen. The videos cover keys to look for during
                                coverage as well as technique and positional topics.</p>
                            <p><span class="youtube"><iframe title="YouTube video player" class="youtube-player"
                                                             type="text/html" width="640" height="360"
                                                             src="//www.youtube.com/embed/_D2gk9qg4DE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                             frameborder="0" allowfullscreen></iframe></span></p>
                            <p><span class="youtube"><iframe title="YouTube video player" class="youtube-player"
                                                             type="text/html" width="640" height="360"
                                                             src="//www.youtube.com/embed/Ug9pRdUnFXA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                             frameborder="0" allowfullscreen></iframe></span></p>

                            <br/>

                        </div>
                        <div class="feedback">
                        </div>
                    </div>
                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1308 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2hockey tag-video"
                         id="post-1308">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/"
                               rel="bookmark">Attacking the High Seam (Video &#038; Examples)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2hockey/"
                                                    rel="tag">m2hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We have gotten a lot of great feedback from users on our video on attacking the high seam
                                off the half-wall. Lots of people wrote us asking to see it in action, so we provided a
                                series of links to additional videos showing the use of the seam to either create a goal
                                or set one up. In case you missed our video &#8211; we have embedded it below. The
                                example video links are located below the video.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/7jp4IjQRk0Q?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p><strong>Examples of this seam in action:</strong></p>
                            <p>Basic Example: <a href="http://video.nhl.com/videocenter/console?id=2013020618-325-h">http://video.nhl.com/videocenter/console?id=2013020618-325-h</a>
                                (watch how the D pulls up at the hash marks)</p>
                            <p>Power Play Example: <a href="http://video.nhl.com/videocenter/?id=2013020603-660-h">http://video.nhl.com/videocenter/?id=2013020603-660-h</a>
                            </p>
                            <p>Different Angle of Attack on the Same Seam: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020616-342-h">http://video.nhl.com/videocenter/console?id=2013020616-342-h</a>
                            </p>
                            <p>Seam Off a Faceoff: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020616-350-h">http://video.nhl.com/videocenter/console?id=2013020616-350-h</a>
                            </p>
                            <p>Seam Exploited Off a Contained Rush: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020613-635-h">http://video.nhl.com/videocenter/console?id=2013020613-635-h</a>
                            </p>
                            <p>Seam w/ Pass: <a href="http://video.nhl.com/videocenter/console?id=2013020615-261-h">http://video.nhl.com/videocenter/console?id=2013020615-261-h</a>
                                (Pause the video 4 seconds in &#8211; notice how the strong-side winger has turned his
                                body and committed in to the puck carrier, opening up the point option)</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1293 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-passing tag-video"
                         id="post-1293">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/surrounding-the-puck-video/"
                               rel="bookmark">Surrounding the Puck (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/passing/"
                                                    rel="tag">passing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Surrounding the puck is a skill for any advanced level player to master. It involves
                                quickly turning your body around the puck to execute a pass in another direction
                                quickly. In this video, we demonstrate the concept in the neutral zone.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/vfnqWHqeCzI?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1290 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-passing tag-video"
                         id="post-1290">
                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/indirect-passing-basic-overview-video/"
                               rel="bookmark">Indirect Passing &#8211; Basic Overview (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">10</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/passing/"
                                                    rel="tag">passing</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In this week&#8217;s video, we cover the basic concepts behind making indirect passes. In
                                particular, we discuss making board and area passes, and what to look for in different
                                scenarios to make effective passes.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/3aQlzpiRcWA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1286 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-power-skating tag-video"
                         id="post-1286">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/quick-start-training-drill-video/"
                               rel="bookmark">Quick Start Training Drill (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/"
                                                    rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s video covers a skill development drill that focuses on the technique,
                                control, and balance necessary for an effective V Start / Quick Start.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/9mDmxEOVU6o?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">
                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1284 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-poke-check tag-video"
                         id="post-1284">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/poke-check-technique-tips-video/"
                               rel="bookmark">Poke Check Technique &#038; Tips (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Feb</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">28</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/poke-check/" rel="tag">poke check</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In this video we outline the basic concepts and techniques behind the poke check. We also
                                cover three different scenarios to use the poke check and offer tips for executing it
                                effectively.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/JHM6So3B9Dw?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>
                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1280 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-attackpad tag-m2-hockey tag-video"
                         id="post-1280">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/expansion-of-reach-drill-attackpad-video/"
                               rel="bookmark">Expansion of Reach Drill &#8211; AttackPad (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/attackpad/" rel="tag">attackpad</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In the 3rd of 3 videos covering the <a
                                        href="http://hockeyshare.com/attackpad/">AttackPad</a>,
                                we demonstrate a drill to develop your expansion of reach:</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/8PLipEsHeJg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>
                        </div>
                        <div class="feedback">

                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1278 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-video"
                         id="post-1278">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/body-movement-drill-attackpad-video/"
                               rel="bookmark">Body Movement Drill &#8211; AttackPad (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">19</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In part two of three in the <a href="http://hockeyshare.com/attackpad">AttackPad</a>
                                stickhandling series, we should you a drill focusing on body movement while maintaining
                                proper dribble control of the puck.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/CPyviZ72PEg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1271 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-stickhandling tag-video"
                         id="post-1271">
                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/dribble-pull-drill-attackpad-video/"
                               rel="bookmark">Dribble &#038; Pull Drill &#8211; AttackPad (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">9</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here is a quick stickhandling drill working on diagonal dribbling with pulls. In the
                                video, we use the <a href="http://hockeyshare.com/attackpad">AttackPad Hockey
                                    Training System</a>, which gives athletes consistent visual markers for reference
                                while progressing with the drill.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/x5SEqDEz_Gc?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>
                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1268 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-power-skating tag-video"
                         id="post-1268">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/hip-open-defense-variation-video/"
                               rel="bookmark">Hip Open Defense Variation (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">28</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/"
                                                    rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s video is a variation on the drill demonstrated in our last video. In
                                this drill, we add in a defensive pivot off a one-foot stop to develop balance and
                                change of direction power.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/jUnQWpaIqNk?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/category/hockey-instructional-video/page/2/">Older
                        Posts &raquo;</a>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

