@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1209 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-advanced-coaching-platform tag-drill-diagrammer"
                         id="post-1209">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-com-features/drill-diagrammer-update/"
                               rel="bookmark">Drill Diagrammer Update</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">5</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/advanced-coaching-platform/"
                                                    rel="tag">advanced coaching platform</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagrammer/"
                                                    rel="tag">drill diagrammer</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We&#8217;re excited to announce the addition of several requested features to our <a
                                        href="http://hockeyshare.com/drills/learn_more.php">Drill Diagrammer</a>:
                            </p>
                            <ul>
                                <li>Added &#8220;Triangle Object&#8221; &#8211; at the request of many of our
                                    international users, we&#8217;ve added the triangle object to make it easier to
                                    adhere to local drill standards
                                </li>
                                <li>Added &#8220;Attack Triangle Object&#8221; &#8211; let&#8217;s face it, lots of
                                    drills utilize the Attack Triangle &#8211; it&#8217;s a great product &#8211; now we&#8217;ve
                                    made adding them to your drills a breeze with the new object
                                </li>
                                <li>Added &#8220;Agility Ladder Object&#8221; &#8211; this object has been at the
                                    request of lots of users using our diagrammer for off ice drill stations
                                </li>
                            </ul>
                            <p>Stay tuned for lots more diagrammer updates &#8211; this is just the beginning!</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1188 post type-post status-publish format-standard hentry category-hockeyshare-com-features category-hockey-instructional-video tag-podcast tag-video"
                         id="post-1188">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/hockeysharem2-hockey-training-video-podcast/"
                               rel="bookmark">HockeyShare/M2 Hockey Training Video Podcast</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">20</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/podcast/"
                                                    rel="tag">podcast</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Want to watch our training videos on the go? Now you can subscribe to our HockeyShare /
                                M2 Hockey Training Videos Podcast to get the latest videos delivered right to your
                                computer or mobile device!</p>
                            <table width="100%" border="0">
                                <tbody>
                                <tr>
                                    <td align="center" valign="top" width="160"><a
                                                href="http://itunes.apple.com/us/podcast/hockeyshare-m2-hockey-training/id537334936"
                                                target="_blank"><img src="/img/podcast_150.jpg" alt="Podcast"
                                                                     width="150" height="150" border="0"/></a></td>
                                    <td align="center"><a style="color: #b52222; font-size: 16px; font-weight: bold;"
                                                          href="http://itunes.apple.com/us/podcast/hockeyshare-m2-hockey-training/id537334936"
                                                          target="_blank">Subscribe to Our Podcast</a></td>
                                </tr>
                                </tbody>
                            </table>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1183 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-contest tag-hockey-contest tag-hockeyshot-com"
                         id="post-1183">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/hockeyshot-summer-trick-shot-challenge/"
                               rel="bookmark">HockeyShot Summer Trick Shot Challenge</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">19</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/contest/"
                                                    rel="tag">contest</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-contest/"
                                                    rel="tag">hockey contest</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshot-com/"
                                                    rel="tag">hockeyshot.com</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Been practicing your shots with the 10,000 Pucks Contest&#8230;.have some cool trick
                                shots up your sleeve? Check out <a
                                        href="http://www.hockeyshot.com/Articles.asp?ID=451&amp;Click=40243"
                                        target="_blank">HockeyShot.com&#8217;s Summer Trick Shot Challenge</a>. Film
                                your trick shot, then submit it to the contest for the chance to win some cool prizes!
                            </p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/GEOKhPn5RCk?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a
                                        style="color: #b52222; font-weight: bold; font-size: 16px;"
                                        href="http://www.hockeyshot.com/Articles.asp?ID=451&amp;Click=40243"
                                        target="_blank">Summer Trick Shot Challenge by HockeyShot.com</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1153 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-drill-diagrammer tag-hockeyshare"
                         id="post-1153">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/2-new-features-added-to-drill-diagrammer/"
                               rel="bookmark">2 New Features Added to Drill Diagrammer</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagrammer/"
                                                    rel="tag">drill diagrammer</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <div>We&#8217;re excited to announce two new highly-requested feature additions to the
                                HockeyShare Drill Diagrammer. Now you can create &#8220;Custom Objects&#8221; where you
                                are able to change not only the numbering, but also the main text of the object. We also
                                added an option to create a drill on a blank surface &#8211; this is great for drawing
                                station drills where you want to go in to more depth without being constrained by the
                                lines on the rink. We put together a quick video to highlight the new changes:
                            </div>
                            <div></div>
                            <div style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                           class="youtube-player"
                                                                                           type="text/html" width="640"
                                                                                           height="360"
                                                                                           src="//www.youtube.com/embed/81m5I-L72bc?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                           frameborder="0"
                                                                                           allowfullscreen></iframe></span>
                            </div>
                            <div style="text-align: center;"></div>
                            <div><strong></strong>To celebrate the new changes, this week only we&#8217;re offering a
                                <strong>10% discount</strong> on monthly memberships and <strong>20% off</strong> on all
                                annual, team, and association memberships. Hurry &#8211; offer expires soon! Just use
                                discount code <a><strong>ML0312</strong></a><strong></strong>.
                            </div>
                            <div></div>
                            <div></div>
                            <div style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/signup.php?discount_code=ML0312">Sign
                                    up today to lock in savings</a>. <a
                                        href="http://hockeyshare.com/drills/learn_more.php?hs_a_id=1029">Learn more
                                    about the drill diagrammer &amp; practice planner</a>.
                            </div>
                            <div style="text-align: center;"></div>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1082 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-features tag-hockeyshare"
                         id="post-1082">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/add-practice-plans-to-your-practice-calendar/"
                               rel="bookmark">Add Practice Plans to Your Practice Calendar</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/features/"
                                                    rel="tag">features</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Introducing a new feature for <a
                                        href="http://hockeyshare.com/drills/compare.php">Premium</a>
                                users who use HockeyShare.com to <a href="http://hockeyshare.com/teams/">track their
                                    team stats</a>. Premium users can now link practice plans to specific entries on
                                their team&#8217;s practice calendar. See the video and screenshots below:</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/-RmfZET7wOU?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <div align="center">
                                <div id="attachment_1088" style="width: 510px" class="wp-caption aligncenter"><img
                                            class="size-full wp-image-1088" title="PracticeSchedulePlan-1-web"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2011/11/PracticeSchedulePlan-1-web.jpg"
                                            alt="" width="500" height="383"
                                            srcset="/img/PracticeSchedulePlan-1-web.jpg 500w, /img/PracticeSchedulePlan-1-web-300x229.jpg 300w"
                                            sizes="(max-width: 500px) 100vw, 500px"/>
                                    <p class="wp-caption-text">Select Your Practice from the New or Edit Practice Plan
                                        Page</p></div>
                                <p>&nbsp;</p>
                                <div id="attachment_1089" style="width: 510px" class="wp-caption aligncenter"><img
                                            class="size-full wp-image-1089" title="PracticeSchedulePlans-2-web"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2011/11/PracticeSchedulePlans-2-web.jpg"
                                            alt="" width="500" height="277"
                                            srcset="/img/PracticeSchedulePlans-2-web.jpg 500w, /img/PracticeSchedulePlans-2-web-300x166.jpg 300w"
                                            sizes="(max-width: 500px) 100vw, 500px"/>
                                    <p class="wp-caption-text">Download Icon Appears on Team&#39;s HockeyShare.com Team
                                        Practice Schedule Page</p></div>
                            </div>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1074 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-hockeyshare tag-webinar"
                         id="post-1074">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/free-webinar-thursday-november-10th-2011/"
                               rel="bookmark">Free Webinar &#8211; Thursday, November 10th, 2011</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/webinar/"
                                                    rel="tag">webinar</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We’ve received a lot of requests about doing an overview / quick training webinar for our
                                <a href="http://hockeyshare.com/drills/learn_more.php">Hockey Drill Diagrammer &amp;
                                    Practice Planner</a>. This Thursday, November 10th, 2011 we will be holding a LIVE
                                online webinar at 9am CST. There is no cost to participate in the webinar – use the link
                                below to signup.</p>
                            <p><a href="https://www3.gotomeeting.com/register/390646198">Click Here to Register (Free)
                                    for the Webinar</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1068 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-webinar"
                         id="post-1068">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/drill-diagrammer-practice-planner-live-webinar-nov-3rd/"
                               rel="bookmark">Drill Diagrammer &#038; Practice Planner &#8211; Live Webinar &#8211; Nov
                                3rd</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/webinar/"
                                                    rel="tag">webinar</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We&#8217;ve received a lot of requests about doing an overview / quick training webinar
                                for our <a href="http://hockeyshare.com/drills/learn_more.php">Hockey Drill
                                    Diagrammer &amp; Practice Planner</a>. This Thursday, November 3rd, 2011 we will be
                                holding a LIVE online webinar at 1pm CST. There is <span
                                        style="text-decoration: underline;">no cost</span> to participate in the webinar
                                &#8211; use the link below to signup.</p>
                            <p><a href="https://www3.gotomeeting.com/register/154117566" target="_blank">Click here to
                                    register for the webinar</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1022 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-draw-drills tag-drill-diagramming tag-hockeyshare tag-practice-plan"
                         id="post-1022">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/drawing-a-drill-with-hockeyshares-online-drill-diagrammer/"
                               rel="bookmark">Drawing a Drill with HockeyShare&#8217;s Online Drill Diagrammer</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">6</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/draw-drills/" rel="tag">draw drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagramming/"
                                                    rel="tag">drill diagramming</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/practice-plan/"
                                                    rel="tag">practice plan</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I&#8217;ve had quite a few people as me for a demo video to show just how easy it is to
                                draw a drill using the HockeyShare Online Drill Diagramming platform. Since a picture
                                (or video) is worth a thousand words, I put one together showing how you can draw a
                                drill in just a minute or two and then have access to it any time:</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/DslXBpW-wYg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark&amp;feature=player_embedded"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/drills/learn_more.php"><span
                                                    style="color: #ff0000;">Learn more about drawing drills w/ our platform  (14 day FREE trial available)</span></a></strong></span>
                            </p>
                            <p style="text-align: center;"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/drills/compare.php"><span
                                                    style="color: #ff0000;">Compare subscription options</span></a></strong></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1007 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-drill-diagramming tag-practice-plan"
                         id="post-1007">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/introducing-team-pack-for-coaches/"
                               rel="bookmark">Introducing &#8220;Team Pack&#8221; for Coaches</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">20</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagramming/"
                                                    rel="tag">drill diagramming</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/practice-plan/"
                                                    rel="tag">practice plan</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>With the popularity of our <a href="http://hockeyshare.com/drills/learn_more.php">drill
                                    diagramming and practice planning platform</a>, we&#8217;ve been looking for new and
                                improved ways to connect coaches this coming season. We&#8217;re excited to announce
                                <strong>Team Packs</strong> for coaches. Team Packs allow up to 4 users to have
                                Premium-level access to our drill diagrammer and practice planner, but also allow the 4
                                users to privately share their drills and practices. Browse your coaching staff&#8217;s
                                drills or practices and copy them to your own private database with a single click
                                &#8211; all while keeping them invisible to the public. The best part about this new
                                offering is the price &#8211; at just $150 per year for up to 4 users, that&#8217;s
                                about a 52% savings over having 4 individual annual subscriptions! Already have an
                                annual Premium subscription? No problem, you can upgrade your account to a Team Pack by
                                visiting the <a href="http://hockeyshare.com/account/billing.php">My
                                    Subscriptions</a> page.</p>
                            <p><a style="color: #f00;font-weight:bold;"
                                  href="http://hockeyshare.com/drills/signup_team.php">Create a new Team Pack
                                    Subscription</a></p>
                            <p><a href="http://hockeyshare.com/drills/learn_more.php">Learn more about our
                                    offerings</a>. Not sure which package is right for you? <a
                                        href="http://hockeyshare.com/drills/compare.php">Compare our offerings
                                    here.</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-962 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-drill-diagrammer tag-free-trial tag-practice-planner"
                         id="post-962">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/premium-drill-diagrammer-practice-planner-30-day-free-trial/"
                               rel="bookmark">Premium Drill Diagrammer &#038; Practice Planner &#8211; 14 Day Free
                                Trial</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">10</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagrammer/"
                                                    rel="tag">drill diagrammer</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/free-trial/" rel="tag">free trial</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/practice-planner/"
                                                    rel="tag">practice planner</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We wanted to give users the opportunity to try out all the great <a
                                        href="http://hockeyshare.com/drills/compare.php">features</a> of our Premium
                                level drill diagramming and practice planning platform, without any risk. Now, new users
                                can activate their free 14 day trial with just a single click &#8211; no credit card
                                required.</p>
                            <p>Start your free trial by visiting: <a href="http://hockeyshare.com/freetrial">www.hockeyshare.com/freetrial</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/category/hockeyshare-com-features/page/2/">Older
                        Posts &raquo;</a>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

