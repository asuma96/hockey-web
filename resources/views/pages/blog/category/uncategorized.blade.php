@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1260 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-1260">

                        <h2>
                            <a href="http://hockeyshare.com/blog/uncategorized/drill-shootout-winner-2-on-1-gap-control-w-animation/"
                               rel="bookmark">Drill Shootout Winner: 2 on 1 Gap Control (w/ Animation)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Last week&#8217;s Drill Shootout winner was the 2 on 1 Gap Control</p>
                            <p><a href="http://hockeyshare.com/drills/drill.php?id=63522"><img class="aligncenter"
                                                                                                    title="2 on 1 Gap Control"
                                                                                                    src="https://hs-drill-images-sm.s3.amazonaws.com/image-201496-1355262228.jpg?s=1355934315"
                                                                                                    alt="" width="150"
                                                                                                    height="89"/></a>
                            </p>
                            <p><a href="http://hockeyshare.com/drills/drill.php?id=63522">View Drill w/
                                    Animation</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-124 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-124">

                        <h2><a href="http://hockeyshare.com/blog/uncategorized/totalhockey-net-coupon/"
                               rel="bookmark">TotalHockey.net Coupon</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">10</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Since so many people on this site use TotalHockey.net for their hockey gear needs, I
                                figured I&#8217;d pass along this coupon to save $10 on purchases of $100 or more.</p>
                            <p><strong>Just use the following link</strong>: <a
                                        href="http://www.dpbolvw.net/click-3739194-10539327" target="_top">Save $10 on
                                    Hockey Equipment</a></p>
                            <p>Then, when you go to check out, use the following coupon code: C7C4I9-Y1Z9B8-L3A9T2</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-101 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-101">

                        <h2><a href="http://hockeyshare.com/blog/uncategorized/november-2009-practice-plans/"
                               rel="bookmark">November 2009 Practice Plans</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are my practice plans for November, 2009</p>
                            <p><a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/112509.pdf">Practice
                                    Plan for 11/25/09<br/>
                                </a><a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/112509-Dryland.pdf">Dryland
                                    Plan for 11/25/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/112409.pdf">Practice
                                    Plan for 11/24/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111909.pdf">Practice
                                    Plan for 11/19/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111909-Dryland.pdf">Dryland
                                    Plan for 11/19/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111809.pdf">Practice
                                    Plan for 11/18/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111709.pdf">Practice
                                    Plan for 11/17/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111709-Dryland.pdf">Dryland
                                    Plan for 11/17/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111209.pdf">Practice
                                    Plan for 11/12/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111009.pdf">Practice
                                    Plan for 11/10/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/111009-dryland.pdf">Dryland
                                    Plan for 11/10/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/110509.pdf">Practice
                                    Plan for 11/05/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/110409-Skills.pdf">Practice
                                    Plan for 11/04/09</a><br/>
                                <a href="http://hockeyshare.com/blog/wp-content/uploads/2009/12/110309.pdf">Practice
                                    Plan for 11/03/09</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-79 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-79">

                        <h2><a href="http://hockeyshare.com/blog/uncategorized/happy-thanksgiving/" rel="bookmark">Happy
                                Thanksgiving</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I want to wish everyone a Happy Thanksgiving! We&#8217;re all blessed to have the great
                                game of hockey in our lives, but that&#8217;s just the beginning. Enjoy the time with
                                family and/or friends. God Bless.</p>
                            <p>Skate hard &amp; keep your head up!<br/>
                                -Kevin</p>
                            <p>&#8212;&#8212;&#8212;&#8211;</p>
                            <p>For fun, I&#8217;ve put together some links of friends and supporters. Take a look at
                                their sites &#8211; hope you enjoy them!</p>
                            <p>NiceRink &#8211; The World&#8217;s Best Backyard Ice Rinks! &#8211; <a
                                        href="http://www.nicerink.com" target="_blank">www.nicerink.com</a><br/>
                                Puckmasters Lake Bluff &#8211; Private Hockey Instruction on Plastic &#8211; <a
                                        href="http://www.puckmasterslakebluff.com" target="_blank">www.puckmasterslakebluff.com</a><br/>
                                Sports Stomping Ground &#8211; Sports news focused on the Detroit area &#8211; <a
                                        href="http://www.sportsstompingground.com " target="_blank">www.sportsstompingground.com </a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-72 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-72">

                        <h2><a href="http://hockeyshare.com/blog/uncategorized/inspirational-video/"
                               rel="bookmark">Inspirational Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here&#8217;s an inspirational video I watched earlier today and thought I&#8217;d share.
                                Enjoy.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/MslbhDZoniY?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-68 post type-post status-publish format-standard hentry category-uncategorized"
                         id="post-68">

                        <h2><a href="http://hockeyshare.com/blog/uncategorized/team-building-resources/"
                               rel="bookmark">Team Building Resources</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/uncategorized/"
                                                rel="category tag">Uncategorized</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I&#8217;ve been getting a lot of questions lately about some good exercises / games to
                                help bring your team closer. Here are some links to team building resources I&#8217;ve
                                found helpful:</p>
                            <p>
                                <a href="http://s3.amazonaws.com/assets.ngin.com/attachments/document/0002/6727/Team_Building.doc"
                                   target="_blank">USA Hockey Team Building</a><br/>
                                <a href="http://wilderdom.com/games/InitiativeGames.html" target="_blank">Team Building
                                    Activities, Initiative Games, &amp; Problem Solving Exercises</a><br/>
                                <a href="http://www.group-games.com/" target="_blank">Group Games</a><br/>
                                <a href="http://www.azkidsnet.com/brainteasers.htm" target="_blank">Brain Teasers</a>
                            </p>
                            <ul></ul>
                            <p>If you&#8217;ve got some other resources you think I should list, drop me an email and I&#8217;ll
                                get them added here.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

