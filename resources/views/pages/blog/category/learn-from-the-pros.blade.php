@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1316 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-video"
                         id="post-1316">

                        <h2>
                            <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/"
                               rel="bookmark">Bruins Illustrate Importance of Driving the Net &#038; Net Front
                                Presence</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The Boston Bruins put on a clinic on the importance of net drives and net front presence
                                in their 6-3 win over the NY Rangers on Sunday, March 2nd, 2014. Also notice the puck
                                support when entering the zone &#8211; great examples:</p>
                            <p>Boston &#8211; Goal #1: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-221-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-221-h</a>
                                Quality puck dump, aggressive 1st man, solid puck support, net drive</p>
                            <p>Boston &#8211; Goal #2: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-401-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-401-h</a>
                                Good rush / puck support, net drive, patience with puck on entry, D activating in rush
                            </p>
                            <p>Boston &#8211; Goal #3: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-419-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-419-h</a>
                                Quality puck movement, movement away from puck (players making themselves available for
                                the puck carrier), net front presence</p>
                            <p>Boston &#8211; Goal #4: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-632-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-632-h</a>
                                Great patience entering the zone, good puck protection, net drive</p>
                            <p>Boston &#8211; Goal #5: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-643-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-643-h</a>
                                Aggressive puck pursuit w/ good containment creates turnover, wide drive to allow
                                support, net drive</p>
                            <p>Boston &#8211; Goal #6: <a
                                        href="http://video.nhl.com/videocenter/console?id=2013020916-865-h"
                                        target="_blank">http://video.nhl.com/videocenter/console?id=2013020916-865-h</a>
                                Good puck movement in zone, net front presence for tip</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1168 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-dryland tag-off-ice tag-stickhandling tag-video"
                         id="post-1168">

                        <h2>
                            <a href="http://hockeyshare.com/blog/learn-from-the-pros/off-ice-stickhandling-video-series-part-1-of-3/"
                               rel="bookmark">Off Ice Stickhandling Video Series (Part 1 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice/" rel="tag">off ice</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As players across the world prepare to hone their skills in the off-season, we have put
                                together a 3-part video series covering the basics of training stickhandling off ice. In
                                part one we cover basic technique, dribbles, and practice drills to help you train
                                properly.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/XNYejzWquYA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Part two will cover additional (more advanced) drills, and part
                                three will discuss ways to vary your training to keep things fresh and develop new
                                skills.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-627 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-627">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-9/"
                               rel="bookmark">Learn from the Pros &#8211; Week 9</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">10</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Another exciting NHL season has come to a close &#8211; this year with one of the
                                original six teams bringing home the Stanley Cup. Although the finish was a bit strange,
                                it&#8217;s safe to say it was an exciting final game. In this week&#8217;s Learn from
                                the Pros segment, we&#8217;ll take a look at the last two goals of the 2009-10 NHL
                                season.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-9/#more-627"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-619 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-619">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-8/"
                               rel="bookmark">Learn from the Pros &#8211; Week 8</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As the final two teams battle for the cup, there are plenty of intense moments and great
                                plays. This week we&#8217;ll look at Claude Giroux&#8217;s OT winner vs the Blackhawks
                                on 6/2/10.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-8/#more-619"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">


                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-613 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-613">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-7/"
                               rel="bookmark">Learn from the Pros &#8211; Week 7</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">22</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As the final four teams inch closer to their goal of playing for the Stanley Cup, the
                                intensity level of games continues to rise. This week in our Learn from the Pros
                                segment, we&#8217;ll take a look at a goal from the Western Confrence finals between the
                                Hawks and Sharks. The goal we&#8217;re looking at comes from Chicago&#8217;s Dustin
                                Byfuglin (#33) on a nice setup from Patrick Kane (#88).</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-7/#more-613"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-606 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-606">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-6/"
                               rel="bookmark">Learn from the Pros &#8211; Week 6</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>What a week in the playoffs. Upsets and game sevens &#8211; it doesn&#8217;t get much
                                more exciting than this! In this week&#8217;s Learn from the Pros segment, we take a
                                look at a goal by Montreal&#8217;s Mike Cammalleri. Cammalleri&#8217;s goal helped hoist
                                the Canadian&#8217;s past the Penguins in their seven game series.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-6/#more-606"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-597 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-597">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-5/"
                               rel="bookmark">Learn from the Pros &#8211; Week 5</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Playoff hockey this year has certainly delivered excitement as expected. One series not
                                short on rivalry is the Boston vs. Philly match-up. In this week&#8217;s Learn from the
                                Pros posting, we&#8217;ll take a look at a Boston goal scored off a blocked shot and a
                                big hit.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-5/#more-597"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-581 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-581">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-4/"
                               rel="bookmark">Learn from the Pros &#8211; Week 4</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p style="text-align: left;">This week&#8217;s Learn from the Pros clip features Marian
                                Hossa&#8217;s goal against Nashville on 4/24/10 to put the Blackhawks up 3 games to 2 in
                                their first round series. Hossa, who was serving a 5-minute penalty for boarding comes
                                out of the box, finds a break in Nashville&#8217;s coverage and puts home a rebound.</p>
                            <p style="text-align: left;"><a
                                        href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-4/#more-581"
                                        class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-564 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-learn-from-the-pros tag-nhl"
                         id="post-564">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-3/"
                               rel="bookmark">Learn from the Pros &#8211; Week 3</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s Learn from the Pros segment look at one of Alex Ovechkin&#8217;s goals
                                against Montreal on April 21st, 2010. The goal is scored on a power play rush started by
                                Capital&#8217;s Norris Trophy finalist Mike Green. Some subtle plays and a great shot
                                lead to Ovechkin&#8217;s first of the night.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/Bc8_jDfMKpE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">[0:21] &#8211; Capitals #52 Mike Green begins this power play
                                rush by skating with the puck. The first thing to notice is he has his head up surveying
                                the ice. Next, he skates with the puck until he gets pressured by a Montreal
                                forechecker. Too often on the power play breakout players panic with the puck and move
                                it too quickly instead of having patience. By waiting for the forechecker to come to
                                him, Green opens opens up a passing lane.</p>
                            <p style="text-align: left;">[0:26] &#8211; Capitals #19 Nicklas Backstrom takes the outlet
                                pass from Green and enters the zone with his head up. By being able to see the ice, he
                                identifies Ovechkin (#8) and makes a pass slightly behind his body. By making the pass
                                opposite his body&#8217;s momentum Backstrom is able to create space against the
                                Montreal defense.</p>
                            <p style="text-align: left;">[0:28] &#8211; Ovechkin (#8) receives this pass and <strong>does
                                    NOT stickhandle</strong> at all. Instead, he releases a quick (hard) snap shot
                                <strong>across his body</strong> &#8211; meaning the puck is going the opposite way his
                                body is. This forces Montreal goalie #31 Carey Price to move laterally. When a shooter
                                forces a goaltender to move laterally, it creates openings in the goaltender&#8217;s
                                stance. Ovechkin&#8217;s hard shot finds a way past Price&#8217;s far-side.</p>
                            <p style="text-align: left;">Take a look at the <a
                                        href="http://hockeyshare.com/drills/drill.php?id=220">Against the Grain</a>
                                drill for ideas on how to work on this type of shot.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-548 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-hockey-coaching tag-learn-from-the-pros tag-nhl"
                         id="post-548">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-2/"
                               rel="bookmark">Learn from the Pros &#8211; Week 2</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s Learn from the Pros video clip features a goal from Vancouver&#8217;s
                                Daniel Sedin from brother Henrik Sedin on April 15th vs. the LA Kings.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/SK2CMDsm9YE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p>[0:02] The play begins by Daniel Sedin (#22) carrying the puck out of the zone on the far
                                wall with King&#8217;s forward Justin Williams (#14) defending. Sedin realizes he is
                                going to be angled off, so he escapes and buys time with the puck, moving it to his
                                brother Henrik Sedin (#33) in the middle of the ice. This simple escape move has changed
                                the rush for the Canucks from a 1 on 3 rush to a 3 on 3 rush.</p>
                            <p>[0:03] This is where the entire play is made. Daniel Sedin (#22) keeps his feed moving
                                after he has passed the puck. Williams then turns back and fails to finish his check,
                                leaving him one-step behind Daniel Sedin in a foot-race up ice.</p>
                            <p>[0:05] Henrik Sedin looks up and exploits the large gap given by the Kings defensemen by
                                first moving inside the attacking zone, then creates a bigger gap (as big as the &#8220;Stanley
                                Cup Playoffs&#8221; logo in the ice!) by bringing the puck back out away from the
                                defenders. This allows Daniel Sedin to get involved with the play now that he has beat
                                Williams back up ice.</p>
                            <p>[0:06] Henrik Sedin quickly slows up and lays a beautiful pass out to his brother Daniel
                                in prime scoring area. It should be noted that the Kings weakside defenseman (#6 &#8211;
                                Sean O&#8217;Donnell) is in relatively good position covering Vancouver&#8217;s
                                weak-side forward (#14 &#8211; Alex Burrows), however Vancouver has turned this rush
                                into a 4 on 3 with a trailer jumping in on the play &#8211; this gives Vancouver lots of
                                options and is very difficult to defend with Daniel Sedin having gotten in front of
                                Justin Williams.</p>
                            <p>[0:08] Instead of simply shooting the puck, Daniel Sedin changes the puck location to the
                                inside, forcing Kings goalie (#32 &#8211; Jonathan Quick) to shift his weight and
                                square-up to the puck. Sedin finishes with an amazing backhander top-shelf high
                                glove-side. There is a great replay at the [0:54] mark as well.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/category/learn-from-the-pros/page/2/">Older
                        Posts &raquo;</a>

                <td valign="top" width="250">
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

