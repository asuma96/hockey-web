@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-842 post type-post status-publish format-standard hentry category-comments-thoughts tag-goals tag-hockey-coaching"
                         id="post-842">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/hockey-new-year-resolutions/"
                               rel="bookmark">Hockey New Year Resolutions</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/goals/"
                                                    rel="tag">goals</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The new year is a time commonly associated with new resolutions. A new year brings a
                                fresh mental start. In the first HockeyShare Blog Post of 2011, I&#8217;d like to
                                solicit interaction from the community and find out what your Hockey New Year
                                Resolutions are!</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/hockey-new-year-resolutions/#more-842"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-811 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-hockey-coaching tag-hockey-instructional-video tag-video"
                         id="post-811">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/circle-pass-pivot-drill-video/"
                               rel="bookmark">Circle Pass Pivot Drill &#8211; Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The Circle Pass Pivot Drill is along the same lines as the <a
                                        href="http://hockeyshare.com/blog/hockey-drills/figure-8-passing-video/">Figure
                                    8 Passing Drill</a> in terms of skill focus. It&#8217;s a great drill to develop
                                individual skill in agility, body control, passing/receiving, and controlling the puck
                                at awkward body angles.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/circle-pass-pivot-drill-video/#more-811"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-717 post type-post status-publish format-standard hentry category-hockey-tips category-hockey-instructional-video tag-hockey-coaching tag-hockey-crossovers tag-power-skating tag-youth-hockey-coaching"
                         id="post-717">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/crossovers-in-depth-video/"
                               rel="bookmark">Crossovers &#8211; In Depth [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">30</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey Tips</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-crossovers/"
                                                    rel="tag">hockey crossovers</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/"
                                                    rel="tag">power skating</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The hockey crossover is an essential part of any hockey player&#8217;s skating arsenal.
                                In this video, we break down the crossover into easy teaching points and give ideas on
                                how to coach your players into using better technique.</p>
                            <p style="text-align: left;"><a
                                        href="http://hockeyshare.com/blog/hockey-instructional-video/crossovers-in-depth-video/#more-717"
                                        class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-634 post type-post status-publish format-standard hentry category-hockey-systems tag-hockey-coaching tag-hockey-systesm tag-youth-hockey-coaching"
                         id="post-634">

                        <h2><a href="http://hockeyshare.com/blog/hockey-systems/simple-penalty-kill-forecheck/"
                               rel="bookmark">Simple Penalty Kill Forecheck</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">29</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/"
                                                rel="category tag">Hockey Systems</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-systesm/"
                                                    rel="tag">hockey systesm</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Killing off a penalty can be one of the most critical turning points of a game. Your team
                                finally ices the puck, and you get a fresh set of legs on the ice to go pressure the
                                opposing team while they&#8217;re setting up their breakout&#8230;.now what? If you&#8217;re
                                dealing with older players, it is important your players know their responsibilities and
                                the lanes they&#8217;re defending.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-systems/simple-penalty-kill-forecheck/#more-634"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-561 post type-post status-publish format-standard hentry category-resources tag-bench-management tag-hockey-coaching tag-hockeyshare-download"
                         id="post-561">

                        <h2><a href="http://hockeyshare.com/blog/resources/bench-management-lineu-card-download/"
                               rel="bookmark">Bench Management &#8211; Lineup Card [Download]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">24</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/bench-management/"
                                                    rel="tag">bench management</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-download/"
                                                    rel="tag">hockeyshare download</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Managing the bench during game situations can be difficult at best. Players get hurt,
                                equipment fails, penalties are called &#8211; you name it, it changes your game plan.
                                Good coaches are able to react and make decisions quickly. One of the tools I&#8217;ve
                                found most useful over the years has been the lineup card. This allows me to quickly
                                glance down and know my first choice for most situations. It is also extremely useful if
                                you&#8217;re in a game where line match-ups are important. I&#8217;ve provided a PDF
                                download of the template I use for my lineup cards. It includes 4 offensive and
                                defensive lines, 2 goaltenders, 2 power play units, 2 penalty kill units, two groups for
                                end of the game play (pulling goalie, or defending a one-goal lead), and areas for notes
                                and diagrams. I hope you find this useful in your coaching!</p>
                            <p><a href="http://www.mediafire.com/?yywmydiyt5y" target="_blank">» Download the
                                    HockeyShare Lineup Card (PDF)</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-548 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-hockey-coaching tag-learn-from-the-pros tag-nhl"
                         id="post-548">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-2/"
                               rel="bookmark">Learn from the Pros &#8211; Week 2</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s Learn from the Pros video clip features a goal from Vancouver&#8217;s
                                Daniel Sedin from brother Henrik Sedin on April 15th vs. the LA Kings.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/SK2CMDsm9YE?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p>[0:02] The play begins by Daniel Sedin (#22) carrying the puck out of the zone on the far
                                wall with King&#8217;s forward Justin Williams (#14) defending. Sedin realizes he is
                                going to be angled off, so he escapes and buys time with the puck, moving it to his
                                brother Henrik Sedin (#33) in the middle of the ice. This simple escape move has changed
                                the rush for the Canucks from a 1 on 3 rush to a 3 on 3 rush.</p>
                            <p>[0:03] This is where the entire play is made. Daniel Sedin (#22) keeps his feed moving
                                after he has passed the puck. Williams then turns back and fails to finish his check,
                                leaving him one-step behind Daniel Sedin in a foot-race up ice.</p>
                            <p>[0:05] Henrik Sedin looks up and exploits the large gap given by the Kings defensemen by
                                first moving inside the attacking zone, then creates a bigger gap (as big as the &#8220;Stanley
                                Cup Playoffs&#8221; logo in the ice!) by bringing the puck back out away from the
                                defenders. This allows Daniel Sedin to get involved with the play now that he has beat
                                Williams back up ice.</p>
                            <p>[0:06] Henrik Sedin quickly slows up and lays a beautiful pass out to his brother Daniel
                                in prime scoring area. It should be noted that the Kings weakside defenseman (#6 &#8211;
                                Sean O&#8217;Donnell) is in relatively good position covering Vancouver&#8217;s
                                weak-side forward (#14 &#8211; Alex Burrows), however Vancouver has turned this rush
                                into a 4 on 3 with a trailer jumping in on the play &#8211; this gives Vancouver lots of
                                options and is very difficult to defend with Daniel Sedin having gotten in front of
                                Justin Williams.</p>
                            <p>[0:08] Instead of simply shooting the puck, Daniel Sedin changes the puck location to the
                                inside, forcing Kings goalie (#32 &#8211; Jonathan Quick) to shift his weight and
                                square-up to the puck. Sedin finishes with an amazing backhander top-shelf high
                                glove-side. There is a great replay at the [0:54] mark as well.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-542 post type-post status-publish format-standard hentry category-comments-thoughts tag-hockey-coaching tag-tryouts tag-youth-hockey-coaching"
                         id="post-542">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/tryout-overview-part-2/"
                               rel="bookmark">Tryout Overview [Part 2]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>So, now you have taken the time to <a
                                        href="http://hockeyshare.com/blog/comments-thoughts/tryout-overview-part-one/">properly
                                    plan out your tryout sessions</a>, and now find yourself with an evaluation sheet in
                                hand. Many coaches get overwhelmed during this process &#8211; after all, there is a lot
                                to watch. Assuming you&#8217;re breaking your tryout into three distinct drill
                                categories &#8211; skill, competitive, scrimmage &#8211; we will take a look at what
                                skills and subtleties to watch for in each of these three areas.</p>
                            <p><strong>Skill Drills</strong>: In order to properly run skill drills, you need a good
                                comprehension of the talent level you&#8217;re working with. At the younger and/or lower
                                caliber levels, your skills should focus on the basics: forward skating, backward
                                skating, cross overs, stopping, basic puck handling, basic shooting, and basic passing.
                                <span style="background-color: #ffffec;">The older and/or higher caliber the group, the more you should implement drills to force the performance of skills at a higher pace.</span>
                                Evaluators should watch for notable aspects (both good and bad) of each player&#8217;s
                                ability in the following categories: skating, shooting, passing, puck handling. Each of
                                these skills should be broken down into the appropriate drills for the level. For
                                example, to work on cross-overs (skating), you could break it down as follows:</p>
                            <p><em><span style="color: #888888;">Beginner Groups</span></em>: Basic cross-overs around
                                the circles &#8211; watch for basic technique and balance</p>
                            <p><em><span style="color: #888888;">Intermediate Groups</span></em>: Cross-overs around the
                                tops &amp; bottoms of the circles only (not all the way around) &#8211; watch for basic
                                technique, balance, speed, and ability to transition between cross-over directions.</p>
                            <p><em><span style="color: #888888;">Advanced Groups</span></em>: Have players skate down
                                the ice performing one cross-over to the right, then one cross-over to the left (repeat
                                the length of the ice) &#8211; watch for technique, speed, transition between
                                directions, ability to maintain balance/strong body position, and generation of power
                                with each push.</p>
                            <p><span style="background-color: #ffffec;">The most important skill to watch for during these drills is skating.</span>
                                If a player can skate well, the rest of the game gets a lot easier. Skating affects
                                every aspect of the game &#8211; from a foot race to a loose puck, to maintaining
                                balance in front of an opposing team&#8217;s net. Players who skate efficiently are
                                often times easier to work with when it comes time for positioning. While evaluating
                                skating abilities, be sure to <span style="background-color: #ffffec;">include drills that force players to change directions and move laterally</span>.
                                It doesn&#8217;t do any good to be the fastest skater from end-to-end if you aren&#8217;t
                                able to turn or change directions while maintaining your momentum.</p>
                            <p><strong>Competitive Drills</strong>: During competitive drills is where you look for your
                                work-horses. <span style="background-color: #ffffec;">Small area games and in-tight competitive drills often expose strengths and weaknesses quicker than any other types of drills</span>
                                since the players have no place to hide or blend in. One of my favorite drills to run
                                during tryouts is the <a href="http://hockeyshare.com/drills/drill.php?id=187">Corner
                                    Battles</a> drill. This simple one-on-one drill shows me right away who is willing
                                to mix it up in the corners. I also recommend running 1 on 1 drills to allow you to
                                isolate both forwards and defensemen. The <a
                                        href="http://hockeyshare.com/drills/drill.php?id=194">1 on 1 Full Ice</a>
                                drill is one of the oldest, most basic 1 on 1 drills there is &#8211; but it works
                                great. You get to see how the defensemen handle the puck, shoot, set their gap, and
                                handle the rush. You also get to see if your forwards are willing to get in front of the
                                net, have the necessary speed, have creativity, and the desire to fight through a check.
                                <span style="background-color: #ffffec;">Coaches shouldn&#8217;t be afraid to adjust the lines to get a desired matchup on rushes or battle drills.</span>
                                It&#8217;s a tryout, and you&#8217;re looking to see the level each player can compete
                                at.</p>
                            <p><strong>Scrimmage Time</strong>: This is an evaluator&#8217;s time to see if the notes on
                                a player thus far transfer into game-scenarios. It&#8217;s also the <span
                                        style="background-color: #ffffec;">time to find out which players have the much-coveted &#8220;hockey sense.&#8221;</span>
                                Hockey sense is (simply put) the ability to see the ice, properly anticipate plays, and
                                react accordingly. This portion of the tryouts often makes decisions for coaches even
                                more difficult. Many times, you can run into a player who has a great core set of
                                skills, but isn&#8217;t able to translate them into game scenarios. You may also run
                                into the opposite &#8211; a player with a weak core set of skills, but seems to get the
                                job done consistently. Which player you give preference to when choosing your team is
                                your own personal decision. In my opinion, I would rather take a player who can perform
                                during a game with a weaker core set of skills than one who has good skills but no
                                game-time performance &#8211; with one HUGE caveat: the player MUST have a strong
                                skating ability.</p>
                            <p>A couple final thoughts on the tryout process&#8230;</p>
                            <p>If you have multiple people evaluating, don&#8217;t be surprised if you have differing
                                opinions on players. Different people look for different things while they&#8217;re
                                evaluating. <span style="background-color: #ffffec;">I always recommend having neutral hockey-knowledgable evaluators assist in picking your team.</span>
                                By neutral, I simply mean they do not have a child on the ice being evaluated, and have
                                not coached the majority of the players in the past. Ideally, you get someone who has no
                                connection with any of the players on the ice. This removes a level of emotion from
                                decisions.</p>
                            <p>Finally, <span style="background-color: #ffffec;">accept the fact that you most likely will not please everybody</span>.
                                Don&#8217;t give in to threats of &#8220;if my kid doesn&#8217;t make the top team, we&#8217;re
                                taking him/her somewhere else.&#8221; To those situations, my typical response is
                                &#8220;do what you have to do.&#8221; While it may come off as terse, I firmly believe
                                coaches need to evaluate as fairly and impartially as possible. Coaches will often be
                                put in tough personal and political situations during tryouts. Personally, I&#8217;ve
                                had to cut board member&#8217;s players, cut friend&#8217;s players, and have had people
                                stop talking to me because of my decisions. Make your decisions with integrity and stick
                                to your guns.</p>
                            <p>Good luck to all the players and coaches going through the tryout process!</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-514 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-hockey-coaching tag-learn-from-the-pros tag-nhl"
                         id="post-514">

                        <h2><a href="http://hockeyshare.com/blog/learn-from-the-pros/learn-from-the-pros-week-1/"
                               rel="bookmark">Learn from the Pros &#8211; Week 1</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">9</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/learn-from-the-pros/"
                                                    rel="tag">Learn from the Pros</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/nhl/"
                                                    rel="tag">nhl</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I&#8217;ve decided to start a new weekly section called &#8220;Learn from the Pros&#8221;
                                which will feature plays from professional hockey. The idea is to take small pieces of
                                the game and break them down so they can be used as learning tools for coaches and
                                players. This week, we&#8217;ll look at two goals &#8211; the first from Andrew Ladd
                                against the St. Louis Blues, and the second from Justin Williams against the Anaheim
                                Ducks.</p>
                            <p>Each teaching keypoint will also include the time on the YouTube video to pause the clip
                                so you&#8217;ve got a freeze-frame of the the play developing. So, for example, if the
                                intended freeze-frame is at the 8 second mark, it will be denoted before the breakdown
                                in the following format: [0:08]. To scroll to that portion of the video, simply drag the
                                scrubber on the timeline to the desired time sequence. Note: the times are not always
                                exact, as sometimes you&#8217;ll get slightly different frames when you &#8220;scrub&#8221;
                                to the time you want. Use the time-markers as approximate spots where you can
                                start-and-stop the video to get a quality freeze-frame.</p>
                            <p><strong>Andrew Ladd &#8211; 4/7/10</strong></p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/OVaewlf7hwA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p>[0:06] This play starts out with Kris Versteeg getting up the ice ahead of his teammates.
                                Instead of attempting to drive the St. Louis defenseman to get a scoring opportunity, he
                                wisely buys time and space by stopping and using his body to protect the puck from the
                                defending player.</p>
                            <p>[0:07] The next Blackhawk into the zone is John Madden who drives in strong-side, giving
                                Versteeg an option to throw the puck down the wall. After Versteeg passes to the
                                trailer, Andrew Ladd, Madden drives to the net and is in great position to tip the puck
                                or play a rebound.</p>
                            <p>[0:08] Now, Versteeg makes an incredible pass (not one I&#8217;d recommend many players
                                try in anything other than pick-up hockey) to Andrew Ladd &#8211; BUT, the backchecking
                                defenseman (Mike Weaver) for St. Louis makes two critical mistakes: 1) he fails to
                                identify Ladd as a scoring threat 2) he turns his back on the puck in favor of looking
                                at Byfuglien to cover.</p>
                            <p>[0:10] This creates a ton of space for Ladd to shoot. Weaver is now forced to play the 2
                                on 1 rush from the weak-side post.</p>
                            <p>[0:36] Ladd releases a quick snap-shot off one foot. The key here is the quick release,
                                there is no big wind-up, and only a single stickhandle to release the puck. In this
                                freeze-frame, you can clearly see he has his head up the entire way and is looking for
                                open parts of the net to shoot at.</p>
                            <p><strong>Justin Williams &#8211; 4/6/10</strong></p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/0vXdo85S1mg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">[0:06] Williams enters the zone on a 2 on 2 rush with a
                                backchecker. Technically, LA is out-manned in this rush. The opportunity begins by Ducks
                                Defenseman Steve Eminger having given Williams a bit too much space as he crossed the
                                blue line. Eminger needed to have a tighter gap when the rush entered the zone. Instead,
                                he&#8217;s still about two stick-lengths away from Williams as he gains the blue line.
                                This allows the forwards to criss-cross and open up space.</p>
                            <p style="text-align: left;">[0:08] The Ducks backchecker, Saku Koivu, gets caught reaching
                                for the puck. At this point, Williams now has body position established on Eminger, and
                                the Anze Kopitar is driving toward the net, bringing his defenseman with him.</p>
                            <p style="text-align: left;">[0:10] Eminger is forced to make a dive in desperation. Kopitar
                                drove the net going to the far post, bringing his defender with him, which opens up ice
                                in front of the goaltender.</p>
                            <p style="text-align: left;">[0:34] Ducks goalie Curtis McElhinney plays the shot, but is
                                still outside the crease, leaving Williams with room on the short-side to reach around
                                him and stuff the puck in.</p>
                            <p style="text-align: left;">
                            <p style="text-align: left;">I hope you enjoy this new section. If you have plays you&#8217;d
                                like to see broken down, find me a clip on YouTube and leave them in the comments.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-487 post type-post status-publish format-standard hentry category-hockey-drills category-hockey-instructional-video tag-edgework tag-hockey-coaching tag-power-skating"
                         id="post-487">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/power-skating-edgework-circuit-video/"
                               rel="bookmark">Power Skating / Edgework Circuit [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">31</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/edgework/" rel="tag">edgework</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/power-skating/"
                                                    rel="tag">power skating</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>After a couple of crazy weeks, we&#8217;re back with new video from M2 Hockey &amp;
                                HockeyShare! This week we wanted to do something a little bit different than normal.
                                This week&#8217;s video is a skating circuit that&#8217;s meant to be gone through in
                                succession without rest. As players become better skaters and get more comfortable with
                                the drills, the tempo can be increased, and modifications can be made to the circuit.
                                These fundamental edgework drills are the foundation for almost every skating maneuver
                                in hockey &#8211; so you can never get enough practice on them. For those who don&#8217;t
                                think power skating is important, here&#8217;s a sobering fact: NHL teams do power
                                skating!!! The more efficient you become on your skates, the better overall hockey
                                player you will become. Hope you enjoy the video!</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/YtwM1T3_x6M?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-467 post type-post status-publish format-standard hentry category-comments-thoughts tag-hockey-coaching tag-youth-hockey-coaching"
                         id="post-467">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/favorite-players/"
                               rel="bookmark">Favorite Players</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">24</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>If you&#8217;ve ever heard a coach (in any sport) tell you they don&#8217;t have favorite
                                players, there&#8217;s a pretty good chance they&#8217;re lying to you. I believe every
                                coach has favorite players. Why would a coach have a favorite player(s)? The answer is
                                easy really. Coaches are human. What coach wouldn&#8217;t prefer to work with players
                                who:</p>
                            <p>1) Listen<br/>
                                2) Work Hard<br/>
                                3) Are Respectful<br/>
                                4) Are Well-Behaved<br/>
                                5) Have Good Attitudes</p>
                            <ol></ol>
                            <p>Any coach would take players like these in a heart-beat. It&#8217;s not hard to see why
                                players who don&#8217;t meet one or more of the above criteria may cause frustration for
                                the coach. Is it easier to be around and work with a player who gives 100% every shift,
                                or one that goes out and skates half-speed and takes bad penalties? Easy answer,
                                right?</p>
                            <p>Here is the key in dealing with your players. All of them &#8211; favorite or not &#8211;
                                need to be treated equally. Rules need to be enforced the same amongst all the players
                                from top to bottom. This isn&#8217;t to say your approach with each player should be
                                identical, but guidelines need to be set for behavior, and whether or not your favorite
                                player or least favorite player violate a guideline, the punishment needs to be equal.
                                If the &#8220;punishment&#8221; for being late to a team function is to sit the first
                                period of the next game, the rule needs to be enforced with each and every person.</p>
                            <p>It&#8217;s OK to have favorites &#8211; just be aware of how you treat them, and be sure
                                you&#8217;re holding them to the same standards you&#8217;re holding your &#8220;least
                                favorite&#8221; player to. If there is a big inconsistency, you&#8217;ve got a sure-fire
                                formula for disaster within your own team. Players will begin resenting each other,
                                parents will turn on you, and you&#8217;ll have a lot less fun coaching.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/tag/hockey-coaching/page/2/">Older Posts &raquo;</a>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

