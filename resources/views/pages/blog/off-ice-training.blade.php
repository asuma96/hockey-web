@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-965 post type-post status-publish format-standard hentry category-comments-thoughts tag-dryland tag-off-ice-training"
                         id="post-965">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/summer-training-thoughts/"
                               rel="bookmark">Summer Training Thoughts</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The summer is here, and many players are now enjoying the &#8220;off-season.&#8221; This
                                is the time of year where good players become great. This time of year separates the
                                players who are serious about the game, and those who are not. I can&#8217;t tell you
                                the number of times I&#8217;ve been asked &#8220;what should my player do this summer&#8221;
                                &#8211; and I&#8217;m sure many other coaches out there hear it all the time. I wanted
                                to share some quick thoughts on how to approach the off-season.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/summer-training-thoughts/#more-965"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-904 post type-post status-publish format-standard hentry category-cool-links tag-dryland tag-off-ice-training tag-off-season-workout"
                         id="post-904">

                        <h2><a href="http://hockeyshare.com/blog/cool-links/off-ice-training-for-the-off-season/"
                               rel="bookmark">Off-Ice Training for the Off-Season</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">15</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/cool-links/"
                                                rel="category tag">Cool Links</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-season-workout/"
                                                    rel="tag">off-season workout</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>It&#8217;s no secret that training during the off-season can make you a better hockey
                                player. The issue most players/coaches have is they don&#8217;t know WHAT to do. Friend
                                and fellow hockey training professional Jeremy Weiss has put together a program called
                                S3, and has released <strong><span
                                            style="text-decoration: underline;">FREE</span></strong> videos to explain
                                what he&#8217;s got. You can watch the first video of his by clicking the link below.
                            </p>
                            <p><span style="color: #ff0000;"><strong><a
                                                href="https://www.e-junkie.com/ecom/gb.php?ii=592553&amp;c=ib&amp;aff=143742&amp;cl=79043"
                                                target="_blank">Watch the first Video Here</a> &#8211; Weight Lifting &amp; Periodization for Hockey</strong></span>
                            </p>
                            <p>&nbsp;</p>
                            <p><span style="color: #999999;">Note: Yes, this is an affiliate link, and I do get a percentage if you end up purchasing the product.  However, as many of you already know, I don&#8217;t promote products I don&#8217;t believe in or haven&#8217;t seen.  I&#8217;ve seen the product he&#8217;s  put together and think it&#8217;s one of the best out there right now. </span>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-832 post type-post status-publish format-standard hentry category-hockey-drills category-hockey-instructional-video tag-dryland tag-hockey-instructional-video tag-off-ice-training"
                         id="post-832">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/"
                               rel="bookmark">Agility Ladder Drills for On-Ice Quickness</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">14</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We put together a new video featuring three of our favorite off-ice agility ladder drills
                                to help players develop their quickness and foot speed.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/#more-832"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-624 post type-post status-publish format-standard hentry category-hockey-drills tag-dryland tag-off-ice-training tag-stickhandling tag-usa-hockey"
                         id="post-624">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/20stickhandling-drill-videos/"
                               rel="bookmark">20 Stickhandling Drill Videos</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/" rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/usa-hockey/" rel="tag">usa hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Sometimes some of the best content on the web is hard to dig up. While looking for some
                                off-ice stickhandling drills, I came across a 20-part video series offered free online
                                by USA Hockey. This series is part of their National Team Development Program training.
                                The videos do a nice job of explaining the drills and showing the execution. The videos
                                can be found at the following link:</p>
                            <p><a href="http://www.usahockey.com/Template_Usahockey.aspx?NAV=CO&amp;id=19434"
                                  target="_blank">http://www.usahockey.com/Template_Usahockey.aspx?NAV=CO&amp;id=19434</a>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-534 post type-post status-publish format-standard hentry category-comments-thoughts tag-dryland-training tag-hockey-training tag-off-ice-training"
                         id="post-534">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/how-to-approach-off-season-training/"
                               rel="bookmark">How to Approach Off-Season Training</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">12</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland-training/"
                                                    rel="tag">dryland training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-training/"
                                                    rel="tag">hockey training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Often when the season wraps up, I&#8217;ll get asked the question: &#8220;Coach, what do
                                I need to work on in the off-season?&#8221; I usually respond to this by reversing the
                                question and asking them what they think they need to work on. When they look at me with
                                a blank stare, I usually try to guide them through a series of questions to help
                                evaluate their play and identify &#8220;holes&#8221; in their game &#8211; and of course
                                share my feedback with them after they&#8217;ve started to identify things for
                                themselves. The off-season provides a great opportunity for committed players to develop
                                and improve their fundamental skills. I believe players need to approach off-season
                                training in the following sequence:</p>
                            <p><strong>1) Players must identify the areas of their game they want to improve</strong>.
                                This needs to be done with honest self-evaluation. Most older players know the areas
                                they struggle with &#8211; younger players will need to be given more guidance. A list
                                of two or three skills to improve is typically more than enough. Identifying too many
                                areas is a recipe for disaster and disappointment.</p>
                            <p>2) Once players have their weaknesses, they should <strong>work with their coaching staff
                                    to come up with a training regiment</strong>. This doesn&#8217;t necessarily mean
                                the coach should be giving out complete summer training programs for each player
                                (although, this would be great), but most coaches will be more than happy to share ideas
                                on ways to train.</p>
                            <p>3) <strong>Players need to follow through consistently on the plan</strong>. If a player
                                improves just a little bit every day, the results/success will compound. Repetition is
                                the mother of skill &#8211; get out there and <span style="text-decoration: underline;">consistently</span>
                                take action! It is OK to take days off &#8211; in fact, depending on the training, it
                                may be better for the body to take time off.</p>
                            <p>4) <strong>Players should seek educated instruction/direction during their
                                    training</strong> for both safety and habit reasons. If someone takes 10,000 shots
                                with terrible technique, they&#8217;re not doing a lot other than re-enforcing bad
                                habits. However, if he or she take 10,000 shots with some guidance, instruction, and
                                correction &#8211; they stand a much better chance of developing their skills.</p>
                            <p>5) <strong>Identify on-ice training opportunities to evaluate how the training is
                                    working</strong>. Many areas of the game can be worked on off-ice: stickhandling,
                                shooting, foot speed, balance, hand-eye coordination, acceleration, power, reaction
                                time, etc. However, it is a good thing to get players on the ice from time-to-time to
                                see how their training is working. This can be something as simple as open hockey or a
                                public skate.</p>
                            <p>6) <strong>Work hard <span style="text-decoration: underline;">and</span> smart</strong>.
                                One thing a lot of players get wrong is mistaking hard work for smart work. Players must
                                understand &#8211; working hard on the wrong things won&#8217;t help once the season
                                rolls around! Players who come in bragging about how much they improved their bench
                                press or bicep curl have indeed wasted a lot of time and energy.</p>
                            <p>The depth players should take these guidelines depends on age and competition level. An
                                eight year-old obviously should not be doing an intense weight program &#8211; but
                                instead occasionally stickhandling or shooting pucks in the driveway will go a long way.
                                Midget players with aspirations of continuing their career, on the other hand, should be
                                looking to be hitting a weight room and getting on the ice more frequently to hone their
                                skills. Not sure what to work on? You can never go wrong with working on skating
                                technique!</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-308 post type-post status-publish format-standard hentry category-hockey-drills tag-dryland-training tag-hockey-training tag-off-ice-training"
                         id="post-308">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-drills/off-ice-drills-to-develop-quick-feet/"
                               rel="bookmark">Off-Ice Drills to Develop Quick Feet</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Feb</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">5</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland-training/"
                                                    rel="tag">dryland training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-training/"
                                                    rel="tag">hockey training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p><img class="alignright size-full wp-image-309" title="Quick Feet"
                                    src="/img/fast_feet.jpg"
                                    alt="" width="150" height="131"/>Developing &#8220;quick feet&#8221; is essential
                                for hockey players. Players with quicker feet can typically accelerate, turn, and
                                re-accelerate faster than other players &#8211; giving them a distinct advantage. One of
                                the best and most simple tools we&#8217;ve come across is the <a
                                        href="http://www.power-systems.com/p-2361-dot-drill-mat.aspx?affId=95646"
                                        target="_blank">Dot Drill Mat</a>. It&#8217;s basically just a thick rubber mat
                                with dots drawn on it (photo below). You can certainly draw the pattern on the ground
                                with chalk, but I will say it&#8217;s very handy to have the actual mat. We have them in
                                our workout facility for our team, and use them on a weekly basis.</p>
                            <p>Here are some resources we&#8217;ve found from around the web. Hopefully these resources
                                give you some new and fresh ideas when working with your team to develop quick feet.</p>
                            <p><a href="http://tinyurl.com/y8n2unz" target="_blank">http://tinyurl.com/y8n2unz</a></p>
                            <p><a href="http://tinyurl.com/yclrlz5" target="_blank">http://tinyurl.com/yclrlz5</a></p>
                            <p><a href="http://tinyurl.com/y9ddglw" target="_blank">http://tinyurl.com/y9ddglw</a></p>
                            <p>If you&#8217;re interested in purchasing a Dot Drill Mat, I would recommend getting them
                                through Power Systems. Here is the product link: <a href="http://tinyurl.com/yfhoom2"
                                                                                    target="_blank">http://tinyurl.com/yfhoom2</a>
                            </p>
                            <p style="text-align: center;"><a
                                        href="http://www.power-systems.com/p-2361-dot-drill-mat.aspx?affId=95646"
                                        target="_blank"><img class="alignnone size-full wp-image-310" title="30700"
                                                             src="/img/30700.jpg"
                                                             alt="" width="189" height="250"/></a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

