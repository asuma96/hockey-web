@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">

                    <div class="post-1077 post type-post status-publish format-standard hentry category-comments-thoughts tag-opinion tag-usa-hockey"
                         id="post-1077">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/"
                               rel="bookmark">Your Opinion: USA Hockey Age-Specific Coaching Modules</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/opinion/"
                                                    rel="tag">opinion</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/usa-hockey/" rel="tag">usa hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Those coaching youth hockey in the US this season were faced with new requirements in
                                order to maintain certification. Coaches &#8211; regardless of level or experience
                                &#8211; are now required to take an age-specific online module in addition to the
                                regular certification requirements.</p>
                            <p><span id="more-1077"></span></p>
                            <p>Here is a quick overview of the modules and new requirements:</p>
                            <ul>
                                <li>Video-based sections</li>
                                <li>Quizzes after most sections</li>
                                <li>Users must re-watch videos if the quiz is not passed with a 60% or greater score
                                </li>
                                <li>Takes approximately 8 hours to complete</li>
                                <li>Once you begin a module, you have 30 days to complete it (although it must be
                                    complete prior to Jan 1 of the current season)
                                </li>
                                <li>Coaches coaching multiple age levels must take the age-specific module for each
                                    level he/she is coaching
                                </li>
                                <li>This is a one-time requirement per age-group</li>
                                <li>Modules cost approximately $10 per age-group</li>
                            </ul>
                            <p>These new requirements have triggered a variety of responses &#8211; <strong>our question
                                    to you is:</strong></p>
                            <p><strong><span style="text-decoration: underline;"><span
                                                style="color: #ff0000; text-decoration: underline;">What do you think of these new requirements?</span></span></strong>
                                Good? Bad? Changes need to be made? Let us know what you think!</p>
                            <p><em>Leave a comment below to chime in to the conversation&#8230;</em></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <div>46 Comments
                    </div>
                    <br/>

                    <div class="hockeyshare_comment authcomment" id="comment-1207">

                        <div class="hockeyshare_comment_by"><a href='http://hockeyshare.com' rel='external nofollow'
                                                               class='url'>Kevin</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 3:50pm</div>

                        <p>My personal opinion&#8230;.</p>
                        <p><strong>The good</strong>: I believe this is a step in the right direction in helping improve
                            the level of coaching across the nation. The modules do contain good information. What I
                            really like to see is USA Hockey is finally starting to leverage video.</p>
                        <p><strong>The bad</strong>: The program launched right in line with the hockey season &#8211;
                            making finding the time for the modules difficult. It would have been much better to have
                            launched these during the summer when things are less hectic.</p>
                        <p>Those coaches having to take multiple age-specific modules are forced to sit through roughly
                            85% of the same content. The amount of material that changes between modules seemed very
                            minimal.</p>
                        <p><strong>The scary</strong>: How many good coaches will we lose because they don&#8217;t want
                            to or have the time to complete the age-specific modules.</p>
                        <p><strong>Bottom line</strong>: In my opinion, USA Hockey should leverage the video content and
                            make it FREELY available to users. Coaches who truly want to become better coaches will seek
                            out the information on their own (if it&#8217;s easily accessible). I believe this program
                            is a great start, but still has some kinks to be worked out. </p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1208">

                        <div class="hockeyshare_comment_by">bob stachowiak Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:11pm</div>

                        <p>I too have to do 2 modules. Pain, Repeating and it is time consuming.<br/>
                            I love the content. I have been using the same ideas for 18 years. This verifies that I been
                            on the right path.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1209">

                        <div class="hockeyshare_comment_by"><a href='http://HockeyShare' rel='external nofollow'
                                                               class='url'>Bob</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:13pm</div>

                        <p>Looks a little like a money thing.Eight hours to complete and we are in the heart of the
                            first half of the season, not good planning. If the coaches have been certified are they
                            really going to learn anything more? Good coaches are always looking and researching, is
                            this going to make a bad coach better? Not likely. Why not have a spring and fall seminar
                            where a large group gets together and its done. Where are the parent mandatory modules?</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1210">

                        <div class="hockeyshare_comment_by">Howard Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:36pm</div>

                        <p>I&#8217;d do it for sure. Skills vary as players become older, and drills are designed to
                            further challenge the player. Having a database of age-appropriate drills is a good idea.
                            And $10 isn&#8217;t alot of money. 8 hours is time-consuming, but maybe there is a way to
                            save info and do it bit by bit. The only thing these age modules don&#8217;t teach, or wont
                            teach, is how in 2011 a Midget player is quite different thatn a Midget player of 25 years
                            ago. Attitude changes because of environment changes put a coach on their toes. One needs to
                            have same-aged children at home to be able to understand their dynamics, so they can handle
                            a team of Midgets for 7 months.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment authcomment" id="comment-1211">

                        <div class="hockeyshare_comment_by"><a href='http://hockeyshare.com' rel='external nofollow'
                                                               class='url'>Kevin</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:39pm</div>

                        <p>@Howard &#8211; I agree that $10 isn&#8217;t a bad price for the return. Also, to clarify,
                            you are able to complete the module section by section at your own pace, as long as it is
                            completed within 30 days.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1212">

                        <div class="hockeyshare_comment_by"><a href='http://www.IceHockeyDrills.Info'
                                                               rel='external nofollow' class='url'>Coach Nielsen</a>
                            Says:
                        </div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:39pm</div>

                        <p>I hate to say any training isn&#8217;t a good idea, but this seems like a money grab to me.
                            USA hockey has so many level 4 coaches that no longer have to take classes that it feels
                            like they needed a way to reach in their pocket one more time. I&#8217;ve been coaching for
                            27 years and have avoided the level 5 certification process because I thought it was just
                            another money grab. What&#8217;s next, mandatory level 5 certification? Yearly video
                            certification?</p>
                        <p>I lost a good coach on my team because he doesn&#8217;t have the time to watch the videos and
                            two of my other coaches have yet to take the course and may not due to time constraints.
                            What happens in January if none of my assistant coaches have taken the exam? Am I left to
                            coach on my own? Most coaches are volunteers and give enough of their time already, so
                            asking guys to take this course even though they have reached level 4 certification which in
                            the past meant no more certifications necessary is just unfair.</p>
                        <p>There was an entire section in the 18 module about nutrition. I asked USA hockey to make that
                            section available to all parents for free because they have far more control over nutrition
                            than I do and the head of USA hockey told me it was only for the coaching seminar or I could
                            hold a parents meeting and show them the nutrition section.</p>
                        <p>Again, training is a good thing but good coaches are always trying to learn everything they
                            can and bad coaches are just bad coaches. Not sure an online video course is going to make a
                            big difference. At least it was only $10.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1213">

                        <div class="hockeyshare_comment_by">Rick Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:58pm</div>

                        <p>I agree in part that it&#8217;s about the money. I also think that good coaches will be lost.
                            I coach a high school and a youth team. I do not need to take the modules for high school.
                            To be honest I may not take them for the youth team because of time. I am a physical
                            education teacher, I regularly attend clinics, look up information on various web-sites talk
                            and learn from other coaches etc. I have a fairly extensive coaching and playing background
                            and think that the requirements are excessive. Why not have a certification test that
                            coaches can take and uf they qualify not have to go through the modules? </p>
                        <p>On another note I think USA hockey has missed the boat on a few things because they are too
                            hung up on trying to develop ultra skilled kids rather than hockey players. They would
                            rather follow the European model than the best in the world just to our north. The no tag up
                            off-sides and no check are absolutely absurd and will ultimately hurt the game. The no check
                            was for money because more kids drop the game following the Pee Wee major year. Their
                            thought&#8230;get rid of checking keep more kids. Kids who are going to drop out because of
                            contact will still drop out after playing with it for a year. I also believe more kids drop
                            out at that age for personal reasons &#8211; other sports, school, girls, no longer want the
                            commitment etc. Incidentally, I also think its ridiculous that USA hockey should not be
                            telling coaches or parents at what age kids should specialize as they do in the module.<br/>
                            Ultimately I understand the concept but I think it needs more thought and some
                            modifications.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1214">

                        <div class="hockeyshare_comment_by">Skip Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 4:59pm</div>

                        <p>Waaayyyyy too long. All training is good I agree. But on the surface this just looks to be
                            another fund raiser for USA hockey. Do the math. Huge money maker to support a growing
                            beauracracy. </p>
                        <p>Lots of good information but&#8230;&#8230;</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1215">

                        <div class="hockeyshare_comment_by">Coach BJ Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 5:13pm</div>

                        <p>At first thought,I thought it was going to be a waste of time and I would learn nothing, and
                            also had my opinions on the money grab. In the end, my opinion changed, the videos were
                            quick, concise, easy to follow and gave good info. The time spent doing this was very
                            minimal. I have been coaching for a long time, I would rather there be some barriers to
                            coaching than just let anyone without any knowledge teaching hockey the wrong way to our
                            kids. I&#8217;ve seen it ruin programs for years. USA Hockey provides many great programs
                            for kids across the country that develop interest in this great game. These programs don&#8217;t
                            run themselves and they cost real money.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1216">

                        <div class="hockeyshare_comment_by">Dan Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 5:26pm</div>

                        <p>I have only been coaching for 7 years. Started in midget and High school, and now back with
                            mites. I was looking forward to getting my level 4 next year, but after these modules I
                            think I am done coaching youth hockey. Time consuming, the money. I know it&#8217;s only
                            $10. Add USA hockey membership, background checks, and your time. It&#8217;s getting a
                            little absurd. I never thought doing something that I enjoy so much, would nickel and dime a
                            person so much. The smile on the kid&#8217;s faces is more than enough pay for this
                            volunteer, but the coach isn&#8217;t smiling anymore.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1217">

                        <div class="hockeyshare_comment_by">jim Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 5:40pm</div>

                        <p>Seems to be a money thing for USA hockey&#8230;all the trainings in the world doesn&#8217;t
                            and won&#8217;t make you goodg with the kids or a good coach&#8230;ensuring coaches have the
                            best interest at heart is needed.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1218">

                        <div class="hockeyshare_comment_by">Michael Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 6:18pm</div>

                        <p>While I applaud the effort USA Hockey is making in offering another source of information and
                            further educating volunteers with pertinent and insightful information, the idea of making
                            it, mandatory or else, is rather off putting and perhaps a bit absurd, after all, where
                            would USA hockey be without the volunteers? </p>
                        <p>The content certainly is informational however the required time commitment is completely off
                            the mark. Make the information available to anyone interested and allow for them to work
                            through the material at their own pace. </p>
                        <p>A standard curriculum and progression of drills through each age category as a basis would
                            likely prove more beneficial for new coaches. Providing additional insight as to the nuances
                            of the game and offering ideas surrounding situational play would move beyond the standard
                            drill, drill, drill mentality. Utilizing past playing professionals insights as to what
                            players should be thinking and how players should approach game situations would do more to
                            further coaches ability to relay important learning points of the game to players.</p>
                        <p>Good effort but perhaps a bit misguided and not completely thought through with regard to the
                            final impact on the volunteers of the game!</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1219">

                        <div class="hockeyshare_comment_by">Steve Walsh Says:</div>
                        <div class="hockeyshare_comment_time">November 17th, 2011 a30 6:36pm</div>

                        <p>I have spent a lot of my personal time and money to continually improve as a coach. I am USA
                            Hockey level 5 and always continue to attend coaching seminars when they become available. I
                            was not at all pleased to find out about these online modules when they came out. They are
                            very time consuming and yes, they are quite repetitive. The only thing that I believe truly
                            came out of this is that it puts all of the assistant coaches on the same page as the head
                            coach. By this I mean that what we are teaching to our specific age group is AGE
                            APPROPRIATE. I have been more frustrated over the years by coaches trying to impose their
                            last college or high school practice on 7-10 year old kids. USA Hockey is far from perfect
                            and I certainly have differences of opinions at times with their decisions. At the end of
                            the day, they are light years ahead of any other youth sports organization in my opinion.
                            Thank you.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1220">

                        <div class="hockeyshare_comment_by">Artemus Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 12:19am</div>

                        <p>I decided to become a coach after watching many Canadian players screaming and yelling at
                            local kids; not teaching or growing the sport but alienating new kids from the local hockey
                            program. After completing several modules of the USA online training, I feel that it truly
                            reinforces the concept that hockey is about the kids and not ones personal frustrations of
                            not making it to the NHL. So, Kudos for USA Hockey and hopefully, this is a step in the
                            right direction in growing the sport of hockey.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1221">

                        <div class="hockeyshare_comment_by">Coach in Colorado Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 12:20am</div>

                        <p>I am really frustrated and disappointed in USA Hockey. I live in the Colorado Springs area
                            where the USA Hockey is Head Quartered and I have seen youth hockey in the area just spiral
                            down. Most recently the ADM model and now these Modules. I personally feel that the modules
                            are about making money. I found most of the content not that big of a deal &#8212; nothing
                            special. Some of it was actually poor. And some modules really should be directed at
                            parents. After I completed the modules I went back to look at one of them, but since I
                            completed the modules I could not watch them anymore. And one of those I wanted to show
                            parents I was responded to that I could purchase the DVD of that module – so what did I pay
                            for? And did I need to spend all that time to pick up a few facts that might useful – it
                            wasn’t worth the time or the money.</p>
                        <p>Prior to this year I have coached kids at all age groups. I volunteer my time, but I do not
                            have the time to do all the modules let alone fork over $10 for each age group. USA hockey
                            is chasing me and other coaches away.</p>
                        <p>Go look at the Pointstreak.com rankings the last few years of the hockey association in
                            Colorado Springs that is incubator for USA Hockey (csaha.com). They are bottoms dwellers at
                            almost every team in the state league. And now the association is trying to move their top
                            programs under a AAA umbrella to justify why they are not using the ADM model. Not only is
                            ADM hurting kids, but they are hypocrites for what they are really doing. The better kids
                            are driving an hour or more to Denver. Parents I talk to are really frustrated with the ADM
                            model and don’t support it. But I think this is also about money, as USA Hockey said they
                            want to keep more kids in hockey and the ADM will keep kids playing longer, especially at
                            the Bantam age. But all this is doing is dumbing down hockey. Making it so weaker kids can
                            supposedly compete. But kids keep score, kids want to compete, and they want to play at the
                            best level they can. Stronger kids move up, weaker kids move down. And the same is true for
                            coaches and these modules are not going to make mediocre coach a good coach.</p>
                        <p>Oh, by-the-way it is the Bantam/Midget age that kids quit hockey and all other sports or
                            interests or hobbies. Kids mature, they discover other interests, the get more independence
                            especially cars, and other distractions like girls. </p>
                        <p>USA Hockey is trying to social engineer the game for their profit. I expect the membership
                            numbers to go down, or the ADM program to fade away. And I hope the modules follow suit.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1223">

                        <div class="hockeyshare_comment_by">Joel Prive Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 5:35am</div>

                        <p>I have completed 2 modules, squirt and peewee. Some was identical, which was a waste of time
                            however most of this was good info even the hour on nutrition. It creates awareness and
                            should open a coaches mind to all aspects of the game and player. The way hockey is coached
                            in the US needs to change. Drills with kids standing in line are old school, how many people
                            still use wood sticks? We adopted these new methods two years ago with great success, team
                            went from a 2-24 season to a 36-8. Yes it needs some improvment but I believe all coaches
                            willing to learn and change will be better coaches after completing these. As far as 10
                            bucks most parent coaches have probably dropped 100+ for their 10 year olds stick, 250 in
                            skates, 85 for some ridiculous replica jersey, the list goes on so not really an issue.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1224">

                        <div class="hockeyshare_comment_by">Dave Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 7:36am</div>

                        <p>I like the idea and content to help coaches gain a better understanding of how to
                            commmunicate with the different age groups. I am personally struggling with making 8 hours
                            available to fit these in between my full time job and coaching approximatly 20 hours per
                            week and weekend.</p>
                        <p>One thing USA Hockey could consider is creating several practice plan templates for the
                            various age groups. I know in most of the Coaching seminars and conversations with other
                            Coaches, the primary focus is trying to develop good, meaningful practice plans that help
                            develop the players. New Coaches have a tendency to emulate drills they have seen from other
                            Coaches which may not be applicable for the age group they coach at all. I know in our
                            organization, we utilize the ADM program, but the Coaches were never coached as to how to
                            impliment the program.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1225">

                        <div class="hockeyshare_comment_by">Pops Ryan Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 7:45am</div>

                        <p>Do I ever wish that I had the answer to this one. What I do know is that true purpose of USA
                            Hockey is to develop the NDP and other elite level programs. For the rest of us, we get a
                            crumby magazine a few times a year, and&#8230;.they teach you how to memorize the lyrics,
                            but come no where close to teaching you how to write the music. Again, I do not have the
                            answer on how to do this &#8211; only know that USA Hockey is basically a joke.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1226">

                        <div class="hockeyshare_comment_by">Mike S. Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 8:04am</div>

                        <p>$$$$$$$$$$$ for sure&#8230;</p>
                        <p>Is what I thought from the begining..</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1227">

                        <div class="hockeyshare_comment_by">Kevin S Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 8:23am</div>

                        <p>I thought the video modules were very good and i think USA hockey should do more NON REQUIRED
                            video training, where a coach can visit a video library on different topics at his/her
                            choice. I agree with most of the comments, I think the timing was tough w/ the season
                            already underway and mult age groups modules are too repetitive.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1228">

                        <div class="hockeyshare_comment_by">Charlie G Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 8:37am</div>

                        <p>Good information, but it could have been conveyed in half the time. Presenters spoke as if we
                            all had only a 3rd grade education. Found myself falling asleep during some of the modules
                            (and I still answered everything correctly).<br/>
                            We are all volunteers and have to already balance work, family life and coaching. Now, add
                            this 8-10 hours in that I personally had to spread out over a month&#8217;s time and my wife
                            is none too happy. We will lose good coaches if USA Hockey continues to make these
                            mandatory. Why not weed out the bad coaches via annual seminar&#8217;s and testing? Could be
                            done in the spring/summer by each section.</p>
                        <p>As ACE coordinator for my organization, I am regularly researching and providing info to my
                            coaching colleagues and this is just a little overkill.</p>
                        <p>Agree with all the comments regarding those who coach multiple age groups. Way to
                            redundant.</p>
                        <p>Although $10 is pocket change, do they really need to hit us up for this? Again, we are
                            VOLUNTEERS, we don&#8217;t get paid to do this, why charge us for a MANDATORY course. I&#8217;d
                            rather see them up the annual USA Hockey fee by $5 for everyone to pay for this.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1229">

                        <div class="hockeyshare_comment_by">David McBride Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 8:50am</div>

                        <p>The time it takes to go over the material is to much. I live in a rural area and do not have
                            access to high speed internet. So I get to watch 20 seconds at a time with about a one
                            minute load time to watch that 20 seconds. I am going throug the 12U module and I have 7
                            hours into it and I am only through section 7 with 11 more to go.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1230">

                        <div class="hockeyshare_comment_by">Tyler Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 9:25am</div>

                        <p>I think the information is a good start. As someone who&#8217;s main sport is soccer-believe
                            it or not- USA hockey is way ahead of soccer on the coaching tools and education materials
                            for coaches, especially for players at the younger ages.<br/>
                            I recently took my level 3 and the 14U module I am in the middle of, is almost exactly the
                            same as the level 3. I am re-watching many of the videos they showed in the level 3
                            training.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1231">

                        <div class="hockeyshare_comment_by">Brandon Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 10:20am</div>

                        <p>I am not part of the USA hockey program but was interested to read on this topic. Hockey
                            Canada used to require coaches clinics to include a practical book you had to hand in within
                            the couple of months following. The community level needed practice plans and game plans and
                            parent meetings. It was a lot of work and really didn&#8217;t get handed in a lot. They took
                            that out this year, and the course was enough. At the development level (AA&amp;AAA) most of
                            the work book was completed in class and it was stat taking to identify trends and skills.
                            Pretty simple. We are stepping away from more work. With these courses hockey Canada DVDs
                            are handed out to watch on your own time. They cover all kinds of skills and are very
                            useful. </p>
                        <p>I am one of the guys who can spend his whole days in my hockey work and happily do it and
                            then spend my whole night doing more. But I do understand many coaches are in a bind for
                            time, and adding these videos sounds tough. I wonder if they had released them earlier in
                            the year (July) as is suggested above if they would be easier for coaches to work with. I
                            would love to get my hands on these videos just to see the ideas out there and what approach
                            USA Hockey has. I think the cost is reasonable, hockey Canada doesn&#8217;t offer a course
                            under $130. Most associations here will pay for those kinds of coaching expenses.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1232">

                        <div class="hockeyshare_comment_by">Kevin Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 1:08pm</div>

                        <p>I agree with all the comments on the timing of this program as it delivered far too late and
                            also given the length of the program the 30 day window is too short. USA Hockey needs to
                            restructure this program into an overall set of general information with smaller age
                            appropriate sections. If you are a volunteer that coaches 2 age groups there shouldn&#8217;t
                            be any incremental cost for the additional age group section. In fact you should be
                            complemented for taking the time to review other age appropriate sections. The material is
                            worth reviewing but the majority of coaches are volunteers and taking 8-16 hours for 1 to 2
                            modules in a 30 day timespan is ridiculous. Why should USA Hockey punish coaches with full
                            time jobs by pulling them off the bench? They need to put more thought into this because at
                            the end of the day this felt like a way to generate $&#8217;s without thinking about the
                            impact on the coaches that love this game and are there to help kids learn to love the game
                            as well. By the way does anyone know what next year&#8217;s modules look like? Another 8-10
                            hours of the same material?</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1233">

                        <div class="hockeyshare_comment_by">Terry Barber Says:</div>
                        <div class="hockeyshare_comment_time">November 18th, 2011 a30 1:29pm</div>

                        <p>I think the material is excellent but too much to try to complete in the three months USA
                            Hockey gave us to complete it for this season especially during hockey season. I think they
                            should make it a year long process or break it down into pieces for each year.</p>
                        <p>One concern is that in a rural area where I live, it is already hard enough to find coaches.
                            One of my young assistant coaches told me that if he knew how much time this would have
                            taken, he would have not told me he would help coach this year. He could be a great future
                            coach but USA Hockey is making it hard for young coaches with the extensive requirements. I
                            understand the need for them, but can they be implemented in a more reasonable fashion?</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1236">

                        <div class="hockeyshare_comment_by">Tony D Says:</div>
                        <div class="hockeyshare_comment_time">November 21st, 2011 a30 12:37pm</div>

                        <p>As a coach doing multiple levels Mite &#8211; Bantam and Midget some of the sections are
                            repetative such as the Concussion, Nutrition, Drugs and Alcohol.<br/>
                            I feel that if you have done one of these sections in a prior module you should not have to
                            sit through it again, never mind 3, 4 or even 5 times.<br/>
                            As others have said more time to phase it in would have been nice.</p>
                        <p>As Coaching director for our Club (which we are all Volunteers) the added cost (even though
                            it is only $10.00) of having 75+ coaches reimbursed adds up.<br/>
                            In a sport that is already expensive and as with many organizations trying to keep the cost
                            down its just another added expense that will get passed on.</p>
                        <p>Also with the new CEP Requirements a current Level 3 (pre May 1st 2011) MUST become a Level 4
                            by Dec 31st 2011 if they are coaching Bantam and up to remain on the bench.<br/>
                            However a New Level 1 (post May 1st 2011) can be on the bench at these levels.<br/>
                            This is where we need our experienced coaches the most.<br/>
                            Level 4 is $265.00 compared to $40.00 clinic or $55.00 for on-line re-certification.</p>
                        <p>Many of us do not make $$ from Hockey. We do it because we love it, but everything has a
                            price.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1272">

                        <div class="hockeyshare_comment_by">walkerny Says:</div>
                        <div class="hockeyshare_comment_time">December 3rd, 2011 a31 7:32am</div>

                        <p>Moneymaker for USAH. Nothing new or valuable here, unless you are clueless to the sport.
                            Better material available on ANY aspect of this sport available online from many sources for
                            FREE. They rolled this out after all the coaches had signed up for the season on purpose. A
                            lot of coaches would not have signed up to assist on multiple teams. A lot of coaches are
                            just going to limit themselves next year to one team rather than be assistant coaches on
                            additional teams. This will put more stress on lone coaches, you will get LESS instruction
                            on the ice with fewer coaches to help run practice.<br/>
                            No other youth sport abuses it&#8217;s volunteers the way USAH does. Bottom line, the value
                            of anything, unasked for, forced on you, is minimal.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1298">

                        <div class="hockeyshare_comment_by">Jim Says:</div>
                        <div class="hockeyshare_comment_time">December 13th, 2011 a31 10:08pm</div>

                        <p>My problem is the redundancy of the information. Lets keep with a &#8220;train the trainer&#8221;
                            program and have this presented at our annual coaches meeting in town. I had to do the
                            peewee module as well as my level three cep. The level three was awful. Actually had a high
                            school goalie coach sit up there and read a power point presentation bullet by bullet. There
                            was also a section on teaching the PP and PK. It struck me as odd seeing we had Roger Grillo
                            of USA hockey tell us we should not even be doing breakouts in practice. I do like the
                            theories on the area games and practices re skill development. I will tell you I will never
                            re certify.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1306">

                        <div class="hockeyshare_comment_by">Matty Says:</div>
                        <div class="hockeyshare_comment_time">December 19th, 2011 a31 10:11am</div>

                        <p>I also think it is a money thing. With a so-so internet connection is has taken me over 9
                            hours to get only half way through my modules. I now have resorted to watching the videos at
                            work and risking my employment to continue to coach. I am a non-parent coach and this may be
                            my last year if I have to go through this again. Is it a requirement that all USA Certified
                            hockey coaches must have high speed internet service?</p>
                        <p>Put me in a auditorium with a hundred(s) other coaches for a Saturday &#8211; much better
                            environment and the chance to network with other coaches.</p>
                        <p>I agree USA hockey is way ahead of all other sports in coaching education. But there has to
                            be a better way than making us all sit in front a computer for 8-20 hours to complete a
                            module.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1313">

                        <div class="hockeyshare_comment_by">Coach Will Says:</div>
                        <div class="hockeyshare_comment_time">December 23rd, 2011 a31 12:57pm</div>

                        <p>So, there are always positives to reinforcing the education and structure of coaches.
                            Especially in an organization such as USAH.</p>
                        <p>However, timing of this was aweful. Time constraint rediculous. Do they not understand what
                            being a volunteer is about? I wake up to coach at 5:00 a.m. I then rush home to prepare for
                            work. After work I rush home to begin studying to advance myself in my career and complete
                            my internship. I have a family, yet the people of USAH think I still need to devote even
                            more time than the hours on ice, in a car, and in a rink to their needs?</p>
                        <p>I hate to say it, but this is something that should remain voluntary. In the instance of who
                            I coach (U-19 Girls) our record and skill development says it all. In 6 seasons of existance
                            5 have placed both state and regional banners in our rink. The higher quantity of girls to
                            play at a higher level as well as those to find a roster spot on a state or regional team is
                            greater out of our program locally than any other of the 6 across our state.</p>
                        <p>Call it cocky, but I&#8217;d say our structure is pretty good all things considered. To add
                            icing, we have been implementing an ADM system designed by a private company for several
                            years now.</p>
                        <p>And lastly, could they not speak a little faster in these videos? They should only be half
                            the lenght they are because half these guys talk and move their lips at a speed which a
                            snail riding a molassis freeway would pass&#8230;</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1321">

                        <div class="hockeyshare_comment_by"><a href='http://teamchaos.jrfuryhockey.com'
                                                               rel='external nofollow' class='url'>Coach Trip</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">December 29th, 2011 a31 11:20am</div>

                        <p>I liked the modules but agree with the general assessment; they were way too long and time
                            consuming at the beginning of a season. Thankfully USA Hockey extended the deadline for
                            those that didn&#8217;t get in by the end of the year. </p>
                        <p>Making the girls module something extra is silly. All coaches will meet up against girls in
                            hockey at some time and this extra module should just be integrated. Instead you just have
                            coaches saying they only coach boys and skipping the extra module. This makes little
                            sense. </p>
                        <p>On the whole though it is a step in the right direction. </p>
                        <p>I&#8217;d like USA hockey to present more age specific practice plans with videos. </p>
                        <p>For instance, you can download a ton of PDF material on ADM practice plans but last I looked
                            there was only one video. When I was teaching ADM&#8217;s last year when ADM&#8217;s were
                            relatively new I had little other guidance. I used this one video as parent reinforcement
                            embedded on my team site but I would have liked more to help me educate the youth
                            parents. </p>
                        <p>All in all I give it thumbs up.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1324">

                        <div class="hockeyshare_comment_by">derek Says:</div>
                        <div class="hockeyshare_comment_time">January 1st, 2012 a31 6:45pm</div>

                        <p>these things are really stupid. USA hockey is taking a weird turn. To make these mandatory
                            and unable to fast forward and all that&#8230;really sh*tty. It&#8217;s all common sense. If
                            the Coaches meetings were run better this wouldn&#8217;t have to happen. 8 hours of trying
                            not to fall asleep. GET US ON THE ICE SHOW US REAL STUFF!</p>
                        <p>And another thing!! In the first moments of the first video the dude says how thankful they
                            are for all the VOLUNTEERS (ME) for taking the time&#8230;blah blah&#8230;</p>
                        <p>SO WHY DO I HAVE TO PAY FOR THESE INSANE MODULES..AND THEN!! THREATEN TO NOT LET ME COACH MY
                            KIDS. SO THEN THEY HAVE NO SEASON!?!? </p>
                        <p>THIS MAKES NO SENSE WHATSOEVER USAHOCKEY&#8230;</p>
                        <p>No wonder why my high school league left and went to AAU&#8230;</p>
                        <p>Keep up the good work!</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1327">

                        <div class="hockeyshare_comment_by">Byron Says:</div>
                        <div class="hockeyshare_comment_time">January 7th, 2012 a31 2:58pm</div>

                        <p>Good information but way to long. Most of the presentations could be done in significantly
                            less time.</p>
                        <p>Since I coach multiple age groups I got to hear the same lessons multiple times. Many of the
                            presentations covered multiple age groups instead of just the age specific module. </p>
                        <p>Would be much better to have a general module and then short age specific modules.</p>
                        <p>Most of what was covered is already covered in the CEP classes and review modules. </p>
                        <p>USA Hockey preaches maximizing productivity on ice but seems to be perfectly willing to waste
                            off ice time for coaches.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1331">

                        <div class="hockeyshare_comment_by">Coach Says:</div>
                        <div class="hockeyshare_comment_time">January 13th, 2012 a31 1:21pm</div>

                        <p>I had to take CEP 2 this year to coach second year mites. In the CEP class, the instructor
                            asked how many people took CEP 1 last year. About 90 of the 120 people raised their hand.
                            Then, he went on to say: &#8220;You had to pay money to take this class, now I think we all
                            understand why the new requirement was enacted.&#8221;</p>
                        <p>All USA Hockey needs to do is provide the material and give some suggestions on approach and
                            age/skill targets. As somebody else mentioned, make the material optional but available. I
                            have the desire to be a good coach. USA Hockey&#8217;s logic that forced training will
                            produce better coaches is flawed. Excellence comes from within not through wasting my time.
                            Nutrition + drug and alcohol modules for Mites, really guys?</p>
                        <p>In our hockey association we are required to volunteer 12 hours a year. Coaching fulfills all
                            12 hours, yet I spend 80-90, about 20 of those in CEP training at my own expense. It would
                            take less time and cost me less money to volunteer to run the clock at games or be a caller
                            at bar bingo (where there is 1:1 volunteer hour credit).</p>
                        <p>The folks at USA Hockey are a bunch of self-important prima donnas. In their quest to &#8216;improve
                            the game&#8217; they are actually destroying it. Rumor is that next year all parents will
                            have to do a &#8216;Locker Room Policy&#8217; module. Unbelievable.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1338">

                        <div class="hockeyshare_comment_by">Jim Says:</div>
                        <div class="hockeyshare_comment_time">January 31st, 2012 a31 12:03am</div>

                        <p>Very disappointed with new requirements. Seems like a great way to get coaches to stop
                            volunteering. Definitely a way to generate money for USA hockey bureaucracy, a bunch of guys
                            who never got real jobs after playing a game. I&#8217;ve coached for over 15 years at both
                            hockey and baseball and your charging money to require that everyone tests out on common
                            sense issues that apply to any sport.<br/>
                            Your modules take up more time than my paramedic continuing education modules. How&#8217;s
                            that for perspective? Please just do background checks on the coaches and distribute some
                            practice plans and drills and let the people who truly volunteer do what they do</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1349">

                        <div class="hockeyshare_comment_by">Bill Tremel Says:</div>
                        <div class="hockeyshare_comment_time">February 12th, 2012 a29 2:46pm</div>

                        <p>Okay, I embrace USA Hockey and everything they do. I take time out of my work and personal
                            life to make sure that I&#8217;m the best coach I can be for my son and all the kids I&#8217;ve
                            helped coach of the years. I totally embrace the additional training and the testing. There
                            are too many &#8220;bad&#8221; coaches out there that need to be weeded out.</p>
                        <p>Now here&#8217;s my beef with the online training. This new training is a heavy bandwidth
                            hog. If you don&#8217;t have broadband, you&#8217;re out of luck. I&#8217;ve been trying to
                            take my Level 3 squirt training for 4 months. A little at a time. I&#8217;ve finally given
                            up, as you cannot do this training with dial-up. It simply does not work.</p>
                        <p>USA hockey needs to understand that not everyone lives in a big city and has access to high
                            speed internet. I&#8217;ve failed to complete this training on time. Not because I have not
                            given up my own personal time. But because the false expectations that USA Hockey has about
                            all American&#8217;s having easy access to broadband.</p>
                        <p>I&#8217;ve email USA hockey about this several times, and each time, they would simply pass
                            me through one or two videos stating that I should have better and quicker access to the
                            material. But they ignore the fact that I&#8217;ve stated that I can only do these with a
                            dial-up internet connection.</p>
                        <p>I&#8217;ve also recommended that they send out the video material on a CD and allow us to log
                            in and take the tests separately. This is the only why I&#8217;m ever going to be able to
                            comply.</p>
                        <p>I will most likely loose my certification (along with many other GOOD coaches) due to this
                            overlooked situation.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1350">

                        <div class="hockeyshare_comment_by">Chas. Heilman Says:</div>
                        <div class="hockeyshare_comment_time">February 12th, 2012 a29 5:33pm</div>

                        <p>I have been coaching house hockey for 13 years. Girls midget hockey now for 6 years. I will
                            never coach college, AA, AAA, Jr&#8217;s, or any other upper level hockey. Now I am forced
                            to become a level 4 coach or quit. Level 4 clinics are never near my area. It will take a
                            day there and a day back. Plus the days there. Plus the room, gas food, etc. I fear that
                            midget hockey may not be available after my current card expires. I can not eat the cost on
                            top of the expense of coaching as it is. So sad.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1351">

                        <div class="hockeyshare_comment_by">Billy Says:</div>
                        <div class="hockeyshare_comment_time">February 15th, 2012 a29 3:27pm</div>

                        <p>My other coach and I were unaware of this module to be completed until 3 days before it was
                            due. Due to time constraints we were unable to complete the module in time. I was on the
                            14th section out of 17 when it timed out and my other coach was on 11. I have been in
                            contact with usa hockey reps for the past few days and there is nothing that we do. USA
                            hockey kept the $10 and wont allow us to finish them. My other coach and I are the only two
                            coaches on the team so our team now has no coach. I understand we dropped the ball big time
                            but I feel to remove a coach from the team is very extreme. I would gladly pay an additional
                            $30 or so to log back in to complete the module or pay a fine for not completing it. Either
                            way, our team has no coach a week before playoff and I cant help but feel that this is not
                            fair and benefits no one, especially the kids. We both are thought of very highly in the
                            association and now are restored to being treated like criminals with a &#8220;no contact or
                            team interaction&#8221; policy. I am a good coach and care about these kids very much. I
                            donate 6 days a week to this team trying to make these kids better players as well as people
                            and to create a love of the game. I hope USA hockey considers different consequences next
                            time as these only do not benefit anyone.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1532">

                        <div class="hockeyshare_comment_by">Chris Says:</div>
                        <div class="hockeyshare_comment_time">December 5th, 2012 a31 11:32pm</div>

                        <p>I have no desire to be a head coach, however I&#8217;m more than happy to volunteer as an
                            assistant coach. With that said, I think the coaching requirements for assistant/part-time
                            coaches are completely unreasonable. I can understand wanting to standardize the USA Hockey
                            methods and procedures for all coaches but the numerous levels, classes, on-line video, etc,
                            drive away many capable and willing individuals. For example, my son&#8217;s squirt team has
                            one head coach and myself as an assistant. The head coach has missed time (weeks) due to
                            work requirements leaving me to fill in for practices and games. I cant afford the cost/time
                            from work to attend a class (which are during the week in a town 2 hours away!). USA Hockey
                            needs to re-examine their certification requirements for assistants to keep volunteers like
                            myself motivated and willing to participate in the future. I would suggest having the
                            assistants complete the on-line video lesson as their only requirement. This would make it
                            time and cost effective along with establishing a better incentive for someone like myself
                            to remain and support USA Hockey in the future.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1538">

                        <div class="hockeyshare_comment_by">Lauren Lynn Says:</div>
                        <div class="hockeyshare_comment_time">December 10th, 2012 a31 2:20pm</div>

                        <p>I cannot stand these requirements or USA hockey &#8211; I did my time to get to the level 4
                            level by the time I was 19 before these coaching module requirements. I&#8217;ve been
                            coaching every level from Elementary, Middle School, High school and club hockey. All of
                            which are boys teams &#8211; which every seminar I&#8217;ve gone to, the sexist pigs that
                            run them tell me women should only coach girls because they are so &#8216;sensitive&#8217;
                            and you have to treat them special. Bullcrap. I&#8217;ve been playing long enough to play
                            college men&#8217;s d1 hockey, get drafted and play pro women&#8217;s hockey and now they
                            tell me that I have to take hours out of my week that I don&#8217;t have (coaching 4
                            different age groups as well as traveling to play professionally and give private lessons,
                            balancing a full time job, and finish my own education) All the coaching I give I don&#8217;t
                            get paid for because I don&#8217;t want kids growing up with the awful hockey experiences I
                            had. USA hockey makes it so I don&#8217;t even want to be involved. It&#8217;s a joke. They
                            are doing one of two things &#8211; making it either more difficult for people who shouldn&#8217;t
                            get their card don&#8217;t want to, or just trying to get more money out of people. My
                            thoughts are it&#8217;s a little of both. Either way &#8211; I&#8217;ve been this close to
                            walking away from coaching because of this waste of time. So frustrating.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1555">

                        <div class="hockeyshare_comment_by"><a href='http://www.none.edu' rel='external nofollow'
                                                               class='url'>Jon - Madison WI</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">December 28th, 2012 a31 11:05am</div>

                        <p>OK I have been watching the age development videos for the last 3.5 hours.<br/>
                            I am not the coach of my son&#8217;s team but I have seen a coach get sick at playdowns and
                            not have another certified coach available so the team had to forfeit.</p>
                        <p>many of the the modules are repeats of the info covered in the level 3 coaching
                            re-certification.</p>
                        <p>My son is a bantam and this is the last time I will be doing this. I have been coaching kids
                            since 1990 and this is just too time consuming for that I am getting out of it.</p>
                        <p>The youth programs do not have the time, or facilities available to them to do strength and
                            conditioning programs mentioned in the videos. Maybe some of the High School programs could
                            use these but the rec / youth hockey players &amp; coaches really can not. </p>
                        <p>I would really like to see some of these &#8220;USA Hockey experts&#8221; coach in the
                            facilities we use in the real world.<br/>
                            As I watch the videos all I see is a lot of wasted ice in practice. maybe some videos
                            showing average instead of elite level players would be more real world.</p>
                        <p>It has cost me $60 to take this &#8220;class&#8221; and it just looks like a money grab to
                            me.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1561">

                        <div class="hockeyshare_comment_by">Bob Says:</div>
                        <div class="hockeyshare_comment_time">January 1st, 2013 a31 10:15am</div>

                        <p>I think the on-line modules are great! I am a high school teacher and the modules cover some
                            child development and child psychology which was a great refresher for me. I think it is
                            good for coaches who do not have a degree in education to understand the psychological and
                            physical developments of the players. </p>
                        <p>I think the modules are little too long and should be started in the summer because coaches
                            have less time during the season or should be edited down since some of the material is
                            redundant. </p>
                        <p>I think USA Hockey should make all parents participate in some on-line module so they
                            understand why coaches are instructing the players the way they are. Often times parents
                            have no idea about hockey in the non-traditional hockey markets and need to be educated too.
                            USA Hockey is preaching to the proverbial choir and needs to inform parents.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-2355">

                        <div class="hockeyshare_comment_by">PHIL Says:</div>
                        <div class="hockeyshare_comment_time">September 18th, 2013 a30 6:00am</div>

                        <p>BEEN COACHING FOR 19 YEARS. JUST PAID FOR USA INS, PAID TO RECERT , DID A BACKROUND CHECK NOW
                            NEED TO DO A MUDULE . LOTS OF TIME FOR ME AND LOTS OF MONEY FOR USA HOCKEY. JUST THINK HOW
                            MUCH MONEY USA HOCKEY BRINGS IN.SO WE CAN HAVE THE OK TO COACH FOR FREE.</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-2368">

                        <div class="hockeyshare_comment_by">Chuck Says:</div>
                        <div class="hockeyshare_comment_time">October 10th, 2013 a31 11:51am</div>

                        <p>I&#8217;m disgusted with the age specific modules. I&#8217;ve taken two and now need to take
                            a third. I don&#8217;t feel the material is useful, I can get the same information for free
                            online. The $10 and time sink are absurd. Coaches in my club are handling multiple teams due
                            to a lack of volunteers willing to coach. The lack of volunteers is a direct result of the
                            mandatory requirements place on coaches by USA Hockey. How much more regulation is needed
                            before clubs start folding?</p>

                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-16314">

                        <div class="hockeyshare_comment_by">Clint Says:</div>
                        <div class="hockeyshare_comment_time">April 8th, 2015 a30 9:06pm</div>

                        <p>Has anyone saved the PDFs for the pee wee module? Thanks.</p>

                    </div>

                    <br/>

                    <p>
                        <a href="http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/feed/"><abbr
                                    title="Really Simple Syndication">RSS</abbr> feed for comments on this post.</a></p>

                    <p>Sorry, the comment form is closed at this time.</p>

                    <br/>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

