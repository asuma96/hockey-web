@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1191 post type-post status-publish format-standard hentry category-10000-pucks category-hockey-instructional-video tag-10000-pucks tag-off-ice-shooting tag-shooting tag-video"
                         id="post-1191">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/off-ice-shooting-games/"
                               rel="bookmark">Off Ice Shooting Games</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">11</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-shooting/"
                                                    rel="tag">off ice shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>With the 10,000 Pucks Contest already logging in over 2.5 million shots, we thought it
                                would be good to post a video covering some games you can play while you&#8217;re
                                shooting your pucks to keep your training fresh!</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/yJXRsS66jS0?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Ultimate Goalie Link: <a
                                        href="http://hockeyshot.hockeyshare.com/ug" target="_blank">hockeyshot.hockeyshare.com/ug</a>
                            </p>
                            <p style="text-align: left;">HockeyShot Trick Shot Challenge: <a
                                        href="http://hockeyshare.com/trickshot" target="_blank">hockeyshare.com/trickshot</a>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-1133 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-one-timer tag-shooting tag-video"
                         id="post-1133">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/one-timers-video/"
                               rel="bookmark">One Timers &#8211; Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">25</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/one-timer/" rel="tag">one timer</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s video covers the basic technique of the one-timer shot. One-timers are
                                excellent for creating fast scoring opportunities and not giving the goaltender a chance
                                to set prior to the shot.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/ZJAmx89gDrQ?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Key Teaching Points</p>
                            <ul>
                                <li>Puck should be shot similar to a slap shot, located between the middle of your feet
                                    and the back foot
                                </li>
                                <li>Hands just over shoulder-width apart (this will vary based on the degree of knee
                                    bend and &#8220;sitting&#8221; in to your shot)
                                </li>
                                <li>Transfer the weight to the back leg</li>
                                <li>Low stance on the shot motion, transferring your weight to your front foot</li>
                                <li>Open your hips and front foot toward your target</li>
                                <li>Be prepared (and practice) adjusting your stance to accomodate for passes slightly
                                    off target
                                </li>
                            </ul>
                            <div>Example from the NHL:</div>
                            <div style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                           class="youtube-player"
                                                                                           type="text/html" width="640"
                                                                                           height="360"
                                                                                           src="//www.youtube.com/embed/qjezs-zCcyQ?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                           frameborder="0"
                                                                                           allowfullscreen></iframe></span>
                            </div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-1108 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-m2-hockey tag-shooting tag-video"
                         id="post-1108">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/quick-shooting-release-video/"
                               rel="bookmark">Quick Shooting Release &#8211; Video</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">6</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/m2-hockey/" rel="tag">m2 hockey</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s video covers the technique and practice tips for releasing a shot off
                                of one quick cross-body lateral move.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/uNwhBaVcKN8?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Teaching points:</p>
                            <ul>
                                <li>Eliminate additional stick handles</li>
                                <li>Transfer your weight to shooting side</li>
                                <li>Knee bend to generate more power</li>
                            </ul>
                            <div><strong><em>Want to see this move in action? Check out the 2nd goal by Datsyuk in the
                                        video below: </em></strong></div>
                            <div style="text-align: center;"></div>
                            <div style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                           class="youtube-player"
                                                                                           type="text/html" width="640"
                                                                                           height="360"
                                                                                           src="//www.youtube.com/embed/z2j7ipa7hJM?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                           frameborder="0"
                                                                                           allowfullscreen></iframe></span>
                            </div>
                            <div style="text-align: center;"></div>
                            <div style="text-align: left;"><em><strong>&#8230;And Henrik Zetterberg&#8217;s goal against
                                        Anaheim:</strong></em></div>
                            <div style="text-align: left;"></div>
                            <div style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                           class="youtube-player"
                                                                                           type="text/html" width="640"
                                                                                           height="360"
                                                                                           src="//www.youtube.com/embed/QjgIIWz7tPY?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                           frameborder="0"
                                                                                           allowfullscreen></iframe></span>
                            </div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-955 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-hockey-instructional-video tag-shooting tag-video"
                         id="post-955">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/off-ice-shooting-tips-video/"
                               rel="bookmark">Off Ice Shooting Tips [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>With the start of <a href="http://hockeyshare.com/10000pucks">10,000 Pucks</a> this
                                week, we thought it would be appropriate to post a video with some tips on how to
                                practice your shots off-ice.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/z5pHAo5PQCQ?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p>As we prepare to shoot our next round of videos, we want your feedback. What would you
                                like to see? Let us know by leaving a comment here, tweeting us <a
                                        href="http://twitter.com/hockeyshare" target="_blank">@hockeyshare</a>, or
                                posting to our wall on Facebook at <a href="http://www.facebook.com/hockeyshare"
                                                                      target="_blank">www.facebook.com/hockeyshare</a>.
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-898 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-backhand tag-hockey-instructional-video tag-shooting tag-video"
                         id="post-898">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/backhand-shots-video/"
                               rel="bookmark">Backhand Shots [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Feb</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/backhand/" rel="tag">backhand</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The backhand shot is one of the most difficult for goaltenders to stop, as the release
                                point and exact location of the shot are very tough to read. Unfortunately, many players
                                fail to properly practice this crucial shot.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/backhand-shots-video/#more-898"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-861 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-hockey-instructional-video tag-shooting tag-video tag-wrist-shot"
                         id="post-861">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/wrist-shot-video/"
                               rel="bookmark">Wrist Shot [Video]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jan</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/shooting/" rel="tag">shooting</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/wrist-shot/" rel="tag">wrist shot</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The wrist shot is the most fundamental shot in the game. In this week&#8217;s video we
                                cover the basic technique of the wrist shot.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/wrist-shot-video/#more-861"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

