@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">

                    <div class="post-1019 post type-post status-publish format-standard hentry category-comments-thoughts category-hockey-tips tag-early-season tag-team-activities tag-team-building tag-team-chemistry"
                         id="post-1019">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/10-early-season-team-building-ideas/"
                               rel="bookmark">10 Early Season Team Building Ideas</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/"
                                                rel="category tag">Hockey Tips</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/early-season/" rel="tag">early season</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-activities/"
                                                    rel="tag">team activities</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-building/"
                                                    rel="tag">team building</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-chemistry/"
                                                    rel="tag">team chemistry</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Getting a team to gel together can be a big task if you&#8217;ve got a lot of new players
                                on your team. Below is a list of ten ideas to improve your team&#8217;s chemistry early
                                in the season.</p>
                            <ol>
                                <li><strong>Early Season Tournament/Road Trip</strong> &#8211; ideally, pick a
                                    tournament where you&#8217;re out of your home town and parents/players must stay in
                                    a hotel. This lets players get to know each other away from the rink setting, and
                                    gives parents time to socialize in the evenings. If you&#8217;re in a hotel and have
                                    time between games, try planning a team &#8220;pot-luck&#8221; lunch/dinner where
                                    players are required to attend as opposed to everyone heading their own direction
                                    for meals.
                                </li>
                                <li><strong>Ropes Courses</strong> &#8211; a ropes course will force players to work
                                    together as a team to achieve a common goal &#8211; just like in the season. It will
                                    also force some players to address their fears (especially if you&#8217;re doing a
                                    high ropes course) and get support from their teammates.
                                </li>
                                <li><strong>Team Building Activities</strong> &#8211; choose a day and location away
                                    from the rink and plan group challenges (mental as well as physical). Activities
                                    that force players to communicate and interact are excellent in establishing trust
                                    among teammates. For some ideas on activities, <a
                                            href="http://hockeyshare.com/blog/uncategorized/team-building-resources/">check
                                        out our blog post on Team Building Resources</a>.
                                </li>
                                <li><strong>Team Cook Out</strong> &#8211; this can be done at the rink, or if a parent
                                    is kind enough to open their home, at a family&#8217;s house. Ideally there would be
                                    an activity the players can do (pool, ping pong, swimming, etc.) which will focus
                                    them to one area. Avoid allowing video games to be the central focus, as the amount
                                    of communication and group interaction is severely lessened.
                                </li>
                                <li><strong>Change Locker Room Seats</strong> &#8211; players love to get into a routine
                                    and sit next to their buddies in the locker room. This can be okay as the season
                                    progresses, but if you&#8217;ve got a team with a lot of new skaters, forcing
                                    players to sit in different locations will cause them to talk with and get to know
                                    people outside their small clique.
                                </li>
                                <li><strong>Paint Balling</strong> &#8211; not every team will have access to this, but
                                    teams that do will find that their players will enjoy the competition and have a
                                    great time being together away from the rink. You could also plan for a team cookout
                                    after the paint balling event!
                                </li>
                                <li><strong>Team Workout</strong> &#8211; you see this in the NHL quite a bit &#8211;
                                    players and coaches will do team runs, bike rides, canoeing, etc. Although it may
                                    not be quite as much fun as some of the other activities listed above, you&#8217;ll
                                    be getting the group together and also helping their overall conditioning.
                                </li>
                                <li><strong>Mix Lines / D Partners</strong> &#8211; early in the season, forcing players
                                    to play with skaters other than the one or two players they&#8217;re used to will
                                    not only get players to work together and communicate, but will prepare older
                                    players for future tryout camps where they&#8217;ll be playing with skaters they&#8217;ve
                                    never played with before.
                                </li>
                                <li><strong>Team Video</strong> &#8211; have some fun with this one &#8211; especially
                                    early in the season. Instead of doing game tape review or something expected, have
                                    some fun and watch an entertaining video or movie. Maybe even get some pizzas for
                                    the players (without telling them). For older groups, <a
                                            href="http://totalhockey.com/Product.aspx?itm_id=1946&amp;div_id=2">The
                                        Tournament</a> is a great choice. For younger groups, Miracle may be a better
                                    idea.
                                </li>
                                <li><strong>Personal Information</strong> &#8211; before or after a practice, hold a
                                    team gathering in the locker room and have players get up and introduce themselves
                                    one-by-one. It is also helpful to have 3 or 4 questions they need to answer while it
                                    is their turn. Simple questions like the following tend to work well: favorite
                                    hockey team, one thing we didn&#8217;t know about you, home town, etc.
                                </li>
                            </ol>
                            <div>Do you have another idea to add to the list? Leave a comment below to contribute! Good
                                luck this season!
                            </div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-774 post type-post status-publish format-standard hentry category-comments-thoughts tag-quotes tag-team-building tag-youth-hockey-coaching"
                         id="post-774">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/team-building-quotes/"
                               rel="bookmark">Team Building Quotes</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">4</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/quotes/"
                                                    rel="tag">quotes</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-building/"
                                                    rel="tag">team building</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Many coaches have expressed an interest in getting some quotes to use with their team.
                                Anyone who knows me, knows I love to use inspirational / teambuilding quotes in the
                                locker room. Here are some of my favorites:</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/team-building-quotes/#more-774"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-139 post type-post status-publish format-standard hentry category-comments-thoughts tag-hockey-quotes tag-inspirational-quotes tag-team-building tag-team-building-quotes tag-teamwork-quotes"
                         id="post-139">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/great-team-building-quotes/"
                               rel="bookmark">Great Team Building Quotes</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-quotes/"
                                                    rel="tag">hockey quotes</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/inspirational-quotes/"
                                                    rel="tag">inspirational quotes</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-building/"
                                                    rel="tag">team building</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/team-building-quotes/"
                                                    rel="tag">team building quotes</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/teamwork-quotes/"
                                                    rel="tag">teamwork quotes</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here are a few great inspirational / team-building quotes I&#8217;ve found over the
                                years. I hope you enjoy!</p>
                            <p>&#8220;A championship team is a team of champions&#8221; &#8211; Unknown</p>
                            <p>&#8220;A group becomes a team when each member is sure enough of himself and his
                                contribution to praise the skills of others.&#8221; &#8211; Norman S Hidle</p>
                            <p>&#8220;It is amazing how much you can accomplish when it doesn&#8217;t matter who gets
                                the credit&#8221; &#8211; Unknown</p>
                            <p>&#8220;Teamwork divides the task and doubles the success&#8221; &#8211; Unknown</p>
                            <p>&#8220;Always give 100%, and you&#8217;ll never have to second-guess yourself&#8221;
                                &#8211; Dan Valenti</p>
                            <p>&#8220;Continuous effort &#8211; not strength or intelligence &#8211; is the key to
                                unlocking our potential.&#8221; &#8211; Liane Cordes</p>
                            <p>&#8220;Discipline is the bridge between goals and accomplishments&#8221; &#8211; Jim
                                Rohn</p>
                            <p>&#8220;Leadership is an attitude before it becomes an ability&#8221; &#8211; A.S. Migs
                                Damiani</p>
                            <p>&#8220;I&#8217;m a great believer in luck, and I find the harder I work, the more luck I
                                have.&#8221; &#8211; Thomas Jefferson</p>
                            <p>&#8220;Success doesn&#8217;t come to you&#8230;you go to it!&#8221; &#8211; Unknown</p>
                            <p>&#8220;Surmounting difficulty is the crucible that forms character&#8221; &#8211; Tony
                                Robbins</p>
                            <p>&#8220;The higher your energy level, the more efficient your body. The more efficient
                                your body, the better you feel and the more you will use your talent to produce
                                outstanding results.&#8221; &#8211; Tony Robbins</p>
                            <p>&#8220;Whatever you do on a continuous basis creates a pattern and it becomes who you
                                are. If you make a conscious decision to try to do the right things in all situations,
                                then that&#8217;s just who you are, and you become a high character guy&#8221; &#8211;
                                Tony Richardson</p>
                            <p>&#8220;Your chances of success in any undertaking can always be measured by your belief
                                in yourself&#8221; &#8211; Unknown</p>
                            <p>&#8220;Coming together is a beginning. Keeping together is progress. Working together is
                                success.&#8221; &#8211; Henry Ford</p>
                            <p>&#8220;Talent wins games, but teamwork and intelligence wins championships&#8221; &#8211;
                                Michael Jordan</p>
                            <p>&#8220;Alone we can do so little; together we can do so much.&#8221; &#8211; Helen
                                Keller</p>
                            <p>&#8220;None of us is as smart as all of us&#8221; &#8211; Ken Blanchard</p>
                            <p>&#8220;If everyone is moving forward together, then success takes care of itself&#8221;
                                &#8211; Henry Ford</p>
                            <p>&#8220;Teamwork is the ability to work as a group toward a common vision, even if that
                                vision becomes extremely blurry&#8221; &#8211; Unknown</p>
                            <p>&#8220;Remember upon the conduct of each depends the fate of all&#8221; &#8211; Alexander
                                the Great</p>
                            <p>&#8220;People have been known to achieve more as a result of working with others than
                                against them.&#8221; &#8211; Dr. Allan Fromme</p>
                            <p>&#8220;If a team is to reach its potential, each player must be willing to subordinate
                                his personal goals to the good of the team&#8221; &#8211; Bud Wilkinson</p>
                            <p>&#8220;Overcoming barriers to performance is how groups become teams&#8221; &#8211;
                                Katzenbach &amp; Smith</p>
                            <p>&#8220;A snowflake is one of God&#8217;s most fragile creations, but look what they can
                                do when they stick together&#8221; &#8211; Unknown</p>
                            <p>&#8220;It&#8217;s not necessarily the amount of time you spend at practice that counts;
                                it&#8217;s what you put into the practice&#8221; &#8211; Eric Lindros</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

