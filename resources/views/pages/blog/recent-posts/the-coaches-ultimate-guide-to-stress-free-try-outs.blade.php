@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>
        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1371 post type-post status-publish format-standard hentry category-comments-thoughts"
                         id="post-1371">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/"
                               rel="bookmark">The Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a></h2>
                        <div class="blog-border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Imagine your team about to take the ice in a championship game.</p>
                            <p>Would you feel stressed as a coach?</p>
                            <p>I know I’d definitely be feeling some nervous anticipation! Not as much, though, as I sometimes feel
                                dealing with try-outs!<span class="tve_image_frame"><br/>
</span></p>
                            <p>Ah, hockey try-outs. A necessary part of your coaching position but not always the most fun. How many
                                of you out there cringe a little bit when you think about having to cut kids and watching them cry,
                                dealing with angry parents (who may or may not even understand the game of hockey) complaining about
                                your decisions, feeling the pressure of politics from your club or organization?</p>
                            <p>There are a lot of reason most hockey coaches don’t love try-outs!</p>
                            <p>Well, some of these problems are never going to go away entirely, but there are absolutely some
                                best-practices and some considerations you can make as a coach to make your life 100x easier come
                                try-outs next season.</p>
                            <p>Let’s dig in and find out what you can do to make your next set of try-outs a much more enjoyable,
                                stress-free experience and get you the best possible roster for your team!</p>
                            <p><span class="bold_text"><strong><a href="http://www.winsmarter.com/go-hockeyshare-com"
                                                                  target="_blank">Heck, we’ll even give you the three forms you need to make your next try-out the smoothest, most stress-free try-out you’ve ever run!</a></strong></span>
                            </p>
                            <p>Before we get to that though, let’s give you a quick outline of what you need to be thinking about to
                                make your life easier!</p>
                            <h3>Planning &#8211; Before the Try-Outs<span class="tve_image_frame"><br/>
</span></h3>
                            <h4>Be prepared</h4>
                            <p>Alexander Graham Bell said it all. The more prepared you are the better the results will be. So let’s
                                start at the beginning:</p>
                            <ul class="thrv_wrapper" style="margin-bottom: 15px !important;">
                                <li class="" style="margin-bottom: 10px;"><b>Always plan your sessions in advance.</b> How many
                                    sessions will you have? How much ice time for each? Have you factored in pre- and post-session
                                    meetings, warm-ups and water breaks? Answer these questions and use them to develop a written
                                    plan for tryouts &#8211; the specific drills and timing you want and make sure everyone involved
                                    understands the set-up and sequence of events &#8211; this is especially important if you won’t
                                    be on the ice during the try-outs, but we’ll talk more about that later. Not only is this one of
                                    the most important things you can do to reduce your stress, but the better your preparation the
                                    more professional you appear to players and parents and the more credibility you gain. Fear not,
                                    though, you’ve come to the right place! HockeyShare has some <a
                                            href="http://hockeyshare.com/drills/learn_more.php" target="_blank">excellent
                                        resources</a> to help you plan effective try-out session on the ice.
                                </li>
                                <li class="" style="margin-bottom: 10px;"><b>Find colleagues to help you during tryouts.</b> As hard
                                    as we might try, we still only have one set of eyes and one set of hands. If you want reduce
                                    your stress-level, lightening your load with more eyes and more hands is an absolute MUST! In a
                                    perfect world, you’re gonna want some help setting up and running drills on the ice, and you’re
                                    definitely gonna want some help sitting in the stands evaluating and taking notes. I’d suggest
                                    using those with more experience and those who are more trusted as your off-ice evaluators and
                                    other volunteers to help you set up and manage drills on the ice. Some best practices here are:
                                </li>
                                <li class=""
                                    style="padding-left: 0px !important; margin-left: 50px !important; margin-bottom: 10px;"><b>Avoid
                                        the use of any “parent-coaches”</b> or anyone with a connection to any of the kids trying
                                    out &#8211; particularly in the role of an evaluator! Hey we all love our kids and all come with
                                    our own inherent biases &#8211; let’s do our best to not let it factor into our decision making
                                    and to maintain a perception of fairness for all kids involved!
                                </li>
                                <li class=""
                                    style="padding-left: 0px !important; margin-left: 50px !important; margin-bottom: 10px;">If
                                    possible, try to <b>get some help evaluating </b>from some coaches who have never coached any of
                                    the kids before. Same reason &#8211; if we can avoid it, let’s not let prior biases influence
                                    putting the best possible team on the ice!
                                </li>
                                <li class=""
                                    style="padding-left: 0px !important; margin-left: 0px !important; margin-bottom: 10px !important;">
                                    <b>Create an Evaluation Form.</b> So now that you’ve got some friends helping you evaluate,
                                    let’s make their lives easier and your life easier. By coming up with an evaluation form, you’re
                                    letting your staff know exactly and specifically what you want them looking for while watching
                                    each kid. You’re also getting consistent criteria for you to use in your decision making
                                    process. Let’s not make things harder by having to compare apples to oranges!
                                </li>
                            </ul>
                            <p style="margin-left: 20px !important; padding-left: 30px;">Coaches can always opt to take their own
                                notes instead of using your exact form, but ask them to at least use the form as a guide.</p>
                            <p style="margin-left: 20px !important; padding-left: 30px;">On your form, you can include things
                                like:</p>
                            <ul class="thrv_wrapper" style="margin-left: 80px !important; margin-bottom: 20px !important;">
                                <li class="">Speed</li>
                                <li class="">Skating transitions (forward-to-backward, etc.)</li>
                                <li class="">Giving/receiving passes</li>
                                <li class="">Shooting</li>
                                <li class="">“Hockey sense&#8221;</li>
                                <li class="">Any other “non-skill” characteristics you&#8217;re looking for. Some examples:</li>
                                <li class="" style="margin-left: 50px !important;">Leadership</li>
                                <li class="" style="margin-left: 50px !important;">Hustle &#8211; full effort before, during and
                                    after every drill
                                </li>
                                <li class="" style="margin-left: 50px !important;">Physicality</li>
                                <li class="" style="margin-left: 50px !important;">Focus</li>
                                <li class="" style="margin-left: 50px !important;">Accountability (remembering all their equipment,
                                    being on time, etc.)
                                </li>
                                <li class="" style="margin-left: 50px !important;">Resilience (ability to recover from mistakes, not
                                    pouting or throwing a tantrum, etc.)
                                </li>
                                <li class="" style="margin-left: 50px !important;">Coachability (ability to take coaching or
                                    criticism)
                                </li>
                                <li class="">Some space for their own general comments</li>
                            </ul>
                            <div class="thrv_paste_content thrv_wrapper" style="margin-bottom: 0px !important;">
                                <ul class="">
                                    <li class="" style="padding-left: 0px; margin-left: 0px !important;"><strong>Consider seeking
                                            out feedback from prior coaches of kids you don’t already know.</strong> There are
                                        definitely some pros and cons to this so we’ll let you make your own decision here, but some
                                        things to consider:
                                    </li>
                                </ul>
                                <div class="thrv_paste_content thrv_wrapper"
                                     style="margin-top: 0px !important; margin-bottom: 0px !important; padding: 0px !important; margin-left: 50px !important;">
                                    <ul class="thrv_wrapper" style="margin-top: 0px !important;">
                                        <li class="" style="margin-bottom: 0px !important; margin-top: 10px !important;">Pros
                                            <ul class="">
                                                <li class="" style="margin-left: 30px !important;">learn a bit about players you
                                                    don’t know
                                                </li>
                                                <li class="" style="margin-bottom: 10px !important; margin-left: 30px !important;">
                                                    hear about their attitude and team orientation
                                                </li>
                                                <li class="" style="margin-left: 30px !important;">find out how much the actually
                                                    scored in games vs. what you see in tryouts
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="thrv_wrapper" style="margin-top: 0px !important;">
                                        <li class="" style="margin-bottom: 0px !important;">Cons
                                            <ul class="">
                                                <li class="" style="margin-left: 30px !important; margin-bottom: 0px !important;">
                                                    might give you unfair preconceived notion about some players &#8211; be careful
                                                    here
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h3>Get the Info Out</h3>
                            <p>Like most aspects of coaching, communication is key here. Keeping your players and parents
                                well-informed before try-outs even start is one of the best things you can possibly do to make the
                                process smoother for everyone. Likewise, making sure your coaches and evaluators know the game plan
                                ahead of time helps everything move along well.</p>
                            <p><b>The single best suggestion I can give you</b> to keep players and parents informed is to create a
                                flyer that’s distributed to them ahead of time with all the info they need about the process.
                                Include things like the schedule and location of each session, the criteria upon which you’re
                                evaluating the kids, best practices for parents, and tips and suggestions for players to have the
                                best possible try-out they can.</p>
                            <p>The other <b>huge benefit</b> of this form that can’t be overstated is the perception of organization
                                and professionalism it presents to everyone involved in your try-out process. It’s important to
                                understand the psychology here &#8211; you’re the professional, you’re presenting yourself
                                accordingly and that’s going to make it a lot easier for players and parents to accept your
                                decisions later in the process.</p>
                            <p><b><a href="http://www.winsmarter.com/go-hockeyshare-com" target="_blank">This is so important, we
                                        couldn’t possibly leave you hanging! Get the exact form we use in your inbox right now!</a>
                                </b></p>
                            <p>Now that the players and parents are squared away. Let’s get the coaches and evaluators ready to go
                                as well. Get your Evaluation Forms out to your staff a day or two before the try-outs start so they
                                have a chance to check out what you’re looking for and ask any questions they might have. Same goes
                                for your try-out plans &#8211; the specific drills and timing you want &#8211; to make sure all
                                coaches are on the same page.</p>
                            <h3>Try-Out Days</h3>
                            <h4>Logistical Considerations</h4>
                            <p>Alright, it’s try-out day! Here are some things to consider to get things moving in a good
                                direction:</p>
                            <ul class="thrv_wrapper" style="margin-bottom: 0px !important;">
                                <li class="" style="margin-bottom: 10px;"><b>Arrive early. </b>No need for comment here!</li>
                                <li class="" style="margin-bottom: 10px;"><b>Quick meeting.</b> At beginning of each tryout session,
                                    meet face-to-face with the players, let them know the structure of that day’s session, remind
                                    them what you’re looking for and wish everyone good luck.
                                </li>
                                <li class="" style="margin-bottom: 10px;"><b>Avoid parent conversations</b>. Do your best to steer
                                    clear of any conversations with parents while you’re at the try-outs, even if you know them, to
                                    avoid any perceptions of favoritism.
                                </li>
                                <li class="" style="margin-bottom: 10px;"><b>On the ice or in the stands? </b>This has been a debate
                                    amongst hockey coaches for years! The bottom line is your decision about where to position
                                    yourself during try-outs is going to depend on a lot factors: your experience, your coaching
                                    staff and your comfort level with them, the level of the players, etc. Think about the
                                    advantages of each option and choose what you think is best! Here are some things to think
                                    about:
                                </li>
                                <li class="" style="margin-left: 50px !important; margin-bottom: 10px;"><b>For multiple session
                                        try-outs</b> consider being on ice initially and then in stands for at least the last
                                    session.
                                </li>
                                <li class="" style="margin-left: 50px !important; margin-bottom: 10px;"><b>If it’s a single-session
                                        try-out</b> consider being in the stands after giving your initial introduction on the ice.
                                    Make sure your coaching staff knows the exact drills and timing you want to see &#8211;
                                    specifics are important!
                                </li>
                                <li class="" style="margin-left: 50px !important; margin-bottom: 10px;"><b>Try to observe at least
                                        the final tryout session from the stands</b> (preferably where no one can disturb you!). At
                                    this point there’s probably only a few kids you really need to watch (the ones on the bubble)
                                    and giving yourself a different perspective can be helpful.
                                </li>
                                <li class="" style="margin-bottom: 10px;"><b>Keep the evaluators on their own. </b>Evaluators should
                                    sit away from all parents for obvious reasons. If you’re really feeling crazy, consider a closed
                                    try-out with no parents watching at all if your club or league allows. Just remember, that might
                                    cause more problems than it prevents &#8211; consider all your options!
                                </li>
                                <li class="" style="margin-bottom: 10px;"><b>Don’t “pre-label”.</b> Make sure you’re not deciding
                                    the fate of any kids at try-outs before they actually try-out! Treat every kid the same to
                                    preserve the perception of fairness and impartiality. For example, have coaches demonstrate
                                    drills rather than asking a kid who was on the team last year to show it, and potentially give
                                    an impression of favoritism. Perception is reality for most players and parents!
                                </li>
                            </ul>
                            <h3>On the Ice<span class="tve_image_frame"><br/>
</span></h3>
                            <p>There’s no right or wrong way to run your try-outs on the ice and obviously there are a lot of
                                considerations that go into how you structure things: age, ability level, coaching resources, number
                                of kids, etc. Trust your hockey experience and instincts to organize your try-out the best way you
                                can. Hey, you’re the coach for a reason!</p>
                            <p>Here are some things for you to consider:</p>
                            <p><b>Warm-ups<br/>
                                </b>Obviously if time is a constraining factor, you want to get down to business and warming up is
                                probably low on the priority list for many coaches during try-outs. Think about this though: not
                                only is a good warm-up important to allow kids to perform at their best (isn’t that what we’re
                                looking for here?), but watching who takes their warm-up seriously and who just goes through the
                                motions can start to tell you a lot about kids’ attitudes.</p>
                            <p><b>Skill Drills</b><br/>
                                Skill drills are always an important component of try-outs since they can give you a good idea of
                                where your kids all stack up skill-wise, as well as further insight into each kid’s attitude towards
                                doing skill work. As a coach, you should be careful to choose drills that are developmentally
                                appropriate for the level of the kids trying out.</p>
                            <p>Keep your life simple! Think about choosing drills that allow you to evaluate single, specific
                                criteria rather than drills where you’re trying to judge several different things all at the same
                                time. For example, drills that specifically evaluate things like transitioning from forwards to
                                backwards skating, puck handling and giving and receiving passes, might be more accurately evaluated
                                if they were all part of their own quicker drills.</p>
                            <p><b>Competitive Drills</b><br/>
                                Including one-on-one and small group competitive drills is another great way to get a better idea of
                                where kids measure up against each other and see how well they compete. One-on-one’s and
                                two-on-one’s can help a lot to evaluate each kid’s ability to move the puck offensively and play
                                defense. These small group set-ups also allow you to easily manipulate the match-ups on each
                                repetition as you see fit to get a sense of where certain kids stack up against each other.</p>
                            <p><b>Scrimmages</b><br/>
                                In addition to the standard full team scrimmages, think about spending some time doing full ice
                                three-on-three’s. This spreads out the game and gives more space for you to carefully assess each
                                player’s skills as well as their &#8220;hockey sense,” creativity, positioning, conditioning,
                                ability to beat guys one-on-one, how much hustle they show on defensively and of course their
                                overall attitude.</p>
                            <p>Full team scrimmages are also an important tool in your arsenal. Continue to change up the lines
                                frequently to get different looks and vary the competition, giving you a fuller picture of each kid.
                                Remember that you’re not just observing their skills here, but looking for their hockey sense, as
                                well &#8211; their ability to read the situation, see the big picture, be in the right spot,
                                anticipate the puck, etc. Creativity, ability to read the play and continuous movement are also good
                                things to watch for in these scrimmages that skill work alone won’t show you.</p>
                            <p><b>Goalie Considerations</b><br/>
                                Goalies can be a unique animal when it comes to try-outs so it’s important to think about the best
                                way to handle them ahead of time. Depending on how many goalies you have trying out and on your
                                ice-time limitations, you might consider having a goalie-only try-out session sometime before the
                                main try-outs. That allows you to initially screen out anyone whose skills are not yet at the right
                                level before your actual try-outs. That can be important, as having too many goalies at your regular
                                try-outs can make it hard to evaluate each of them.</p>
                            <p>Because goalies are unique, another thing to think about might be assigning one coach to be a
                                goalie-only evaluator. In addition, when assessing goalies try to commit at least some of your ice
                                time to “goalie friendly” drills. For example, having multiple players line up pucks and shoot rapid
                                fire at a goalie, might test his reaction time, but is not a very game-realistic scenario!
                                Obviously, in addition to these drills, watching them in scrimmage situations is also important.</p>
                            <h3>Choosing Your Team<span class="tve_image_frame"><br/>
</span></h3>
                            <p>This is real life. Deciding on your roster is usually a bit more complicated than we’d like it to be.
                                So let’s quickly touch on a few things to think about to help you out here!</p>
                            <h3>Take Advantage of Your Evaluators</h3>
                            <p>Hey, they’re here for a reason! At the end of each session, block out some time to sit down with your
                                evaluators and compare notes. Be prepared for different people to have different opinions about the
                                same kid, though. Invariably, different coaches will notice different things, and that’s ok! In
                                fact, that’s the whole reason you asked for help! It’s not possible to see everything yourself. The
                                important thing here is, if they’re seeing something different than you, figure out why.</p>
                            <p>If you’re looking to simplify things, another great tactic is to ask your evaluators to rank all the
                                kids trying out, with each kid on either an offensive or defensive list. This can be a quick way to
                                confirm or to question what your thoughts were and can be an easy jumping off point for the
                                conversation where there are discrepancies.</p>
                            <p>Another useful tactic is to consider having someone keep track of how many goals each kid scores over
                                the course of the try-outs in both drills and scrimmages. This can be a good way to help identify
                                the kids who just have a better knack for finding the net.</p>
                            <h3>Narrow Your Focus</h3>
                            <p>Towards the end of your try-outs, there will likely be several kids you already know will make the
                                team and several kids who will not. So let’s take advantage of that and make our lives a bit easier.
                                Now’s the time when you can selectively ignore those kids while watching practice, freeing you up to
                                pay closer attention to kids on the bubble. This number should hopefully decrease as the try-outs
                                progress.</p>
                            <p>If you have several sessions in which to run your try-outs, you can also consider cutting the kids
                                you know won’t make it at the end of each session and bring back fewer kids for the subsequent
                                sessions. This lets you watch the kids on the bubble a little more carefully and helps you avoid the
                                situation where a &#8220;bubble kid” maybe looks better than he really is due to match-ups against
                                some of the less-skilled kids in the earlier sessions.</p>
                            <p>Skills aside, you might also think about what each kid would contribute to the group or team dynamic.
                                This could be the difference maker for some of these players and the best fit for your team is not
                                necessarily the biggest, fastest or strongest kid.</p>
                            <h3>Do the Parents Make the Team?</h3>
                            <p style="margin-bottom: 20px !important;">One last thing you might think about is: How are a kid’s
                                parents going to be to deal with? Hate to say it, but this has to be a real thought for coaches in
                                this day and age.</p>
                            <ul class="thrv_wrapper" style="margin-top: 0px !important; margin-bottom: 10px !important;">
                                <li class="">Are they high maintenance?</li>
                                <li class="">Do they bad mouth other players, teams, clubs or coaches they’ve been involved with?
                                </li>
                            </ul>
                            <p>These are very real factors that can not only contribute to your enjoyment of the season but to the
                                cohesiveness of your team as a whole!</p>
                            <h3>Letting them Know<span class="tve_image_frame"><br/>
</span></h3>
                            <p>No coach enjoys telling a kid they didn’t make your team, but it’s a part of the process we can’t get
                                rid of. The way you handle cuts as a coach is so important for several reasons. First, fair or not,
                                it’s a way that many gauge your professionalism and your compassion for the kids you coach. It can
                                have a big influence on how people view both you and your organization, team or club. Let’s take a
                                quick look at a few ways to make it easier, less-stressful and more painless for everyone
                                involved!</p>
                            <h3>The Sealed Envelope Technique</h3>
                            <p>Once again, there’s no right or wrong way to do you cuts, but this is one method that has been
                                effective. (Gotta give credit to HockeyShare’s own Coach Kevin Muller for this one!)</p>
                            <p>After the final tryout, each player receives a sealed envelope with their name on it. The envelope
                                will either have a &#8220;congratulations&#8221; letter or a &#8220;thanks for trying out&#8221;
                                letter. The players receive the envelope AFTER they’ve showered and are ready to leave. Players are
                                instructed NOT to open the letters until they get to their cars (parents are informed the same
                                thing). This way, if they&#8217;re cut, they&#8217;re already in the car and can leave quietly
                                without embarrassment and it also decreases the likelihood of an angry parent coming back into the
                                rink.</p>
                            <p>Players selected are asked to come back inside for a meeting.</p>
                            <h3>The 24-Hour Rule</h3>
                            <p>The day after the try-outs end, post the selected roster online. Let all parents know that any
                                conversations about your decisions will not be held until after the roster is posted. Emotions can
                                be high for certain parents immediately after the letters are distributed and that might not lend
                                itself to productive discussion. This period will hopefully allow those who are unhappy some time to
                                cool off and have a productive discussion if warranted.</p>
                            <p>Another good suggestion is to wait until after 9 PM the day after try-outs to post your roster, to
                                further minimize the likelihood of getting any calls until the following day.</p>
                            <h3>Giving Feedback</h3>
                            <p>It’s important to let each kid get some feedback as to where they need to improve. I’ve always felt
                                that if we’re going to cut a kid, we have a responsibility to let him or her know what they can do
                                to improve for next season and give them some hope. After all, as ambassadors of the sport, we don’t
                                ever want our try-out to be the reason a kid quits!</p>
                            <p>As much as we might not want to deal with unhappy parents, giving the kids some feedback is just the
                                right thing to do. One suggestion is to set aside an afternoon in the coming days when the team is
                                not practicing to have 15 minute meetings with any player or parent interested in receiving
                                feedback. It could also be done over email or phone, but is much more impactful for the player when
                                done face-to-face. Always start by telling the player what they did well and then transition into
                                where they need to improve.</p>
                            <p>Obviously, we need to be prepared for those who are critical of your decisions. The fact is you’re
                                never going to be able to please everyone. If you’re the coach, it’s your team and your decisions
                                but there will likely be a few parents, board members or friends who are unhappy and make it known
                                that they disagree with your decisions, and that’s ok. In these meetings, always make sure to be a
                                good listener, first and foremost. If a parent or player is talking, sit quietly and listen. Don’t
                                cut them off, don’t interrupt and let them finish completely. If needed, take notes so you can
                                respond to all the points once they are done talking. Many times parents just want to be able to say
                                their peace, and by letting them go until they&#8217;re completely finished, you’re allowing them to
                                get that satisfaction. Even if they still disagree with you, they’ll never be able to say that you
                                didn’t listen to them!</p>
                            <h3>Putting It All Together</h3>
                            <p>Coaching hockey is fun, rewarding and challenging all at the same time. You as coaches are in the
                                unique position to share your passion and knowledge with the next generation of players and coaches
                                and help create great young men and women in the process.</p>
                            <p>Let’s be honest, it sucks to have an otherwise AWESOME experience become a stressful, frustrating
                                headache because of try-outs.</p>
                            <p>Now, imagine a situation where your try-outs run smoothly, your decision-making process is easier
                                than ever and parent complaints are at an all-time minimum.</p>
                            <p>How much more would you enjoy your coaching position? How much more quickly could you just get to
                                work doing what you do best &#8211; coaching and developing your team?</p>
                            <p>Hopefully this guide has given you some great ideas to get there, but we want to make this system
                                bulletproof for you by giving you the materials you need to solve these problems now!</p>
                            <p><b>So&#8230; as a special offer only for HockeyShare readers, we’ll send you our premium <span
                                            style="text-decoration: underline;"><span
                                                class="underline_text">Try-Out Survival Kit</span></span> for <span
                                            style="color: #e60e0e;">FREE</span>. Just tell us where to send it.</b></p>
                            <hr/>
                            <p><i><span style="font-weight: 400;"><br/>
Pete Jacobson created WinSmarter to help coaches with the biggest frustrations we all sometimes struggle with: things like dealing with difficult parents, motivating your players, recruiting more kids into your program, fundraising, increasing participation in off-season activities and much more. Get started right now with the </span></i><a
                                        href="http://www.winsmarter.com/go-hockeyshare-com"><b><i>Hockey Coaches’ Try-Out Survival
                                            Kit</i></b></a><i><span style="font-weight: 400;">.</span></i></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <div>No Comments
                    </div>
                    <br/>
                    <p>No comments yet.</p>

                    <p>
                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/feed/"><abbr
                                    title="Really Simple Syndication">RSS</abbr> feed for comments on this post.</a></p>

                    <p>Sorry, the comment form is closed at this time.</p>

                    <br/>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

