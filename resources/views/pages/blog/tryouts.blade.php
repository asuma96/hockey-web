@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">

                    <div class="post-997 post type-post status-publish format-standard hentry category-comments-thoughts tag-talent tag-tryouts"
                         id="post-997">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/tryouts-7-factors-other-than-talent/"
                               rel="bookmark">Tryouts: 7 Factors Other than Talent</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/talent/"
                                                    rel="tag">talent</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As many clubs enter the most stressful time of the year &#8211; tryouts &#8211; I wanted
                                to share a few thoughts on factors coaches consider when making decisions. The talent
                                aspect is obvious &#8211; talented players are what coaches look for on the ice when it
                                comes to performance, but there are other aspects coaches use to make their final
                                decisions. Here is a list of a few:</p>
                            <ol>
                                <li><strong>Coachability</strong> &#8211; can the player take direction, or does the
                                    player think he/she &#8220;knows it all&#8221;? This is arguably the most important
                                    quality of a player &#8211; even above talent.
                                </li>
                                <li><strong>Work Ethic</strong> &#8211; is the player inherently lazy, or do they give
                                    you full effort every time they&#8217;re on the ice? Lazy players make coaching more
                                    difficult and decreases the efficiency of the coach &#8211; he/she will need to
                                    focus more on getting an honest effort, rather than teaching.
                                </li>
                                <li><strong>Accountability</strong> &#8211; does the player have a good track record to
                                    showing up to all the practices, games, and team functions&#8230;or is there always
                                    a reason they can&#8217;t make it? When players miss practices, coaches are forced
                                    to revisit old topics instead of being able to build off them.
                                </li>
                                <li><strong>Club History</strong> &#8211; has the player been in the association for an
                                    extended period of time, or are they known for jumping from club to club every
                                    season? Coaches concerned about player development want players who will likely be
                                    with them for multiple years.
                                </li>
                                <li><strong>Team Fit</strong> &#8211; does the player&#8217;s style of play fit in with
                                    what the team needs? Teams don&#8217;t need 20 players who have amazing hands but
                                    will never go into a corner or finish a check. Good teams have players that fit
                                    different roles within the team. This is often where players with more talent can be
                                    passed by in favor of a player who possesses the skills needed to round out a team.
                                </li>
                                <li><strong>Other Coaches Recommendations</strong> &#8211; hockey is a small world.
                                    Coaches often look to previous coaches for advice. If a player was nothing but a
                                    pain for another coach, there&#8217;s a good chance the next coach down the road
                                    will know about it as well.
                                </li>
                                <li><strong>Parents</strong> &#8211; believe it or not, this can factor in to decisions.
                                    Are the player&#8217;s parents known for being a bit &#8220;crazy&#8221;? Did they
                                    openly bash the club, team, or coaching staff when things were not going well?
                                    Coaches are humans &#8211; like it or not, most coaches will take a player with a
                                    bit less talent, but a family who is supportive over a player with more talent, but
                                    has crazy parents.
                                </li>
                            </ol>
                            <div>Good luck to all the coaches, players, and parents as we begin the process of another
                                great hockey season! &#8230;.and remember, sometimes the most important team is the one
                                you <span style="text-decoration: underline;"><strong>DON&#8217;T</strong></span> make
                                &#8211; those can be the ones that push you to a new level and force you to re-evaluate
                                where you&#8217;re at and why you didn&#8217;t make it!
                            </div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-916 post type-post status-publish format-standard hentry category-cool-links tag-tryouts"
                         id="post-916">

                        <h2><a href="http://hockeyshare.com/blog/cool-links/tryouts-past-posts/" rel="bookmark">Tryouts
                                &#8211; Past Posts</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/cool-links/"
                                                rel="category tag">Cool Links</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As many teams begin prepping for tryouts, I wanted to share some of the content we&#8217;ve
                                posted in the past regarding the topic. Tryouts are certainly a tense time &#8211;
                                hopefully some of the tips and topics I&#8217;ve written about in the past will be
                                helpful for you moving forward! Best of luck!</p>
                            <p>Past Posts on Tryouts: <a href="http://hockeyshare.com/blog/?s=tryouts">http://hockeyshare.com/blog/?s=tryouts</a>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-693 post type-post status-publish format-standard hentry category-resources tag-scouts tag-tryouts tag-youth-hockey-coaching"
                         id="post-693">

                        <h2><a href="http://hockeyshare.com/blog/resources/qualities-coaches-scouts-look-for/"
                               rel="bookmark">Qualities Coaches &#038; Scouts Look For</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">15</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/resources/"
                                                rel="category tag">Resources</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/scouts/"
                                                    rel="tag">scouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Here&#8217;s an article from Minnesota Hockey outlining some key points coaches and
                                scouts look for in hockey players:</p>
                            <p><a href="http://go.madmimi.com/redirects/7de2629d8ce2ea5be4755ab310d9ddf7?pa=1814571722">Download
                                    the PDF from Minnesota Hockey</a></p>
                            <p>Certainly some great points for players to take into their tryouts!</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-670 post type-post status-publish format-standard hentry category-comments-thoughts tag-tryouts tag-youth-hockey"
                         id="post-670">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-coaches/"
                               rel="bookmark">5 Tryout Tips for Coaches (Part 3 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey/" rel="tag">youth hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In part 3 of 3, we cover tips for coaches during tryouts. It&#8217;s no secret tryouts
                                can be one of the most stressful points of the season. With a few well-planned parts to
                                your sessions, you can eliminate a lot of the difficulty typically associated with this
                                time of year.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-coaches/#more-670"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-667 post type-post status-publish format-standard hentry category-comments-thoughts tag-tryouts tag-youth-hockey"
                         id="post-667">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-parents/"
                               rel="bookmark">5 Tryout Tips for Parents (Part 2 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">21</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey/" rel="tag">youth hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This post is part 2 of a three-part series revolving around tryouts. These posts will
                                cover tryout tips for players, parents, and coaches. This focus is on tips/thoughts for
                                parents. Parents have one of the most difficult parts of the process &#8211; they&#8217;re
                                utterly helpless, everything is in the hands of the player and coaches.</p>
                            <p>Disclaimer: These &#8220;tips&#8221; for parents come from a <strong>coaching </strong>perspective.
                                They&#8217;re not meant to be &#8220;scolding&#8221; in any way &#8211; instead, they&#8217;re
                                simply to help give a coaches perspective on some common interactions during one of the
                                most difficult times of the season.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-parents/#more-667"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-665 post type-post status-publish format-standard hentry category-comments-thoughts tag-tryouts tag-youth-hockey"
                         id="post-665">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-players/"
                               rel="bookmark">5 Tryout Tips for Players (Part 1 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">19</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey/" rel="tag">youth hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>During the tryout times, I get a lot of players who ask me about the tryout process. Over
                                the past year or so, I&#8217;ve written several pieces about tryouts (just <a
                                        href="http://hockeyshare.com/blog/?s=tryouts">search this blog for &#8220;tryouts&#8221;</a>),
                                but I wanted to give players (hopefully some of my own trying out as well) a couple
                                quick tips for entering tryouts.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/5-tryout-tips-for-players/#more-665"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-656 post type-post status-publish format-standard hentry category-comments-thoughts tag-commentary tag-juniors tag-tryouts"
                         id="post-656">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/pins-needles/" rel="bookmark">Pins
                                &#038; Needles</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">23</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/commentary/" rel="tag">commentary</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/juniors/" rel="tag">juniors</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Some may have been wondering where I&#8217;ve been for the past few weeks, and why the
                                post consistency has decreased. I&#8217;ve been heavily involved in taking the ice down
                                and re-installing it at our local rink. This weekend I&#8217;m actually up in Minnesota
                                watching an NAHL tryout. As I sit in the lobby observing about a hundred kids coming in
                                (some I&#8217;ve coached, some I&#8217;ve coached against), one thing is very apparent&#8230;.everyone
                                is on pins &amp; needles. This is the final tryout camp for one of the teams in the
                                North American Hockey League. Players enter with the hopes of making a high-level
                                US-based junior team. Parents wait nervously in the stands and lobby, sometimes pacing
                                back and forth, chain smoking, or just sitting there fidgeting. It&#8217;s fun hockey to
                                watch because every player on the ice is competing. There are some obvious cuts and some
                                obvious returning players, but the rest remains very close in talent. Many perceptions
                                of junior hockey tryouts are they serve primarily as a fundraiser for the organization.
                                I suppose if you broke it down, it&#8217;s easy to see how that would be an easy
                                conclusion to jump to. Just for fun, here are the numbers of the camp I&#8217;m
                                watching:</p>
                            <p>100 players (approximately) x $250 each player = $25,000</p>
                            <p>Ice expense of approximately 25 hours at $150/hr = $5,250</p>
                            <p>Total <span style="text-decoration: underline;">approximate </span>profit: $19,750
                                &#8211; not bad for a weekend&#8217;s work! No matter what the dollar amount equals out
                                to, my main hope is the players attending are being treated honestly and fairly. Let me
                                make it clear by saying I am NOT saying they aren&#8217;t being treated fairly/honestly.
                                I hope that when players attend ANY junior tryout (or any other level tryout for that
                                matter), the coaching staff is having a completely honest conversation with the players
                                as opposed to stringing them along to get more money out of them. Good luck to all the
                                skaters on the ice this weekend!</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-542 post type-post status-publish format-standard hentry category-comments-thoughts tag-hockey-coaching tag-tryouts tag-youth-hockey-coaching"
                         id="post-542">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/tryout-overview-part-2/"
                               rel="bookmark">Tryout Overview [Part 2]</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">16</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/youth-hockey-coaching/"
                                                    rel="tag">youth hockey coaching</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>So, now you have taken the time to <a
                                        href="http://hockeyshare.com/blog/comments-thoughts/tryout-overview-part-one/">properly
                                    plan out your tryout sessions</a>, and now find yourself with an evaluation sheet in
                                hand. Many coaches get overwhelmed during this process &#8211; after all, there is a lot
                                to watch. Assuming you&#8217;re breaking your tryout into three distinct drill
                                categories &#8211; skill, competitive, scrimmage &#8211; we will take a look at what
                                skills and subtleties to watch for in each of these three areas.</p>
                            <p><strong>Skill Drills</strong>: In order to properly run skill drills, you need a good
                                comprehension of the talent level you&#8217;re working with. At the younger and/or lower
                                caliber levels, your skills should focus on the basics: forward skating, backward
                                skating, cross overs, stopping, basic puck handling, basic shooting, and basic passing.
                                <span style="background-color: #ffffec;">The older and/or higher caliber the group, the more you should implement drills to force the performance of skills at a higher pace.</span>
                                Evaluators should watch for notable aspects (both good and bad) of each player&#8217;s
                                ability in the following categories: skating, shooting, passing, puck handling. Each of
                                these skills should be broken down into the appropriate drills for the level. For
                                example, to work on cross-overs (skating), you could break it down as follows:</p>
                            <p><em><span style="color: #888888;">Beginner Groups</span></em>: Basic cross-overs around
                                the circles &#8211; watch for basic technique and balance</p>
                            <p><em><span style="color: #888888;">Intermediate Groups</span></em>: Cross-overs around the
                                tops &amp; bottoms of the circles only (not all the way around) &#8211; watch for basic
                                technique, balance, speed, and ability to transition between cross-over directions.</p>
                            <p><em><span style="color: #888888;">Advanced Groups</span></em>: Have players skate down
                                the ice performing one cross-over to the right, then one cross-over to the left (repeat
                                the length of the ice) &#8211; watch for technique, speed, transition between
                                directions, ability to maintain balance/strong body position, and generation of power
                                with each push.</p>
                            <p><span style="background-color: #ffffec;">The most important skill to watch for during these drills is skating.</span>
                                If a player can skate well, the rest of the game gets a lot easier. Skating affects
                                every aspect of the game &#8211; from a foot race to a loose puck, to maintaining
                                balance in front of an opposing team&#8217;s net. Players who skate efficiently are
                                often times easier to work with when it comes time for positioning. While evaluating
                                skating abilities, be sure to <span style="background-color: #ffffec;">include drills that force players to change directions and move laterally</span>.
                                It doesn&#8217;t do any good to be the fastest skater from end-to-end if you aren&#8217;t
                                able to turn or change directions while maintaining your momentum.</p>
                            <p><strong>Competitive Drills</strong>: During competitive drills is where you look for your
                                work-horses. <span style="background-color: #ffffec;">Small area games and in-tight competitive drills often expose strengths and weaknesses quicker than any other types of drills</span>
                                since the players have no place to hide or blend in. One of my favorite drills to run
                                during tryouts is the <a href="http://hockeyshare.com/drills/drill.php?id=187">Corner
                                    Battles</a> drill. This simple one-on-one drill shows me right away who is willing
                                to mix it up in the corners. I also recommend running 1 on 1 drills to allow you to
                                isolate both forwards and defensemen. The <a
                                        href="http://hockeyshare.com/drills/drill.php?id=194">1 on 1 Full Ice</a>
                                drill is one of the oldest, most basic 1 on 1 drills there is &#8211; but it works
                                great. You get to see how the defensemen handle the puck, shoot, set their gap, and
                                handle the rush. You also get to see if your forwards are willing to get in front of the
                                net, have the necessary speed, have creativity, and the desire to fight through a check.
                                <span style="background-color: #ffffec;">Coaches shouldn&#8217;t be afraid to adjust the lines to get a desired matchup on rushes or battle drills.</span>
                                It&#8217;s a tryout, and you&#8217;re looking to see the level each player can compete
                                at.</p>
                            <p><strong>Scrimmage Time</strong>: This is an evaluator&#8217;s time to see if the notes on
                                a player thus far transfer into game-scenarios. It&#8217;s also the <span
                                        style="background-color: #ffffec;">time to find out which players have the much-coveted &#8220;hockey sense.&#8221;</span>
                                Hockey sense is (simply put) the ability to see the ice, properly anticipate plays, and
                                react accordingly. This portion of the tryouts often makes decisions for coaches even
                                more difficult. Many times, you can run into a player who has a great core set of
                                skills, but isn&#8217;t able to translate them into game scenarios. You may also run
                                into the opposite &#8211; a player with a weak core set of skills, but seems to get the
                                job done consistently. Which player you give preference to when choosing your team is
                                your own personal decision. In my opinion, I would rather take a player who can perform
                                during a game with a weaker core set of skills than one who has good skills but no
                                game-time performance &#8211; with one HUGE caveat: the player MUST have a strong
                                skating ability.</p>
                            <p>A couple final thoughts on the tryout process&#8230;</p>
                            <p>If you have multiple people evaluating, don&#8217;t be surprised if you have differing
                                opinions on players. Different people look for different things while they&#8217;re
                                evaluating. <span style="background-color: #ffffec;">I always recommend having neutral hockey-knowledgable evaluators assist in picking your team.</span>
                                By neutral, I simply mean they do not have a child on the ice being evaluated, and have
                                not coached the majority of the players in the past. Ideally, you get someone who has no
                                connection with any of the players on the ice. This removes a level of emotion from
                                decisions.</p>
                            <p>Finally, <span style="background-color: #ffffec;">accept the fact that you most likely will not please everybody</span>.
                                Don&#8217;t give in to threats of &#8220;if my kid doesn&#8217;t make the top team, we&#8217;re
                                taking him/her somewhere else.&#8221; To those situations, my typical response is
                                &#8220;do what you have to do.&#8221; While it may come off as terse, I firmly believe
                                coaches need to evaluate as fairly and impartially as possible. Coaches will often be
                                put in tough personal and political situations during tryouts. Personally, I&#8217;ve
                                had to cut board member&#8217;s players, cut friend&#8217;s players, and have had people
                                stop talking to me because of my decisions. Make your decisions with integrity and stick
                                to your guns.</p>
                            <p>Good luck to all the players and coaches going through the tryout process!</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-64 post type-post status-publish format-standard hentry category-comments-thoughts tag-hockey-coaching tag-tryouts"
                         id="post-64">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/my-thoughts-on-tryouts/"
                               rel="bookmark">My Thoughts on Tryouts</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">11</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-coaching/"
                                                    rel="tag">hockey coaching</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/tryouts/" rel="tag">tryouts</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Every year I get countless questions about what I look for during tryouts. I thought I&#8217;d
                                summarize some of my thoughts on tryouts for everyone. Hopefully players, coaches and
                                parents can get some value in these ideas.</p>
                            <p>So&#8230;what am I looking for at tryouts?</p>
                            <p><strong>Character</strong> &#8211; This tells me more about you than any skill you may
                                demonstrate on the ice. There are a lot of different pieces to this – I’ll cover just a
                                few here:</p>
                            <p><strong>Drive</strong> &#8211; are you continuously pushing yourself and giving it your
                                best effort? If you&#8217;re not going to do that at tryouts, why would I think you&#8217;re
                                going to do it in the State Championship game???</p>
                            <p><strong>Mental toughness</strong> &#8211; if you make a mistake, how do you react to it?
                                Guess what &#8211; every single player that&#8217;s ever played the game makes mistakes!
                            </p>
                            <p>“<em>Life is 10% of what happens to me and 90% of how I react to it.</em>” – John Maxwell<br/>
                                When you make a mistake, do you give up? grab another puck in the drill? slam your stick
                                on the boards/ice (I categorize this as &#8220;pouting&#8221;…this is a pet peeve of
                                mine)? When you make a mistake, brush yourself off, learn from it, and keep playing!
                                Realize you WILL make mistakes, don’t hold yourself to the impossible standard of being
                                perfect, or you’ll never live up to your own expectations and you’ll always be
                                frustrated! Frustrated players seldom perform well!</p>
                            <p><strong>Physical toughness</strong> &#8211; are you soft on the puck, or do you finish
                                every check? Are you willing to take a hit to make a play, or will you bail out? Hockey
                                is a physical game – I want players who are willing to be tough and put it all on the
                                line in key situations.</p>
                            <p><strong>Leadership</strong> – will you step up and lead a drill? Help other players out?
                                Or will you sneak to the back of the line so that the coaches won’t notice your
                                mistakes? Everyone is a leader, but in different ways. If you’re a talker – talk. If
                                you’re a worker – work. If you’re enthusiastic – be enthusiastic.</p>
                            <p><strong>Hockey Sense</strong> – Players who can see and read the ice well will make plays
                                for you – even if their skill level isn’t at the same level as others. Hockey Sense is
                                one of the more difficult aspects of our game to teach. I believe it can be taught, but
                                not nearly as easily as skills.</p>
                            <p><strong>Skill</strong>– the primary skill I look at is skating. If you can skate well,
                                the rest of the game is easy. Good skaters will consistently win races to pucks, win
                                battles in the corner, and put themselves in the better positions. I can’t think of a
                                single hockey skill more important than skating.</p>
                            <p><strong>Positional Play</strong> – if you’re in position, you’ve got a much better chance
                                at making the proper play and/or decisions. Do I prefer players with skill over
                                positioning? Absolutely! Don Lucia summarized it quite well by saying: “If you can’t do
                                it technically, you can’t do it tactically.” If you’ve got the core skills down,
                                teaching the proper positions shouldn’t be difficult.</p>
                            <p><strong>The “Little Things”</strong> – this is also quite a large category. When I refer
                                to “little things” I’m not referring to them being insignificant. To me, it’s the
                                “little things” that make “big things” possible (such as championships). Many of these
                                tie in closely with “Character”, but I’ll list a few of the “little things” that I look
                                for.</p>
                            <p><strong>Preparedness</strong> &#8211; did you come to the rink ready to go, or did your
                                skates need to be sharpened…or did you forget your jerseys….or are you missing a piece
                                of equipment. Those distractions before a game or tryout will be enough to drive any
                                coach crazy. Make sure you’re ready to go when you get to the rink anytime – not just at
                                tryouts.</p>
                            <p><strong>Drill Wreckers</strong> – pay attention when coaches are explaining drills.
                                There’s nothing more frustrating that a player who is fourth in line and messes the
                                drill up after the first 3 executed it without problem. That’s a lack of focus!</p>
                            <p><strong>Skate Hard to the Bench</strong> – if you’re scrimmaging, don’t slowly wander
                                your way to the bench. Skate your butt off from the time you set foot on the ice until
                                you’re back on the bench. The vast majority of players DON’T do this…so if you’re one of
                                the few that does, coaches will notice!</p>
                            <p><strong>Shooting Pucks After the Coach’s Whistle</strong> – as a coach, few things annoy
                                me more than when players waste valuable ice time shooting pucks after the whistle has
                                blown. If the coach blows the whistle, skate hard to the huddle (or next station).</p>
                            <p><strong>Final Thoughts to Players….</strong><br/>
                                I’ve got just a few final thoughts for any player going into tryouts. Don’t be an
                                invisible player – if you’re afraid to make mistakes, most likely nobody will even
                                realize you’re on the ice. You’ll just sort of blend in with every other player on the
                                ice. During tryouts, it’s good to stand out – even if sometimes it’s for the wrong
                                reasons. Here’s a scenario I see every year……</p>
                            <p>we’re running a full ice 1 on 1 drill…the forward makes a move, the defenseman catches an
                                edge and falls over. The forward walks in (now on a breakaway) and finishes the drill.
                                99% of the time, what does the defenseman do &#8211; gives up on the play.</p>
                            <p>Don’t be that player – now you’ve got 2 things working against you. First, you fell over
                                in a 1 on 1 drill…which is never a good thing, but more importantly, you just QUIT. Next
                                time something like that happens to you, get up and chase down that forward – no matter
                                how far ahead he/she is. I guarantee you if you do that, most coaches will put much more
                                merit into that extra effort and character you showed than the fact that you fell
                                over.</p>
                            <p>Good luck to everyone!</p>
                            <p>Keep your skates sharp, skate hard, and keep your head up. See you around the rinks.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

