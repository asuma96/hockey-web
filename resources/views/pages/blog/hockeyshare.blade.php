@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1153 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-drill-diagrammer tag-hockeyshare"
                         id="post-1153">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/2-new-features-added-to-drill-diagrammer/"
                               rel="bookmark">2 New Features Added to Drill Diagrammer</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagrammer/"
                                                    rel="tag">drill diagrammer</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <div>We&#8217;re excited to announce two new highly-requested feature additions to the
                                HockeyShare Drill Diagrammer. Now you can create &#8220;Custom Objects&#8221; where you
                                are able to change not only the numbering, but also the main text of the object. We also
                                added an option to create a drill on a blank surface &#8211; this is great for drawing
                                station drills where you want to go in to more depth without being constrained by the
                                lines on the rink. We put together a quick video to highlight the new changes:
                            </div>
                            <div></div>
                            <div style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                           class="youtube-player"
                                                                                           type="text/html" width="640"
                                                                                           height="360"
                                                                                           src="//www.youtube.com/embed/81m5I-L72bc?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                           frameborder="0"
                                                                                           allowfullscreen></iframe></span>
                            </div>
                            <div style="text-align: center;"></div>
                            <div><strong></strong>To celebrate the new changes, this week only we&#8217;re offering a
                                <strong>10% discount</strong> on monthly memberships and <strong>20% off</strong> on all
                                annual, team, and association memberships. Hurry &#8211; offer expires soon! Just use
                                discount code <a><strong>ML0312</strong></a><strong></strong>.
                            </div>
                            <div></div>
                            <div></div>
                            <div style="text-align: center;"><a
                                        href="http://hockeyshare.com/drills/signup.php?discount_code=ML0312">Sign
                                    up today to lock in savings</a>. <a
                                        href="http://hockeyshare.com/drills/learn_more.php?hs_a_id=1029">Learn more
                                    about the drill diagrammer &amp; practice planner</a>.
                            </div>
                            <div style="text-align: center;"></div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <br/>


                    <p>&nbsp;</p>

                    <div class="post-1082 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-features tag-hockeyshare"
                         id="post-1082">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/add-practice-plans-to-your-practice-calendar/"
                               rel="bookmark">Add Practice Plans to Your Practice Calendar</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">18</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/features/" rel="tag">features</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Introducing a new feature for <a href="http://hockeyshare.com/drills/compare.php">Premium</a>
                                users who use HockeyShare.com to <a href="http://hockeyshare.com/teams/">track their
                                    team stats</a>. Premium users can now link practice plans to specific entries on
                                their team&#8217;s practice calendar. See the video and screenshots below:</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/-RmfZET7wOU?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <div align="center">
                                <div id="attachment_1088" style="width: 510px" class="wp-caption aligncenter"><img
                                            class="size-full wp-image-1088" title="PracticeSchedulePlan-1-web"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2011/11/PracticeSchedulePlan-1-web.jpg"
                                            alt="" width="500" height="383"
                                            srcset="/img/PracticeSchedulePlan-1-web.jpg 500w, /img/PracticeSchedulePlan-1-web-300x229.jpg 300w"
                                            sizes="(max-width: 500px) 100vw, 500px"/>
                                    <p class="wp-caption-text">Select Your Practice from the New or Edit Practice Plan
                                        Page</p></div>
                                <p>&nbsp;</p>
                                <div id="attachment_1089" style="width: 510px" class="wp-caption aligncenter"><img
                                            class="size-full wp-image-1089" title="PracticeSchedulePlans-2-web"
                                            src="http://hockeyshare.com/blog/wp-content/uploads/2011/11/PracticeSchedulePlans-2-web.jpg"
                                            alt="" width="500" height="277"
                                            srcset="/img/PracticeSchedulePlans-2-web.jpg 500w, /img/PracticeSchedulePlans-2-web-300x166.jpg 300w"
                                            sizes="(max-width: 500px) 100vw, 500px"/>
                                    <p class="wp-caption-text">Download Icon Appears on Team&#39;s HockeyShare.com Team
                                        Practice Schedule Page</p></div>
                            </div>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-1074 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-hockeyshare tag-webinar"
                         id="post-1074">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/free-webinar-thursday-november-10th-2011/"
                               rel="bookmark">Free Webinar &#8211; Thursday, November 10th, 2011</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Nov</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">7</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/webinar/" rel="tag">webinar</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We’ve received a lot of requests about doing an overview / quick training webinar for our
                                <a href="http://hockeyshare.com/drills/learn_more.php">Hockey Drill Diagrammer &amp;
                                    Practice Planner</a>. This Thursday, November 10th, 2011 we will be holding a LIVE
                                online webinar at 9am CST. There is no cost to participate in the webinar – use the link
                                below to signup.</p>
                            <p><a href="https://www3.gotomeeting.com/register/390646198">Click Here to Register (Free)
                                    for the Webinar</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-1022 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-draw-drills tag-drill-diagramming tag-hockeyshare tag-practice-plan"
                         id="post-1022">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/drawing-a-drill-with-hockeyshares-online-drill-diagrammer/"
                               rel="bookmark">Drawing a Drill with HockeyShare&#8217;s Online Drill Diagrammer</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">6</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/draw-drills/" rel="tag">draw drills</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/drill-diagramming/"
                                                    rel="tag">drill diagramming</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/practice-plan/"
                                                    rel="tag">practice plan</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>I&#8217;ve had quite a few people as me for a demo video to show just how easy it is to
                                draw a drill using the HockeyShare Online Drill Diagramming platform. Since a picture
                                (or video) is worth a thousand words, I put one together showing how you can draw a
                                drill in just a minute or two and then have access to it any time:</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/DslXBpW-wYg?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark&amp;feature=player_embedded"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/drills/learn_more.php"><span
                                                    style="color: #ff0000;">Learn more about drawing drills w/ our platform  (14 day FREE trial available)</span></a></strong></span>
                            </p>
                            <p style="text-align: center;"><span style="color: #ff0000;"><strong><a
                                                href="http://hockeyshare.com/drills/compare.php"><span
                                                    style="color: #ff0000;">Compare subscription options</span></a></strong></span>
                            </p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-689 post type-post status-publish format-standard hentry category-10000-pucks category-hockeyshare-contests tag-10000-pucks tag-contests tag-hockeyshare"
                         id="post-689">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/"
                               rel="bookmark">10,000 Pucks &#8211; Final Results</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">13</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contests/" rel="tag">contests</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The results are in for the 10,000 Pucks 2010 contest. In this year&#8217;s contest, we
                                had 527 players logging shots from around the world. The total shot tally for this
                                season was an impressive: 1,922,853. Not only did we surpass our goal of seeing 1
                                million shots logged, we almost broke the 2 million mark! Now, the moment everyone&#8217;s
                                been waiting for&#8230;.the player results.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/#more-689"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbspnbsp;</p>

                    <div class="post-661 post type-post status-publish format-standard hentry category-10000-pucks tag-10000-pucks tag-hockeyshare"
                         id="post-661">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-update/" rel="bookmark">10,000
                                Pucks Update</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Well, it&#8217;s official &#8211; this year&#8217;s contest topped the charts with over
                                1.2 million shots logged from around the world! Wow &#8211; I would have never guessed a
                                few years back when we started running this it would ever get this big! Great to see
                                everyone working so hard to reach their goals&#8230;.which is the true purpose of this
                                contest.</p>
                            <p><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-update/#more-661"
                                  class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-645 post type-post status-publish format-standard hentry category-10000-pucks tag-10000-pucks tag-hockeyshare"
                         id="post-645">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-update/"
                               rel="bookmark">10,000 Pucks Contest Update</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">1</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As the first month of the contest draws to a close, we&#8217;ve seen a record 740,000 +
                                shots logged in the contest from over 480 participants across the world (yes, we even
                                have some over-seas participants). It&#8217;s great to see everyone so dedicated to
                                improving themselves.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-update/#more-645"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-616 post type-post status-publish format-standard hentry category-hockeyshare-contests tag-10000-pucks tag-hockeyshare"
                         id="post-616">

                        <h2><a href="http://hockeyshare.com/blog/hockeyshare-contests/10000-pucks-2010/"
                               rel="bookmark">10,000 Pucks &#8211; 2010</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>HockeyShare is excited to announce the launch of the 2010 &#8211; 10,000 Pucks, 1 Summer
                                Contest. This will be the 4th year of the contest, and we&#8217;re excited to see it
                                continue to grow. Last year we had over 500 participants from across the US &amp; Canada
                                shooting pucks.</p>
                            <p><a href="http://srv.ezinedirector.net/?n=3774676&amp;s=93138682" target="_blank">Visit
                                    the 10,000 Pucks Page</a></p>
                            <p><strong>Coaches: </strong>you can have your team compete and track their progress. Just
                                create your team, and have your players register for the contest (they can select their
                                team when they signup).</p>
                            <p><strong>Players:</strong> start logging your shots beginning June 1st.</p>
                            <p><a href="http://srv.ezinedirector.net/?n=3774677&amp;s=93138682" target="_blank">www.hockeyshare.com/10000pucks/</a>
                            </p>
                            <p>Skate hard &amp; keep your head up! See you around the rinks.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-555 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-hockeyshare tag-hockeyshare-features"
                         id="post-555">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/new-hockeyshare-look-launching/"
                               rel="bookmark">New HockeyShare Look Launching&#8230;</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">20</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-features/"
                                                    rel="tag">hockeyshare features</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Over the next few days we will be rolling out a major update to the website. The look
                                will be changed a bit, some new functionality will be added, areas will be cleaned up,
                                and the forums will be updated. This is not a minor site update, so please be patient
                                with us as we implement the changes. During this time, you may experience temporary
                                outages. We will be working to restore all functionality and access as quickly as
                                possible. The updates are expected to start this evening and finished by Thursday
                                evening.</p>
                            <p>We appreciate your patience while we keep trying to improve the site!</p>
                            <p><em><strong>Update</strong></em>: Good news &#8211; we&#8217;re expecting to have
                                Facebook Connect up and running shortly! This will allow you to log in (and register) to
                                HockeyShare with your Facebook username/password! Your username/password will not be
                                ever be visible to HockeyShare. FB Connect works by using a Facebook page to
                                authenticate users, then returns the status securely to the HockeyShare servers.</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <div class="post-302 post type-post status-publish format-standard hentry category-hockeyshare-com-features tag-hockeyshare tag-hockeyshare-features"
                         id="post-302">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockeyshare-com-features/help-hockeyshare-improve/"
                               rel="bookmark">Help HockeyShare Improve (Free E-Book)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Feb</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/"
                                                rel="category tag">HockeyShare.com Features</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare-features/"
                                                    rel="tag">hockeyshare features</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We&#8217;re always looking for new ways to make HockeyShare.com bigger and better. We&#8217;d
                                like to hear from you what you&#8217;d like to see added/improved upon. We created a
                                brief (2 multiple choice questions) survey to help us collect your feedback. We&#8217;d
                                love to hear from you! Click the link below to take the brief survey. As our way of
                                saying &#8220;thank you&#8221; for taking the time to help us grow, after you complete
                                the survey, you&#8217;ll receive a <span style="text-decoration: underline;"><strong>free download</strong></span>
                                of our &#8220;<em><strong>5 High-Tempo Small Area Games</strong></em>&#8221; e-book.</p>
                            <p style="text-align: center;"><a href="http://www.surveymonkey.com/s/XVNYD75"
                                                              target="_blank"><strong><em>Click Here to Take the
                                            Survey</em></strong></a></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <p>&nbsp;</p>

                    <a href="http://hockeyshare.com/blog/tag/hockeyshare/page/2/">Older Posts &raquo;</a>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

