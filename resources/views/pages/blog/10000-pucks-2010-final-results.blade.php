@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-689 post type-post status-publish format-standard hentry category-10000-pucks category-hockeyshare-contests tag-10000-pucks tag-contests tag-hockeyshare"
                         id="post-689">

                        <h2><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/"
                               rel="bookmark">10,000 Pucks &#8211; Final Results</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">13</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/"
                                                rel="category tag">10000 Pucks</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/"
                                                rel="category tag">HockeyShare Contests</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/10000-pucks/" rel="tag">10000 Pucks</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/contests/"
                                                    rel="tag">contests</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockeyshare/" rel="tag">hockeyshare</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The results are in for the 10,000 Pucks 2010 contest. In this year&#8217;s contest, we
                                had 527 players logging shots from around the world. The total shot tally for this
                                season was an impressive: 1,922,853. Not only did we surpass our goal of seeing 1
                                million shots logged, we almost broke the 2 million mark! Now, the moment everyone&#8217;s
                                been waiting for&#8230;.the player results.</p>
                            <p><span id="more-689"></span>In this year&#8217;s contest, there were 102 players who
                                reached their goal of shooting 10,000 Pucks. Congratulations to everyone reached this
                                milestone. Here were the top-10 shooters this summer:</p>
                            <ol>
                                <li><strong>rmbawcom &#8211; 65,577 shots</strong></li>
                                <li><strong>Matthew21 &#8211; 62,650 shots</strong></li>
                                <li><strong>d-man &#8211; 56,406 shots</strong></li>
                                <li>TeamV &#8211; 36,040 shots</li>
                                <li>Scoomes &#8211; 32,551 shots</li>
                                <li>Puckman_25 &#8211; 28,014 shots</li>
                                <li>Baileysommers &#8211; 23,594 shots</li>
                                <li>Chrisv99 &#8211; 20,000 shots</li>
                                <li>Ericbourhill &#8211; 17,805 shots</li>
                                <li>Eurbank &#8211; 16,141 shots</li>
                            </ol>
                            <p>Since HockeyShare is a community-based site, we would love to see photos of you/your
                                player shooting pucks this past summer. If you have photos you&#8217;d like to share,
                                please email them to kevin@hockeyshare.com and we&#8217;ll try to get a video made up
                                from all the participants who submit photos. The top-three players should contact me via
                                email (at the address above) to receive the prize claim forms.</p>
                            <p>Players reaching 10,000 Pucks:</p>
                            <p>rmbawcom<br/>
                                Matthew21<br/>
                                d-man<br/>
                                TeamV<br/>
                                scoomes<br/>
                                Puckman_25<br/>
                                baileysommers<br/>
                                chrisv99<br/>
                                ericbourhill<br/>
                                eurbank<br/>
                                Hockey_monkey<br/>
                                Max Williams<br/>
                                Tundra84<br/>
                                Ethan19<br/>
                                kheath<br/>
                                Zdog<br/>
                                shooter14<br/>
                                jcapano<br/>
                                21ryan<br/>
                                Dillon H<br/>
                                Megster<br/>
                                Murbank<br/>
                                Hockeyman99<br/>
                                luke22ojibwa<br/>
                                markstmartin89<br/>
                                JP12<br/>
                                MitchellStreety<br/>
                                rurbank<br/>
                                Marshall<br/>
                                BP12<br/>
                                nick4094<br/>
                                Josh Roesel<br/>
                                JPB-9<br/>
                                bvarady<br/>
                                Sstastney<br/>
                                A-Ron<br/>
                                rdacks<br/>
                                Ryan Liotta<br/>
                                pjh<br/>
                                hockey98<br/>
                                littledude<br/>
                                Tevin Neegaard<br/>
                                jurbank<br/>
                                unterriker<br/>
                                chanus<br/>
                                TR77<br/>
                                bunterriker<br/>
                                MADMAX<br/>
                                Michael Gebhardt<br/>
                                jacob<br/>
                                sabresrgr8<br/>
                                Christopher Macgregor<br/>
                                Togoble<br/>
                                justindavis74<br/>
                                blizzard2000<br/>
                                brianmueller<br/>
                                Tom B<br/>
                                dante17sugarboy<br/>
                                Dominic 88<br/>
                                Speed Demon<br/>
                                cheetah<br/>
                                BMMcAllister<br/>
                                Laserman<br/>
                                Princess2003<br/>
                                Cameron44<br/>
                                Tyler15<br/>
                                huntert<br/>
                                pmurph64<br/>
                                jreineck<br/>
                                coach vetter<br/>
                                MTKA-Justen<br/>
                                duncan23<br/>
                                sniper cantello<br/>
                                JT79<br/>
                                nhawayek<br/>
                                peterepperson<br/>
                                Moose29<br/>
                                N King<br/>
                                John Manning<br/>
                                Michael Clancy<br/>
                                tkemly23<br/>
                                CGammer<br/>
                                ww24<br/>
                                cho8<br/>
                                Camden13<br/>
                                willwhitney<br/>
                                JaxKatz<br/>
                                ethanjones<br/>
                                CL16<br/>
                                idr<br/>
                                DannyFitz<br/>
                                charlesl<br/>
                                NCarl<br/>
                                tylp24<br/>
                                Kyle Mangin<br/>
                                Joey2000<br/>
                                Pstastney<br/>
                                SingMeAway<br/>
                                Ryan Cherepacha<br/>
                                MM10<br/>
                                avajaschke<br/>
                                sidchard<br/>
                                Griffin<br/>
                                Alex Hess<br/>
                                Tony<br/>
                                tuscandream<br/>
                                jjudge7<br/>
                                logiebill<br/>
                                bgheble<br/>
                                Patrick Whalen<br/>
                                BTBN<br/>
                                Matthew Gaz<br/>
                                bailey87<br/>
                                Garret.Bates<br/>
                                andyb<br/>
                                Adam Macboy<br/>
                                BigB<br/>
                                jradmirals18<br/>
                                johnbeckett<br/>
                                Nash61<br/>
                                alex dicosmo<br/>
                                Moose9<br/>
                                Sam Brown<br/>
                                codysgameworld<br/>
                                bm30<br/>
                                Kyle Kluzak<br/>
                                bb25<br/>
                                camman4<br/>
                                taylorchiu<br/>
                                vequixr<br/>
                                Simonw<br/>
                                MrSnrub<br/>
                                mapulaski<br/>
                                anthonyscanzuso<br/>
                                Briantid<br/>
                                Wings8<br/>
                                BGammer<br/>
                                Jack66<br/>
                                DanielB<br/>
                                adamchard</p>
                            <p><strong>2011 Goal: 2 Million Pucks Shot!!!</strong></p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <div>No Comments
                    </div>
                    <br/>
                    <p>No comments yet.</p>

                    <p><a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-2010-final-results/feed/"><abbr
                                    title="Really Simple Syndication">RSS</abbr> feed for comments on this post.</a></p>

                    <p>Sorry, the comment form is closed at this time.</p>

                    <br/>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

