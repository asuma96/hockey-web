@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-950 post type-post status-publish format-standard hentry category-comments-thoughts"
                         id="post-950">

                        <h2>
                            <a href="http://hockeyshare.com/blog/comments-thoughts/usa-hockey%e2%80%99s-checking-rule-change-proposal-%e2%80%93-hitting-the-mark/"
                               rel="bookmark">USA Hockey’s Checking Rule Change Proposal – Hitting the Mark?</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" style="border:1px #CCC solid;">
                                            <tr>
                                                <td align="center" style="background-color:#F66; color:#FFF;">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#CCC; color:#000;">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p><span style="font-size: 13px; font-weight: normal;">USA Hockey will vote on a rule change in June, which would move the legal age for body checking from U12 (Peewee) to U14 (Bantam).  This rule change has spurred a lot of discussion among coaches debating on whether or not it is the right move.</span><span
                                        style="font-size: 13px; font-weight: normal;"> </span></p>
                            <p><span style="font-size: 13px; font-weight: normal;"><span id="more-950"></span></span>
                            </p>
                            <p>Before going into a discussion of whether the rule change best serves the players, let’s
                                take a look at the primary reasons USA Hockey is citing for considering the change.</p>
                            <p><strong>Reason #1</strong>: According to sports science research, 9-12 years old is the
                                optimal window for skill acquisition. The claim is the removal of checking would allow
                                two more years of hockey, cognitive, and physical development.</p>
                            <p><strong>Reason #2</strong>: Research indicates there is a 3-fold increased risk of
                                game-related injury (concussions and severe injuries included) when playing in a
                                checking league versus a non-checking league.</p>
                            <p><strong>Reason #3</strong>: The 11-year old brain is more susceptible to suffering a
                                concussion, and is more susceptible to long-term damage if it is concussed. (Information
                                taken from a Mayo Clinic Concussion Symposium)</p>
                            <p>Here is USA Hockey’s video outlining these reasons as well as demonstrating some examples
                                of what will and will not be allowed:</p>
                            <p><a href="http://www.youtube.com/watch?v=Uao224AWuvM">httpv://www.youtube.com/watch?v=Uao224AWuvM</a>
                            </p>
                            <p>The proposal also includes progressively increasing the amount of body contact in Mite,
                                Squirt, and Peewee age groups. Mite contact will be increased by introducing more
                                cross-ice games, which creates less space and more collisions. Increasing the amount of
                                small area competitive games and stations, as well as allowing more contact in games
                                will increase contact at the Squirt level. Peewee contact should be progressed by
                                teaching full-body checking in practice and dryland training – although I’m not sure how
                                they see this as <span style="text-decoration: underline;">increasing</span> the amount
                                of contact at this age level.</p>
                            <h2>The Debate</h2>
                            <p>This proposed change has stirred up a lot of emotion and controversy among players,
                                parents, and coaches alike. If you break down the first two primary reasons USA Hockey
                                has presented for the change, there appears to be arguments on both sides of the
                                fence.</p>
                            <p><strong>Reason #1</strong>: If players 9-12-years are in a window for optimal skill
                                acquisition, wouldn’t checking be a skill which should be developed and mastered through
                                the players career? I whole-heartedly agree the primary focus of this age group should
                                be skill development, but I do see checking as a skill.</p>
                            <p><strong>Reason #2</strong>: While this reason really can’t be argued, the question
                                becomes – does this also apply to Bantams…or Midgets…or Juniors…etc? Does this mean we
                                should take checking out of the game completely to make it safer? I struggle with this
                                argument because I believe there always going to be an inherent risk of injury with a
                                contact sport. Hockey is a contact sport.</p>
                            <p><strong>Reason #3</strong>: I cannot debate this particular reason – I am not a medical
                                expert. The argument certainly makes sense to me in terms of the physical development of
                                kids. In my opinion, this item should have been used as their primary reasoning behind
                                the proposal.</p>
                            <h2>Benefits / Detriments</h2>
                            <p>I believe the proposal does positively serve some groups, while doing a disservice to
                                other groups. In house-league (recreational) hockey, I believe this rule change could
                                help increase participation and will have an overall positive impact. Many of the
                                players playing at the house level lack the fundamental skills necessary to safely and
                                effectively body check in game scenarios.</p>
                            <p>While the proposal may serve recreation level hockey well, I don’t believe it will serve
                                competitive hockey players nearly as well. Players who have a solid foundation in the
                                skills necessary (skating, edgework, etc.) to safely execute a body check will now be
                                losing two seasons of developing this skill. There are two main points where I believe
                                this change does a disservice:</p>
                            <p><strong>Point #1</strong>: Checking will be moving to an age category where players are
                                bigger, faster, and stronger. According to USA Hockey’s “Window of Trainability” chart (<a
                                        href="http://www.admkids.com/images/content/WindowsofTrainability.jpg">http://www.admkids.com/images/content/WindowsofTrainability.jpg</a>),
                                players between 12-14 are in an optimal training window for speed and strength. It has
                                been my experience that there is a large disparity in the strength between first year
                                Bantams and second year Bantams – in part due to kids beginning to grow and starting to
                                mature. I believe this physical disparity will cause more issues than we currently see
                                at the U12 (Peewee) level where checking is currently introduced.</p>
                            <p><strong>Point #2</strong>: The structure of many States push youth players into a high
                                school or midget programs. These programs typically have a 4-year age spread (Freshmen –
                                Senior). Especially in the case of high school programs (think JV &amp; Varsity teams),
                                it is often the case where players will only play a single year of Bantam hockey before
                                joining a high school team. Now there will be players being introduced to an environment
                                with a huge age/maturity span with only a single year of checking experience under their
                                belts. In my opinion, this scenario is not conducive to the safety of the players
                                involved.</p>
                            <h2>A Big Challenge</h2>
                            <p>&nbsp;</p>
                            <p>Any coach who has been coaching through the last significant rule changes in USA Hockey
                                &#8211; immediate off sides and the new standard of enforcement for stick penalties –
                                know the frustration that came along with the changes. In my opinion, the single biggest
                                challenge the checking proposal faces is the ability for officials to consistently call
                                the new standards. The new standard will introduce a lot of gray-area and put the burden
                                on the officials to differentiate between incidental contact and intentional body
                                checking. Hopefully parents, coaches and players will have some patience with officials
                                as they work through the kinks, but there will remain the potential of some very
                                heated/emotional situations.</p>
                            <h2>The Core of it All / My Recommendations</h2>
                            <p>I believe the core of this issue really lies in education and development of the skill of
                                checking. I’m not a fan of putting rules in place instead of addressing a core
                                deficiency. Here are my thoughts/recommendations for USA Hockey instead of implementing
                                the current checking proposal:</p>
                            <p><strong>Increase/Update the Materials Available</strong> &#8211; USA Hockey does give
                                coaches some good resources to learn how to teach checking (<a
                                        href="http://www.usahockey.com//Template_Usahockey.aspx?NAV=CO_07_04&amp;ID=270654">http://www.usahockey.com//Template_Usahockey.aspx?NAV=CO_07_04&amp;ID=270654</a>)
                                &#8211; however, I believe something sorely lacking is an updated video for coaches
                                discussing how to teach checking on-ice. There is a descent (albeit a little
                                old-looking) USA Hockey “Heads Up” Hockey video –but to find it, I had to search YouTube
                                and find it uploaded by a user (not by USA Hockey): <a
                                        href="http://www.youtube.com/watch?v=yMV9Q97k3sk">http://www.youtube.com/watch?v=yMV9Q97k3sk</a>
                            </p>
                            <p>In case you haven’t yet seen our videos I’ve included them here:</p>
                            <p style="text-align: center;"><a href="http://www.youtube.com/watch?v=JrDWtvJvt4g">httpv://www.youtube.com/watch?v=JrDWtvJvt4g</a>
                            </p>
                            <p style="text-align: center;"><a href="http://www.youtube.com/watch?v=FgkxNLxLR0s">httpv://www.youtube.com/watch?v=FgkxNLxLR0s</a>
                            </p>
                            <p><strong>Start a Checking Safety Initiative</strong> – Much like rolling out cross-ice 8U
                                programs, in my opinion, USA Hockey should put similar efforts into checking training.
                                This could include USA Hockey sanctioned checking clinics (organized at a State level),
                                traveling reps to work with coaches, and dedicated checking clinics for coaches to learn
                                how to teach the skills properly. There could also be a requirement for all Peewee
                                players to go through checking training (or a checking clinic) prior to being allowed to
                                play.</p>
                            <p><strong>Make a Minor Change</strong> – Instead of introducing such a drastic measure,
                                along with increased training for coaches, I believe if there were to be one change, it
                                should be to eliminate open ice checking at the U12 (Peewee) level. Often times, these
                                are some of the most dangerous because you have kids who are lacking skill (in checking)
                                attempting to make a big hit. I believe this small change would help (at least a bit) to
                                reduce the number of injuries occurring.</p>
                            <h2>Conclusion</h2>
                            <p>While I do believe the proposal will make the game safer in some instances, I believe it
                                will actually make it more dangerous in others. I believe the focus needs to be on
                                education of those involved in teaching the skill of checking. I don’t ever want to see
                                a kid hurt, but at the same time, I recognize this is a contact sport. Contact sports
                                carry an inherent risk of injury – no matter what age level you begin the contact.
                                Assuming this proposal passes, my hope is we’re not taking an issue that currently
                                plagues our sport at the U12 level and simply transplanting it to the U14 level.</p>
                            <p>As always, I’d love to hear your comments and opinions on this topic. Please feel free to
                                leave a comment below:</p>

                            <br/>

                        </div>


                        <div class="feedback">


                        </div>

                    </div>


                    <div>6 Comments
                    </div>
                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1099">

                        <div class="hockeyshare_comment_by">QMandTM Says:</div>
                        <div class="hockeyshare_comment_time">June 2nd, 2011 a30 9:55am</div>

                        <p>Well said and reasoned, I agree wholeheartedly. I&#8217;ve just gone thru coaching two years
                            of Peewees and one year of Bantams, the size differential at the Bantam level is staggering.
                            Yes, there is some disparity at the Peewee level but it is much larger at the Bantam level.
                            We need to teach these kids how to check, and maybe more importantly how to receive a check,
                            BEFORE they have the speed and mass to do damage not wait until they get bigger. </p>
                        <p>The concussion issue is important but the majority of injuries I saw at the hitting levels
                            were not head injuries and if taught properly a concussion should hardly ever be the result
                            of a check. I think the answer to concussions is to totally eliminate shots to the head, any
                            head contact should be treated like a hit from behind, auto major penalty, with maybe more
                            actual short handed time to be dealt out, not just a kid sitting in the box for 10 minutes
                            while his team is only down a man for a minute and a half. Punish his team with a 5 minute
                            penalty and you&#8217;ll see coaches emphasize this in their coaching.</p>
                        <p>I saw more shots to the head to defensemen keeping the puck in at the blue line or who have
                            to wait for the zone to clear if the puck trickles out. Watch a Peewee or Bantam game and
                            see how many shots these d-men take that they wouldn&#8217;t have to if we had tag-up
                            offsides. Talk about a rule that inadvertently leads to head shots!!!!</p>


                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1100">

                        <div class="hockeyshare_comment_by">Coach Rick Vaile Says:</div>
                        <div class="hockeyshare_comment_time">June 2nd, 2011 a30 1:21pm</div>

                        <p>Kevin you have some good points. QmandTM very good point about the tag up rule. Now my views
                            is a simple one.</p>
                        <p>First a little about my career.I grew up playing hockey were all we had was Helmets with chin
                            guards and hitting was allowed at all ages. I started when I was 6 yrs old. I only played
                            one year of no hit hockey when the took it out of Pee Wee for my 2nd year (1980)of Pee Wee.
                            Eventually they put it back in. So it has been tried here in Canada and I am not sure on the
                            what the findings were and impossible to find but it was allowed a few years later. I was a
                            blessed with a ton of speed , skill and size and could hit with the best of them. I was
                            taught to hit with mt hands down and with the shoulder. </p>
                        <p>Now having said the above I believe MUSCLE MEMORY and in my experience as a player and a
                            coach the kids in the younger ages below Pee Wee are learning to skate they develop lots of
                            speed but the downfall is they get the heads down muscle memory at a young age and it is
                            very hard to get muscle memory out of a players skill set. The kids are so padded up that
                            they think they are invincible. Full face helmets, Neck Guards, Huge shoulder pads, huge
                            elbow pads. </p>
                        <p>My view on changing concussions is&#8230;.</p>
                        <p>#1 &#8211; Have checking at all ages with mandatory checking clinic for all coaches and
                            players before the season starts and before any player or coach can participate.</p>
                        <p>#2 &#8211; Get rid of all the hard plastic Elbow pads &amp; Shoulder pads. This will minimize
                            the impacts when a head shot has happened. Keep the pants and shin guards as protective as
                            possible.</p>
                        <p>#3 &#8211; Helmets and neck guards are fine with me. I have never been a big fan of Visors
                            but each to his own in the higher leagues visor or not I have seen puck and sticks go up
                            under the visor and cause brutal damage as well. It takes a while but when the kids or
                            adults get into visor you can see a progress to a better heads up game due to the fact that
                            they have to be careful around sticks and pucks. The thing I see is it has to be learned and
                            takes longer at the older ages due to muscle memory.</p>
                        <p>#4 &#8211; Make head shots a mandatory game misconduct just like hitting from behind. This
                            will be tough as it is always in the judgement of the Ref in the lower house leagues. Maybe
                            have a game camera/s at every rink that can be viewed. Kind of like a security camera that
                            tapes the game automatically from one or more angles. Cameras and video system can be set up
                            for minimal dollars compared to other problems that exist.</p>
                        <p>5# &#8211; Take helmets right out of the NHL. They did it in the old days maybe it is the
                            extreme that is needed to make contact more respected. I know this would never happen but as
                            an extreme measure who knows.</p>


                    </div>

                    <br/>

                    <div class="hockeyshare_comment color1" id="comment-1101">

                        <div class="hockeyshare_comment_by">QMandTM Says:</div>
                        <div class="hockeyshare_comment_time">June 3rd, 2011 a30 11:59am</div>

                        <p>Good points Coach. I agree wholeheartedly and would love to see checking at all levels with
                            mandatory instruction. I go back to my 2 years coaching Peewee&#8217;s and my fellow coach
                            and I did some sort of checking drill, with focus on giving AND receiving, just about every
                            practice. We almost always shared the ice with another PW team (one year a team above us,
                            the next the team below) and I never saw these other coaches working on this. Which was
                            always disappointing, especially when I had to coach some of their players the following
                            year. Same goes for this last Bantam year of coaching, I just don&#8217;t understand what
                            these other coaches are thinking and if there is one thing I&#8217;m most happy about when I
                            think about coaching my own son it is the knowledge that I taught him how to play both sides
                            of the physical game correctly.</p>
                        <p>My son is a defenseman so perhaps that is why those head shots at the point stick out to me,
                            but in 3 years so far of checking hockey it is the only time I&#8217;ve see him take a
                            vulnerable hit. Not taking the puck behind the net (where he has gotten croaked more than a
                            few times), not taking the puck up ice, not fighting in the corner. It&#8217;s when kids
                            come flying out to the point and use only their flailing arms and hands to land a head
                            shot.</p>
                        <p>I swear some of the teams we&#8217;ve played never need shoulder pads because they&#8217;ve
                            never delivered a shoulder check and that is poor coaching and poor muscle memory, you are
                            exactly right. Make it a zero-tolerance penalty, put the offending team down a man for 5
                            minutes and you&#8217;ll see coaches working on this in practice. Either that or spend the
                            whole practice working on the penalty kill.</p>


                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1102">

                        <div class="hockeyshare_comment_by">defilippis luigi Says:</div>
                        <div class="hockeyshare_comment_time">June 4th, 2011 a30 10:54pm</div>

                        <p>Guys don&#8221;t make that dumb mistake as they did here in quebec. Now after x years of not
                            having bodychecks before bantam, now they are thinking going back to pee wee.<br/>
                            I think that there is to many guys trying to reinvent the game. Seeing that were digging
                            ourself more into a whole and admitting probably we should of respected players more<br/>
                            by understanding the fundementals of the game instead of trying to create what is exciting
                            to us the fans, we probably would have less injuries. I have coached more than 20 years.Why
                            do you think quebec is producing so few players for the N H L. Young players before bantam
                            have nothing to worry about and when they are faced with tryouts in bantam they become
                            vulnerable and get injured twice as much. Players are thaught how to bodycheck and turn
                            around to protect the puck. This forces them to play with their head down and think its all
                            about keeping the puck.Players are way more talented today and way dumber when it comes to
                            taking decisions.So many turnovers, its become a farce, and this just to make a spectacle of
                            a game that once took complete players to play.<br/>
                            I predict that you won&#8221;t have the time to remember names on the ice because the
                            players we are seeing are a dime a dozen. Never saw so many dumb mistakes in the N H L.As
                            for bantam kids teach them to always face the hits, absorb and always take a pass
                            angeling.Now their teaching kids how to take passes pointing north and also expect the
                            opposing player not to take him out so we look good in the developement of players.Like
                            everything else today we are trying to make it easier for the kids to succeed.In the old
                            days we found a way to succeed. We were responsible of our own results. Alot of kids in
                            bantam don&#8221;t want to play competition because they are afraid of the contact. Good
                            players. If these kids were introduced at an early age they wouldn&#8221;t even realize
                            because they would of grown into it.At a certain age they become aware of pain and the
                            posibility of getting hurt. At a young age they just don&#8217;t care and it doesn&#8221;t
                            become an issue.I in my 17 years of coaching double letter teams, I can say I had only 2
                            players that suffered concussions. Thankyou for the chance to express myself.</p>


                    </div>

                    <br/>

                    <div class="hockeyshare_comment authcomment" id="comment-1103">

                        <div class="hockeyshare_comment_by"><a href='http://hockeyshare.com' rel='external nofollow'
                                                               class='url'>Kevin</a> Says:
                        </div>
                        <div class="hockeyshare_comment_time">June 5th, 2011 a30 8:35am</div>

                        <p>defilippis &#8211; thanks for sharing your perspectives. I was hoping we&#8217;d get a
                            response from someone in Quebec on the topic. You made me realize there&#8217;s another
                            group the rule change would NOT serve well&#8230;..any US-based peewee or bantam team
                            playing games in Canada against teams where checking is introduced earlier. </p>
                        <p>I know lots of tournaments in MI and MN alone which have Canadian teams traveling in&#8230;and
                            also know of lots of teams from the States traveling up to Canada for tournaments. Not a
                            good situation with the mix of checking rules.</p>


                    </div>

                    <br/>

                    <div class="hockeyshare_comment color2" id="comment-1189">

                        <div class="hockeyshare_comment_by">JoelM Says:</div>
                        <div class="hockeyshare_comment_time">October 24th, 2011 a31 8:41pm</div>

                        <p>Kevin,<br/>
                            Just found your site and thought I&#8217;d add to this debate despite the fact that it&#8217;s
                            a little old. I&#8217;ve been coaching from mite to college over the last 30 years, as well
                            as officiated at all those levels, as well. I&#8217;ve also had the opportunity to coach the
                            women&#8217;s game at all those levels as well which gives me a pretty broad perspective on
                            this issue.<br/>
                            I understand the idea that moving to Bantam checking will have some down sides as you and
                            the posters have listed. I&#8217;m much more in favor of the change than you or your posters
                            are for a number of reasons and will list them along with a few modifications to the rule we
                            have now that might be a better route to take. First off, we must all understand that the
                            majority of hockey being played at the youth levels is a B level and below. There are many
                            great A AA or AAA programs out there that have players that play at a very high level, but
                            the largest number of participants are below those levels. With this reality USA hockey is
                            attempting to eliminate the hitting at the Pee-Wee level for a number of reasons you state
                            above. The hitting end of the game for far too many of these lower levels is not only not in
                            the players best interest, but often overly stressed by the coaches who are selected to
                            coach these less than top end talent. The reality is that taking away the hitting piece will
                            hopefully push these coaches to stress a sounder practice plan that emphasizes skating and
                            skills. There will be a number of these less than top level players that break into that
                            category and this route will be much easier with checking taken out of the skills that need
                            to be learned. You might guess, my rule change would recommend that the top levels be
                            allowed to have checking.<br/>
                            Having also coached a AAA team in Quebec for 4 summers, I would disagree with defilippis
                            above and argue that the problem is more a the situation that Quebec players as he states
                            are &#8220;all about keeping the puck.&#8221; Quebec is a unique place where culturally
                            there is a love of showmanship and being flamboyant. I remember having a conversation with a
                            Q parent about how excited he was about a coast to coast play by a player who scored a
                            beautiful goal to finish the rush. He was shocked when I told him that I would of
                            congratulated the player on a nice goal then told him to sit out for the remainder of the
                            period and let me know if before the next one if he would move the puck to the open
                            teammates the next time or remain on the pine. His reaction was astonishment and he asked if
                            I had not heard all the parents yelling their approval. I told him that&#8217;s the problem
                            in Quebec, the showmanship is celebrated instead of corrected.<br/>
                            I can also tell you in the women&#8217;s game the players have a much greater chance of
                            playing catch up with the big hitting taken out of the game. The biggest piece of this
                            entire debate always will and always has gone back to the simplest of solutions and that&#8217;s
                            having coaches that can competently teach the right things at the right age and development
                            phase and having a motivated group of players that want to work hard and are supported by
                            their parents.<br/>
                            Sorry about the long post and congrats on your blog, you add many useful ideas for hockey
                            folks to think about.</p>


                    </div>

                    <br/>


                    <p>
                        <a href="http://hockeyshare.com/blog/comments-thoughts/usa-hockey%e2%80%99s-checking-rule-change-proposal-%e2%80%93-hitting-the-mark/feed/"><abbr
                                    title="Really Simple Syndication">RSS</abbr> feed for comments on this post.</a></p>

                    <p>Sorry, the comment form is closed at this time.</p>

                    <br/>


                    <p>&nbsp;</p>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

