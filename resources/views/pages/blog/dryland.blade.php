@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>

        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        class="puks_a"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <div class="post-1233 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-dryland tag-video"
                         id="post-1233">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/slide-board-part-2-video/"
                               rel="bookmark">Slide Board &#8211; Part 2 (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Oct</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Part two of the slide board series covers ways to vary your training by adding
                                stickhandling and passing dynamics.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/2ApZwofe6sw?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a href="http://hockeyshare.com/slideboard"
                                                              target="_blank">Slide Board Information</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <br/>

                    <p>&nbsp;</p>

                    <div class="post-1222 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-dryland tag-video"
                         id="post-1222">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/slide-board-part-1-video/"
                               rel="bookmark">Slide Board &#8211; Part 1 (Video)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Sep</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Part one of this series covers basic equipment overviews and tips on how to lengthen your
                                hockey stride. Parts two and three will cover training variations and athletic training
                                using the slide board.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/gXoSI_kriqc?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: center;"><a href="http://hockeyshare.com/slideboard"
                                                              target="_blank">Slide Board Information</a></p>
                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1202 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-dryland tag-plyo tag-video"
                         id="post-1202">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/lateral-plyo-box-walks/"
                               rel="bookmark">Lateral Plyo Box Walks</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">13</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/plyo/" rel="tag">plyo</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>This week&#8217;s video features another exercise using <a
                                        href="http://www.hockeyshot.com/Hockey_Plyometrics_machines_s/141.htm?Click=40243"
                                        target="_blank">HockeyShot&#8217;s Plyo Boxes</a>. This week, we look at a lunge
                                exercise used to maintain posture and powerful stance, while developing leg strength.
                            </p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/VPUBQWMXx0I?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1196 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-dryland tag-plyo"
                         id="post-1196">

                        <h2><a href="http://hockeyshare.com/blog/hockey-instructional-video/plyo-box-squat-jumps/"
                               rel="bookmark">Plyo Box Squat Jumps</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Aug</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">2</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/plyo/"
                                                    rel="tag">plyo</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The ability to generate quick and explosive power is essential for every hockey player.
                                In this video, we cover the Plyo Box Squat Jumps. This exercise requires the athlete to
                                get into a low powerful stance and explode up to the platform while staying on the balls
                                of the feet to maintain balance and stability. This is a great exercise to increase leg
                                strength and explosive power.</p>
                            <p>We recommend using a high-quality plyo box for this exercise. Here&#8217;s a link to the
                                ones we used, available at <a href="http://www.hockeyshot.com?Click=40243"
                                                              target="_blank">HockeyShot.com</a>: <a
                                        href="http://www.hockeyshot.com/Plyoboxes_Deluxe_p/res-train-037.htm?Click=40243"
                                        target="_blank">Deluxe Plyo Boxes</a></p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/Iv3a5H4IWJw?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1175 post type-post status-publish format-standard hentry category-hockey-instructional-video tag-dryland tag-stickhandling tag-video"
                         id="post-1175">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/off-ice-stickhandling-video-series-part-3-of-3/"
                               rel="bookmark">Off Ice Stickhandling Video Series (Part 3 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">May</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">17</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>In the 3rd segment of our Off Ice Stickhandling Series we cover ways keep your training
                                fresh. This video covers the use of training aids (links below) and body movements to
                                vary your stickhandling training.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/7JmQ-pd6BsQ?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <ul>
                                <li>
                                    <a href="http://www.nicerink.com/store/product.php?productid=264&amp;cat=0&amp;page=2"
                                       target="_blank">Stick Weights</a></li>
                                <li>
                                    <a href="http://www.nicerink.com/store/product.php?productid=133&amp;cat=0&amp;page=1"
                                       target="_blank">Attack Triangles</a></li>
                                <li>
                                    <a href="http://www.nicerink.com/store/product.php?productid=266&amp;cat=16&amp;page=1"
                                       target="_blank">Smart Hockey Ball</a></li>
                            </ul>
                            <p>Note that the above links are <strong><span
                                            style="text-decoration: underline;">NOT</span></strong> affiliate links, and
                                HockeyShare does <span style="text-decoration: underline;"><strong>NOT</strong></span>
                                receive any commissions on sales generated through the use of the links. These links are
                                provided for reference and because we believe these tools will greatly enhance your
                                training routine.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-1168 post type-post status-publish format-standard hentry category-learn-from-the-pros tag-dryland tag-off-ice tag-stickhandling tag-video"
                         id="post-1168">

                        <h2>
                            <a href="http://hockeyshare.com/blog/learn-from-the-pros/off-ice-stickhandling-video-series-part-1-of-3/"
                               rel="bookmark">Off Ice Stickhandling Video Series (Part 1 of 3)</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Apr</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">26</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/"
                                                rel="category tag">Learn from the Pros</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice/"
                                                    rel="tag">off ice</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/video/"
                                                    rel="tag">video</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>As players across the world prepare to hone their skills in the off-season, we have put
                                together a 3-part video series covering the basics of training stickhandling off ice. In
                                part one we cover basic technique, dribbles, and practice drills to help you train
                                properly.</p>
                            <p style="text-align: center;"><span class="youtube"><iframe title="YouTube video player"
                                                                                         class="youtube-player"
                                                                                         type="text/html" width="640"
                                                                                         height="360"
                                                                                         src="//www.youtube.com/embed/XNYejzWquYA?wmode=transparent&amp;fs=1&amp;hl=en&amp;modestbranding=1&amp;iv_load_policy=3&amp;showsearch=0&amp;rel=1&amp;theme=dark"
                                                                                         frameborder="0"
                                                                                         allowfullscreen></iframe></span>
                            </p>
                            <p style="text-align: left;">Part two will cover additional (more advanced) drills, and part
                                three will discuss ways to vary your training to keep things fresh and develop new
                                skills.</p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-965 post type-post status-publish format-standard hentry category-comments-thoughts tag-dryland tag-off-ice-training"
                         id="post-965">

                        <h2><a href="http://hockeyshare.com/blog/comments-thoughts/summer-training-thoughts/"
                               rel="bookmark">Summer Training Thoughts</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jul</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">3</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/"
                                                rel="category tag">Comments &amp; Thoughts</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>The summer is here, and many players are now enjoying the &#8220;off-season.&#8221; This
                                is the time of year where good players become great. This time of year separates the
                                players who are serious about the game, and those who are not. I can&#8217;t tell you
                                the number of times I&#8217;ve been asked &#8220;what should my player do this summer&#8221;
                                &#8211; and I&#8217;m sure many other coaches out there hear it all the time. I wanted
                                to share some quick thoughts on how to approach the off-season.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/comments-thoughts/summer-training-thoughts/#more-965"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-904 post type-post status-publish format-standard hentry category-cool-links tag-dryland tag-off-ice-training tag-off-season-workout"
                         id="post-904">

                        <h2><a href="http://hockeyshare.com/blog/cool-links/off-ice-training-for-the-off-season/"
                               rel="bookmark">Off-Ice Training for the Off-Season</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Mar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">15</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/cool-links/"
                                                rel="category tag">Cool Links</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-season-workout/"
                                                    rel="tag">off-season workout</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>It&#8217;s no secret that training during the off-season can make you a better hockey
                                player. The issue most players/coaches have is they don&#8217;t know WHAT to do. Friend
                                and fellow hockey training professional Jeremy Weiss has put together a program called
                                S3, and has released <strong><span
                                            style="text-decoration: underline;">FREE</span></strong> videos to explain
                                what he&#8217;s got. You can watch the first video of his by clicking the link below.
                            </p>
                            <p><span style="color: #ff0000;"><strong><a
                                                href="https://www.e-junkie.com/ecom/gb.php?ii=592553&amp;c=ib&amp;aff=143742&amp;cl=79043"
                                                target="_blank">Watch the first Video Here</a> &#8211; Weight Lifting &amp; Periodization for Hockey</strong></span>
                            </p>
                            <p>&nbsp;</p>
                            <p><span style="color: #999999;">Note: Yes, this is an affiliate link, and I do get a percentage if you end up purchasing the product.  However, as many of you already know, I don&#8217;t promote products I don&#8217;t believe in or haven&#8217;t seen.  I&#8217;ve seen the product he&#8217;s  put together and think it&#8217;s one of the best out there right now. </span>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-832 post type-post status-publish format-standard hentry category-hockey-drills category-hockey-instructional-video tag-dryland tag-hockey-instructional-video tag-off-ice-training"
                         id="post-832">

                        <h2>
                            <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/"
                               rel="bookmark">Agility Ladder Drills for On-Ice Quickness</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Dec</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">14</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a>,<a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                rel="category tag">Instructional Video</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/hockey-instructional-video/"
                                                    rel="tag">Instructional Video</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>We put together a new video featuring three of our favorite off-ice agility ladder drills
                                to help players develop their quickness and foot speed.</p>
                            <p>
                                <a href="http://hockeyshare.com/blog/hockey-instructional-video/agility-ladder-drills-for-on-ice-quickness/#more-832"
                                   class="more-link">Click to continue&#8230;</a></p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                    <div class="post-624 post type-post status-publish format-standard hentry category-hockey-drills tag-dryland tag-off-ice-training tag-stickhandling tag-usa-hockey"
                         id="post-624">

                        <h2><a href="http://hockeyshare.com/blog/hockey-drills/20stickhandling-drill-videos/"
                               rel="bookmark">20 Stickhandling Drill Videos</a></h2>
                        <div class="blog_border">
                            <table width="100%">
                                <tr>
                                    <td width="50">
                                        <table width="50" class="puks_table">
                                            <tr>
                                                <td align="center" class="puks_td_one">Jun</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="puks_td">8</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        Posted by Kevin - Filed under: <a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/"
                                                rel="category tag">Hockey Drills</a><br/>
                                        <span class="tags"><strong>Tags:</strong> <a
                                                    href="http://hockeyshare.com/blog/tag/dryland/"
                                                    rel="tag">dryland</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/off-ice-training/"
                                                    rel="tag">off-ice training</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/stickhandling/"
                                                    rel="tag">stickhandling</a>, <a
                                                    href="http://hockeyshare.com/blog/tag/usa-hockey/" rel="tag">usa hockey</a></span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="storycontent">
                            <p>Sometimes some of the best content on the web is hard to dig up. While looking for some
                                off-ice stickhandling drills, I came across a 20-part video series offered free online
                                by USA Hockey. This series is part of their National Team Development Program training.
                                The videos do a nice job of explaining the drills and showing the execution. The videos
                                can be found at the following link:</p>
                            <p><a href="http://www.usahockey.com/Template_Usahockey.aspx?NAV=CO&amp;id=19434"
                                  target="_blank">http://www.usahockey.com/Template_Usahockey.aspx?NAV=CO&amp;id=19434</a>
                            </p>

                            <br/>

                        </div>

                        <div class="feedback">

                        </div>

                    </div>

                    <p>&nbsp;</p>

                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

