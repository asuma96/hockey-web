@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content_blog">
        <br/>


        <!-- Content -->
        <br/>
        <h1>Official HockeyShare Blog <a href="http://feeds.feedburner.com/HockeyshareBlog" rel="alternate"
                                         type="application/rss+xml" target="_blank"><img
                        src="//www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt=""
                        style="vertical-align:middle;border:0"/></a></h1>
        <table border="0" width="100%">
            <tr>
                <td valign="top">
                    <rss version="2.0"
                         xmlns:content="http://purl.org/rss/1.0/modules/content/"
                         xmlns:dc="http://purl.org/dc/elements/1.1/"
                         xmlns:atom="http://www.w3.org/2005/Atom"
                         xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"

                    >
                        <channel>
                            <title>Comments on: Your Opinion: USA Hockey Age-Specific Coaching Modules</title>
                            <atom:link
                                    href="http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/feed/"
                                    rel="self" type="application/rss+xml"/>
                            <link>
                            http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/</link>
                            <description>Official blog of HockeyShare.com by Kevin Muller</description>
                            <lastBuildDate>Thu, 09 Apr 2015 03:06:12 +0000</lastBuildDate>
                            <sy:updatePeriod>hourly</sy:updatePeriod>
                            <sy:updateFrequency>1</sy:updateFrequency>
                            <generator>https://wordpress.org/?v=4.7.4</generator>
                            <item>
                                <title>By: Clint</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-16314</link>
                                <dc:creator><![CDATA[Clint]]></dc:creator>
                                <pubDate>Thu, 09 Apr 2015 03:06:12 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-16314</guid>
                                <description><![CDATA[Has anyone saved the PDFs for the pee wee module? Thanks.]]>
                                </description>
                                <content:encoded><![CDATA[<p>Has anyone saved the PDFs for the pee wee module?
                                        Thanks.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Chuck</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-2368</link>
                                <dc:creator><![CDATA[Chuck]]></dc:creator>
                                <pubDate>Thu, 10 Oct 2013 17:51:56 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-2368</guid>
                                <description><![CDATA[I&#039;m disgusted with the age specific modules. I&#039;ve taken
                                    two and now need to take a third. I don&#039;t feel the material is useful, I can
                                    get the same information for free online. The $10 and time sink are absurd. Coaches
                                    in my club are handling multiple teams due to a lack of volunteers willing to coach.
                                    The lack of volunteers is a direct result of the mandatory requirements place on
                                    coaches by USA Hockey. How much more regulation is needed before clubs start
                                    folding?]]>
                                </description>
                                <content:encoded><![CDATA[<p>I&#8217;m disgusted with the age specific modules. I&#8217;ve
                                        taken two and now need to take a third. I don&#8217;t feel the material is
                                        useful, I can get the same information for free online. The $10 and time sink
                                        are absurd. Coaches in my club are handling multiple teams due to a lack of
                                        volunteers willing to coach. The lack of volunteers is a direct result of the
                                        mandatory requirements place on coaches by USA Hockey. How much more regulation
                                        is needed before clubs start folding?</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: PHIL</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-2355</link>
                                <dc:creator><![CDATA[PHIL]]></dc:creator>
                                <pubDate>Wed, 18 Sep 2013 12:00:51 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-2355</guid>
                                <description><![CDATA[BEEN COACHING FOR 19 YEARS. JUST PAID FOR USA INS, PAID TO RECERT
                                    , DID A BACKROUND CHECK NOW NEED TO DO A MUDULE . LOTS OF TIME FOR ME AND LOTS OF
                                    MONEY FOR USA HOCKEY. JUST THINK HOW MUCH MONEY USA HOCKEY BRINGS IN.SO WE CAN HAVE
                                    THE OK TO COACH FOR FREE.]]>
                                </description>
                                <content:encoded><![CDATA[<p>BEEN COACHING FOR 19 YEARS. JUST PAID FOR USA INS, PAID TO
                                        RECERT , DID A BACKROUND CHECK NOW NEED TO DO A MUDULE . LOTS OF TIME FOR ME AND
                                        LOTS OF MONEY FOR USA HOCKEY. JUST THINK HOW MUCH MONEY USA HOCKEY BRINGS IN.SO
                                        WE CAN HAVE THE OK TO COACH FOR FREE.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Bob</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1561</link>
                                <dc:creator><![CDATA[Bob]]></dc:creator>
                                <pubDate>Tue, 01 Jan 2013 16:15:02 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1561</guid>
                                <description><![CDATA[I think the on-line modules are great! I am a high school teacher
                                    and the modules cover some child development and child psychology which was a great
                                    refresher for me. I think it is good for coaches who do not have a degree in
                                    education to understand the psychological and physical developments of the players.

                                    I think the modules are little too long and should be started in the summer because
                                    coaches have less time during the season or should be edited down since some of the
                                    material is redundant.

                                    I think USA Hockey should make all parents participate in some on-line module so
                                    they understand why coaches are instructing the players the way they are. Often
                                    times parents have no idea about hockey in the non-traditional hockey markets and
                                    need to be educated too. USA Hockey is preaching to the proverbial choir and needs
                                    to inform parents.]]>
                                </description>
                                <content:encoded><![CDATA[<p>I think the on-line modules are great! I am a high school
                                        teacher and the modules cover some child development and child psychology which
                                        was a great refresher for me. I think it is good for coaches who do not have a
                                        degree in education to understand the psychological and physical developments of
                                        the players. </p>
                                    <p>I think the modules are little too long and should be started in the summer
                                        because coaches have less time during the season or should be edited down since
                                        some of the material is redundant. </p>
                                    <p>I think USA Hockey should make all parents participate in some on-line module so
                                        they understand why coaches are instructing the players the way they are. Often
                                        times parents have no idea about hockey in the non-traditional hockey markets
                                        and need to be educated too. USA Hockey is preaching to the proverbial choir and
                                        needs to inform parents.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Jon - Madison WI</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1555</link>
                                <dc:creator><![CDATA[Jon - Madison WI]]></dc:creator>
                                <pubDate>Fri, 28 Dec 2012 17:05:49 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1555</guid>
                                <description><![CDATA[OK I have been watching the age development videos for the last
                                    3.5 hours.
                                    I am not the coach of my son&#039;s team but I have seen a coach get sick at
                                    playdowns and not have another certified coach available so the team had to forfeit.

                                    many of the the modules are repeats of the info covered in the level 3 coaching
                                    re-certification.

                                    My son is a bantam and this is the last time I will be doing this. I have been
                                    coaching kids since 1990 and this is just too time consuming for that I am getting
                                    out of it.

                                    The youth programs do not have the time, or facilities available to them to do
                                    strength and conditioning programs mentioned in the videos. Maybe some of the High
                                    School programs could use these but the rec / youth hockey players &#038; coaches
                                    really can not.

                                    I would really like to see some of these &quot;USA Hockey experts&quot; coach in the
                                    facilities we use in the real world.
                                    As I watch the videos all I see is a lot of wasted ice in practice. maybe some
                                    videos showing average instead of elite level players would be more real world.

                                    It has cost me $60 to take this &quot;class&quot; and it just looks like a money
                                    grab to me.]]>
                                </description>
                                <content:encoded><![CDATA[<p>OK I have been watching the age development videos for the
                                        last 3.5 hours.<br/>
                                        I am not the coach of my son&#8217;s team but I have seen a coach get sick at
                                        playdowns and not have another certified coach available so the team had to
                                        forfeit.</p>
                                    <p>many of the the modules are repeats of the info covered in the level 3 coaching
                                        re-certification.</p>
                                    <p>My son is a bantam and this is the last time I will be doing this. I have been
                                        coaching kids since 1990 and this is just too time consuming for that I am
                                        getting out of it.</p>
                                    <p>The youth programs do not have the time, or facilities available to them to do
                                        strength and conditioning programs mentioned in the videos. Maybe some of the
                                        High School programs could use these but the rec / youth hockey players &amp;
                                        coaches really can not. </p>
                                    <p>I would really like to see some of these &#8220;USA Hockey experts&#8221; coach
                                        in the facilities we use in the real world.<br/>
                                        As I watch the videos all I see is a lot of wasted ice in practice. maybe some
                                        videos showing average instead of elite level players would be more real world.
                                    </p>
                                    <p>It has cost me $60 to take this &#8220;class&#8221; and it just looks like a
                                        money grab to me.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Lauren Lynn</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1538</link>
                                <dc:creator><![CDATA[Lauren Lynn]]></dc:creator>
                                <pubDate>Mon, 10 Dec 2012 20:20:33 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1538</guid>
                                <description><![CDATA[I cannot stand these requirements or USA hockey - I did my time to
                                    get to the level 4 level by the time I was 19 before these coaching module
                                    requirements. I&#039;ve been coaching every level from Elementary, Middle School,
                                    High school and club hockey. All of which are boys teams - which every seminar I&#039;ve
                                    gone to, the sexist pigs that run them tell me women should only coach girls because
                                    they are so &#039;sensitive&#039; and you have to treat them special. Bullcrap. I&#039;ve
                                    been playing long enough to play college men&#039;s d1 hockey, get drafted and play
                                    pro women&#039;s hockey and now they tell me that I have to take hours out of my
                                    week that I don&#039;t have (coaching 4 different age groups as well as traveling to
                                    play professionally and give private lessons, balancing a full time job, and finish
                                    my own education) All the coaching I give I don&#039;t get paid for because I don&#039;t
                                    want kids growing up with the awful hockey experiences I had. USA hockey makes it so
                                    I don&#039;t even want to be involved. It&#039;s a joke. They are doing one of two
                                    things - making it either more difficult for people who shouldn&#039;t get their
                                    card don&#039;t want to, or just trying to get more money out of people. My thoughts
                                    are it&#039;s a little of both. Either way - I&#039;ve been this close to walking
                                    away from coaching because of this waste of time. So frustrating.]]>
                                </description>
                                <content:encoded><![CDATA[<p>I cannot stand these requirements or USA hockey &#8211; I
                                        did my time to get to the level 4 level by the time I was 19 before these
                                        coaching module requirements. I&#8217;ve been coaching every level from
                                        Elementary, Middle School, High school and club hockey. All of which are boys
                                        teams &#8211; which every seminar I&#8217;ve gone to, the sexist pigs that run
                                        them tell me women should only coach girls because they are so &#8216;sensitive&#8217;
                                        and you have to treat them special. Bullcrap. I&#8217;ve been playing long
                                        enough to play college men&#8217;s d1 hockey, get drafted and play pro women&#8217;s
                                        hockey and now they tell me that I have to take hours out of my week that I don&#8217;t
                                        have (coaching 4 different age groups as well as traveling to play
                                        professionally and give private lessons, balancing a full time job, and finish
                                        my own education) All the coaching I give I don&#8217;t get paid for because I
                                        don&#8217;t want kids growing up with the awful hockey experiences I had. USA
                                        hockey makes it so I don&#8217;t even want to be involved. It&#8217;s a joke.
                                        They are doing one of two things &#8211; making it either more difficult for
                                        people who shouldn&#8217;t get their card don&#8217;t want to, or just trying to
                                        get more money out of people. My thoughts are it&#8217;s a little of both.
                                        Either way &#8211; I&#8217;ve been this close to walking away from coaching
                                        because of this waste of time. So frustrating.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Chris</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1532</link>
                                <dc:creator><![CDATA[Chris]]></dc:creator>
                                <pubDate>Thu, 06 Dec 2012 05:32:44 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1532</guid>
                                <description><![CDATA[I have no desire to be a head coach, however I&#039;m more than
                                    happy to volunteer as an assistant coach. With that said, I think the coaching
                                    requirements for assistant/part-time coaches are completely unreasonable. I can
                                    understand wanting to standardize the USA Hockey methods and procedures for all
                                    coaches but the numerous levels, classes, on-line video, etc, drive away many
                                    capable and willing individuals. For example, my son&#039;s squirt team has one head
                                    coach and myself as an assistant. The head coach has missed time (weeks) due to work
                                    requirements leaving me to fill in for practices and games. I cant afford the
                                    cost/time from work to attend a class (which are during the week in a town 2 hours
                                    away!). USA Hockey needs to re-examine their certification requirements for
                                    assistants to keep volunteers like myself motivated and willing to participate in
                                    the future. I would suggest having the assistants complete the on-line video lesson
                                    as their only requirement. This would make it time and cost effective along with
                                    establishing a better incentive for someone like myself to remain and support USA
                                    Hockey in the future.]]>
                                </description>
                                <content:encoded><![CDATA[<p>I have no desire to be a head coach, however I&#8217;m more
                                        than happy to volunteer as an assistant coach. With that said, I think the
                                        coaching requirements for assistant/part-time coaches are completely
                                        unreasonable. I can understand wanting to standardize the USA Hockey methods and
                                        procedures for all coaches but the numerous levels, classes, on-line video, etc,
                                        drive away many capable and willing individuals. For example, my son&#8217;s
                                        squirt team has one head coach and myself as an assistant. The head coach has
                                        missed time (weeks) due to work requirements leaving me to fill in for practices
                                        and games. I cant afford the cost/time from work to attend a class (which are
                                        during the week in a town 2 hours away!). USA Hockey needs to re-examine their
                                        certification requirements for assistants to keep volunteers like myself
                                        motivated and willing to participate in the future. I would suggest having the
                                        assistants complete the on-line video lesson as their only requirement. This
                                        would make it time and cost effective along with establishing a better incentive
                                        for someone like myself to remain and support USA Hockey in the future.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Billy</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1351</link>
                                <dc:creator><![CDATA[Billy]]></dc:creator>
                                <pubDate>Wed, 15 Feb 2012 21:27:26 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1351</guid>
                                <description><![CDATA[My other coach and I were unaware of this module to be completed
                                    until 3 days before it was due. Due to time constraints we were unable to complete
                                    the module in time. I was on the 14th section out of 17 when it timed out and my
                                    other coach was on 11. I have been in contact with usa hockey reps for the past few
                                    days and there is nothing that we do. USA hockey kept the $10 and wont allow us to
                                    finish them. My other coach and I are the only two coaches on the team so our team
                                    now has no coach. I understand we dropped the ball big time but I feel to remove a
                                    coach from the team is very extreme. I would gladly pay an additional $30 or so to
                                    log back in to complete the module or pay a fine for not completing it. Either way,
                                    our team has no coach a week before playoff and I cant help but feel that this is
                                    not fair and benefits no one, especially the kids. We both are thought of very
                                    highly in the association and now are restored to being treated like criminals with
                                    a &quot;no contact or team interaction&quot; policy. I am a good coach and care
                                    about these kids very much. I donate 6 days a week to this team trying to make these
                                    kids better players as well as people and to create a love of the game. I hope USA
                                    hockey considers different consequences next time as these only do not benefit
                                    anyone.]]>
                                </description>
                                <content:encoded><![CDATA[<p>My other coach and I were unaware of this module to be
                                        completed until 3 days before it was due. Due to time constraints we were unable
                                        to complete the module in time. I was on the 14th section out of 17 when it
                                        timed out and my other coach was on 11. I have been in contact with usa hockey
                                        reps for the past few days and there is nothing that we do. USA hockey kept the
                                        $10 and wont allow us to finish them. My other coach and I are the only two
                                        coaches on the team so our team now has no coach. I understand we dropped the
                                        ball big time but I feel to remove a coach from the team is very extreme. I
                                        would gladly pay an additional $30 or so to log back in to complete the module
                                        or pay a fine for not completing it. Either way, our team has no coach a week
                                        before playoff and I cant help but feel that this is not fair and benefits no
                                        one, especially the kids. We both are thought of very highly in the association
                                        and now are restored to being treated like criminals with a &#8220;no contact or
                                        team interaction&#8221; policy. I am a good coach and care about these kids very
                                        much. I donate 6 days a week to this team trying to make these kids better
                                        players as well as people and to create a love of the game. I hope USA hockey
                                        considers different consequences next time as these only do not benefit
                                        anyone.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Chas. Heilman</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1350</link>
                                <dc:creator><![CDATA[Chas. Heilman]]></dc:creator>
                                <pubDate>Sun, 12 Feb 2012 23:33:46 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1350</guid>
                                <description><![CDATA[I have been coaching house hockey for 13 years. Girls midget
                                    hockey now for 6 years. I will never coach college, AA, AAA, Jr&#039;s, or any other
                                    upper level hockey. Now I am forced to become a level 4 coach or quit. Level 4
                                    clinics are never near my area. It will take a day there and a day back. Plus the
                                    days there. Plus the room, gas food, etc. I fear that midget hockey may not be
                                    available after my current card expires. I can not eat the cost on top of the
                                    expense of coaching as it is. So sad.]]>
                                </description>
                                <content:encoded><![CDATA[<p>I have been coaching house hockey for 13 years. Girls
                                        midget hockey now for 6 years. I will never coach college, AA, AAA, Jr&#8217;s,
                                        or any other upper level hockey. Now I am forced to become a level 4 coach or
                                        quit. Level 4 clinics are never near my area. It will take a day there and a day
                                        back. Plus the days there. Plus the room, gas food, etc. I fear that midget
                                        hockey may not be available after my current card expires. I can not eat the
                                        cost on top of the expense of coaching as it is. So sad.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                            <item>
                                <title>By: Bill Tremel</title>
                                <link>
                                http://hockeyshare.com/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/comment-page-1/#comment-1349</link>
                                <dc:creator><![CDATA[Bill Tremel]]></dc:creator>
                                <pubDate>Sun, 12 Feb 2012 20:46:04 +0000</pubDate>
                                <guid isPermaLink="false">http://hockeyshare.com/blog/?p=1077#comment-1349</guid>
                                <description><![CDATA[Okay, I embrace USA Hockey and everything they do. I take time out
                                    of my work and personal life to make sure that I&#039;m the best coach I can be for
                                    my son and all the kids I&#039;ve helped coach of the years. I totally embrace the
                                    additional training and the testing. There are too many &quot;bad&quot; coaches out
                                    there that need to be weeded out.

                                    Now here&#039;s my beef with the online training. This new training is a heavy
                                    bandwidth hog. If you don&#039;t have broadband, you&#039;re out of luck. I&#039;ve
                                    been trying to take my Level 3 squirt training for 4 months. A little at a time. I&#039;ve
                                    finally given up, as you cannot do this training with dial-up. It simply does not
                                    work.

                                    USA hockey needs to understand that not everyone lives in a big city and has access
                                    to high speed internet. I&#039;ve failed to complete this training on time. Not
                                    because I have not given up my own personal time. But because the false expectations
                                    that USA Hockey has about all American&#039;s having easy access to broadband.

                                    I&#039;ve email USA hockey about this several times, and each time, they would
                                    simply pass me through one or two videos stating that I should have better and
                                    quicker access to the material. But they ignore the fact that I&#039;ve stated that
                                    I can only do these with a dial-up internet connection.

                                    I&#039;ve also recommended that they send out the video material on a CD and allow
                                    us to log in and take the tests separately. This is the only why I&#039;m ever going
                                    to be able to comply.

                                    I will most likely loose my certification (along with many other GOOD coaches) due
                                    to this overlooked situation.]]>
                                </description>
                                <content:encoded><![CDATA[<p>Okay, I embrace USA Hockey and everything they do. I take
                                        time out of my work and personal life to make sure that I&#8217;m the best coach
                                        I can be for my son and all the kids I&#8217;ve helped coach of the years. I
                                        totally embrace the additional training and the testing. There are too many
                                        &#8220;bad&#8221; coaches out there that need to be weeded out.</p>
                                    <p>Now here&#8217;s my beef with the online training. This new training is a heavy
                                        bandwidth hog. If you don&#8217;t have broadband, you&#8217;re out of luck. I&#8217;ve
                                        been trying to take my Level 3 squirt training for 4 months. A little at a time.
                                        I&#8217;ve finally given up, as you cannot do this training with dial-up. It
                                        simply does not work.</p>
                                    <p>USA hockey needs to understand that not everyone lives in a big city and has
                                        access to high speed internet. I&#8217;ve failed to complete this training on
                                        time. Not because I have not given up my own personal time. But because the
                                        false expectations that USA Hockey has about all American&#8217;s having easy
                                        access to broadband.</p>
                                    <p>I&#8217;ve email USA hockey about this several times, and each time, they would
                                        simply pass me through one or two videos stating that I should have better and
                                        quicker access to the material. But they ignore the fact that I&#8217;ve stated
                                        that I can only do these with a dial-up internet connection.</p>
                                    <p>I&#8217;ve also recommended that they send out the video material on a CD and
                                        allow us to log in and take the tests separately. This is the only why I&#8217;m
                                        ever going to be able to comply.</p>
                                    <p>I will most likely loose my certification (along with many other GOOD coaches)
                                        due to this overlooked situation.</p>
                                    ]]>
                                </content:encoded>
                            </item>
                        </channel>
                    </rss>


                <td valign="top" width="250">
                    <!-- begin sidebar -->
                    <div id="mysidebar">

                        <ul>
                            <li id="categories-3" class="widget widget_categories"><br/><br/><span class="widget_title">Blog Categories</span>
                                <ul>
                                    <li class="cat-item cat-item-20 current-cat"><a
                                                href="http://hockeyshare.com/blog/category/10000-pucks/">10000
                                            Pucks</a>
                                    </li>
                                    <li class="cat-item cat-item-266"><a
                                                href="http://hockeyshare.com/blog/category/a-different-approach/">A
                                            Different Approach</a>
                                    </li>
                                    <li class="cat-item cat-item-3"><a
                                                href="http://hockeyshare.com/blog/category/comments-thoughts/">Comments
                                            &amp; Thoughts</a>
                                    </li>
                                    <li class="cat-item cat-item-5"><a
                                                href="http://hockeyshare.com/blog/category/cool-links/">Cool
                                            Links</a>
                                    </li>
                                    <li class="cat-item cat-item-15"><a
                                                href="http://hockeyshare.com/blog/category/hockey-drills/">Hockey
                                            Drills</a>
                                    </li>
                                    <li class="cat-item cat-item-30"><a
                                                href="http://hockeyshare.com/blog/category/hockey-systems/">Hockey
                                            Systems</a>
                                    </li>
                                    <li class="cat-item cat-item-87"><a
                                                href="http://hockeyshare.com/blog/category/hockey-tips/">Hockey
                                            Tips</a>
                                    </li>
                                    <li class="cat-item cat-item-48"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-contests/">HockeyShare
                                            Contests</a>
                                    </li>
                                    <li class="cat-item cat-item-97"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-surveys/">HockeyShare
                                            Surveys</a>
                                    </li>
                                    <li class="cat-item cat-item-12"><a
                                                href="http://hockeyshare.com/blog/category/hockeyshare-com-features/">HockeyShare.com
                                            Features</a>
                                    </li>
                                    <li class="cat-item cat-item-14"><a
                                                href="http://hockeyshare.com/blog/category/hockey-instructional-video/"
                                                title="Instructional hockey videos provided by M2 Hockey in conjunction with HockeyShare.com.">Instructional
                                            Video</a>
                                    </li>
                                    <li class="cat-item cat-item-101"><a
                                                href="http://hockeyshare.com/blog/category/learn-from-the-pros/">Learn
                                            from the Pros</a>
                                    </li>
                                    <li class="cat-item cat-item-125"><a
                                                href="http://hockeyshare.com/blog/category/non-hockey/">Non-Hockey</a>
                                    </li>
                                    <li class="cat-item cat-item-4"><a
                                                href="http://hockeyshare.com/blog/category/practice-plans/">Practice
                                            Plans</a>
                                    </li>
                                    <li class="cat-item cat-item-6"><a
                                                href="http://hockeyshare.com/blog/category/resources/">Resources</a>
                                    </li>
                                    <li class="cat-item cat-item-158"><a
                                                href="http://hockeyshare.com/blog/category/scooters-corner/">Scooter&#039;s
                                            Corner</a>
                                    </li>
                                    <li class="cat-item cat-item-1"><a
                                                href="http://hockeyshare.com/blog/category/uncategorized/">Uncategorized</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="search-3" class="widget widget_search"><br/><br/><span class="widget_title">Search this Blog</span>
                                <form role="search" method="get" id="searchform" class="searchform"
                                      action="http://hockeyshare.com/blog/">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                    </div>
                                </form>
                            </li>
                            <li id="recent-posts-3" class="widget widget_recent_entries"><br/><br/><span
                                        class="widget_title">Recent Posts</span>
                                <ul>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs/">The
                                            Coaches&#8217; Ultimate Guide to Stress-Free Try-Outs</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training/">Tips
                                            for getting the most out of your spring training</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/coaching-leadership-self-awareness-questions/">Coaching
                                            Leadership Self-Awareness Questions</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/cool-links/3-on-2-rushes-plays/">3 on
                                            2 Rushes / Plays</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/penalty-killing-tips-tricks/">Penalty
                                            Killing Tips &#038; Tricks</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/a-different-approach/high-quality-year-end-meetings/">High-Quality
                                            Year-End Meetings</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/learn-from-the-pros/importance-of-driving-the-net/">Bruins
                                            Illustrate Importance of Driving the Net &#038; Net Front Presence</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/hockey-instructional-video/attacking-the-high-seam-video-examples/">Attacking
                                            the High Seam (Video &#038; Examples)</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/10000-pucks/10000-pucks-contest-2013/">10,000
                                            Pucks Contest &#8211; 2013</a>
                                    </li>
                                    <li>
                                        <a href="http://hockeyshare.com/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast/">Kevin
                                            from HockeyShare on Weiss Tech Hockey&#8217;s Podcast</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="archives-3" class="widget widget_archive"><br/><br/><span class="widget_title">Archives</span>
                                <ul>
                                    <li><a href='http://hockeyshare.com/blog/2016/08/'>August 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2016/05/'>May 2016</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/11/'>November 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2015/03/'>March 2015</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/03/'>March 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2014/01/'>January 2014</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/05/'>May 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/04/'>April 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/03/'>March 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/02/'>February 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2013/01/'>January 2013</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/12/'>December 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/11/'>November 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/10/'>October 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/09/'>September 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/08/'>August 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/07/'>July 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/06/'>June 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/05/'>May 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/04/'>April 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/03/'>March 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/02/'>February 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2012/01/'>January 2012</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/12/'>December 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/11/'>November 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/10/'>October 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/09/'>September 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/08/'>August 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/07/'>July 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/06/'>June 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/05/'>May 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/04/'>April 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/03/'>March 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/02/'>February 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2011/01/'>January 2011</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/12/'>December 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/11/'>November 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/10/'>October 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/09/'>September 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/08/'>August 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/07/'>July 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/06/'>June 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/05/'>May 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/04/'>April 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/03/'>March 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/02/'>February 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2010/01/'>January 2010</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/12/'>December 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/11/'>November 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/10/'>October 2009</a></li>
                                    <li><a href='http://hockeyshare.com/blog/2009/09/'>September 2009</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- end sidebar -->
                </td>
            </tr>
        </table>
        <br>
    </div>
</div>

