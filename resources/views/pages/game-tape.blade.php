@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Game Tape Review</span>&nbsp;<a
                    href="http://hockeyshare.com/game-tape/" class="breadcrumb_link">Game Tape Home</a></div>
        <h1>Game Tape Review</h1>

        <table class="game-tape-one" width="100%">
            <tr>
                <td width="50%" valign="top" class="game_tape">
                    <h2>Administrators / Coaches</h2>
                    <h3>My Associations</h3>

                    <ul>
                        <li>It appears you do not have access to any teams or associations. Would you like to <a
                                    href="association_new.php">sign up</a>?
                        </li>
                    </ul>
                    <h3>Options</h3>
                    <ul>
                        <li><a href="learn_more.php">Learn More About Game Tape Review</a></li>
                    </ul>
                </td>
                <td valign="top"
                    class="game_tape game_tape_test">
                    <h2>Player Login</h2>
                    <form method="post" action="" name="playerLoginForm" id="playerLoginForm">
                        <table width="100%">
                            <tr>
                                <td class="association">Association:</td>
                                <td><select name="id" id="association" onchange="loadTeams()">
                                        <option value="" selected="selected">----- Choose an Association -----</option>
                                        <option value="1059">Delano</option>
                                        <option value="1044">HockeyShare</option>
                                        <option value="1094">Maine WH</option>
                                        <option value="1035">Milwaukee Jr. Admirals AAA</option>
                                        <option value="1075">Muller Hockey</option>
                                        <option value="1101">Oshawa Minor Midget AAA</option>
                                        <option value="1064">Vics</option>
                                    </select></td>
                            </tr>
                            <tr id="team_row" class="us_tourns">
                                <td>Team:</td>
                                <td>
                                    <div id="team_select_div"></div>
                                </td>
                            </tr>

                            <tr id="player_row" class="us_tourns">
                                <td>Player:</td>
                                <td>
                                    <div id="player_select_div"></div>
                                </td>
                            </tr>
                            <tr id="password_row" class="us_tourns">
                                <td>Password:</td>
                                <td><input type="password" name="p" value=""/></td>
                            </tr>
                            <tr id="login_row" class="us_tourns">
                                <td>&nbsp;</td>
                                <td><input type="submit" name="Submit" value="Log In"/></td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
        @include('includes.commercial')
        <br>
    </div>
</div>

