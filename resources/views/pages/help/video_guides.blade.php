@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content video_guides">
        <br/>

        <h3 class="title-text">How to Track Your Stats (Overview)</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6726407" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">This video is a full overview on how to set up your team and begin tracking stats through
            hockeyshare.com.</p>

        <a name="user_access">&nbsp;</a>
        <h3 class="title-text">Adding User Access</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6823929" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">This video outlines the process of adding authorized users to your team account. By adding other users, you
            can allow assistant coaches, team managers, parents, etc. the ability to update your team page.</p>

        <a name="email">&nbsp;</a>
        <h3 class="title-text">Sending Announcements via Email</h3>
        <p>
            <iframe  src="https://player.vimeo.com/video/6830811" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign" >Here we'll show you how you can send your announcements to your email list with a single click! This is a
            great feature if you've got important information that needs to be sent out quickly.</p>

        <a name="directions">&nbsp;</a>
        <h3 class="title-text">Adding Driving Directions</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6831022" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">Add driving directions with Google Maps automatically to all your away games.</p>

        <a name="custom">&nbsp;</a>
        <h3 class="title-text">Creating Custom Pages</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6822954" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">Create custom pages for your team page with just a couple clicks!</p>

        <a name="upload">&nbsp;</a>
        <h3 class="title-text">Upload Files</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6725430" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">Upload files to share with your team. Files can be uplaoded to the following sections: Announcements,
            Practices, and Custom Pages.</p>

        <a name="practices">&nbsp;</a>
        <h3 class="title-text">Adding Multiple Practices</h3>
        <p >
            <iframe  src="https://player.vimeo.com/video/6834487" width="640" height="400" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </p>

        <p class="video_allign">Fill in your practice schedule quickly by using the repeat option.</p>
        @include('includes.commercial')
        <br>
    </div>

</div>