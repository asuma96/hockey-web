@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content"><br />
        <h2>Association/Team Package Overview</h2>
        <p>Association/Team packages allow users to maintain independent accounts but share drills and practices seamlessly. Learn how to connect your coaching staff in the video below.</p>
        <div align="center">
            <iframe class="walkthrough_iframe" src="//www.youtube.com/embed/6UtwNuhlJKs" frameborder="0" allowfullscreen></iframe>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

