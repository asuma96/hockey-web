@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Diagrammer Tips &amp; Tricks</h2>
        <p>Start diagramming your drills like a pro - learn time saving tips and tricks in the following video. The
            diagrammer has so many time-saving features, we created two videos just to scratch the surface. Ready to
            sign up? <a href="http://hockeyshare.com/drills/compare.php">Compare our plans</a> and be up and running
            in minutes!</p>
        <h4 class="title-text">Basic Tips &amp; Tricks</h4>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/a5O7C-a7M4c" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <h4 class="title-text">Advanced Tips &amp; Tricks</h4>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/Ss0SI5WgOIM" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <br>
        <p class="center"><a href="walkthrough-1.php">&laquo; Advanced Coaching Platform Overview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                    href="walkthrough-3.php">Organize Drills &amp; Practices &raquo;</a></p>
        @include('includes.commercial')
        <br>
    </div>
</div>

