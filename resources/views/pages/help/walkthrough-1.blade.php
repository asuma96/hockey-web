@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Advanced Coaching Platform Overview</h2>
        <p>The video below gives you a quick overview of some of the key functionality of HockeyShare's Advanced
            Coaching Platform. See how HockeyShare can become your ultimate Assistant Coach this season and save you
            hundreds of hours from having to re-draw your drill or write your practice plans out by hand. Ready to sign
            up? <a href="http://hockeyshare.com/drills/compare.php">Compare our plans</a> and be up and running in
            minutes!</p>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/fp5E3qrR5Q0" frameborder="0"
                    allowfullscreen></iframe>
        </div><br>
        <br>
        <p class="center"><a href="walkthrough-2.php">Diagrammer Tips &amp; Tricks &raquo;</a></p>
        @include('includes.commercial')
        <br>
    </div>
</div>

