@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Drill Diagrammer Help</h1>
        <p>The videos below are designed to give you an overview on how to use the Practice Planner from HockeyShare. If
            you still have questions or are experiencing difficulties using the planner, please <a href="/interact">contact
                us</a>.</p>

        <p>&raquo; <a href="http://hockeyshare.com/drills/diagrammer_help.php">Drill Diagrammer Help</a></p>
        <h2>Basic Functionality Overview</h2>
        <div align="center">
            <iframe src="https://player.vimeo.com/video/23482029?byline=0&amp;portrait=0&amp;color=ffffff" width="640"
                    height="480" frameborder="0"></iframe>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

