@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <h3 class="title_text">Getting Started with Stat Tracking at HockeyShare.com</h3>
        <p>Below is a quick step-by-step guide to help you get started tracking your hockey team's statistics with
            HockeyShare.com. Please read these directions carefully, as they answer many common questions we receive
            about this feature. If you have any questions, please <a class="underline"
                                                                     href="http://hockeyshare.com/contact/">contact
                us</a>.</p>
        <p class="center">
            <iframe src="https://player.vimeo.com/video/6726407" class="frame_stat" webkitallowfullscreen
                    mozallowfullscreen allowfullscreen></iframe>
        </p>
        <br/>
        <div class="comment-box">
            <div class="number-box">1</div>
            <div class="content-box"><b>Register for a HockeyShare.com account</b> by visiting our <a class="underline"
                                                                                                      href="http://hockeyshare.com/register/"
                                                                                                      title="Register for HockeyShare.com">registration
                    page</a>. If you're already registered, log in using the login box on the right-hand side of the
                screen.
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">2</div>
            <div class="content-box">
                <p><b>Add a New Team</b> - Click on @if (Auth::check()) <a class="underline"
                                                                           href="http://hockeyshare.com/teams/">Track
                        Stats</a> @else <a class="underline" href="http://hockeyshare.com/login/">Track Stats</a> @endif
                    link under the <strong>Coaches</strong> menu. You will see a link to @if(Auth::check())<a
                            class="underline" href="http://hockeyshare.com/teams/addteam.php">@else <a
                                class="underline" href="http://hockeyshare.com/login"> @endif Add a New Team</a>. Click
                        that link and fill in the details of your team. Options are explained below:</p>
                <br/>
                <p><strong>Team Name</strong> - the full name of the team you're tracking stats for (<em>ex - 2007-2008
                        Kenosha Komets Varsity</em>)</p>
                <br/>
                <p><strong>Short Name</strong> - the abbreviated name of the team you're tracking stats for (<em>ex -
                        Komets Varsity</em>)</p>
                <br/>
                <p><strong>Age Class</strong> - age classification for the team (<em>ex - Squirt, Peewee, Novice,
                        etc.</em>)</p>
                <br/>
                <p><strong>Location</strong> - the general location of the team. This information is used in team
                    searches. (<em>ex - Kenosha, WI</em>)</p>
                <br/>
                <p><strong>Team Logo </strong>- team/club logo (<em>must be .jpg or .gif format</em>)</p>
                <br/>
                <p><strong>Require Password</strong> - this option will require ALL users to enter a password before
                    they're able to view any of the team pages. This password should be <strong>different</strong> than
                    your login password, as everyone who wants to view the team page will need to be given this
                    password.</p><br/>
                <p><strong>Hide Stats</strong> - this option will hide the team and player statistics from all viewers
                    except the person tracking the stats (when logged in). This feature is handy if you want to keep
                    track of your team's stats, but don't want the players/parents to see them during the season.</p>
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">3</div>
            <div class="content-box"><b>Add players</b> to your roster by choosing the @if (Auth::check()) <a
                        class="underline" href="http://hockeyshare.com/teams/">My
                    Teams</a> @else <a class="underline" href="http://hockeyshare.com/login/">My
                    Teams</a> @endif option from the Coaches menu. Click on Add Player to add a player to your roster.
                <em>Note</em>: The &quot;My Teams&quot; page will be your main page for administering the team.
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">4</div>
            <div class="content-box">
                <p><b>Specify your home</b> rink by choosing the <strong>Home Rink</strong> option from
                    your @if (Auth::check()) <a class="underline" href="http://hockeyshare.com/teams/">My
                        Teams</a> @else <a class="underline" href="http://hockeyshare.com/login/">My
                        Teams</a> @endif page. Specifying a home
                    rink allows the HockeyShare system to automatically create driving directions to your road games (if
                    you enter/choose a visiting rink address). This is a great time-saver feature and keeps you from
                    having to manually get the directions and email them out to everyone.</p><br/>
                <p class="center"><a class="underline"
                                     href="http://hockeyshare.com/team/rink_directions.php?id=97&gid=813"
                                     target="_blank">View Example of Driving Directions</a></p>
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">5</div>
            <div class="content-box"><b>Add games to your schedule</b> by choosing the <strong>Add Games</strong> option
                from your @if (Auth::check()) <a class="underline" href="http://hockeyshare.com/teams/">My
                    Teams</a> @else <a class="underline" href="http://hockeyshare.com/login/">My
                    Teams</a> @endif page. If you're
                entering a road game, you're able to select an existing rink from our database or enter in the name and
                address of a rink. Entering this information will allow our system to automatically generate driving
                directions from your home rink. The directions can be found by clicking the globe icon on the team's
                schedule page.
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">6</div>
            <div class="content-box"><b>Add team announcements</b> by choosing <strong>Add New</strong> to the right of
                Announcements on your @if (Auth::check()) <a class="underline" href="http://hockeyshare.com/teams/">My
                    Teams</a> @else <a class="underline" href="http://hockeyshare.com/login/">My
                    Teams</a> @endif page.
                Announcements are a good way to let your team know about the latest news, schedule changes, etc.
            </div>
        </div>
        <br/>
        <div class="comment-box">
            <div class="number-box">7</div>
            <div class="content-box"><b>Enter game results and statistics</b> by choosing the <strong>Enter Game
                    Results/Stats</strong> option from your @if (Auth::check()) <a class="underline"
                                                                                   href="http://hockeyshare.com/teams/">My
                    Teams</a> @else <a class="underline" href="http://hockeyshare.com/login/">My
                    Teams</a> @endif page. This page will allow you to enter the following game information: score,
                shots on
                goal, game notes (not visible to the public), games played, goals, assists, +/-, PIM, shots on goal,
                faceoffs won, faceoffs lost, faceoffs tied, saves, goals against and minutes played.
            </div>
        </div>
        <br/>
        @include('includes.commercial')
        <br>
    </div>
</div>
