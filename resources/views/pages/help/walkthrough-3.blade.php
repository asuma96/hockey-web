@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2 >Organizing Your Drills &amp; Practices</h2>
        <p>Now that you're getting all your drills created, we want to show you some good practices for keeping your
            drills and practices organized as they grow. The videos below walk you through how to manage your personal
            databases. Ready to sign up? <a href="http://hockeyshare.com/drills/compare.php">Compare our plans</a>
            and be up and running in minutes!</p>
        <h4 class="title-text">Organizing Your Drills</h4>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/FQBokFmbK18" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <h4 class="title-text">Organizing Your Practice Plans</h4>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/2FFhPN-Fb7o" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <br>
        <p class="center"><a href="walkthrough-2.php">&laquo; Diagrammer Tips &amp;
                Tricks</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="walkthrough-4.php">Creating Animated Drills /
                Systems &raquo;</a></p>
        @include('includes.commercial')
        <br>
    </div>
</div>

