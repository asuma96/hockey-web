@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>AttackPad - Online Video Support</h1>
        <h3 class="attackpad_h3 title_text">Where do I enter my activation code to view the AttackPad videos?</h3>
        <p>To enter the activation code, please visit the following URL:</p>
        <p><a href="http://hockeyshare.com/ttp/attackpad">http://hockeyshare.com/ttp/attackpad/</a></p>
        <p>Please note that you will need to <a href="http://hockeyshare.com/register/">create an account</a> with
            HockeyShare.com (free) before you are able to enter your activation code. The above link can also be
            accessed via the <strong>More</strong> menu in the top navigation.</p>
        <h3 class="attackpad_h3 title_text">My activation code isn't working, can you help?</h3>
        <p>If you are receiving an error message indicating an invalid activation code, please check for the common
            causes:</p>
        <ol>
            <li>Ensure you do not have any additional spaces entered at the end of the code</li>
            <li>Make sure to include the 3-digit prefix (including the dash): Ex: <strong>000-1a2b3c4d5e6f7g</strong>
            </li>
            <li>Double check letters / numbers that can be mistaken for each other such as: zeros and capital o, lower
                case L and the number 1, capital I and the number 1, etc.
            </li>
        </ol>
        <p>If you are still unable to get your activation code to work, please contact <a
                    href="http://www.ttpsports.com/" target="_blank">TTP Sports</a> to obtain a replacement. Please do
            <strong>NOT</strong> contact HockeyShare, as we are unable to provide a replacement code.</p>
        <h3 class="attackpad_h3 title_text">I lost my access code</h3>
        <p>If you have lost your access code, please contact <a href="http://www.ttpsports.com/" target="_blank">TTP
                Sports</a> for a replacement (<strong>NOT</strong> HockeyShare).</p>
        <h3 class="attackpad_h3 title_text">I have other questions about the AttackPad</h3>
        <p>Please contact <a href="http://www.ttpsports.com/" target="_blank">TTP Sports</a> for any questions regarding
            the AttackPad.</p>
        <h3 class="attackpad_h3 title_text">Where can I buy the AttackPad?</h3>
        <p>The AttackPad is sold through many reputable retailers. We recommend <a
                    href="http://hockeyshot.com/AttackPad-Hockey-Training-System-p/stickhandling-aid-025.htm?Click=40243"
                    target="_blank">HockeyShot.com</a>.</p>
        @include('includes.commercial')
        <br>
    </div>
</div>

