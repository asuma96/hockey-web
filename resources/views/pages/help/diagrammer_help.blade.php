@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <h1>Drill Diagrammer Help</h1>
        <p>The videos below are designed to give you an overview on how to use the various features of the online drill
            diagrammer from HockeyShare. If you still have questions or are experiencing difficulties using the
            diagrammer, please <a href="/interact">contact us</a>.</p>
        <p>&raquo; <a href="http://hockeyshare.com/drills/practiceplanner_help.php">Practice Planner Help</a></p>
        <h2>Basic Functionality Overview</h2>
        <div align="center">
            <iframe src="https://player.vimeo.com/video/23382123?byline=0&amp;portrait=0&amp;color=ffffff" width="640"
                    height="360" frameborder="0"></iframe>
        </div>

        <h2>Objects</h2>
        <div align="center">
            <h3 class="title_text">Basic Numbering</h3>
            <iframe src="https://player.vimeo.com/video/23382035?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Advanced Numbering</h3>
            <iframe src="https://player.vimeo.com/video/23382016?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Object Rotation</h3>
            <iframe src="https://player.vimeo.com/video/23382327?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Object Layering</h3>
            <iframe src="https://player.vimeo.com/video/23382285?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Deleting Objects</h3>
            <iframe src="https://player.vimeo.com/video/23382158?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

        </div>

        <h2>Actions</h2>
        <div align="center">

            <h3 class="title_text">Basic Drawing</h3>
            <iframe src="https://player.vimeo.com/video/23382029?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Freehand Drawing</h3>
            <iframe src="https://player.vimeo.com/video/23382165?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Action Snapping</h3>
            <iframe src="https://player.vimeo.com/video/23382006?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>


            <h3 class="title_text">Line Merging</h3>
            <iframe src="https://player.vimeo.com/video/23478574?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

        </div>

        <h2>Basics</h2>
        <div align="center">
            <h3 class="title_text">Rink Options</h3>
            <iframe src="https://player.vimeo.com/video/23382308?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Changing Colors</h3>
            <iframe src="https://player.vimeo.com/video/23382134?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Undo/Redo</h3>
            <iframe src="https://player.vimeo.com/video/23397527?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Saving</h3>
            <iframe src="https://player.vimeo.com/video/23382344?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">JPG Export</h3>
            <iframe src="https://player.vimeo.com/video/23382226?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">High-Resolution JPG Export <span class="premiumonly">Premium Feature</span></h3>
            <iframe src="https://player.vimeo.com/video/23382207?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>

            <h3 class="title_text">Clearing Surface</h3>
            <iframe src="https://player.vimeo.com/video/23382146?byline=0&amp;portrait=0&amp;color=ffffff"
                    class="diagrammer_help" frameborder="0"></iframe>
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

