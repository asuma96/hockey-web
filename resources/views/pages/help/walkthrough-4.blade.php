@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Introduction to Animation</h2>
        <p>Harness the power of visual learning - let your athletes see how drills and systems time out in real time by
            animating your drills. Ready to sign up? <a href="http://hockeyshare.com/drills/compare.php">Compare our
                plans</a> and be up and running in minutes!</p>
        <div class="center">
            <iframe class="walkthrough_iframe" src="https://www.youtube.com/embed/pEiABbJ77as" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <p class="button-walk center"><a href="walkthrough-3.php">&laquo; Organize Your Drills &amp; Practices</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                    href="http://hockeyshare.com/acp/compare/" class=" rpbutton">Sign Up Now</a></p>
        @include('includes.commercial')
        <br>
    </div>
</div>

