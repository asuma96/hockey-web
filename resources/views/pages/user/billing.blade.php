@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="billing">
            <h2>Purchase Information &amp; History</h2>
            <p>Below is a list of your past and current billings, subscriptions, and receipts. Active subscriptions can
                be updated and/or cancelled by using the options below. Printable receipts are available using the links
                in the receipts section below. If you have questions about your account, please <a href="/interact">contact
                    us</a>.</p>

            <div align="right"><a href="http://hockeyshare.com/upgrade/acp.php" class="pbutton">New Advanced Coaching
                    Platform Subscription</a></div>
            <h2>Advanced Coaching Platform Subscriptions</h2>
            <i>No current or past subscriptions found.</i>

            <br/>
            <h2>Receipts</h2><a name="receipts">&nbsp;</a>
            <i>No receipts found.</i>
        </div>
        @include('includes.commercial')
    </div>
</div>