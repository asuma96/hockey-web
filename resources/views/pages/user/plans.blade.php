@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h2>My Practice Plans</h2>

        <form method="post" action="">
            <div class="plan_quickaccess">

                <i class="fa fa-plus-circle fa-1x  plan_new" title="Create New Practice Plan"></i> <a
                        href="/drills/practiceplans/edit.php?folder=0">Create New Practice</a>

                &nbsp;&nbsp;
                <a href="/drills/practice_plan_defaults.php"><i class="fa fa-cog "
                                                                title="Practice Plan Preferences"></i></a>

                &nbsp;&nbsp;
                <a href="/drills/my_practice_plans_stats.php"><i class="fa fa-bar-chart x1 "
                                                                 title="Practice Plan Stats"></i></a>
                &nbsp;&nbsp;
                <a href="https://hockeyshare.groovehq.com/knowledge_base/categories/practice-planning"
                   target="_blank"><i class="fa fa-question-circle-o  plan_help red" title="Help / Support"></i></a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-search " style="color:gray;"></i> <input type="text" placeholder="Search Plans"
                                                                         name="search" value=""> <input
                        type="submit" value="Go" name="Search">

                <input type="hidden" name="folder" value="0">
                <input type="hidden" name="uid" value="0">
                <input type="hidden" name="aid" value="0">
                <input type="hidden" name="afid" value="0">
                <input type="hidden" name="start" value="0">
            </div>
        </form>

        <form method="post" action="?" id="practicePlanForm">
            <div class="pp_container">
                <div class="pp_col1">
                    <div class="plan_folders"><i class="fa fa-folder-open-o" style="color:white;"></i>&nbsp;&nbsp;My
                        Folders
                    </div>

                    <div class="plan_folder plan_folder_active">
                        &nbsp;&nbsp;
                        &nbsp;&nbsp;
                        <a href="?folder=0">My Practice Plans</a>
                    </div>

                    <div class="plan_folder">
                        <i class="fa fa-trash-o"></i>&nbsp;&nbsp;<a href="?trash=1">Deleted Plans</a>
                    </div>

                    <div class="practiceplan_folders"><input type="text" placeholder="Create New Folder"
                                                             name="foldername" value="" class="plan_newfolder_input">
                        <input type="submit" value="Go" name="newFolder" class="newfolder_button"></div>
                </div>
                <div class="pp_col2">
                    <div class="plan_headers">
                        <div class="plan_checkbox"><input type="checkbox" name="dummy" value="" onclick="checkedAll()"
                                                          class="" title="Check All"></div>
                        Practice Plan Library
                    </div>

                    <div>&nbsp;</div>
                    <div class="plan_center subtle_nu">No plans found</div>
                    <div>&nbsp;</div>

                    <div class="plan_folders plan_navigation">
                        &nbsp;
                    </div>
                </div>

                <div class="pp_col3">

                    <div class="plan_upgrade">
                        <i class="fa fa-dashboard x1 plan_white"></i> Upgrade to Premium
                    </div>

                    <div class="plan_center">

                        <div>&nbsp;</div>

                        Add Email, PDF, and Team View features by upgrading to Premium today. Plans start at just $8/mn.

                        <div>&nbsp;</div>

                        <input type="button" value="Upgrade Today" class="upgrade_button" onclick="upgradeToPremium()">

                        <div>&nbsp;</div>

                    </div>

                    <div class="plan_folders"><i class="fa fa-list" style="color:white;"></i>&nbsp;&nbsp;Options</div>

                    <div class="plan_option_subhead">Display Count</div>

                    <div class="plan_right">

                        <select name="show" class="plan_folder_select">
                            <option value="10" selected="selected">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                        </select>

                        <input type="checkbox" id="saveDisplayCount" value="saveDisplayCount" name="saveDisplayCount"
                               class="" title="Save as default for my account">
                        &nbsp;
                        <input type="submit" value="Update" name="updateDisplayCount" class="option_button">
                    </div>
                    <div>&nbsp;</div>

                    <div class="plan_option_subhead">Folder Management</div>

                    <div class="plan_right">
                        <select name="folder_move" class="plan_folder_select">
                            <option value="0" selected="selected" class="plan_folder_option">My Practice Plans</option>
                        </select>
                    </div>
                    <div class="plan_right">
                        <br>
                        <input type="submit" name="movePlans" value="Move Plans" class="option_button">
                    </div>
                    <div>&nbsp;</div>
                    
                </div>
            </div>

            <input type="hidden" name="folder" value="0">
            <input type="hidden" name="uid" value="0">
            <input type="hidden" name="aid" value="0">
            <input type="hidden" name="afid" value="0">
            <input type="hidden" name="start" value="0">
            <input type="hidden" name="search" value="">

        </form>
        <div class="pp_clear">&nbsp;</div>
        @include('includes.commercial')
    </div>
</div>