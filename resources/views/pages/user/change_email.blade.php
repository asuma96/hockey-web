@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Update Your Email Address</h1>

        <form method="post" action="" name="account_form">

            <fieldset class="hs_fieldset">
                <legend class="hs_legend">My Info</legend>
                <div><label for="email" class="hs_label">Current Email: </label> </div>
                <div class="clear_all_10"></div>
                <div><label for="new_email" class="hs_label">New Email: </label> <input type="text" name="new_email"
                                                                                        id="new_email" value=""/></div>
                <div class="clear_all_10"></div>
                <div class="clear_all_10"></div>
                <div><label for="password" class="hs_label">Password: </label> <input type="password" value=""
                                                                                      id="password" name="password"/>
                    <span class="subtle_nu">Required for account security</span></div>
                <div class="clear_all_10"></div>
            </fieldset>

            <fieldset class="hs_fieldset">
                <div><label for="Submit" class="hs_label">&nbsp;</label> <input type="submit" name="Submit" id="Submit"
                                                                                value="Save Changes"/></div>
                <div class="clear_all_10"></div>
            </fieldset>

        </form>
        @include('includes.commercial')
    </div>
</div>