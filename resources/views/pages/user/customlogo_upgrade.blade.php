@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>

        <h2>Add Custom Logos to Your Practice Plan</h2>

        <table width="100%">
            <tr>
                <td valign="top">
                    <p>HockeyShare is committed to listening to our customers. One of the most requested features was
                        the ability to add custom logos to practice plans. We're excited to now offer that functionality
                        with our Premium accounts for a <strong>one time</strong> charge of only $19.99*. Add a
                        professional touch to your practices by using your team's logo on all your practices. To get
                        started, just click on the button to the right.</p>
                    <p class="subtle_nu">*Active Premium, Team, or Association membership required</p>
                </td>
                <td style="vertical-align:top; text-align:center; padding:5px; width:280px;">
                    <div style="padding:10px; font-size:large; font-weight:bold;">Only $19.99*</div>
                    <a href="http://hockeyshare.com/upgrade/acp.php">
                        <img src="/img/pp_cust_logo_upgrade_btn.jpg"
                             width=" 250px"
                             height="51px" alt="Upgrade Today" style="border:none; padding:5px;"/>
                    </a>
                </td>
            </tr>
        </table>

        <div style="text-align:center; padding:5px;">
            <img src="/img/pp_cust_logo.jpg" height="367" width="960"
                 alt="Add Custom Logos to Your Practice Plans" style="border:none;"/>
        </div>
        @include('includes.commercial')
    </div>
</div>