@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Update Your Account Password</h1>
        <form method="post" action="">
            <fieldset class="hs_fieldset">
                <legend class="hs_legend">Update Password</legend>

                <div>
                    <div class="update_div">Current Password:</div>
                    <div style="padding:5px;"><input type="password" name="current_password" value=""/></div>
                </div>
                <div>
                    <div class="update_div">New Password:</div>
                    <div style="padding:5px;"><input type="password" name="new_password" value=""/> <span
                                class="subtle_nu">(must be at least 6 characters)</span></div>
                </div>
                <div>
                    <div class="update_div">Confirm New Password:</div>
                    <div style="padding:5px;"><input type="password" name="new_password_confirm" value=""/></div>
                </div>
                <div>
                    <div class="update_div">&nbsp;</div>
                    <div style="padding:5px;"><input type="submit" name="updatePassword" value="Update Password"/></div>
                </div>
            </fieldset>
        </form>
        @include('includes.commercial')
    </div>
</div>