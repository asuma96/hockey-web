@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h1>Edit Practice Plan</h1>

        <form method="post" action="/drills/practiceplans/edit.php">

            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                    <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1">General
                            Info</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#tabs-2">Sharing</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#tabs-3">Lines / Games</a></li>
                </ul>

                <div id="tabs-1">
                    <table border="0">
                        <tr>
                            <td width="150"><strong>Title: </strong></td>
                            <td colspan="3"><input type="text" name="title" value=""
                                                   class="edit_td"
                                                   maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td width="150"><strong>Date: </strong></td>
                            <td width="170"><input type="text" id="date" name="date" value="2017-05-16"/></td>
                            <td width="120"><strong>Focus: </strong></td>
                            <td><input type="text" name="focus" style="width:150px;" value=""/></td>
                        </tr>
                        <tr>
                            <td><strong>Start Time: </strong></td>
                            <td><input type="text" size="5" name="time" value="9:08"/> <select name="ampm">
                                    <option value="PM">PM</option>
                                    <option value="AM" selected="selected">AM</option>
                                </select></td>
                            <td><strong>Group: </strong></td>
                            <td><input type="text" name="groupdesc" style="width:150px;" value=""/></td>
                        </tr>
                        <tr>
                            <td><strong>Length: </strong></td>
                            <td><input type="text" size="3" name="length" maxlength="3" value=""/> minutes</td>
                            <td><strong>Level: </strong></td>
                            <td><select name="level">
                                    <option value="MITE">Novice/Mite</option>
                                    <option value="SQ">Atom/Squirt</option>
                                    <option value="PW">Peewee</option>
                                    <option value="BAN">Bantam</option>
                                    <option value="MIDG">Midget</option>
                                    <option value="HS">High School</option>
                                    <option value="PREP">Prep</option>
                                    <option value="JR">Junior</option>
                                    <option value="COL">College</option>
                                    <option value="PRO">Professional</option>
                                    <option value="ADULT">Adult</option>
                                    <option value="GIRLS">Girls</option>
                                    <option value="OTHER">Other</option>
                                    <option value="R-AS">Ringette - Active Start</option>
                                    <option value="R-U9">Ringette - U9</option>
                                    <option value="R-U10">Ringette - U10</option>
                                    <option value="R-U12">Ringette - U12</option>
                                    <option value="R-U14">Ringette - U14</option>
                                    <option value="R-U16">Ringette - U16</option>
                                    <option value="R-U19">Ringette - U19</option>
                                    <option value="R-HP">Ringette - High Performance</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Prepared By: </strong></td>
                            <td colspan="3"><input type="text" name="preparedby" value="" style="width:450px;"/></td>
                        </tr>
                        <tr>
                            <td valign="top"><strong>Custom Logo:</strong></td>
                            <td colspan="3">
                                <a href="customlogo_upgrade.php">Optional
                                    Add-On</a> <span
                                        class="edit_span">NEW</span>
                            </td>
                        </tr>


                        <tr>
                            <td valign="top"><strong>Notes: </strong></td>
                            <td colspan="3"><textarea rows="6" style="width:450px;" name="notes"></textarea></td>
                        </tr>
                        <tr>
                            <td valign="top"><strong>Folder:</strong></td>
                            <td colspan="3"><select name="folderid">
                                    <option value="0" selected="selected">Uncategorized</option>
                                </select></td>
                        </tr>
                        <tr>
                            <td valign="top"><strong>Template: </strong></td>
                            <td colspan="3"><select name="template">
                                    <option value="1">Template #1 (Grid w/ Start Times)</option>
                                    <option value="8">Template #1.1 (Grid w/ Start Times - No Page Breaks)</option>
                                    <option value="11">Template #2 (Grid, NO Start Times)</option>
                                    <option value="14">Template #2.1 (Grid, NO Start Times - No Page Breaks)</option>
                                    <option value="12">Template #3 (Simple List w/ Start Times)</option>
                                    <option value="15">Template #3.1 (Simple List w/ Start Times - No Page Breaks)
                                    </option>
                                    <option value="24">Template #3.2 (Simple List w/ Start Times, Logo)</option>
                                    <option value="13">Template #4 (Simple List, NO Start Times)</option>
                                    <option value="16">Template #4.1 (Simple List, NO Start Times - No Page Breaks)
                                    </option>
                                    <option value="18">Template #5 (Compact)</option>
                                    <option value="21">Template #6 (Landscape) - Lines/Game Support</option>
                                    <option value="20">Template #6 (Portrait) - Lines/Game Support</option>
                                    <option value="22">Template #7 (Landscape)</option>
                                    <option value="23">Template #8 (Variation of Template #3)</option>
                                    <option value="17">zzzPro (BETA)</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4">
                                <table border="0">
                                    <tr>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template1.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template1_tn.jpg"
                                                        height="105" width="125" alt="Template #1" border="0"/></a>
                                        </td>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template2.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template2_tn.jpg"
                                                        height="105" width="125" alt="Template #2" border="0"/></a>
                                        </td>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template3.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template3_tn.jpg"
                                                        height="105" width="125" alt="Template #3" border="0"/></a>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"><a href="/img/template1.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #1</a></td>
                                        <td valign="top" align="center"><a href="/img/template2.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #2</a></td>
                                        <td valign="top" align="center"><a href="/img/template3.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #3</a><sup>*</sup>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template4.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template4_tn.jpg"
                                                        height="105" width="125" alt="Template #4" border="0"/></a>
                                        </td>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template5.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template5_tn.jpg"
                                                        height="105" width="125" alt="Template #5" border="0"/></a>
                                        </td>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template6_portrait.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template6_tn_portrait.jpg"
                                                        height="105" width="125" alt="Template #6" border="0"/></a>
                                        </td>
                                        <td valign="top" width="130" align="center">
                                            <a href="/img/template7.jpg" onclick="return hs.expand(this)"
                                               class="highslide"><img
                                                        src="/img/template7_tn.jpg"
                                                        height="105" width="125" alt="Template #7" border="0"/></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"><a href="/img/template4.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #4</a><sup>*</sup>
                                        </td>
                                        <td valign="top" align="center"><a href="/img/template5.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #5</a></td>
                                        <td valign="top" align="center"><a href="/img/template6_portrait.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #6</a></td>
                                        <td valign="top" align="center"><a href="/img/template7_portrait.jpg"
                                                                           onclick="return hs.expand(this)"
                                                                           class="highslide">Template #7</a></td>
                                    </tr>
                                </table>
                                <div class="subtle_nu"><sup>*</sup> Indicates template does not support custom logos
                                </div>

                            </td>
                        </tr>

                        <tr>
                            <td valign="top"><strong>Associate w/ Practice: </strong></td>
                            <td colspan="3"><i><a href="http://hockeyshare.com/acp/compare/">Premium</a> users can
                                    associate practice plans with events on their practice schedule when using
                                    HockeyShare.com's Stat Tracking feature!</i>
                            </td>
                        </tr>

                    </table>
                </div>

                <div id="tabs-2">
                    <table border="0" cellpadding="6" cellspacing="0">
                        <tr>
                            <td class="edit_td_new">Team Share:</td>
                            <td class="edit_td_new" nowrap="nowrap"><i>Premium users can share
                                    practice plans with other members of your team/coaching staff via a
                                    password-protected page</i></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><i>You are not a member of any association</i></td>
                        </tr>

                    </table>
                </div>

                <div id="tabs-3">

                    <h2>Lines</h2>

                    <div style="text-align:right;"><a
                                                      title="Default lines can be set via Drills &amp; Practices &raquo; Preferences / Settings (under My Practice Plans heading)">Load
                            Default Lines</a></div>

                    <table border="0">
                        <tr>
                            <th>Color</th>
                            <th>Lines</th>
                            <th>D Pair</th>
                            <th>Goalies</th>
                        </tr>
                        <tr>
                            <td><input type="text" id="color1" name="pro[color1]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f1" name="pro[f1]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d1" name="pro[d1]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="g1" name="pro[g1]" value="" size="20" maxlength="20"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="color2" name="pro[color2]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f2" name="pro[f2]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d2" name="pro[d2]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="g2" name="pro[g2]" value="" size="20" maxlength="20"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="color3" name="pro[color3]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f3" name="pro[f3]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d3" name="pro[d3]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="g3" name="pro[g3]" value="" size="20" maxlength="20"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="color4" name="pro[color4]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f4" name="pro[f4]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d4" name="pro[d4]" value="" size="20" maxlength="20"/></td>
                            <th>Injuries</th>
                        </tr>
                        <tr>
                            <td><input type="text" id="color5" name="pro[color5]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f5" name="pro[f5]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d5" name="pro[d5]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="inj1" name="pro[inj1]" value="" size="20" maxlength="20"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="color6" name="pro[color6]" value="" size="20" maxlength="20"/>
                            </td>
                            <td><input type="text" id="f6" name="pro[f6]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="d6" name="pro[d6]" value="" size="20" maxlength="20"/></td>
                            <td><input type="text" id="inj2" name="pro[inj2]" value="" size="20" maxlength="20"/></td>
                        </tr>
                    </table>

                    <h2>Upcoming Games</h2>
                    <table border="0">
                        <tr>
                            <th>Date</th>
                            <th>Game</th>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game1_date]" id="game1_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game1_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game2_date]" id="game2_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game2_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game3_date]" id="game3_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game3_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game4_date]" id="game4_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game4_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game5_date]" id="game5_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game5_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="pro[game6_date]" id="game6_date" value="" size="10"
                                       maxlength="10"/></td>
                            <td><input type="text" name="pro[game6_desc]" value="" size="35" maxlength="50"/></td>
                        </tr>
                    </table>

                </div>

            </div>
            <p align="right"><input type="submit" name="SubmitReturn" value="Update &amp; Return to My Plans">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                        type="submit" name="Submit" value="Update Practice Plan Details"
                        class="edit_p_new"/></p>

            <input type="hidden" name="ppid" value="0"/>
        </form>
        @include('includes.commercial')
    </div>
</div>