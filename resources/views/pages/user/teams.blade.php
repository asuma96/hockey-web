@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <div class="margin_teams">
            <br/>

            <h1>My Teams Administration</h1>
            <p>HockeyShare is proud to offer free stat tracking for hockey teams. With HockeyShare's free stat tracking
                feature, you can: create announcements, track stats, email your team members, create player profiles,
                track
                your game schedule (w/ driving directions), track game schedules, create custom pages, list relevant
                website
                links, make game notes on your gametape, password protect your team's page, and more.</p>
            <p>To get started, please visit our <a href="http://hockeyshare.com/stat_tracking_guide.php">Getting Started
                    Guide</a> or watch our <a href="http://hockeyshare.com/teams/video_guides.php">Video Tutorials</a>.
            </p>
            <p align="right"><a href="addteam.php" class="pbutton">Add a New Team</a></p>


            <h2><i class="fa fa-lock fa-1x "
                   title="Password  required to view team information.<br /><br />Team password options can be accessed via the Team Details link below.<br /><br />NOTE: Team admins are NOT required to enter the team password if they are logged in to their HockeyShare account."></i>&nbsp;
                {{--{{$team}}--}}</h2>

            <table border="0" width="100%">
                <tr>
                    <td width="150"><i class="fa fa-users"></i>&nbsp; <b>Roster:</b></td>
                    <td><a href="/team/index.php?id=6060" class="myteams">View</a> | <a href="addplayer.php?id=6060"
                                                                                        class="myteams">Add Player</a> |
                        <a
                                href="editplayer.php?id=6060" class="myteams">Edit Player</a> | <a
                                href="cutouts.php?id=6060" target="_blank" class=""
                                title="Drag and drop your players around to create your perfect line combinations!">Team
                            Cut-Outs</a></td>
                </tr>
                <tr>
                    <td>
                        <div class="fa fa-car"></div>
                        &nbsp; <b>Games: </b></td>
                    <td><a href="/team/schedule.php?id=6060" class="myteams">View</a> | <a
                                href="/team/boxscores.php?id=6060" class="myteams">Box Scores</a> | <a
                                href="addgame.php?id=6060" class="myteams">Add Game</a> <a
                                href="http://hockeyshare.com/teams/video_guides.php#directions"><i
                                    class="fa fa-info-circle"></i></a> | <a href="editgame.php?id=6060" class="myteams">Edit
                            Game</a> | <a href="editgametype.php?id=6060">Manage Game Types</a></td>
                </tr>
                <tr>
                    <td><i class="fa fa-calendar"></i>&nbsp; <strong>Practices:</strong></td>
                    <td><a href="/team/practice_schedule.php?id=6060">View</a> | <a href="addpractice.php?id=6060">Add
                            Practices</a> <a href="http://hockeyshare.com/teams/video_guides.php#practices"><i
                                    class="fa fa-info-circle"></i></a> | <a href="editpractice.php?id=6060">Edit
                            Practice</a></td>
                </tr>
                <tr>
                    <td><i class="fa fa-line-chart"></i>&nbsp; <b>Stats: </b></td>
                    <td><a href="/team/stats.php?id=6060" class="myteams">View</a> <a
                                href="/team/stats.php?id=6060&pf=1"
                                target="_blank"><i
                                    class="fa fa-print"></i></a> | <a href="gameresults.php?id=6060" class="myteams">Enter
                            Game Results/Stats</a></td>
                </tr>
                <tr>
                    <td><i class="fa fa-bullhorn"></i>&nbsp; <b>Announcements: </b></td>
                    <td><a href="/team/announcements.php?id=6060" class="myteams">View</a> | <a
                                href="addannouncement.php?id=6060" class="myteams">Add New</a> <a
                                href="http://hockeyshare.com/teams/video_guides.php#upload"><i
                                    class="fa fa-info-circle"></i>

                        </a> | <a href="editannouncement.php?id=6060" class="myteams">Edit</a> | <a
                                href="send_email.php?id=6060">Send
                            to
                            List </a> <a href="http://hockeyshare.com/teams/video_guides.php#email"><i
                                    class="fa fa-info-circle"></i></a></td>
                </tr>
                <tr>
                    <td valign="top"><i class="fa fa-link"></i>&nbsp; <strong>Links: </strong></td>
                    <td><a href="/team/team_links.php?id=6060">View</a> | <a href="addlink.php?id=6060">Add New</a> | <a
                                href="editlink.php?id=6060">Edit Links</a></td>
                </tr>
                <tr>
                    <td valign="top"><i class="fa fa-sticky-note"></i>&nbsp; <strong>Custom Pages:</strong></td>
                    <td><a href="addcustom.php?id=6060">Add New Custom Page</a> <a
                                href="http://hockeyshare.com/teams/video_guides.php#custom"><i
                                    class="fa fa-info-circle"></i></a> | <a href="editcustom.php?id=6060">Edit Custom
                            Page</a>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><i class="fa fa-gear"></i>&nbsp; <strong>Team Admin:</strong></td>
                    <td><a href="email_list.php?id=6060">Email List</a> | <a href="editteam.php?id=6060">Team
                            Details</a> | <a
                                href="teamurl.php?id=6060">Team URL</a> | <a href="add_home_rink.php?id=6060">Home
                            Rink</a> | <a
                                href="add_user_access.php?id=6060">Add User Access</a> <a
                                href="http://hockeyshare.com/teams/video_guides.php#user_access"><i
                                    class="fa fa-info-circle"></i>

                        </a> | <a href="archive.php?id=6060">Hide this Team from My List</a> | <a
                                href="reset_team.php?id=6060">Reset
                            Team Data</a></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td align="right"><a href="deleteteam.php?id=6060" style="color:#FF0000;"><i class="fa fa-remove"
                                                                                                 class="menu_a"></i>
                            Delete
                            This
                            Team</a></td>
                </tr>
            </table>
            <br/>
            <div class="teams_div"><i
                        class="fa fa-link"></i> &nbsp;<b>Team URL: </b>
                <a href="http://hockeyshare.com/team/?id=6060">http://hockeyshare.com/team/?id=6060</a>
                <br><br>
                <span class="subtle_nu">Send this link to your team so they can view your team. If your team is password protected, they will need to enter the team password you set up prior to viewing any of the team pages.</span>
            </div>
            <hr/>

            <div class="teams_div_new"><i
                        class="fa fa-eye"></i> &nbsp;<a href="?show_archived=1">Show All My Teams</a></div>
            <br/>
            <h2><i class="fa fa-bookmark fa-1x"></i> &nbsp;My Bookmarked Teams</h2>
            <div><i>No bookmarked teams found.</i></div>
            @include('includes.commercial')
        </div>
    </div>
</div>