@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>

        <h1>Practice Plan Default Options</h1>
        <p>Use the options below to auto-populate various aspects of every practice plan you create. If you set defaults
            options below, you will still be able to modify them while creating practice plans.</p>
        <form method="post" action="practice_plan_defaults.php">
            <table border="0" width="100%">
                <tr>
                    <td colspan="2">
                        <div class="pp_headers">Basic Options</div>
                    </td>
                </tr>
                <tr>
                    <td><strong>Default Start Time: </strong></td>
                    <td><input type="text" name="time" value="" size="6" maxlength="6"/> <select name="ampm">
                            <option value="" selected="selected">-</option>
                            <option value="AM">AM</option>
                            <option value="PM">PM</option>
                        </select></td>
                </tr>
                <tr>
                    <td><strong>Length: </strong></td>
                    <td><input type="text" name="length" size="4" maxlength="4" value=""/></td>
                </tr>
                <tr>
                    <td><strong>Prepared By: </strong></td>
                    <td><input type="text" name="preparedby" style="width:450px;" value=""/></td>
                </tr>

                <tr>
                    <td><strong>Group: </strong></td>
                    <td><input type="text" name="groupdesc" value=""/></td>
                </tr>
                <tr>
                    <td><strong>Level: </strong></td>
                    <td><select name="level">
                            <option value="MITE">Novice/Mite</option>
                            <option value="SQ">Atom/Squirt</option>
                            <option value="PW">Peewee</option>
                            <option value="BAN">Bantam</option>
                            <option value="MIDG">Midget</option>
                            <option value="HS">High School</option>
                            <option value="PREP">Prep</option>
                            <option value="JR">Junior</option>
                            <option value="COL">College</option>
                            <option value="PRO">Professional</option>
                            <option value="ADULT">Adult</option>
                            <option value="GIRLS">Girls</option>
                            <option value="OTHER">Other</option>
                            <option value="R-AS">Ringette - Active Start</option>
                            <option value="R-U9">Ringette - U9</option>
                            <option value="R-U10">Ringette - U10</option>
                            <option value="R-U12">Ringette - U12</option>
                            <option value="R-U14">Ringette - U14</option>
                            <option value="R-U16">Ringette - U16</option>
                            <option value="R-U19">Ringette - U19</option>
                            <option value="R-HP">Ringette - High Performance</option>
                        </select></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Template: </strong></td>
                    <td colspan="3"><select name="template">
                            <option value="" selected="selected">-</option>
                            <option value="1">Template #1 (Grid w/ Start Times)</option>
                            <option value="8">Template #1.1 (Grid w/ Start Times - No Page Breaks)</option>
                            <option value="10">Template #6 (BETA)</option>
                            <option value="11">Template #2 (Grid, NO Start Times)</option>
                            <option value="12">Template #3 (Simple List w/ Start Times)</option>
                            <option value="13">Template #4 (Simple List, NO Start Times)</option>
                            <option value="14">Template #2.1 (Grid, NO Start Times - No Page Breaks)</option>
                            <option value="15">Template #3.1 (Simple List w/ Start Times - No Page Breaks)</option>
                            <option value="16">Template #4.1 (Simple List, NO Start Times - No Page Breaks)</option>
                            <option value="17">zzzPro (BETA)</option>
                            <option value="18">Template #5 (Compact)</option>
                            <option value="19">Beta</option>
                            <option value="20">Template #6 (Portrait) - Lines/Game Support</option>
                            <option value="21">Template #6 (Landscape) - Lines/Game Support</option>
                            <option value="22">Template #7 (Landscape)</option>
                            <option value="23">Template #8 (Variation of Template #3)</option>
                            <option value="24">Template #3.2 (Simple List w/ Start Times, Logo)</option>
                        </select></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Folder: </strong></td>
                    <td colspan="3"><select name="folder">
                            <option value="0" selected="selected">Uncategorized</option>
                        </select></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="pp_headers">PDF Options</div>
                    </td>
                </tr>
                <tr>
                    <td><strong>PDF Font Size:</strong></td>
                    <td><input name="pdffontsize" type="text" id="pdffontsize" value="11" size="3" maxlength="3"/>
                        <select name="pdffontunit">
                            <option value="px" selected="selected">px</option>
                            <option value="em">em</option>
                        </select>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="pp_headers">Email Options</div>
                    </td>
                </tr>
                <tr>
                    <td><strong>Email From Name:</strong></td>
                    <td><input type="text" name="emailname" style="width:450px;" value=""/></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Default Email List:</strong></td>
                    <td>
                        <textarea name="emaillist" id="emaillist" cols="45" rows="10"></textarea>

                        <p><em><strong>enter each email address on a separate line (max 50 addresses) </strong></em></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="pp_headers">Sharing Options</div>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong>Associations:</strong></td>
                    <td>
                        <i>N/A</i></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="pp_headers">Default Line Combinations</div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">

                        <table border="0">
                            <tr>
                                <th>Color</th>
                                <th>Lines</th>
                                <th>D Pair</th>
                                <th>Goalies</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="color1" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f1" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d1" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="g1" value="" size="20" maxlength="20"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="color2" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f2" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d2" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="g2" value="" size="20" maxlength="20"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="color3" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f3" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d3" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="g3" value="" size="20" maxlength="20"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="color4" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f4" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d4" value="" size="20" maxlength="20"/></td>
                                <th>Injuries</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="color5" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f5" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d5" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="inj1" value="" size="20" maxlength="20"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="color6" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="f6" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="d6" value="" size="20" maxlength="20"/></td>
                                <td><input type="text" name="inj2" value="" size="20" maxlength="20"/></td>
                            </tr>

                        </table>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" name="Submit" id="Submit" value="Save Changes"/></td>
                </tr>
            </table>
        </form>
        @include('includes.commercial')
    </div>
</div>