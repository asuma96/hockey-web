@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <table width="100%" border="0" cellspacing="3" cellpadding="3">
            <tr>
                <td valign="top" width="400"><h3>What You'll Get in the 14-Day Trial</h3>
                    <p>With the FREE 14 Day Trial of the Advanced Coaching Platform, you will get un-restricted access
                        to our entire collection of Premium features. Every account is eligible for one complimentary
                        14-day trial of our services - each trial includes the following benefits: </p>
                    <ul>
                        <li>NO CREDIT CARD NECESSARY</li>
                        <li>14 Days of Full Premium-level access</li>
                        <li>Unlimited Drill Diagramming</li>
                        <li>Unlimited Practice Plan Creation</li>
                        <li>Unlimited Animations</li>
                        <li>Email Practice Plans</li>
                        <li>Export PDF Versions of Drills &amp; Practice Plans</li>
                        <li>Un-Branded Drill Diagrams</li>
                        <li>Share Drills &amp; Practices with Your Team</li>
                    </ul>
                </td>
                <td width="400" align="center" valign="top">

                    <h3>Checking Your Free Trial Eligibility</h3>

                    <p>We are checking your account eligibility for the Free 14 Day Trial...</p>


                    <table width="100%" border="0" class="freetrial_table">
                        <tr>
                            <td width="100" align="center"><img
                                        src="//d3e9hat72fmrwm.cloudfront.net/images/freetrial/success.jpg" height="75"
                                        width="75" alt="Success" border="0"/></td>
                            <td><p>Your account is eligible for a free 14 day trial! Click the button below to instantly
                                    activate your complimentary access.</p>
                                <p align="center"><a href="?start=79081"><img
                                                src="//d3e9hat72fmrwm.cloudfront.net//images/freetrial/starttrial.png"
                                                height="69" width="250" alt="Start My Free Trial" border="0"/></a></p>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="3" cellpadding="3">
            <tr>
                <td width="408" align="center"><img src="//d3e9hat72fmrwm.cloudfront.net/images/freetrial/freetrial.jpg"
                                                    border="0" height="166" width="332" alt="Try the Demo"/></td>
                <td valign="top">

                    <p align="center"><img src="//d3e9hat72fmrwm.cloudfront.net/images/freetrial/benefits.jpg"
                                           border="0" height="197" width="252" alt="Benefits of the Demo"/></p>

                </td>
            </tr>
        </table>

        <table class="full_width_tbl">
            <tr>
                <td class="ttop">

                    <div class="center"><a href="http://hockeyshare.com/acp/learn-more/"><img
                                    src="//d3e9hat72fmrwm.cloudfront.net/images/drills/hsddpp_pro.jpg" height="250"
                                    width="875"
                                    alt="HockeyShare's Drill Diagrammer and Practice Planning Platform - Now Available..."
                                    class="toolTip"
                                    title="HockeyShare's Drill Diagrammer and Practice Planning Platform - Now Available...Click for More Details."/></a>
                    </div>

                </td>
            </tr>
        </table>
        @include('includes.commercial')
    </div>
</div>