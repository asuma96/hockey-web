@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <div class="addteam">
            <br/>
            <h3>Create New Team</h3>
            <form method="post" action="team_nav_redirect.php">
                <p class="addteam_p center"><b>Team
                        Navigation: </b> <select name="page">
                        <option value="index.php">My Teams</option>
                        <option value="addteam.php">Add New Team</option>
                    </select>&nbsp;<input type="submit" name="Submit" value="go"/></p>
            </form>
            <img src="/img/default_team_logo.gif" alt=" Logo"/>
            <form method="post" action="" name="teamaddform" enctype="multipart/form-data" class="niceform">
                <fieldset>
                    <legend>Create a New Team</legend>
                    <table border="0">
                        <tr>
                            <td>Team Name:</td>
                            <td><input type="text" name="teamname" value="" size="45"/></td>
                        </tr>
                        <tr>
                            <td>Short Name:</td>
                            <td><input name="shortname" type="text" value="" maxlength="35"/></td>
                        </tr>
                        <tr>
                            <td>Season:</td>
                            <td><input name="season" type="text" id="season" value=""/>
                                <span class="subtle_nu">(ex: 2012-13)</span></td>
                        </tr>
                        <tr>
                            <td>Age Class:</td>
                            <td><input type="text" name="ageclass" value=""/></td>
                        </tr>
                        <tr>
                            <td>Location:</td>
                            <td><input type="text" name="location" value=""/></td>
                        </tr>
                        <tr>
                            <td valign="top">Team Logo:</td>
                            <td><input type="file" id="logo" name="logo"/></td>
                        </tr>
                        <tr>
                            <td valign="top">Require Password:</td>
                            <td>
                                <input name="reqpassword" type="checkbox" id="reqpassword" value="1"/>
                                <a href="javascript:open_close_group('passwordreq');">Info</a>
                                <div class="addteam_div"
                                     id="passwordreq">If you check this box, a password will be required by ALL users to
                                    view any of the team pages. This feature can be useful if you wish to keep your
                                    player
                                    roster private or have a club privacy policy that would prevent public access to
                                    your
                                    roster. If you check the require password box, you MUST fill in the team password
                                    field
                                    below.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Team Password:</td>
                            <td valign="top"><label>
                                    <input name="password" type="text" id="password" maxlength="35" value=""/>
                                </label></td>
                        </tr>
                        <tr>
                            <td valign="top">Hide Stats:</td>
                            <td><input name="hidestats" type="checkbox" id="hidestats" value="1"/> <a
                                        href="javascript:open_close_group('stathide');">Info</a>
                                <div class="addteam_div" id="stathide">
                                    This option allows you to track your team stats but not make them visible to people
                                    viewing your web page. This can be useful if you decide you do not want
                                    players/parents
                                    to see the stats throughout the season, but still want to track them and even
                                    possibly
                                    view them after the season has completed
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="Submit" value="Create Team"/></td>
                        </tr>
                    </table>

                </fieldset>
            </form>
            @include('includes.commercial')
        </div>
    </div>
</div>