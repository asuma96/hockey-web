@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">»</span><a href="http://hockeyshare.com/plans/"
                                                         class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">»</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                         class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">»</span><a href="http://hockeyshare.com/acp/email/history/"
                                                         class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">»</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h2 id="top">My Drills &amp; Systems</h2>
        <form method="post" action="">
            <div class="drill_quickaccess">

                <a href="https://hockeyshare.groovehq.com/knowledge_base/categories/drill-diagramming" target="_blank">

                </a>
                <a href="/drills/add/"><i
                            class="fa fa-plus-circle fa-2x  drill_new green"></i></a>

                <i class="fa fa-check-square-o fa-2x red " style="cursor:pointer; " onclick="checkedAll()"></i>


                <i class="fa fa-times fa-2x drill_help " onclick="trashDrills()" style="cursor: pointer;"></i>

                <i class="fa fa-refresh fa-2x " style="cursor: pointer;" onclick="reloadDrills()"></i>

                <a href="?trim=0"><i class="fa fa-ellipsis-h fa-2x "></i></a>

                <a href="/drill-store/"><i class="fa fa-shopping-cart fa-2x  drill_help red"></i></a>

                <a href="/drills/my-drills/trash/"><i class="fa fa-trash fa-2x "></i></a>

                <a style="margin-right: 10px" href="https://hockeyshare.groovehq.com/help_center" target="_blank"><i
                            class="fa fa-question-circle fa-2x  drill_help red"></i></a>


                <i class="fa fa-search fa-2x " style="color:gray;"></i> <input class="input_drills" type="text"
                                                                               placeholder="Search Drills"
                                                                               name="search" value=""> <input
                        class="submit_drills option_button"
                        type="submit" value="Go" name="Search">

            </div>
        </form>

        <table class="my_drills_table">
            <tbody>
            <tr>
                <td class="my_drills_td">
                    <form method="post" action="" id="mydrills_form">
                        <div id="my_drills">

                            <input type="hidden" id="has_custom_categories" value="0">
                        </div>
                    </form>
                </td>
                <td style="vertical-align:top;">
                    <div id="sidebar" >
                        <div class="mydrills_category_title">Quick Nav</div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_center">
                            <select id="quick_jump" onchange="scrollTo(this)">
                                <option value="" selected="selected">-- Jump to Category --</option>
                                <option value="999999">Search Results</option>
                                <option value="23">1 on 0</option>
                                <option value="15">1 on 1</option>
                                <option value="21">2 on 0</option>
                                <option value="17">2 on 1</option>
                                <option value="16">2 on 2</option>
                                <option value="22">3 on 0</option>
                                <option value="18">3 on 1</option>
                                <option value="19">3 on 2</option>
                                <option value="20">3 on 3</option>
                                <option value="25">Agility</option>
                                <option value="11">Backchecking</option>
                                <option value="6">Competitive</option>
                                <option value="26">Defense</option>
                                <option value="14">Dryland Training</option>
                                <option value="27">Forwards</option>
                                <option value="8">Goalie</option>
                                <option value="24">Import</option>
                                <option value="3">Passing</option>
                                <option value="1">Puck Control</option>
                                <option value="4">Shooting</option>
                                <option value="5">Skating</option>
                                <option value="7">Small Game</option>
                                <option value="13">Stations</option>
                                <option value="2">Stickhandling</option>
                                <option value="9">Systems</option>
                                <option value="10">Timing</option>
                                <option value="12">Warmup</option>
                            </select>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Usage</div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_center">
                            Total drills: 0 / 15&nbsp;&nbsp;&nbsp;&nbsp;<a href="/drills/compare.php">Upgrade</a></div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Drill Inbox</div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_center">
                            <i class="fa fa-envelope-o fa-lg"></i> &nbsp;
                            <a href="/drills/my-drills/inbox/">View Inbox (0)</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
                                    href="/drills/my-drills/inbox/sent/">Sent</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                    href="https://hockeyshare.groovehq.com/knowledge_base/topics/drill-inbox-overview"
                                    target="_blank"><i class="fa fa-question-circle fa-lg  drill_help red"></i></a>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_right">
                            Send Drills to: <input type="text" id="sendDrillsTo" name="sendDrillsTo"
                                                   placeholder="Username">
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_right">
                            <input type="button" value="Go" onclick="sendInboxDrills()" class="option_button">
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Public Categories</div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_right">
                            <select name="public_category" id="public_category">
                                <option value="" selected="selected">-- Select a Category --</option>
                                <option value="999999">Search Results</option>
                                <option value="23">1 on 0</option>
                                <option value="15">1 on 1</option>
                                <option value="21">2 on 0</option>
                                <option value="17">2 on 1</option>
                                <option value="16">2 on 2</option>
                                <option value="22">3 on 0</option>
                                <option value="18">3 on 1</option>
                                <option value="19">3 on 2</option>
                                <option value="20">3 on 3</option>
                                <option value="25">Agility</option>
                                <option value="11">Backchecking</option>
                                <option value="6">Competitive</option>
                                <option value="26">Defense</option>
                                <option value="14">Dryland Training</option>
                                <option value="27">Forwards</option>
                                <option value="8">Goalie</option>
                                <option value="24">Import</option>
                                <option value="3">Passing</option>
                                <option value="1">Puck Control</option>
                                <option value="4">Shooting</option>
                                <option value="5">Skating</option>
                                <option value="7">Small Game</option>
                                <option value="13">Stations</option>
                                <option value="2">Stickhandling</option>
                                <option value="9">Systems</option>
                                <option value="10">Timing</option>
                                <option value="12">Warmup</option>
                            </select>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_right">
                            <input type="button" onclick="moveDrillsPublic()" value="Move" class="option_button">
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="mydrills_category_title">Assign Custom Categories</div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <table>
                            <tbody>
                            <tr>
                                <td width="35" align="center" valign="top"><img src="/img/idea-red.png"
                                                                                border="0" alt="Idea" height="32"
                                                                                width="32"></td>
                                <td>
                                    Premium users can keep their drills organized with custom categories. <a
                                            href="/drills/compare.php">Learn More</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Team Sharing Options</div>
                        <div class="drill_vertspacer">&nbsp;</div>


                        <div class="drill_center">
                            <img src="/img/idea-red.png" border="0" alt="Idea" height="32" width="32"> &nbsp;
                            Premium users can share their drills with their team &amp; coaching staff. <a
                                    href="/drills/compare.php">Learn More</a>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Association Sharing Options</div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="drill_center">
                            <img src="/img/idea-red.png" border="0" alt="Idea" height="32" width="32"> &nbsp;Association
                            members can share their drills w/ other coaches in the program. <a
                                    href="/drills/compare.php">Learn More</a>
                        </div>

                        <div class="drill_vertspacer">&nbsp;</div>


                        <div class="mydrills_category_title">Favorite Drills <a name="favdrills"> </a></div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_center">
                            <i class="fa fa-star drill_favorite fa-lg yellow"></i> <a href="#/"
                                                                                      onclick="favoriteDrills()"
                                                                                      class="option_button">Mark as
                                Favorite</a>&nbsp;&nbsp;&nbsp;<i class="fa fa-star-o fa-lg "></i> <a href="#/"
                                                                                                     onclick="unFavoriteDrills()"
                                                                                                     class="option_button">Remove
                                from Favorites</a>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Export Drills</div>
                        <div class="drill_vertspacer">&nbsp;</div>
                        <div class="drill_center">
                            <button onclick="exportCSV()" class="option_button">CSV</button>
                            &nbsp;&nbsp;
                            <button onclick="printMultiple()" class="option_button">Print</button>
                            &nbsp;&nbsp;
                            <button onclick="mailMultiple()" class="option_button">Email</button>
                        </div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <div class="mydrills_category_title">Custom Categories</div>
                        <div class="drill_vertspacer">&nbsp;</div>

                        <table>
                            <tbody>
                            <tr>
                                <td width="35" align="center" valign="top"><img src="/img/idea-red.png"
                                                                                border="0" alt="Idea" height="32"
                                                                                width="32"></td>
                                <td>
                                    Premium users can organize their drills by creating custom categories. <a
                                            href="/drills/compare.php">Learn More</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="drill_vertspacer">&nbsp;</div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        @include('includes.commercial')
    </div>
</div>