@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>

        <h2>Drill Inbox</h2>

        <form method="post" action="" name="drillForm">
            <p><i>No drills found in your inbox. You can send drills to other users from the My Drills page.</i></p>
            <div class="page_navigation"><input type="button" name="CheckAll" value="Check All"
                                                onclick="checkAll(document.drillForm['drills[]'])"/>&nbsp;&nbsp;<input
                        type="button" name="UnCheckAll" value="Uncheck All"
                        onclick="uncheckAll(document.drillForm['drills[]'])"/></div>
            <table width="100%" id="table_a">
                <tbody>
                </tbody>
            </table>
            <div class="page_navigation">
                <input type="submit" name="copyMultiple" value="Copy Selected to My Drills">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="deleteMultiple" value="Delete Selected" class="deleteMultiple">
            </div>
        </form>
        <div class="page_navigation sent_navigation">
        </div>
        @include('includes.commercial')
    </div>
</div>