@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h2>Add a Drill</h2>

        <div align="left">
            <img src="/img/step1.jpg" width="875" height="50" alt="Step 2 of 3"
                 class="add_img"/>
        </div>
        <br clear="all"/>
        <form method="post">
            <fieldset class="drillfieldset">
                <legend class="basic_drills">Basic Drill Details</legend>
                <label for="drillname">Drill Name: </label>
                <input type="text" name="drillname" value="" id="drillname" class="drillfields"/>
                <br clear="all"/><br/>

                <label for="drillang">Drill Language: </label>
                <input type="radio" name="drilllang" value="en" checked="checked"/> English <input type="radio"
                                                                                                   name="drilllang"
                                                                                                   value="fr"/> French
                <br clear="all"/><br/>

                <label for="halfice">Drill Type: </label>
                <input type="radio" name="halfice" value="0" checked="checked"/> Full Ice <input type="radio"
                                                                                                 name="halfice"
                                                                                                 value="1"/> 1/2 Ice
                <br clear="all"/><br/>
                <label for="ageid">Minimum Age Level: </label>
                <select name="ageid" id="ageid">
                    <option value="" selected="selected">--Choose an Age Group</option>
                    <option value="1">Mites/Novice</option>
                    <option value="2">Squirt/Atom</option>
                    <option value="3">Peewee</option>
                    <option value="4">Bantam</option>
                    <option value="5">Midget/High School</option>
                    <option value="6">Junior</option>
                    <option value="7">College</option>
                    <option value="8">Professional</option>
                    <option value="9">ALL</option>
                    <option value="10">Ringette - Active Start</option>
                    <option value="11">Ringette - U9</option>
                    <option value="12">Ringette - U10</option>
                    <option value="13">Ringette - U12</option>
                    <option value="14">Ringette - U14</option>
                    <option value="15">Ringette - U16</option>
                    <option value="16">Ringette - U19</option>
                    <option value="17">Ringette - High Performance</option>
                </select>
                <br clear="all"/><br/>

                <label for="categoryid">Public Drill Category: </label>
                <select name="categoryid" id="categoryid">
                    <option value="" selected="selected">--Choose a Category</option>
                    <option value="23">1 on 0</option>
                    <option value="15">1 on 1</option>
                    <option value="21">2 on 0</option>
                    <option value="17">2 on 1</option>
                    <option value="16">2 on 2</option>
                    <option value="22">3 on 0</option>
                    <option value="18">3 on 1</option>
                    <option value="19">3 on 2</option>
                    <option value="20">3 on 3</option>
                    <option value="25">Agility</option>
                    <option value="11">Backchecking</option>
                    <option value="6">Competitive</option>
                    <option value="26">Defense</option>
                    <option value="14">Dryland Training</option>
                    <option value="27">Forwards</option>
                    <option value="8">Goalie</option>
                    <option value="24">Import</option>
                    <option value="3">Passing</option>
                    <option value="1">Puck Control</option>
                    <option value="4">Shooting</option>
                    <option value="5">Skating</option>
                    <option value="7">Small Game</option>
                    <option value="13">Stations</option>
                    <option value="2">Stickhandling</option>
                    <option value="9">Systems</option>
                    <option value="10">Timing</option>
                    <option value="12">Warmup</option>
                </select>
                <br clear="all"/><br/>

                <label for="custcat">Custom Category: </label>

                <select name="custcat" id="custcat" disabled="disabled">
                    <option value="" selected="selected">--Choose a Custom Category</option>
                </select> <a href="http://hockeyshare.com/drills/compare.php"><span class="premiumonly "
                                                                                    title="Premium users can create their own categories for drills">Premium Feature</span></a>
                <br clear="all"/><br/>

                <label for="custcat2">Custom Category 2: </label>

                <select name="custcat2" id="custcat2" disabled="disabled">
                    <option value="" selected="selected">--Choose a Custom Category</option>
                </select> <a href="http://hockeyshare.com/drills/compare.php"><span class="premiumonly "
                                                                                    title="Premium users can create their own categories for drills">Premium Feature</span></a>
                <br clear="all"/><br/>

                <label for="youtube">YouTube Video Link: </label>
                <input type="text" name="youtube" id="youtube" value="" class="drillfields"/> <span class="subtle_nu">Enter Full YouTube URL</span>
                <br clear="all"/><br/>
            </fieldset>

            <fieldset class="drillfieldset">
                <legend>Drill Diagram Options</legend>

                <label for="diagramtype">Options: </label>
                <input type="radio" name="diagramtype" value="D" checked="checked"/> Draw <input type="radio"
                                                                                                 name="diagramtype"
                                                                                                 value="U"/> Upload
                <br clear="all"/><br/>
                <label for="diagramcount"># of Diagrams: </label>
                <select name="diagramcount" id="diagramcount" disabled="disabled">
                    <option value="1" selected="selected">1</option>
                </select>
                <a href="http://hockeyshare.com/drills/compare.php"><span class="premiumonly "
                                                                          title="Premium users can associate up to 5 diagrams to any drill">Premium Feature</span></a>
            </fieldset>

            <fieldset class="drillfieldset">
                <legend class="basic_drills_new">Sharing Options</legend>
                <label for="public">Public: </label>
                <input type="radio" name="public" value="1" checked="checked"/> Yes <input type="radio" name="public"
                                                                                           value="0"
                                                                                           disabled="disabled"/> No <a
                        href="http://hockeyshare.com/drills/compare.php"><span class="premiumonly "
                                                                               title="Premium users can 'hide' their drills from the public listing"> Premium Feature</span></a>

                <br clear="all"/>
                <hr/>
                <i>User is not linked to any associations.</i>
            </fieldset>
            <input type="submit" name="Submit" value="Continue &raquo;" class="rpbutton"/>
        </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        @include('includes.commercial')
    </div>
</div>