@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>HockeyShare Account Information</h1>
        <form method="post" action="" name="account_form">
            <div class="account">
                <fieldset class="hs_fieldset">
                    <legend class="hs_legend_two">My Info</legend>
                    <div><label for="first" class="hs_label">First Name: </label> <input type="text" name="first"
                                                                                         id="first"
                                                                                         value="{{--{{$name}}--}}"/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="last" class="hs_label">Last Name: </label> <input type="text" name="last" id="last"
                                                                                       value="{{--{{}}--}}"/></div>
                    <div class="clear_all_10"></div>
                    <div><label for="email" class="hs_label">Email: </label> {{--{{$email}}--}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                href="change_email.php">Change Email</a></div>
                    <div class="clear_all_10"></div>

                    <div><label for="gender" class="hs_label">Gender: </label> <input type="radio" value="M" id="gender"
                                                                                      name="gender"/> Male <input
                                type="radio" value="F" name="gender"/> Female
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="dob_y" class="hs_label">Birthdate: </label> <select name="dob_y" id="dob_y">
                            <option value="">-</option>
                            <option value="2013">2013</option>
                            <option value="2012">2012</option>
                            <option value="2011">2011</option>
                            <option value="2010">2010</option>
                            <option value="2009">2009</option>
                            <option value="2008">2008</option>
                            <option value="2007">2007</option>
                            <option value="2006">2006</option>
                            <option value="2005">2005</option>
                            <option value="2004">2004</option>
                            <option value="2003">2003</option>
                            <option value="2002">2002</option>
                            <option value="2001">2001</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                            <option value="1997">1997</option>
                            <option value="1996">1996</option>
                            <option value="1995">1995</option>
                            <option value="1994">1994</option>
                            <option value="1993">1993</option>
                            <option value="1992">1992</option>
                            <option value="1991">1991</option>
                            <option value="1990">1990</option>
                            <option value="1989">1989</option>
                            <option value="1988">1988</option>
                            <option value="1987">1987</option>
                            <option value="1986">1986</option>
                            <option value="1985">1985</option>
                            <option value="1984">1984</option>
                            <option value="1983">1983</option>
                            <option value="1982">1982</option>
                            <option value="1981">1981</option>
                            <option value="1980">1980</option>
                            <option value="1979">1979</option>
                            <option value="1978">1978</option>
                            <option value="1977">1977</option>
                            <option value="1976">1976</option>
                            <option value="1975">1975</option>
                            <option value="1974">1974</option>
                            <option value="1973">1973</option>
                            <option value="1972">1972</option>
                            <option value="1971">1971</option>
                            <option value="1970">1970</option>
                            <option value="1969">1969</option>
                            <option value="1968">1968</option>
                            <option value="1967">1967</option>
                            <option value="1966">1966</option>
                            <option value="1965">1965</option>
                            <option value="1964">1964</option>
                            <option value="1963">1963</option>
                            <option value="1962">1962</option>
                            <option value="1961">1961</option>
                            <option value="1960">1960</option>
                            <option value="1959">1959</option>
                            <option value="1958">1958</option>
                            <option value="1957">1957</option>
                            <option value="1956">1956</option>
                            <option value="1955">1955</option>
                            <option value="1954">1954</option>
                            <option value="1953">1953</option>
                            <option value="1952">1952</option>
                            <option value="1951">1951</option>
                            <option value="1950">1950</option>
                            <option value="1949">1949</option>
                            <option value="1948">1948</option>
                            <option value="1947">1947</option>
                            <option value="1946">1946</option>
                            <option value="1945">1945</option>
                            <option value="1944">1944</option>
                            <option value="1943">1943</option>
                            <option value="1942">1942</option>
                            <option value="1941">1941</option>
                            <option value="1940">1940</option>
                            <option value="1939">1939</option>
                            <option value="1938">1938</option>
                            <option value="1937">1937</option>
                            <option value="1936">1936</option>
                            <option value="1935">1935</option>
                            <option value="1934">1934</option>
                            <option value="1933">1933</option>
                            <option value="1932">1932</option>
                            <option value="1931">1931</option>
                            <option value="1930">1930</option>
                            <option value="1929">1929</option>
                            <option value="1928">1928</option>
                            <option value="1927">1927</option>
                            <option value="1926">1926</option>
                            <option value="1925">1925</option>
                            <option value="1924">1924</option>
                            <option value="1923">1923</option>
                            <option value="1922">1922</option>
                            <option value="1921">1921</option>
                            <option value="1920">1920</option>
                            <option value="1919">1919</option>
                            <option value="1918">1918</option>
                            <option value="1917">1917</option>
                            <option value="1916">1916</option>
                            <option value="1915">1915</option>
                            <option value="1914">1914</option>
                        </select>-<select name="dob_m">
                            <option value="0" selected="selected">-</option>
                            <option value="1">Jan</option>
                            <option value="2">Feb</option>
                            <option value="3">Mar</option>
                            <option value="4">Apr</option>
                            <option value="5">May</option>
                            <option value="6">Jun</option>
                            <option value="7">Jul</option>
                            <option value="8">Aug</option>
                            <option value="9">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                        </select>-<select name="dob_d">
                            <option value="0" selected="selected">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </div>
                    <div class="clear_all_10"></div>

                    <div><label for="birthcity" class="hs_label">Birth City: </label> <input type="text"
                                                                                             name="birthcity"
                                                                                             id="birthcity" value=""/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="birthcountry" class="hs_label">Birth Country: </label> <select name="birthcountry"
                                                                                                    id="birthcountry">
                            <option value="" selected="selected" disabled="disabled">-- Country --</option>
                            <option value="CA">Canada</option>
                            <option value="US">United States</option>
                            <option value="" disabled="disabled">--------------</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AX">Aland Islands</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua And Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia And Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei Darussalam</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (keeling) Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CD">Congo, The Democratic Republic Of The</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">C�te D'ivoire</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands (malvinas)</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GG">Guernsey</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard Island And Mcdonald Islands</option>
                            <option value="VA">Vatican City State</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran, Islamic Republic Of</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IM">Isle Of Man</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JE">Jersey</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KP">Korea, Democratic People's Republic Of</option>
                            <option value="KR">Korea, Republic Of</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao People's Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libyan Arab Jamahiriya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macao</option>
                            <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States Of</option>
                            <option value="MD">Moldova, Republic Of</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="ME">Montenegro</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PS">Palestinian Territory, Occupied</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">R�union</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts And Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="PM">Saint Pierre And Miquelon</option>
                            <option value="VC">Saint Vincent And The Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome And Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="RS">Serbia</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard And Jan Mayen</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syrian Arab Republic</option>
                            <option value="TW">Taiwan, Province Of China</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania, United Republic Of</option>
                            <option value="TH">Thailand</option>
                            <option value="TL">Timor-leste</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad And Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks And Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="UM">United States Min. Outlying Isl.</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Viet Nam</option>
                            <option value="VG">Virgin Islands, British</option>
                            <option value="VI">Virgin Islands, U.s.</option>
                            <option value="WF">Wallis And Futuna</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label class="hs_label">User ID: </label> <span id="uid">{{--{{$id}}--}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                                class="subtle_nu">Registered on 2017-05-11</span></div>
                    <div class="clear_all_10"></div>
                    <div><label class="hs_label">Support Key: </label> <span id="support_key">{{--{{$key}}--}}</span>
                    </div>
                    <div class="clear_all_10"></div>
                </fieldset>
                <fieldset class="hs_fieldset">
                    <legend class="hs_legend_one">Your Connection to Hockey</legend>

                    <div><label for="player" class="hs_label">Player: </label> <input type="checkbox" name="player"
                                                                                      id="player" value="1"
                                                                                      onchange="player_info_show('player_info_box')"/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="coach" class="hs_label">Coach: </label> <input type="checkbox" name="coach"
                                                                                    id="coach"
                                                                                    value="1"
                                                                                    onchange="coach_info_show('coach_info_box')"/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="scout" class="hs_label">Scout: </label> <input type="checkbox" name="scout"
                                                                                    id="scout"
                                                                                    value="1"
                                                                                    onchange="scout_info_show('scout_info_box')"/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="ref" class="hs_label">Ref: </label> <input type="checkbox" name="ref" id="ref"
                                                                                value="1"/></div>
                    <div class="clear_all_10"></div>
                    <div><label for="fan" class="hs_label">Fan: </label> <input type="checkbox" name="fan" id="fan"
                                                                                value="1"
                                                                                onchange="fan_info_show('fan_info_box')"/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="parent" class="hs_label">Hockey Parent: </label> <input type="checkbox"
                                                                                             name="parent"
                                                                                             id="parent" value="1"/>
                    </div>

                </fieldset>

                <fieldset class="hs_fieldset" id="player_info_box" class="tournament_listing_1">
                    <legend class="hs_legend_three">Player Information</legend>

                    <div><label for="player_position" class="hs_label">Position: </label> <input type="radio"
                                                                                                 id="player_position"
                                                                                                 name="player_position"
                                                                                                 value="S"/> Player
                        <input
                                type="radio" name="player_position" value="G"/> Goalie
                    </div>
                    <div class="clear_all_10"></div>

                    <div><label for="player_shoot" class="hs_label">Shoot: </label> <input type="radio"
                                                                                           id="player_shoot"
                                                                                           name="player_shoot"
                                                                                           value="L"/>
                        Left <input type="radio" name="player_shoot" value="R"/> Right
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="player_group" class="hs_label">Age Group: </label> <select name="player_group"
                                                                                                id="player_group">

                            <option value="" selected="selected">-</option>
                            <option value="SL">Sledge</option>
                            <option value="IN">Initiation/Mini-Mite (6U)</option>
                            <option value="N">Novice/Mite (8U)</option>
                            <option value="A">Atom/Squirt (10U)</option>
                            <option value="P">Peewee</option>
                            <option value="B">Bantam</option>
                            <option value="MMIN">Midget Minor</option>
                            <option value="MMAJ">Midget Major</option>
                            <option value="JR">Junior</option>
                            <option value="MAJJR">Major Junior</option>
                            <option value="COLLEGE">College</option>
                            <option value="ADULT">Adult</option>
                            <option value="SEMI">Semi-Pro</option>
                            <option value="PRO">Professional</option>
                            <optgroup label="Girls Hockey">
                                <option value="GU10">U10 - Girls</option>
                                <option value="GU12">U12 - Girls</option>
                                <option value="GU14">U14 - Girls</option>
                                <option value="GU16">U16 - Girls</option>
                                <option value="GU19">U19 - Girls</option>
                                <option value="GHS">High School - Girls</option>
                                <option value="GCOLLEGE">College - Womens</option>
                            </optgroup>

                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label for="player_level" class="hs_label">Level: </label> <select name="player_level"
                                                                                            id="player_level">

                            <option value="" selected="selected">-</option>
                            <option value="HOUSE">Rec/In-House</option>
                            <option value="LOCAL">Limited Travel</option>
                            <option value="C">C</option>
                            <option value="B">B</option>
                            <option value="A">A</option>
                            <option value="AA">AA</option>
                            <option value="AAA">AAA</option>
                            <option value="D3">D3 (Collegiate)</option>
                            <option value="D2">D2 (Collegiate)</option>
                            <option value="D1">D1 (Collegiate)</option>

                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label for="player_assn" class="hs_label">Association: </label> <input type="text"
                                                                                                class="hs_text required "
                                                                                                name="player_assn"
                                                                                                id="player_assn"
                                                                                                value=""/>
                    </div>
                    <div class="clear_all_10"></div>

                    <div><label for="player_usah" class="hs_label">USA Hockey ID #: </label> <input type="text"
                                                                                                    name="player_usah"
                                                                                                    id="player_usah"
                                                                                                    value=""/></div>
                    <div class="clear_all_10"></div>

                    <div><label for="player_chln" class="hs_label">Canadian Hockey #: </label> <input type="text"
                                                                                                      name="player_chln"
                                                                                                      id="player_chln"
                                                                                                      value=""/></div>
                    <div class="clear_all_10"></div>

                    <div><label for="player_intid" class="hs_label">Local/Int'l Player ID #: </label> <input type="text"
                                                                                                             name="player_intid"
                                                                                                             id="player_intid"
                                                                                                             value=""/>
                    </div>
                    <div class="clear_all_10"></div>
                </fieldset>
                <fieldset class="hs_fieldset" id="coach_info_box" class="tournament_listing_1">
                    <legend class="hs_legend_three">Coaching Information</legend>

                    <div><label for="coach_group" class="hs_label">Age Group: </label> <select name="coach_group"
                                                                                               id="coach_group">

                            <option value="" selected="selected">-</option>
                            <option value="SL">Sledge</option>
                            <option value="IN">Initiation/Mini-Mite (6U)</option>
                            <option value="N">Novice/Mite (8U)</option>
                            <option value="A">Atom/Squirt (10U)</option>
                            <option value="P">Peewee</option>
                            <option value="B">Bantam</option>
                            <option value="MMIN">Midget Minor</option>
                            <option value="MMAJ">Midget Major</option>
                            <option value="HS">High School</option>
                            <option value="JR">Junior</option>
                            <option value="MAJJR">Major Junior</option>
                            <option value="COLLEGE">College</option>
                            <option value="ADULT">Adult</option>
                            <option value="SEMI">Semi-Pro</option>
                            <option value="PRO">Professional</option>
                            <optgroup label="Girls Hockey">
                                <option value="GU10">U10 - Girls</option>
                                <option value="GU12">U12 - Girls</option>
                                <option value="GU14">U14 - Girls</option>
                                <option value="GU16">U16 - Girls</option>
                                <option value="GU19">U19 - Girls</option>
                                <option value="GHS">High School - Girls</option>
                                <option value="GCOLLEGE">College - Womens</option>

                            </optgroup>
                        </select></div>
                    <div class="clear_all_10"></div>

                    <div><label for="coach_level" class="hs_label">Level: </label> <select name="coach_level"
                                                                                           id="coach_level">

                            <option value="" selected="selected">-</option>
                            <option value="HOUSE">Rec/In-House</option>
                            <option value="LOCAL">Limited Travel</option>
                            <option value="C">C</option>
                            <option value="B">B</option>
                            <option value="A">A</option>
                            <option value="AA">AA</option>
                            <option value="AAA">AAA</option>
                            <option value="D3">D3 (Collegiate)</option>
                            <option value="D2">D2 (Collegiate)</option>
                            <option value="D1">D1 (Collegiate)</option>

                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label for="coach_assn" class="hs_label">Association: </label> <input type="text"
                                                                                               class="hs_text required "
                                                                                               name="coach_assn"
                                                                                               id="coach_assn"
                                                                                               value=""/>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="coach_cert" class="hs_label">Certification Level: </label><select name="coach_cert"
                                                                                                       id="coach_cert">

                            <option value="" selected="selected">-</option>
                            <option value="CS">Hockey Canada - Coach Stream</option>
                            <option value="DS">Hockey Canada - Developmental Stream</option>
                            <option value="HP">Hockey Canada - High Performance Stream</option>

                            <option value="1">USA Hockey - Level 1</option>
                            <option value="2">USA Hockey - Level 2</option>
                            <option value="3">USA Hockey - Level 3</option>
                            <option value="4">USA Hockey - Level 4</option>
                            <option value="5">USA Hockey - Level 5</option>
                        </select> <span class="subtle_nu">(for Hockey Canada and USA Hockey Coaches Only)</span></div>
                    <div class="clear_all_10"></div>
                </fieldset>

                <fieldset class="hs_fieldset" id="scout_info_box" class="tournament_listing_1">
                    <legend class="hs_legend_three">Scout Information</legend>

                    <div><label for="scout_group" class="hs_label">Age Group: </label> <select name="scout_group"
                                                                                               id="scout_group">

                            <option value="" selected="selected">-</option>
                            <option value="B">Bantam</option>
                            <option value="MMIN">Midget Minor</option>
                            <option value="MMAJ">Midget Major</option>
                            <option value="JR">Junior</option>
                            <option value="MAJJR">Major Junior</option>
                            <option value="COLLEGE">College</option>
                            <option value="ADULT">Adult</option>
                            <option value="SEMI">Semi-Pro</option>
                            <option value="PRO">Professional</option>

                        </select></div>
                    <div class="clear_all_10"></div>

                    <div><label for="scout_level" class="hs_label">Level: </label> <select name="scout_level"
                                                                                           id="scout_level">

                            <option value="" selected="selected">-</option>
                            <option value="C">C</option>
                            <option value="B">B</option>
                            <option value="A">A</option>
                            <option value="AA">AA</option>
                            <option value="AAA">AAA</option>
                            <option value="D3">D3 (Collegiate)</option>
                            <option value="D2">D2 (Collegiate)</option>
                            <option value="D1">D1 (Collegiate)</option>

                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label for="scout_assn" class="hs_label">Association: </label> <input type="text"
                                                                                               class="hs_text required "
                                                                                               name="scout_assn"
                                                                                               id="scout_assn"
                                                                                               value=""/>
                    </div>
                    <div class="clear_all_10"></div>


                </fieldset>

                <fieldset class="hs_fieldset" id="fan_info_box" class="tournament_listing_1">
                    <legend class="hs_legend_three">Fan Information</legend>

                    <div><label for="fan_nhlteam" class="hs_label">Favorite NHL Team</label> <select name="fan_nhlteam"
                                                                                                     id="fan_nhlteam">
                            <option value="" selected="selected">----- CHOOSE A TEAM -----</option>
                            <option value="Anaheim Ducks">Anaheim Ducks</option>
                            <option value="Boston Bruins">Boston Bruins</option>
                            <option value="Buffalo Sabres">Buffalo Sabres</option>
                            <option value="Calgary Flames">Calgary Flames</option>
                            <option value="Carolina Hurricanes">Carolina Hurricanes</option>
                            <option value="Chicago Blackhawks">Chicago Blackhawks</option>
                            <option value="Colorado Avalanche">Colorado Avalanche</option>
                            <option value="Columbus Blue Jackets">Columbus Blue Jackets</option>
                            <option value="Dallas Stars">Dallas Stars</option>
                            <option value="Detroit Red Wings">Detroit Red Wings</option>
                            <option value="Edmonton Oilers">Edmonton Oilers</option>
                            <option value="Florida Panthers">Florida Panthers</option>
                            <option value="Los Angeles Kings">Los Angeles Kings</option>
                            <option value="Minnesota Wild">Minnesota Wild</option>
                            <option value="Montreal Canadiens">Montreal Canadiens</option>
                            <option value="Nashville Predators">Nashville Predators</option>
                            <option value="New Jersey Devils">New Jersey Devils</option>
                            <option value="New York Islanders">New York Islanders</option>
                            <option value="New York Rangers">New York Rangers</option>
                            <option value="Ottawa Senators">Ottawa Senators</option>
                            <option value="Philadelphia Flyers">Philadelphia Flyers</option>
                            <option value="Phoenix Coyotes">Phoenix Coyotes</option>
                            <option value="Pittsburgh Penguins">Pittsburgh Penguins</option>
                            <option value="San Jose Sharks">San Jose Sharks</option>
                            <option value="St. Louis Blues">St. Louis Blues</option>
                            <option value="Tampa Bay Lightning">Tampa Bay Lightning</option>
                            <option value="Toronto Maple Leafs">Toronto Maple Leafs</option>
                            <option value="Vancouver Canucks">Vancouver Canucks</option>
                            <option value="Washington Capitals">Washington Capitals</option>
                            <option value="Winnipeg Jets">Winnipeg Jets</option>
                        </select></div>
                    <div class="clear_all_10"></div>

                </fieldset>

                <fieldset class="hs_fieldset">
                    <legend class="hs_legend_three">Location</legend>

                    <div><label for="country" class="hs_label">Country: </label> <select name="country" id="country">
                            <option value="" selected="selected" disabled="disabled">-- Country --</option>
                            <option value="CA">Canada</option>
                            <option value="US">United States</option>
                            <option value="" disabled="disabled">--------------</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AX">Aland Islands</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua And Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia And Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei Darussalam</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (keeling) Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CD">Congo, The Democratic Republic Of The</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">C�te D'ivoire</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands (malvinas)</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GG">Guernsey</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard Island And Mcdonald Islands</option>
                            <option value="VA">Vatican City State</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran, Islamic Republic Of</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IM">Isle Of Man</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JE">Jersey</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KP">Korea, Democratic People's Republic Of</option>
                            <option value="KR">Korea, Republic Of</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao People's Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libyan Arab Jamahiriya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macao</option>
                            <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States Of</option>
                            <option value="MD">Moldova, Republic Of</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="ME">Montenegro</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PS">Palestinian Territory, Occupied</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">R�union</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts And Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="PM">Saint Pierre And Miquelon</option>
                            <option value="VC">Saint Vincent And The Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome And Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="RS">Serbia</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard And Jan Mayen</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syrian Arab Republic</option>
                            <option value="TW">Taiwan, Province Of China</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania, United Republic Of</option>
                            <option value="TH">Thailand</option>
                            <option value="TL">Timor-leste</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad And Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks And Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="UM">United States Min. Outlying Isl.</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Viet Nam</option>
                            <option value="VG">Virgin Islands, British</option>
                            <option value="VI">Virgin Islands, U.s.</option>
                            <option value="WF">Wallis And Futuna</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select></div>
                    <div class="clear_all_10"></div>
                    <div><label for="state" class="hs_label">State/Prov: </label>
                        <select name="state" id="state">

                            <option value="" selected="selected" disabled="disabled">-- State/Prov --</option>
                            <option value="NA"> -- N/A --</option>
                            <option value="AB">Alberta</option>
                            <option value="BC">British Columbia</option>
                            <option value="MB">Manitoba</option>
                            <option value="NB">New Brunswick</option>
                            <option value="NL">Newfoundland and Labrador</option>
                            <option value="NT">Northwest Territories</option>
                            <option value="NU">Nunavut</option>
                            <option value="NS">Nova Scotia</option>
                            <option value="ON">Ontario</option>
                            <option value="PE">Prince Edward Island</option>
                            <option value="QC">Quebec</option>
                            <option value="SK">Saskatchewan</option>
                            <option value="YT">Yukon</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>

                        <span class="subtle_nu">(Canada &amp; US Only)</span></div>
                    <div class="clear_all_10"></div>
                    <div><label for="city" class="hs_label">City: </label> <input type="text" class="hs_text required "
                                                                                  name="city" id="city" value=""/></div>

                </fieldset>

                <fieldset class="hs_fieldset">
                    <legend class="hs_legend">Social</legend>
                    <div><label for="twitter" class="hs_label">Twitter Name: </label> <input type="text"
                                                                                             class="hs_text  "
                                                                                             name="twitter" id="twitter"
                                                                                             value=""> <span
                                class="subtle_nu">Do not enter the @ symbol when entering your Twitter username</span>
                    </div>
                    <div class="clear_all_10"></div>
                    <div><label for="facebook" class="hs_label">Facebook Profile: </label> <input type="text"
                                                                                                  class="hs_text  "
                                                                                                  name="facebook"
                                                                                                  id="facebook"
                                                                                                  value=""> <span
                                class="subtle_nu ">URL to your Facebook Profile - must start with <strong>http://www.facebook.com/</strong></span>
                    </div>
                    <div class="clear_all_10"></div>
                </fieldset>

                <fieldset class="hs_fieldset">
                    <legend class="hs_legend_account">Team Drill/Practice Plan Sharing</legend>
                    <div><label for="teamsharepw" class="hs_label">Team Sharing Password: </label> <input type="text"
                                                                                                          name="teamsharepw"
                                                                                                          id="teamsharepw"
                                                                                                          value="">
                        <span class="subtle_nu">(Must be at least 6 characters)</span></div>
                    <div><br><span class="menu_a">This password should <strong>NOT</strong> be the same as your account password.</span>
                        This password should only be shared with your team and/or coaching staff. You can designate team
                        shared drills and practice plans by checking the "Team Share" option when adding or editing
                        drills or practice plans. Users must specify this password in order to view your team shared
                        content. NOTE: Users will not have to have their own HockeyShare.com account to view your team
                        shared content.
                    </div>
                    <div class="clear_all_10"></div>
                </fieldset>
                <fieldset class="hs_fieldset">
                    <div><label for="Submit" class="hs_label">&nbsp;</label> <input type="submit" name="Submit"
                                                                                    id="Submit"
                                                                                    value="Save Changes"/>
                        <div style="float:right;" class="subtle_nu">Last Updated: Never</div>
                    </div>
                    <div class="clear_all_10"></div>
                </fieldset>

            </div>
        </form>

        <fieldset class="hs_fieldset">
            <legend class="hs_legend_three">Password</legend>
            <a href="http://hockeyshare.com/account/update_password.php">Update Password</a>
        </fieldset>
        @include('includes.commercial')
        <br>
    </div>
</div>

