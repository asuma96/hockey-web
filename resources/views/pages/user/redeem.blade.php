@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <h1>Redeem Gift Certificates</h1>
        <p>Have a HockeyShare gift code or certificate? Redeem it using the form below.</p>

        <table class="redeem_table">
            <tr>
                @if(Auth::check())
                    <td width="50%" valign="top">
                        <h3 class="redeem">Redeem Code</h3>
                        <form method="post" action="">
                            Code: <input type="text" name="giftcode" value=""> <input type="submit" name="Redeem" value="Redeem">
                            <p class="subtle_nu">Enter the code exactly as it appears in your confirmation email. Please note that redemption codes are case-sensitive.</p>
                        </form>
                    </td>
                    @else
                    <td width="50%" valign="top">
                        <h3 class="redeem">Redeem Code</h3>
                        <p class="video-description">You must be <a class="underline"
                                                                    href="http://hockeyshare.com/login/">logged in</a>
                            to redeem a gift code. If you do not have an account, you can <a class="underline"
                                                                                             href="http://hockeyshare.com/register/">register</a>
                            for <strong>FREE</strong> here.</p>
                    </td>
                    @endif

                <td valign="top">
                    <h3 class="redeem">Purchase Gift Certificates</h3>
                    <ul>
                        <li class="video-description"><a class="underline"
                                                         href="http://hockeyshare.com/drills/signup_gift.php">Advanced
                                Coaching Platform (1 Yr. Subscription) - $75.00</a></li>
                    </ul>
                </td>
            </tr>
        </table>
        @include('includes.commercial')
    </div>
</div>