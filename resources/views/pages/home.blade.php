@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    @section('content')
    @endsection
    @section('images')
        <div class="top_head_extend">
            <div class="image-style"><img class="image-size"
                src="//d3e9hat72fmrwm.cloudfront.net/images/index/acp_head_3.jpg"
                                          alt="HockeyShare's Advanced Coaching Platform" usemap="ACPMap"
                /><br/><a href="/acp/learn-more/">
                    <img
                            src="//d3e9hat72fmrwm.cloudfront.net/images/index/acp_learn.jpg" height="50"
                            width="1000"
                            alt="Learn More"/></a></div>
        </div>
        <map name="ACPMap" id="ACPMap">
            <area shape="rect" coords="339,291,811,474" href="/acp/video/animation/" data-featherlight="iframe"
                  data-featherlight-iframe-height="600" data-featherlight-iframe-width="875"
                  alt="Animation Video"/>
            <area shape="rect" coords="345,11,946,259" href="/acp/video/overview/" data-featherlight="iframe"
                  data-featherlight-iframe-height="600" data-featherlight-iframe-width="875"
                  alt="Overview Video"/>
            @if(Auth::check())
            <area shape="rect" coords="872,378,989,495" href="/freetrial" alt="Free Trial"/>
            @else
                <area shape="rect" coords="872,378,989,495" href="/login" alt="Free Trial"/>
            @endif
            <area shape="rect" coords="19,183,328,332" href="/acp/compare/" alt="Sign Up Now"/>
        </map>
    @endsection

    @section('training')
        <div class="page_content">
            <br/>
            <fieldset class="legend">
                <legend class="fieldset-content">Enhance Your Training</legend>
                <div class="margin_image"><a class="margin" href="/training/d-vol1/"><img
                                src="//d3e9hat72fmrwm.cloudfront.net/images/index/dvol1-sq.jpg"
                                alt="HockeyShare Defensemen Shooting Progression"/></a>
                    <a class="margin" href="/drill-store/"><img
                                src="//d3e9hat72fmrwm.cloudfront.net/images/index/home_hsds_sq.jpg"
                                alt="HockeyShare Drill Store"/></a>
                    <a class="margin" href="/training/different-approach/"><img
                                src="//d3e9hat72fmrwm.cloudfront.net/images/index/home_ada_sq.jpg"
                                alt="A Different Approach"/></a>
                </div>
            </fieldset>
            <br/>
            <div class="signup-form">
                <table class="full_width_tbl">
                    <br>
                    <tr>
                        <td class="ttop">
                            <p class="title-text">HockeyShare Videos</p>
                            <div class="center">
                                <iframe class="iframe" src="https://youtube.com/embed/haJw2S7GqOQ?rel=0"
                                        frameborder="0" allowfullscreen></iframe>
                                <br/>
                                <p class="video-text"> Share Video: <input type="text" class="video-window"
                                                                           value="https://hockeyshare.com/video/?vid=haJw2S7GqOQ"/>
                                    <a href="http://hockeyshare.com/video/" class="pbutton button-type">View More
                                        Videos</a><br/>
                                </p>
                            </div>
                            <br/>

                            <p class="title-text">Subscribe to the HockeyShare.com Newsletter</p>
                            <p class="video-description">Get the latest instructional videos, drills, and news straight
                                to your inbox. Simply
                                enter your
                                address below. We do NOT rent or sell our email list...period.</p>

                            <div id="newsletter">

                            </div>
                            <form action="newsletter" method="post">
                                <table class="content-video">
                                    <tr>
                                        <td class="video-description">Email:</td>
                                        <td><input name="email" type="email" size="35" maxlength="255"/></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="video-description"><input checked="checked" name="newsletter"
                                                                             type="checkbox" value="1"/>
                                            HockeyShare Newsletter <br/>
                                            <input checked="checked" name="featured" type="checkbox" value="1"/>
                                            Featured Drills
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input class="input-text-size" name="Submit" type="submit"
                                                   value="  subscribe  "/></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            @include('includes.commercial')
        </div>
        <br/>
    @endsection
</div>


