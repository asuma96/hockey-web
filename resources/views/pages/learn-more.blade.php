@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <p class="center"><img src="/img/acp-header.jpg"
                               alt="Advanced Coaching Platform" width="970" height="87" usemap="#Map"/>
            <map name="Map" id="Map">
                <area shape="rect" coords="708,27,845,56" href="/freetrial/" alt="Free Trial"/>
                <area shape="rect" coords="868,27,960,56" href="/acp/compare/" alt="Buy Now"/>
                <area shape="rect" coords="541,25,674,58" href="/acp/compare/" alt="Pricing"/>
            </map>
        </p>
        <p class="center"><img src="/img/draw-practice-header.jpg"
                               alt="HockeyShare Drill Diagrammer &amp; Practice Planner" class="img_learn"
                               usemap="#Map3"/>
            <map name="Map3" id="Map3">
                <area shape="rect" coords="587,146,876,179"
                      href="http://hockeyshare.com/images/hs_diagrammer_example.pdf" target="_blank"
                      alt="Download Sample Practice Plan"/>
                <area shape="rect" coords="127,104,342,139" href="#tryit" alt="Try It Out"/>
            </map>
        </p>

        <br/>
        <div class="watch">
            <a href="/acp/video/overview/" data-featherlight="iframe" data-featherlight-iframe-height="600"
               data-featherlight-iframe-width="875"><img
                        src="/img/acp_watch_overview.jpg" height="75"
                        width="900" alt="Watch Overview Video"/></a>
        </div>

        <br/>

        <p class="center"><img src="/img/crumple-example.jpg"
                               height="337" width="970" alt="Create Professional Hockey Diagrams"/></p>
        <p>


        <div class="watch">
            <img src="/img/try-it.jpg" height="109" width="540"
                 alt="Try HockeyShare's Drill Diagrammer"/><a id="tryit">&nbsp;</a>
            <div
                    class="mainbody-container flash_fontainer_drill"
                    id="flash_fontainer"
            >
                <iframe id="html5-diagrammer" width="100%" height="533px" frameborder="0">
                    <html lang="en">
                    <body ng-app="HockeyDiagrammer" ng-controller="AppCtrl" class="ng-scope">

                    <div id="" class="hd-body">
                        <div class="hd-load-progress hidden">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" style="width: 100%"></div>
                            </div>
                        </div>
                        <div class="hd-header">
                            <div class="hd-header-title">
                                <input type="text" ng-model="ModeFactory.diagramTitle"
                                       class="ng-pristine ng-untouched ng-valid ng-not-empty">
                                <div class="hd-header-round-progress ng-scope" ng-if="isProgressBarShown()"
                                     ng-class="{'hd-header-round-progress-done': RoundProgressManager.isDone()}">
                                    <div class=" learn_more_div round-progress-wrapper ng-isolate-scope"
                                         tooltip="Video generation (0%)"
                                         tooltip-placement="right" round-progress="" max="100"
                                         current="RoundProgressManager.currentValue()" clockwise="true" radius="20"
                                         stroke="5" bgcolor="rgba(155, 168, 169, .3)">
                                        <svg class="round-progress learn_svg" xmlns="http://www.w3.org/2000/svg"
                                             role="progressbar" aria-valuemin="0" viewBox="0 0 40 40"
                                             aria-valuemax="100" aria-valuenow="0"
                                        >
                                            <circle fill="none" cx="20" cy="20" r="17.5"
                                                    class="learn_more"></circle>
                                            <path fill="none" transform=""
                                                  class="learn_path"
                                                  d="M 20 2.5 A 17.5 17.5 0 0 0 20 2.5"></path>
                                            <g ng-transclude=""></g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="hd-header-right ng-binding">
                                <div class="hd-header-right-modes">
                                    Mode:
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-glass-gray"
                                                ng-class="{'btn-glass-gray': ModeFactory.state != Enums.STATE.DRAWING, 'btn-glass-green': ModeFactory.state == Enums.STATE.DRAWING}"
                                                ng-click="changeState(Enums.STATE.DRAWING)"
                                                ng-disabled="SequenceManager.isDisabled()">
                                            Drawing
                                        </button>
                                        <button class="btn btn-xs btn-glass-green"
                                                ng-class="{'btn-glass-gray': ModeFactory.state != Enums.STATE.ANIMATION, 'btn-glass-green': ModeFactory.state == Enums.STATE.ANIMATION}"
                                                ng-click="changeState(Enums.STATE.ANIMATION)"
                                                ng-disabled="SequenceManager.isDisabled() || ModeFactory.animationAvailable != 1">
                                            Animation
                                        </button>
                                    </div>
                                </div>
                                You are currently in ANIMATION mode

                                <div class="hd-header-top-menu">
                                    <div class="btn-group">
                                        <button ng-disabled="SequenceManager.isDisabled()"
                                                class="btn btn-xs btn-glass-gray" ng-click="exportClickHandler()">
                                            Export PNG
                                        </button>
                                        <button ng-disabled="SequenceManager.isDisabled()"
                                                class="btn btn-xs btn-glass-gray" ng-click="clearDiagrammer()">
                                            Clear
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hd-main ng-scope" ng-controller="DrawCtrl">
                            <div class="hd-main-right">
                                <div class="HD_canvas_wrapper" ng-mousedown="" ng-mouseup="" ng-click="">
                                    <div class="canvas-container learn_div"
                                    >
                                        <canvas id="canvas" class="lower-canvas canvas" width="606" height="352"
                                        ></canvas>
                                        <canvas class="upper-canvas canvas_new" width="606" height="352"
                                        ></canvas>
                                    </div>
                                </div>
                                <div class="hd-action-bar">
                                    <div class="hd-action-bar-color-row gradient-black-gray clearfix ng-hide"
                                         ng-hide="StylesFactory.colorpickerDisabled()">
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Black" ng-click="changeColor(c, true)"
                                                style="background: rgb(0, 0, 0);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Red" ng-click="changeColor(c, true)"
                                                style="background: rgb(218, 54, 55);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Blue" ng-click="changeColor(c, true)"
                                                style="background: rgb(56, 106, 155);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Green" ng-click="changeColor(c, true)"
                                                style="background: rgb(0, 153, 51);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Purple" ng-click="changeColor(c, true)"
                                                style="background: rgb(204, 102, 204);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Orange" ng-click="changeColor(c, true)"
                                                style="background: rgb(255, 153, 0);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Brown" ng-click="changeColor(c, true)"
                                                style="background: rgb(153, 102, 0);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Gray" ng-click="changeColor(c, true)"
                                                style="background: rgb(153, 153, 153);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Gold" ng-click="changeColor(c, true)"
                                                style="background: rgb(241, 217, 0);"></span>
                                        <span
                                                ng-style="{background: c}" class="color-preset ng-scope"
                                                ng-repeat="c in StylesFactory.colors track by $index"
                                                title="Change Color to Teal" ng-click="changeColor(c, true)"
                                                style="background: rgb(60, 193, 162);"></span>

                                        <span class="color-preset color-picker ng-pristine ng-untouched ng-valid ng-not-empty"
                                              colorpicker="" colorpicker-position="top" title="Set a Custom Color"
                                              ng-model="StylesFactory.currentColor"
                                              ng-style="{background: StylesFactory.currentColor}"
                                              style="background: rgb(0, 0, 0);"></span>
                                    </div>

                                    <div class="hd-action-bar-actions-row gradient-black-gray">
                                        <div class="actions-group clearfix">
                                           <span
                                                   ng-repeat="command in CommandsFactory.commandsGroup1"
                                                   class="action-bar-btn ng-scope actionBarIsDisaled non-toggle-btn"
                                                   title="Delete item" ng-click="command.clickHandler()"
                                                   ng-if="command.isShown()"
                                                   ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape273.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape273.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup1"
                                                    class="action-bar-btn ng-scope actionBarIsDisaled non-toggle-btn"
                                                    title="Rotate Item (Counter Clockwise)"
                                                    ng-click="command.clickHandler()" ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape104.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape104.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup1"
                                                    class="action-bar-btn ng-scope actionBarIsDisaled non-toggle-btn"
                                                    title="Rotate Item (Clockwise)" ng-click="command.clickHandler()"
                                                    ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape106.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape106.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup1"
                                                    class="action-bar-btn ng-scope actionBarIsDisaled non-toggle-btn"
                                                    title="Bring To Front" ng-click="command.clickHandler()"
                                                    ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape271.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape271.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup1"
                                                    class="action-bar-btn ng-scope actionBarIsDisaled non-toggle-btn"
                                                    title="Send To Back" ng-click="command.clickHandler()"
                                                    ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape102.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape102.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup1"
                                                    class="action-bar-btn ng-scope actionBarIsDisaled toggle-btn"
                                                    title="Freehand Curve Drawing" ng-click="command.clickHandler()"
                                                    ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/shape136.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/shape136.svg">
                      </span>
                                        </div>

                                        <div class="btn-group">
                                            <button class="btn btn-sm btn-green" ng-click="CommandsManager.undo()"
                                                    ng-disabled="!CommandsManager.canUndo() || SequenceManager.isDisabled()"
                                                    disabled="disabled">
                                                Undo
                                            </button>
                                            <button class="btn btn-sm btn-green" ng-click="CommandsManager.redo()"
                                                    ng-disabled="!CommandsManager.canRedo() || SequenceManager.isDisabled()"
                                                    disabled="disabled">
                                                Redo
                                            </button>
                                        </div>

                                        <div class="actions-group clearfix">
                                            <span
                                                    ng-repeat="command in CommandsFactory.commandsGroup2"
                                                    class="action-bar-btn ng-scope toggle-btn" title="Grab Tool"
                                                    ng-click="command.clickHandler()" ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/grab-icon.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/grab-icon.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup2"
                                                    class="action-bar-btn ng-scope non-toggle-btn" title="Zoom In"
                                                    ng-click="command.clickHandler()" ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/zoom_in.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/zoom_in.svg">
                      </span><span
                                                    ng-repeat="command in CommandsFactory.commandsGroup2"
                                                    class="action-bar-btn ng-scope non-toggle-btn" title="Zoom Out"
                                                    ng-click="command.clickHandler()" ng-if="command.isShown()"
                                                    ng-class="{active: command.isActive(), actionBarIsDisaled: command.isDisabled(), 'toggle-btn': command.isToggleBtn(), 'non-toggle-btn': !command.isToggleBtn()}">
                          <img ng-src="http://hockeyshare.com/drills/html5//img/svg/zoom_out.svg"
                               ng-mousedown="$event.preventDefault()"
                               src="http://hockeyshare.com/drills/html5//img/svg/zoom_out.svg">
                      </span>
                                        </div>

                                        <div class="dark-black-blocker ng-hide"
                                             ng-hide="!SequenceManager.isDisabled()"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hd-main-left">
                                <div class="hd-main-left-content">
                                    <div class="ng-isolate-scope ng-hide"
                                         ng-show="Enums.STATE.DRAWING == ModeFactory.state" justify="true">
                                        <ul class="nav nav-tabs"
                                            ng-class="{'nav-stacked': vertical, 'nav-justified': justified}"
                                            ng-transclude="">
                                            <li ng-class="{active: active, disabled: disabled}" select="true"
                                                class="ng-scope ng-isolate-scope active">
                                                <a href="" ng-click="select()" tab-heading-transclude=""
                                                   class="ng-binding">
                                                    <tab-heading ng-mousedown="$event.preventDefault()"
                                                                 class="ng-scope">Objects
                                                    </tab-heading>
                                                </a>
                                            </li>
                                            <li ng-class="{active: active, disabled: disabled}"
                                                class="ng-scope ng-isolate-scope">
                                                <a href="" ng-click="select()" tab-heading-transclude=""
                                                   class="ng-binding">
                                                    <tab-heading ng-mousedown="$event.preventDefault()"
                                                                 class="ng-scope">Actions
                                                    </tab-heading>
                                                </a>
                                            </li>
                                            <li ng-class="{active: active, disabled: disabled}"
                                                class="ng-scope ng-isolate-scope">
                                                <a href="" ng-click="select()" tab-heading-transclude=""
                                                   class="ng-binding">
                                                    <tab-heading ng-mousedown="$event.preventDefault()"
                                                                 class="ng-scope">Rink
                                                    </tab-heading>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane ng-scope active" ng-repeat="tab in tabs"
                                                 ng-class="{active: tab.active}" tab-content-transclude="tab">

                                                <div ng-repeat="ots in objectTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">General</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                        ng-repeat="option in ots.options"
                                                        title="COACH/INSTRUCTOR"
                                                        swipe-element-start="onPalleteItemDragStart(source)"
                                                        swipe-element="onPalleteItemDrop(source, option)"
                                                        swipe-element-move="onPalleteItemDrag($event)">
                                                        <img draggable="false"
                                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/coach.svg"
                                                             alt="COACH/INSTRUCTOR"
                                                             src="http://hockeyshare.com/drills/html5//img/svg/coach.svg">
                                                        </span> <span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="GOALTENDER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/g.svg"
                                             alt="GOALTENDER"
                                             src="http://hockeyshare.com/drills/html5//img/svg/g.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PLAYER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/x.svg"
                                             alt="PLAYER" src="http://hockeyshare.com/drills/html5//img/svg/x.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PLAYER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/o.svg"
                                             alt="PLAYER" src="http://hockeyshare.com/drills/html5//img/svg/o.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="CUSTOM OBJECT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape127.svg"
                                             alt="CUSTOM OBJECT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape127.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="TRIANGLE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/triangle.svg"
                                             alt="TRIANGLE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/triangle.svg">
                                    </span>
                                                    </div>
                                                </div>
                                                <div ng-repeat="ots in objectTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Offense</div>
                                                    <div class="hd-pallete-container clearfix"><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="FORWARD"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/f.svg"
                                             alt="FORWARD"
                                             src="http://hockeyshare.com/drills/html5//img/svg/f.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="LEFT WINGER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/lw.svg"
                                             alt="LEFT WINGER"
                                             src="http://hockeyshare.com/drills/html5//img/svg/lw.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="RIGHT WINGER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/rw.svg"
                                             alt="RIGHT WINGER"
                                             src="http://hockeyshare.com/drills/html5//img/svg/rw.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="CENTER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/c.svg"
                                             alt="CENTER" src="http://hockeyshare.com/drills/html5//img/svg/c.svg">
                                    </span>
                                                    </div>
                                                </div>
                                                <div ng-repeat="ots in objectTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Defense</div>
                                                    <div class="hd-pallete-container clearfix">
                                                       <span
                                                               class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                               ng-repeat="option in ots.options" title="DEFENCE"
                                                               swipe-element-start="onPalleteItemDragStart(source)"
                                                               swipe-element="onPalleteItemDrop(source, option)"
                                                               swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/d.svg"
                                             alt="DEFENCE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/d.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="LEFT DEFENCEMAN"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/ld.svg"
                                             alt="LEFT DEFENCEMAN"
                                             src="http://hockeyshare.com/drills/html5//img/svg/ld.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="RIGHT DEFENCEMAN"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/rd.svg"
                                             alt="RIGHT DEFENCEMAN"
                                             src="http://hockeyshare.com/drills/html5//img/svg/rd.svg">
                                    </span>
                                                    </div>
                                                </div>
                                                <div ng-repeat="ots in objectTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Objects</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        <span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PUCK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/puck.svg"
                                             alt="PUCK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/puck.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PUCKS"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/pucks.svg"
                                             alt="PUCKS"
                                             src="http://hockeyshare.com/drills/html5//img/svg/pucks.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PYLON"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/pylon.svg"
                                             alt="PYLON"
                                             src="http://hockeyshare.com/drills/html5//img/svg/pylon.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="NUMBERED PYLON"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape44.svg"
                                             alt="NUMBERED PYLON"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape44.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="GOAL"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/goal.svg"
                                             alt="GOAL"
                                             src="http://hockeyshare.com/drills/html5//img/svg/goal.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="MINI GOAL"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/goal_mini.svg"
                                             alt="MINI GOAL"
                                             src="http://hockeyshare.com/drills/html5//img/svg/goal_mini.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="TEXT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape228.svg"
                                             alt="TEXT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape228.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="HOCKEY STICK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/hockey_stick.svg"
                                             alt="HOCKEY STICK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/hockey_stick.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="TIRES"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/tires.svg"
                                             alt="TIRES"
                                             src="http://hockeyshare.com/drills/html5//img/svg/tires.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="RINK DIVIDERS"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/rink_div_s.svg"
                                             alt="RINK DIVIDERS"
                                             src="http://hockeyshare.com/drills/html5//img/svg/rink_div_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="RECTANGLE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/rectangle.svg"
                                             alt="RECTANGLE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/rectangle.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="CIRCLE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/circle.svg"
                                             alt="CIRCLE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/circle.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="POLYGON"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/polygon.svg"
                                             alt="POLYGON"
                                             src="http://hockeyshare.com/drills/html5//img/svg/polygon.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="OVAL"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/oval.svg"
                                             alt="OVAL"
                                             src="http://hockeyshare.com/drills/html5//img/svg/oval.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="LADDER"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/ladder.svg"
                                             alt="LADDER"
                                             src="http://hockeyshare.com/drills/html5//img/svg/ladder.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="ATTACK TRIANGLE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/attack_triangle.svg"
                                             alt="ATTACK TRIANGLE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/attack_triangle.svg">
                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane ng-scope" ng-repeat="tab in tabs"
                                                 ng-class="{active: tab.active}" tab-content-transclude="tab">

                                                <div ng-repeat="ots in actionTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Skating</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        <span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="FORWARD SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape61.svg"
                                             alt="FORWARD SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape61.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="FORWARD SKATING W/ PUCK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape35.svg"
                                             alt="FORWARD SKATING W/ PUCK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape35.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="BACKWARD SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape73.svg"
                                             alt="BACKWARD SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape73.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="BACKWARD SKATING W/ PUCK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape70.svg"
                                             alt="BACKWARD SKATING W/ PUCK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape70.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE FORWARD SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape61_s.svg"
                                             alt="STRAIGHT-LINE FORWARD SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape61_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE FORWARD SKATING W/ PUCK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape35_s.svg"
                                             alt="STRAIGHT-LINE FORWARD SKATING W/ PUCK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape35_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE BACKWARD SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape73_s.svg"
                                             alt="STRAIGHT-LINE BACKWARD SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape73_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE BACKWARD SKATING W/ PUCK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape70_s.svg"
                                             alt="STRAIGHT-LINE BACKWARD SKATING W/ PUCK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape70_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE LATERAL SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape53_s.svg"
                                             alt="STRAIGHT-LINE LATERAL SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape53_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="LATERAL SKATING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape53.svg"
                                             alt="LATERAL SKATING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape53.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="BUTTERFLY SLIDE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape141.svg"
                                             alt="BUTTERFLY SLIDE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape141.svg">
                                    </span>
                                                    </div>
                                                </div>
                                                <div ng-repeat="ots in actionTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Puck</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        <span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE PASSING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape48_s.svg"
                                             alt="STRAIGHT-LINE PASSING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape48_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PASSING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape48.svg"
                                             alt="PASSING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape48.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE SHOOTING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape39_s.svg"
                                             alt="STRAIGHT-LINE SHOOTING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape39_s.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="SHOOTING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape39.svg"
                                             alt="SHOOTING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape39.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PUCKS LINE"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape145.svg"
                                             alt="PUCKS LINE"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape145.svg">
                                    </span>
                                                    </div>
                                                </div>
                                                <div ng-repeat="ots in actionTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Other actions</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        <span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="RIGHT PIVOT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/right_pivot.svg"
                                             alt="RIGHT PIVOT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/right_pivot.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="LEFT PIVOT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/left_pivot.svg"
                                             alt="LEFT PIVOT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/left_pivot.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="ESCAPE RIGHT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/esc_r.svg"
                                             alt="ESCAPE RIGHT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/esc_r.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="ESCAPE LEFT"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/esc_l.svg"
                                             alt="ESCAPE LEFT"
                                             src="http://hockeyshare.com/drills/html5//img/svg/esc_l.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="DIRECTIONAL ARROW"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/directional_arrow.svg"
                                             alt="DIRECTIONAL ARROW"
                                             src="http://hockeyshare.com/drills/html5//img/svg/directional_arrow.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="DROP PASS"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/drop_pass.svg"
                                             alt="DROP PASS"
                                             src="http://hockeyshare.com/drills/html5//img/svg/drop_pass.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="CHECKING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/checking.svg"
                                             alt="CHECKING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/checking.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="BLOCK/PICK"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/block_pick.svg"
                                             alt="BLOCK/PICK"
                                             src="http://hockeyshare.com/drills/html5//img/svg/block_pick.svg">
                                    </span><span
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="STOPPING"
                                                                swipe-element-start="onPalleteItemDragStart(source)"
                                                                swipe-element="onPalleteItemDrop(source, option)"
                                                                swipe-element-move="onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/stopping.svg"
                                             alt="STOPPING"
                                             src="http://hockeyshare.com/drills/html5//img/svg/stopping.svg">
                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane ng-scope" ng-repeat="tab in tabs"
                                                 ng-class="{active: tab.active}" tab-content-transclude="tab">


                                                <div class="hd-pallete-container rinks-list ng-scope">
                                                    <a href="#"
                                                       ng-repeat="o in rinkOptions"
                                                       ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                       ng-bind="o.text"
                                                       ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                       ng-mousedown="$event.preventDefault()"
                                                       class="ng-binding ng-scope active">FULL
                                                        RINK</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">HALF
                                                        RINK (LEFT)</a><a
                                                            href="#" ng-repeat="o in rinkOptions"
                                                            ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                            ng-bind="o.text"
                                                            ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                            ng-mousedown="$event.preventDefault()"
                                                            class="ng-binding ng-scope">HALF RINK (RIGHT)</a>
                                                    <a href="#"
                                                       ng-repeat="o in rinkOptions"
                                                       ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                       ng-bind="o.text"
                                                       ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                       ng-mousedown="$event.preventDefault()"
                                                       class="ng-binding ng-scope">GOALIE
                                                        VIEW</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">NEUTRAL
                                                        ZONES (VERTICAL)</a><a
                                                            href="#" ng-repeat="o in rinkOptions"
                                                            ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                            ng-bind="o.text"
                                                            ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                            ng-mousedown="$event.preventDefault()"
                                                            class="ng-binding ng-scope">NEUTRAL ZONES (HORIZONTAL)</a>
                                                    <a href="#"
                                                       ng-repeat="o in rinkOptions"
                                                       ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                       ng-bind="o.text"
                                                       ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                       ng-mousedown="$event.preventDefault()"
                                                       class="ng-binding ng-scope">PROFESSIONAL
                                                        RINK</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">RINGETTE
                                                        FULL</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">RINGETTE
                                                        HALF (LEFT)</a><a
                                                            href="#" ng-repeat="o in rinkOptions"
                                                            ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                            ng-bind="o.text"
                                                            ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                            ng-mousedown="$event.preventDefault()"
                                                            class="ng-binding ng-scope">RINGETTE HALF (RIGHT)</a>
                                                    <a href="#"
                                                       ng-repeat="o in rinkOptions"
                                                       ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                       ng-bind="o.text"
                                                       ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                       ng-mousedown="$event.preventDefault()"
                                                       class="ng-binding ng-scope">RINGETTE
                                                        NZ</a><a href="#"
                                                                 ng-repeat="o in rinkOptions"
                                                                 ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                 ng-bind="o.text"
                                                                 ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                 ng-mousedown="$event.preventDefault()"
                                                                 class="ng-binding ng-scope">INLINE
                                                        RINK</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">ROLLER
                                                        RINK</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">NET
                                                        VIEW</a><a href="#"
                                                                   ng-repeat="o in rinkOptions"
                                                                   ng-click="RinkManager.setRinkViewViaPopup(o.rinkView)"
                                                                   ng-bind="o.text"
                                                                   ng-class="{active: o.rinkView === ModeFactory.rinkView}"
                                                                   ng-mousedown="$event.preventDefault()"
                                                                   class="ng-binding ng-scope">BLANK
                                                        RINK</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="hd-animation-tab ng-isolate-scope"
                                         ng-show="Enums.STATE.ANIMATION == ModeFactory.state">
                                        <ul class="nav nav-tabs"
                                            ng-class="{'nav-stacked': vertical, 'nav-justified': justified}"
                                            ng-transclude="">
                                            <li ng-class="{active: active, disabled: disabled}" select="true"
                                                class="ng-scope ng-isolate-scope active">
                                                <a href="" ng-click="select()" tab-heading-transclude=""
                                                   class="ng-binding">
                                                    <tab-heading class="ng-scope">Animation options</tab-heading>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">

                                            <div class="tab-pane ng-scope active" ng-repeat="tab in tabs"
                                                 ng-class="{active: tab.active}" tab-content-transclude="tab">


                                                <div class="hd-pallete-container hd-pallete-sequence-table-container ng-scope">
                                                    <table class="table sequence-table">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="3" class="ng-binding">Sequence #7 of
                                                                7
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;">Duration:</td>
                                                            <td style="width: 100px;">
                                                                <div class="input-number-control ng-isolate-scope"
                                                                     model="SequenceManager.getCurrentScene().duration"
                                                                     dsbl="SequenceManager.isDisabled()">
                                                                    <div><input type="number" integer="" min="0"
                                                                                class="form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-min ng-not-empty"
                                                                                ng-model="model" ng-disabled="dsbl"
                                                                                ng-paste="preventPaste($event)"></div>
                                                                    <div class="arrow-up"></div>
                                                                    <div class="arrow-down"></div>
                                                                </div>

                                                            </td>
                                                            <td>s</td>
                                                        </tr>
                                                        <tr>
                                                            <td>End Pause:</td>
                                                            <td>
                                                                <div class="input-number-control ng-isolate-scope"
                                                                     model="SequenceManager.getCurrentScene().endPause"
                                                                     dsbl="SequenceManager.isDisabled()">
                                                                    <div><input type="number" integer="" min="0"
                                                                                class="form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-min ng-not-empty"
                                                                                ng-model="model" ng-disabled="dsbl"
                                                                                ng-paste="preventPaste($event)"></div>
                                                                    <div class="arrow-up"></div>
                                                                    <div class="arrow-down"></div>
                                                                </div>
                                                            </td>
                                                            <td>s</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="clearfix btn-block">
                                <span class="btn btn-sm btn-primary pull-left" ng-click="prevSequence()"
                                      ng-disabled="!SequenceManager.hasPrev() || SequenceManager.isDisabled()">
                                    Prev.
                                </span>
                                                        <span class="btn btn-sm btn-primary pull-right"
                                                              ng-click="nextSequence()"
                                                              ng-disabled="!SequenceManager.hasNext() || SequenceManager.isDisabled()"
                                                              disabled="disabled">
                                    Next
                                </span>
                                                    </div>
                                                    <span class="btn btn-sm btn-green btn-block"
                                                          ng-click="SequenceManager.isDisabled() || newSequence()"
                                                          ng-disabled="SequenceManager.isDisabled()">
                                New Sequence
                            </span>
                                                    <span class="btn btn-sm btn-red btn-block"
                                                          ng-click="SequenceManager.isDisabled() || removeSequence()"
                                                          ng-disabled="SequenceManager.isDisabled()">
                                Delete Sequence
                            </span>
                                                    <span class="btn btn-sm btn-block btn-simple"
                                                          ng-click="SequenceManager.isDisabled() || previewSequence()"
                                                          ng-disabled="SequenceManager.isDisabled()">
                                Preview Sequence
                            </span>
                                                    <span class="btn btn-sm btn-block btn-simple"
                                                          ng-click="SequenceManager.isDisabled() || previewVideo()"
                                                          ng-disabled="SequenceManager.isDisabled()">
                                Preview Video
                            </span>
                                                    <h5 class="press-space-to-stop active"
                                                        ng-class="{active: !SequenceManager.isDisabled()}">
                                                        Press spacebar to
                                                        <span class="btn btn-xs btn-simple learn_more_span"
                                                              ng-click="!SequenceManager.isDisabled() || SequenceManager.pause()"
                                                              ng-disabled="!SequenceManager.isDisabled()"
                                                              disabled=disabled"
                                                        >stop</span>
                                                        video
                                                    </h5>
                                                </div>
                                                <div ng-repeat="ots in sequenceTabSections" class="ng-scope">
                                                    <div class="hd-pallete-subheader ng-binding">Movement</div>
                                                    <div class="hd-pallete-container clearfix">
                                                        <span
                                                                ng-disabled="SequenceManager.isDisabled()"
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options"
                                                                title="STRAIGHT-LINE PATH"
                                                                swipe-element-start="SequenceManager.isDisabled() || onPalleteItemDragStart(source)"
                                                                swipe-element="SequenceManager.isDisabled() || onPalleteItemDrop(source, option)"
                                                                swipe-element-move="SequenceManager.isDisabled() || onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape61_s.svg"
                                             alt="STRAIGHT-LINE PATH"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape61_s.svg">
                                    </span><span
                                                                ng-disabled="SequenceManager.isDisabled()"
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="PATH"
                                                                swipe-element-start="SequenceManager.isDisabled() || onPalleteItemDragStart(source)"
                                                                swipe-element="SequenceManager.isDisabled() || onPalleteItemDrop(source, option)"
                                                                swipe-element-move="SequenceManager.isDisabled() || onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape61.svg"
                                             alt="PATH"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape61.svg">
                                    </span><span
                                                                ng-disabled="SequenceManager.isDisabled()"
                                                                class="hd-draggable-btn ng-scope ng-isolate-scope"
                                                                ng-repeat="option in ots.options" title="SHARP PATH"
                                                                swipe-element-start="SequenceManager.isDisabled() || onPalleteItemDragStart(source)"
                                                                swipe-element="SequenceManager.isDisabled() || onPalleteItemDrop(source, option)"
                                                                swipe-element-move="SequenceManager.isDisabled() || onPalleteItemDrag($event)">
                                        <img draggable="false"
                                             ng-src="http://hockeyshare.com/drills/html5//img/svg/shape48.svg"
                                             alt="SHARP PATH"
                                             src="http://hockeyshare.com/drills/html5//img/svg/shape48.svg">
                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hd-main-left-numbering ng-hide"
                                     ng-hide="ModeFactory.state == Enums.STATE.ANIMATION">
                                    Numbering:
                                    <div class="btn-group">
                                        <label class="btn btn-xs btn-gray ng-pristine ng-untouched ng-valid active ng-not-empty"
                                               ng-model="ModeFactory.numbering"
                                               ng-click="changeNumbering(Enums.NUMBERING.ON)"
                                               btn-radio="Enums.NUMBERING.ON">On</label>
                                        <label class="btn btn-xs btn-gray ng-pristine ng-untouched ng-valid ng-not-empty"
                                               ng-model="ModeFactory.numbering"
                                               ng-click="changeNumbering(Enums.NUMBERING.OFF)"
                                               btn-radio="Enums.NUMBERING.OFF">Off</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colorpicker dropdown ng-scope colorpicker-position-top">
                        <div class="dropdown-menu">
                            <colorpicker-saturation style="background-color: rgb(255, 0, 0);"><i
                                        style="left: 0px; top: 100px;"></i></colorpicker-saturation>
                            <colorpicker-hue><i style="top: 0px;"></i></colorpicker-hue>
                            <colorpicker-alpha><i style="top: 0px;"></i></colorpicker-alpha>
                            <colorpicker-preview style="background-color: rgb(0, 0, 0);"></colorpicker-preview>
                            <button type="button" class="close close-colorpicker">×</button>
                        </div>
                    </div>
                    </body>
                    </html>
                </iframe>

            </div>
            <img src="/img/drag.jpg" height="45" width="950"
                 alt="Drag objects from the left on to the playing surface"/>
        </div>

        <br/>

        <p class="center"><img src="/img/testimonials-header.jpg"
                               height="93" width="970" alt="Testimonails"/></p>

        <div id="slideshow" class="slideshow">
            <img src="/img/testimonials-8.jpg" height="150" width="970"
                 alt="Testimonails"/>
        </div>

        <p class="center"><img src="/img/acp-header.jpg"
                               alt="Advanced Coaching Platform" width="970" height="87" usemap="#Map"/></p>
        @include('includes.commercial')
        <br>
    </div>
</div>