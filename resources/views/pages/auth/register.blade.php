@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Register</h1>
        <p>Want to become a member of one of the fastest growing hockey communities? Simply fill out the form below to
            create your <b><u>FREE</u></b> account today!</p>
        <p>Already have an account? <a href="http://hockeyshare.com/login/">Click here to log in</a>.</p>
        <p class="required">Required Fields</p>
        <form method="post" action="">
            <fieldset class="hs_fieldset">
                <legend class="hs_legend one">User Profile</legend>
                <div><label for="email" class="hs_label">Email: </label> <input type="text" class="hs_text required "
                                                                                name="email" id="email"
                                                                                onchange="checkEmail()" value=""/> <span
                            id="email_status"></span></div>
                <div><label for="username" class="hs_label">Username: </label> <input type="text"
                                                                                      class="hs_text required "
                                                                                      name="username" id="username"
                                                                                      value=""
                                                                                      onchange="checkUsername()"/> <span
                            id="username_available"></span></div>
                <div><label for="password" class="hs_label">Password: </label> <input type="password"
                                                                                      class="hs_text required "
                                                                                      name="password" id="password"/>
                    <span class="subtle_nu">(must be at least <strong>6</strong> characters)</span></div>
                <div><label for="password_confirm" class="hs_label">Confirm Password: </label> <input type="password"
                                                                                                      class="hs_text required "
                                                                                                      name="password_confirm"
                                                                                                      id="password_confirm"/>
                </div>
            </fieldset>

            <fieldset class="hs_fieldset">
                <legend class="hs_legend two">Personal Information</legend>

                <div><label for="primaryrole" class="hs_label">Primary Role: </label>

                    <select name="primaryrole">
                        <option value="" selected="selected">-- Select One (Required) --</option>
                        <option value="C">Coach</option>
                        <option value="P">Player</option>
                        <option value="A">Club Administrator</option>
                        <option value="T">Team Manager</option>
                        <option value="D">Director</option>
                        <option value="F">Parent/Fan</option>
                    </select><br clear="all">
                </div>

                <div><label for="firstname" class="hs_label">First Name: </label> <input type="text"
                                                                                         class="hs_text required "
                                                                                         name="firstname" id="firstname"
                                                                                         value=""/></div>
                <div><label for="lastname" class="hs_label">Last Name: </label> <input type="text"
                                                                                       class="hs_text required "
                                                                                       name="lastname" id="lastname"
                                                                                       value=""/></div>
                <div>
                    <br/>
                    <label for="hear" class="hs_label">How Did You Find Us?: </label>
                    <select name="hear" id="hear" class="hs_text">
                        <option value="" selected="selected">-- Choose One --</option>
                        <optgroup label="Referral">
                            <option value="Coach">Coach</option>
                            <option value="Friend">Friend</option>
                            <option value="CoachesSite">The Coaches Site</option>
                            <option value="TeamAssn">My Team / Association</option>
                        </optgroup>

                        <optgroup label="Social Media">
                            <option value="Newsletter">HockeyShare Newsletter</option>
                            <option value="YouTube">YouTube</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Instagram">Instagram</option>
                        </optgroup>
                        <optgroup label="Other">
                            <option value="Google">Search Engine</option>
                            <option value="Other">Other</option>
                        </optgroup>

                    </select><br clear="all"/>
                </div>
                <div><label for="twitter" class="hs_label">Twitter Username: </label> <input type="text"
                                                                                             class="hs_text "
                                                                                             name="twitter" id="twitter"
                                                                                             value=""/> <span
                            class="subtle_nu">Follow us <a href="https://twitter.com/hockeyshare/"
                                                           target="_new">@hockeyshare</a></span></div>
                <div><label for="instagram" class="hs_label">Instagram Username: </label> <input type="text"
                                                                                                 class="hs_text "
                                                                                                 name="instagram"
                                                                                                 id="instagram"
                                                                                                 value=""/> <span
                            class="subtle_nu">Follow us <a href="https://instagram.com/hockeyshare/"
                                                           target="_new">@hockeyshare</a></span></div>
            </fieldset>
            <fieldset class="hs_fieldset">
                <legend class="hs_legend three">Submit Registration</legend>
                <div><label for="agree" class="hs_label ">Agreement: </label> <input type="checkbox" class="required"
                                                                                     name="agree" value="1" id="agree"/>
                    I agree to the HockeyShare <a href="http://hockeyshare.com/privacy.htm" target="_blank">Terms of
                        Service/Privacy Policy</a><br clear="all"/></div>
                <div><label for="coppa" class="hs_label ">Legal Age: </label> <input type="checkbox" class="required"
                                                                                     name="coppa" value="1" id="coppa"/>
                    The person completing this form is over the age of 13 years old<br clear="all"/></div>
                <div><label for="subscribe" class="hs_label ">Newsletter: </label> <input type="checkbox" class=""
                                                                                          name="subscribe" value="1"
                                                                                          id="subscribe"
                                                                                          checked="checked"/> Receive
                    monthly emails from HockeyShare <br/><br clear="all"/></div>
                <div class="register_padding">
                    <div class="g-recaptcha" data-sitekey="6LezvcISAAAAABQlxts3FvDUSk6gT8rU8SZXgZdE">
                        <div class="contact_us_div">
                            <div>
                                <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LezvcISAAAAABQlxts3FvDUSk6gT8rU8SZXgZdE&amp;co=aHR0cHM6Ly93d3cuaG9ja2V5c2hhcmUuY29tOjQ0Mw..&amp;hl=en&amp;v=r20170515161201&amp;size=normal&amp;cb=6j9zolvqihnz"
                                        title="recaptcha widget" width="304" height="78" frameborder="0" scrolling="no"
                                        name="undefined"></iframe>
                            </div>
                            <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="ccontact_us_textarea g-recaptcha-response"></textarea>
                        </div>
                    </div>
                </div>
                <div class="lable">
                    <div class="g-recaptcha" data-sitekey="6LezvcISAAAAABQlxts3FvDUSk6gT8rU8SZXgZdE"></div>
                </div>
                <br/>
                <div><label for="Submit" class="hs_label">&nbsp;</label> <input type="submit" name="Submit"
                                                                                value="Submit" class="pbutton"
                                                                                id="Submit"/></div>
            </fieldset>
        </form>
        @include('includes.commercial')
        <br>
    </div>

</div>