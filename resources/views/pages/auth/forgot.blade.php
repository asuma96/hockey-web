@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <p>Can't remember your login? Use the form below to reset your password.</p>
        <form method="post" action="">
            <fieldset class="hs_fieldset">
                <legend class="blue_input hs_legend ">Password Recovery</legend>

                <div><label for="email" class="hs_label">Email Address: </label> <input type="text"
                                                                                        class="hs_text required "
                                                                                        name="email" id="email"
                                                                                        value=""/></div>
                <div><label for="Submit" class="hs_label">&nbsp;</label> <input type="submit" name="Submit"
                                                                                value="Send Reset Information"/></div>
            </fieldset>
        </form>
        @include('includes.commercial')
        <br>
    </div>
</div>

