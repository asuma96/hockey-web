@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <form method="post" action="">
            <fieldset class="hs_fieldset">
                <legend class="legend_upp">Log In to HockeyShare.com</legend>
                <div><label for="username" class="hs_label">Username: </label> <input type="text"
                                                                                      class="hs_text required "
                                                                                      name="hsun" id="hsun" value=""/>
                </div>
                <div><label for="password" class="hs_label">Password: </label> <input type="password"
                                                                                      class="hs_text required "
                                                                                      name="hspw" id="hspw"/></div>
                <div><label class="hs_label">&nbsp;</label><input type="checkbox" name="hsrm" value="1"
                                                                  checked="checked"> Remember me
                </div>
                <div><label for="Submit" class="hs_label forgot">&nbsp;</label> <input type="submit" name="Submit"
                                                                                       value="Log In" id="SubmitForm"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                            href="/login/forgot.php">Forgot Password</a></div>
            </fieldset>
            <input type="hidden" name="hstok" value="69fabfd99b514b0318869c01f7b65d12">

        </form>
        <br/>
        <fieldset class="hs_fieldset">
            <legend class="legend_down">Create a New Account</legend>
            <div>New here? You can register for a <span class="free">free</span> account by
                clicking the link below.
            </div>
            <br>
            <p>&raquo; <a href="http://hockeyshare.com/register/">Create a New Account</a></p>
        </fieldset>
        @include('includes.commercial')
        <br>
    </div>
</div>


