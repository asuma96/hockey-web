@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>RSS Feeds</h1>
        <p>Use HockeyShare's RSS feeds to stay up to date with the latest blog posts and drills.</p>
        <ul class="listrss">
            <li><a href="http://feeds.feedburner.com/HockeyshareBlog" target="_blank">HockeyShare Blog RSS</a></li>
            <li><a href="http://feedproxy.google.com/hockeysharedrills" target="_blank">New Hockey Drills at
                    HockeyShare.com</a></li>
        </ul>
        @include('includes.commercial')
        <br>
    </div>
</div>

