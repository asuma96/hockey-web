@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Compare Features</h2>

        <p>Compare the features of the different plans we offer. Every paid plan comes with a 30-day money back
            guarantee. If you're not satisfied for any reason, simply contact us and we'll refund your money. Below is a
            list of some of the key features of each membership level. A full feature comparison can be found <a
                    href="/acp/compare/full/">here</a>.</p>

        <div class="compare_signup1">
            <a href="http://hockeyshare.com/register/"><img src="/img/signup.png" alt="Signup"
                                                            class="signup_one"/></a>
        </div>

        <div class="compare_signup2">
            <a href="http://hockeyshare.com/upgrade/acp.php"><img src="/img/signup.png" alt="Signup"
                                                                  class="signup_two"/></a>
        </div>

        <div class="compare_signup3">
            <a href="http://hockeyshare.com/upgrade/acp_assn.php"><img src="/img/signup.png" alt="Signup"
                                                                       height="41" width="150"/></a>
        </div>

        <div class="clear_all"></div>
        <!-- Feature Comparison -->
        <div class="compare_free">
            <span class="compare_head">FREE</span>
            <br><br>
            <div class="compare_features">
                <ul>
                    <li>1 User</li>
                    <li>15 Drills</li>
                    <li>5 Practices</li>
                    <li>Branded Diagrams</li>
                    <li>Export Drill JPG</li>
                    <li>Printable Practice Plans</li>
                </ul>
            </div>
            <p class="compare_foot"><img src="/img/free.png" alt="Free" height="101" width="150"/></p>
        </div>

        <div class="compare_premium">
            <span class="compare_head">Premium</span> <span class="subtle_nu">( MOST POPULAR )</span>
            <br><br>

            <div class="compare_features">
                <ul>
                    <li>1 User</li>
                    <li>Unlimited Drills</li>
                    <li>Unlimited Practices</li>
                    <li>Un-Branded Diagrams</li>
                    <li>Export Drill JPG &amp; PDF</li>
                    <li>Printable &amp; PDF Practice Plans</li>
                    <li>Email Practice Plans</li>
                    <li>Hide Drills from the Public</li>
                    <li>Team Sharing Page</li>
                    <li><a class="underline" href="http://hockeyshare.com/freetrial/">14 Day Free Trial</a></li>
                    <li>1, 3, 6, &amp; 12 Month Options</li>
                    <li>Convenient Auto-Renewal Available</li>
                    <li>Animation Funcitonality Included</li>
                    <li>1 year: $75.00 (Save 21%)
                    </li>
                    <li>6 mn: $40.00 (Save 16%)</li>
                    <li>3 mn: $22.00 (Save 8%)</li>
                    <li>1 mn: $8.00</li>
                </ul>

                <div class="tcenter">

                    <a href="http://hockeyshare.com/freetrial/"><img class="compare_img compare_button"
                                                                     src="/img/compare_freetrial.png"
                                                                     alt="Free 14 Day Trial"/></a>
                    &nbsp;
                    <a href="/acp/video/overview/" data-featherlight="iframe" data-featherlight-iframe-height="600"
                       data-featherlight-iframe-width="875"><img src="/img/compare_premiumoverview.png"
                                                                 class="compare_img compare_button"
                                                                 alt="Watch Overview"/></a>
                </div>
            </div>
            <p class="compare_foot"><img src="/img/moneyback-premium.png" alt="30 Day Money Back Guarantee"
                                         height="116" width="150"/></p>
        </div>

        <div class="compare_assn">
            <span class="compare_head">Team / Association</span>
            <br><br>

            <div class="compare_features">
                <ul>
                    <li>3 or More Linked Users</li>
                    <li>Unlimited Drills</li>
                    <li>Unlimited Practices</li>
                    <li>Un-Branded Diagrams</li>
                    <li>Export Drill JPG &amp; PDF</li>
                    <li>Printable &amp; PDF Practice Plans</li>
                    <li>Email Practice Plans</li>
                    <li>Hide Drills from the Public</li>
                    <li>Team Sharing Page</li>
                    <li>Share Drills w/ Your Coaches</li>
                    <li>Copy Other Coaches Drills</li>
                    <li>Copy Other Coaches Plans</li>
                    <li><strong>Custom Logo in Practice Plans</strong></li>
                    <li>All Users Get Premium-Level Access</li>
                    <li>Animation Functionality Included for All Users</li>
                    <li>Choose How Many Users You Need</li>
                    <li>Plans as Low as $20/user/year</li>
                </ul>
                <br/>

                <div class="center">
                    <a href="#!" onclick="open_close_group('assn_pricing');" style="cursor:pointer;"><img
                                src="/img/compare_viewpricing.png" class="compare_img compare_button"
                                alt="View Pricing"/></a>
                    &nbsp;
                    <a href="/acp/video/assn/" data-featherlight="iframe" data-featherlight-iframe-height="600"
                       data-featherlight-iframe-width="875"><img src="/img/compare_watchoverview.png"
                                                                 class="compare_img compare_button"
                                                                 alt="Watch Overview"/></a>

                    <div style="display: none;" id="assn_pricing">
                        <table width="100%" id="table_b">
                            <tr>
                                <th># of Users</th>
                                <th>Price Per User</th>
                                <th>Savings</th>
                            </tr>
                            <tr>
                                <td>3 - 4</td>
                                <td>$50.00</td>
                                <td class="subtle_nu">$25.00 per user</td>
                            </tr>
                            <tr>
                                <td>5 - 12</td>
                                <td>$45.00</td>
                                <td class="subtle_nu">$30.00 per user</td>
                            </tr>
                            <tr>
                                <td>13 - 23</td>
                                <td>$40.00</td>
                                <td class="subtle_nu">$35.00 per user</td>
                            </tr>
                            <tr>
                                <td>24 - 34</td>
                                <td>$35.00</td>
                                <td class="subtle_nu">$40.00 per user</td>
                            </tr>
                            <tr>
                                <td>35 - 45</td>
                                <td>$30.00</td>
                                <td class="subtle_nu">$45.00 per user</td>
                            </tr>
                            <tr>
                                <td>46 - 200</td>
                                <td>$25.00</td>
                                <td class="subtle_nu">$50.00 per user</td>
                            </tr>
                            <tr>
                                <td>201 - 500</td>
                                <td>$20.00</td>
                                <td class="subtle_nu">$55.00 per user</td>
                            </tr>
                        </table>

                        <p class="subtle_nu">All team/association subscriptions are one year in
                            length. Savings comparison based on annual Premium subscription at $75.00.</p>
                    </div>
                </div>
            </div>
            <p class="compare_foot"><img src="/img/moneyback-premium.png" alt="30 Day Money Back Guarantee"
                                         height="116" width="150"/></p>
        </div>

        <div class="clear_all"></div>
        <!-- Signup Buttons -->
        <div class="compare_signup1">
            <a href="http://hockeyshare.com/register/"><img src="/img/signup.png" alt="Signup"
                                                            height="41" width="150"/></a>
        </div>

        <div class="compare_signup2">
            <a href="http://hockeyshare.com/upgrade/acp.php"><img src="/img/signup.png" alt="Signup"
                                                                  class="signup_two"/></a>
        </div>

        <div class="compare_signup3">
            <a href="http://hockeyshare.com/upgrade/acp_assn.php"><img src="/img/signup.png" alt="Signup"
                                                                       height="41" width="150"/></a>
        </div>


        <!-- Full Feature Comparison -->
        <p class="tcenter"><span><a
                        href="http://hockeyshare.com/acp/compare/full/"><img
                            src="/img/fullcomparison.png" alt="Signup" height="69" width="250"/></a></span>
        </p>

        <!-- Notes -->
        <p class="subtle_nu tcenter"><em>Note: All prices listed are in USD</em></p>
        @include('includes.commercial')
        <br>
    </div>
</div>