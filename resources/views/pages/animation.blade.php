@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content overview">
        <h4>Animation Overview</h4>
        <div style="color:#CCC;">
            <div class="center">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/pEiABbJ77as" frameborder="0"
                        allowfullscreen></iframe>
            </div>
            <p class="overview_new center"><a href="/drills/learn_more.php" target="_parent">Learn More</a> | <a
                        href="/freetrial/" target="_parent">Start Your Free 14 Day Trial</a></p>
        </div>
    </div>
</div>