<h1><strong>HockeyShare LLC Privacy Policy for HockeyShare.com </strong></h1>
<p>This document discusses the information collection practices made by:</p>
<p>HockeyShare LLC<br/>
    5744 80th Street - Suite #8<br/>
    Kenosha, WI 53142
</p>
<p>P: 262-672-4126<br/>
</p>
<h2>Personally Identifiable Information (PID) Collected</h2>
<p>The following personally identifiable information is currently collected from all registered members:</p>
<ul>
    <li>Date of Birth
        <ul>
            <li>This is used primarily in a broad sense to determine our key demographics. It is also used occasionally
                to determine eligibility for contests and/or giveaways.
            </li>
        </ul>
    </li>
    <li>Email Address
        <ul>
            <li>This is used as the primary contact method for members regarding profile updates, site updates, account
                changes, and contest/giveaway winner notifications.
            </li>
        </ul>
    </li>
    <li>First/Last Name
        <ul>
            <li>Full name is used primarily for individual contests and giveaways to identify users individually and
                help prevent duplicate registrations/entries.
            </li>
        </ul>
    </li>
    <li>Country / Zip Code
        <ul>
            <li>The collected location information (country and zip code) is used to better determine key markets
                viewing our site. It will also be used to present area-relevant content (including advertisements)
                within the website.
            </li>
        </ul>
    </li>
    <li>Player Information (Association, Age Level, Competition Level)
        <ul>
            <li>This information is used to allow HockeyShare LLC to track what types of players are using our services
                and may be used to present competition level relevant content (including advertisements) within the
                website.
            </li>
        </ul>
    </li>
</ul>
<p>The above personally identifiable information collected will NOT be shared with third-parties directly. From
    time-to-time, HockeyShare LLC may send out a mailing on behalf of a third party, but the personally identifiable
    information will be kept secure by HockeyShare LLC. The PID listed above will be collected actively via web-based
    forms. In complience with COPPA, all users under the age of 13 must have a parent/guardian complete our COPPA
    registration form before the account will be activated. If no reply is received within 7 days, the account will be
    deleted from our servers. The COPPA Registration form can be found here: <a
            href="http://hockeyshare.com/coppa.html" target="_blank">http://hockeyshare.com/coppa.html</a></p>
<p>The purpose of collection of the above information shall be the following:</p>
<ol>
    <li>Contest (or giveaway) winner notification</li>
    <li>Account protection &amp; change notification</li>
    <li>Advertisement</li>
</ol>
<h2>Passively collected information</h2>
<p>Non personally identifiable information may be collected passively by the server and/or third party services with the
    sole intention of tracking and monitoring website activity and performance. This information includes (but is not
    limited to) browser type, operating system type, referring website, page views, number of visits, amount of time
    spent on the website, IP address, general geographic location, browser features, available plug-ins, etc.</p>
<h2>COPPA Information</h2>
<p>In compliance of with the Children's Online Privacy Protection Act, parents/guardians of children under the age of
    13:</p>
<ul>
    <li>have the option to agree to the collection and use of the child's information without consenting to the
        disclosure of the information to third parties.
    </li>
    <li>can review the child's personal information, ask to have it deleted and refuse to allow any further collection
        or use of the child's information. The notice also must state the procedures for the parent to follow.
    </li>
    <li>may provide consent to registration and participation by using the following form: <a
                href="http://hockeyshare.com/coppa.html" target="_blank">http://hockeyshare.com/coppa.html</a>
        <a href="http://hockeyshare.com/forums/register.php?do=coppaform" target="_blank"></a>
        <ul>
            <li>This form must be mailed via USPS to the following address:
                <ul>
                    <li>HockeyShare LLC<br/>
                        Attn: Kevin Muller<br/>
                        5744 80th Street - Suite #8<br/>
                        Kenosha, WI 53142
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>
<h2><strong>1. Electronic newsletters policy (Dispatches)</strong></h2>
<p>We will offer a free electronic newsletter to users. HockeyShare LLC gathers the email addresses of users who
    voluntarily subscribe. Users may remove themselves from the mailing list by following the link provided in every
    newsletter that points users to the subscription management page. Users can also subscribe to the newsletters at the
    time of registration. For the protection of your privacy and to help prevent accidental or unwanted registration,
    HockeyShare.com uses a double-opt-in subscription policy. Simply stated this means that the user will receive an
    email after signing up. The user must click the activation link to confirm his/her subscription to the list. If a
    user fails to confirm their subscription, they may be removed from the subscription database. </p>
<p><em>In order to subscribe to the newsletter, the following personal information must be provided:</em></p>
<ul>
    <li>Valid email address</li>
</ul>
<p><em>Intentions of newsletter</em></p>
<ul>
    <li>Primary: Market updates regarding hockeyshare.com</li>
    <li>Secondary: Coaching materials for hockey coaches</li>
    <li>Tertiary: Market sponsor products</li>
</ul>
<p><em>Information handling</em></p>
<ul>
    <li>Our email list is <strong>not</strong> shared with or disclosed to third parties (unless required by law)</li>
</ul>
<h2><strong>2. Message boards/forums policy</strong></h2>
<p>Users of the site's Message Boards and Forums must register (free) in order to post messages, although they needn't
    register to visit the site. During registration the user is required to supply a username, password, and email
    address. Before a user is able to use their account, they must verify their email address is valid. Our message
    board system will send a randomly generated password to the email address entered at registration. This practice
    helps us reduce the number of fake/spam accounts. Additional policy information may be available during the
    registration process. </p>
<p><em>In order to join the forums, the following personal information must be provided:</em></p>
<ul>
    <li>Valid email address</li>
</ul>
<p><em>Information handling</em></p>
<ul>
    <li>Our member list is <strong>not</strong> shared with or disclosed to third parties (unless required by law)</li>
    <li>Members can opt not to receive contacts via email by altering their profile information (Profile &gt;&gt;
        Privacy)
    </li>
</ul>
<h3>3. &quot;E-mail this to a friend&quot; policy</h3>
<p>Our site users can choose to electronically forward a link, page, or documents to someone else by clicking &quot;e-mail
    this to a friend&quot;. The user must provide their email address, as well as that of the recipient. This
    information is used only in the case of transmission errors and, of course, to let the recipient know who sent the
    email. The information is not used for any other purpose.</p>
<h3>4. Polling</h3>
<p>We may offer interactive polls to users so they can easily share their opinions with other users and see what our
    audience thinks about important issues. Opinions or other responses to polls are aggregated and are not identifiable
    to any particular user. HockeyShare LLC may use a system to &quot;tag&quot; users after they have voted, so they can
    vote only once on a particular question. This tag is not correlated with information about individual users.</p>
<h2>5. Surveys</h2>
<p>HockeyShare LLC may occasionally conduct user surveys to better target our content to our audience. We sometimes
    share the aggregated demographic information in these surveys with our sponsors, advertisers and partners. We never
    share any of this information about specific individuals with any third party.</p>
