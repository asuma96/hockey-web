@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h2>Defensemen Shooting Progression Video Series</h2>
        <p>HockeyShare's Defensemen Shooting Progression video series has been put together to help coaches and players
            work on solid fundamental techniques in their training. The video series is approximately 20 minutes long
            and covers 17 different progressions. Each progression is briefly explained along with key points to watch
            for during execution, then demonstrated several times to clarify the execution of the progression. </p>
        <div class="center">
            @if(Auth::check())
                <a href="http://hockeyshare.com/training/d-vol1/purchase.php" class="rpbutton">Purchase for $9.99</a>
            @else
                <a href="http://hockeyshare.com/login" class="rpbutton">Purchase for $9.99</a>
            @endif
        </div>
        <h3>Introduction / Overview</h3>
        <div class="center">
            <iframe src="https://player.vimeo.com/video/200826102" width="945" height="532" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <h3>Sample Chapter - Controlling a Wrapped Puck on the Backhand</h3>
        <div class="center">
            <iframe src="https://player.vimeo.com/video/200828066" width="945" height="532" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <br clear="all"/>
        <div class="center">
            @if(Auth::check())
                <a href="http://hockeyshare.com/training/d-vol1/purchase.php" class="rpbutton">Purchase for $9.99</a>
            @else
                <a href="http://hockeyshare.com/login" class="rpbutton">Purchase for $9.99</a>
            @endif
        </div>
        @include('includes.commercial')
        <br>
    </div>
</div>

