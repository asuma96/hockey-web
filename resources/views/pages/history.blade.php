@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>
        <h2><i class="fa fa-history fa-1x"></i> Recent Emails</h2>

        <table width="100%" cellspacing="0">
            <thead>
            <tr class="resize-th">
                <th align="left">Date</th>
                <th align="left">Subject / Message ID</th>
                <th align="left">Source / Send Location</th>
            </tr>
            </thead>
            <tbody>
            <tr class="email">
                <td class="email_td email_bold"><br/>&nbsp;</td>
                <td class="email_td"><span class="email_bold"></span><br/><span class="email_subtle"></span></td>
                <td class="email_td"><span class="email_bold"></span><br/><span class="email_subtle"></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2"><i class="fa fa-history history"></i> 2017-06-01 08:46:42 - Waiting on report from mail
                    server - refresh page to update
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            </tbody>
        </table>
        @include('includes.commercial')
    </div>
</div>