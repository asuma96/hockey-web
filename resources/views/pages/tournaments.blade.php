@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <p align="right"><a href="http://hockeyshare.com/tournaments/list.php" class="rpbutton">List your tournament
                today.</a></p>
        <h3 class="title-text">Ice Hockey Tournament Listings</h3>
        <p>HockeyShare has one of the largest online tournament listings. We list tournaments in the US and Canada as
            well as overseas! Listing a tournament on our site is free of charge for basic listings. Featured listings
            start at just $5 for the life of the listing. With over 10,000 registered members, you know your tournament
            will get seen! </p>

        <div class="tournament_heading">
            United States Tournaments <span class="tournament_count">57 Tournaments</span></div>
        <div class="us_tourns">

            <div class="tournament_subheading">Arizona <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="toolTip underline"
                   title="Click to Show Tournament Info">AZ Coyotes Cup </a> <span class="tournament_date">Dec 28th - Dec 31st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Scottsdale & Chandler&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Mite, Squirt, PW, Bantam, Midget U16, Midget U18 , High School&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, A, B</span>

            </div>
            <div id="t4965" class="tournament_listing_2 ">

                <strong>Contact: </strong> Mary Stewart | <strong>Phone: </strong> 602-550-3108 |
                <strong>Email: </strong> azcoyotecup@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.azcoyotescup.com/"
                                                                                                  target="_blank">http://www.azcoyotescup.com/</a>

                <br/><br/>
                The AZ Coyote Cup is open to the following Ages: <br/>
                <br/>
                • Mite Half/Cross Ice -- $750.00<br/>
                • Squirt A–B -- $1,550.00 <br/>
                • Peewee AA-A-B -- $1,550.00 <br/>
                • Bantam AA-A-B -- $1,650.00<br/>
                • Midget 16 AA-A/High School -- $1,750.00<br/>
                • Midget 18 AA-A/High School -- $1,750.00<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">Connecticut <span class="tournament_count">11 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" title="Click to Show Tournament Info">SHELTON
                    SHOOT OUT</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: SHELTON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: PEEWEE MINOR '06 & BANTAM MAJOR '03&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA</span>

            </div>

            <div id="t4858" class="tournament_listing_2 ">

                <strong>Contact: </strong> GREG MAXEY | <strong>Phone: </strong> 203-605-3739 | <strong>Email: </strong>
                GMaxey@SportsCenterCT.com <br/><strong>Website: </strong> <a class="underline"
                                                                             href="http://rinksatshelton.com/"
                                                                             target="_blank">http://rinksatshelton.com/</a>

                <br/><br/>
                STATE OF THE ART TWIN RINK FACILITY WITH ACTIVITIES LIKE DRIVING RANGE, MINI GOLF, BATTING CAGES, LASER
                TAG, ETC. <br/>
                <br/>
                COMPETITIVE PLAY, QUALITY OFFICIALS AND PLAYER GIFTS. <br/><br/>
                <div align="right">
                    <div class="tournaments_left"></div>
                    <a class=class="underline" href="javascript:open_close_group('t4858');">Hide Tournament
                        Information</a></div>
            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4859');"
                   title="Click to Show Tournament Info">SHELTON SHOOT OUT</a> <span class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: SHELTON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: SQUIRT MAJOR '07 & BANTAM MINOR '04&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA</span>


            </div>
            <div id="t4859" class="tournament_listing_1">

                <strong>Contact: </strong> GREG MAXEY | <strong>Phone: </strong> 203-605-3739 | <strong>Email: </strong>
                GMaxey@SportsCenterCT.com <br/><strong>Website: </strong> <a class="underline"
                                                                             href="http://rinksatshelton.com/"
                                                                             target="_blank">http://rinksatshelton.com/</a>

                <br/><br/>
                STATE OF THE ART TWIN RINK FACILITY WITH ACTIVITIES LIKE DRIVING RANGE, MINI GOLF, BATTING CAGES, LASER
                TAG, ETC. <br/>
                <br/>
                COMPETITIVE PLAY, QUALITY OFFICIALS AND PLAYER GIFTS. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4859');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4860');"
                   title="Click to Show Tournament Info">SHELTON SHOOT OUT</a> <span class="tournament_date">May 26th - May 28th, 2017</span>
                <br/>

                <span class="subtle_nu"><strong>City</strong>: SHELTON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: GIRLS U12 / U14 / U16 / U19&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA</span>

            </div>
            <div id="t4860" class="tournament_listing_2 ">

                <strong>Contact: </strong> GREG MAXEY | <strong>Phone: </strong> 203-605-3739 | <strong>Email: </strong>
                GMaxey@SportsCenterCT.com <br/><strong>Website: </strong> <a class="underline"
                                                                             href="http://rinksatshelton.com/"
                                                                             target="_blank">http://rinksatshelton.com/</a>

                <br/><br/>
                STATE OF THE ART TWIN RINK FACILITY WITH ACTIVITIES LIKE DRIVING RANGE, MINI GOLF, BATTING CAGES, LASER
                TAG, ETC. <br/>
                <br/>
                COMPETITIVE PLAY, QUALITY OFFICIALS AND PLAYER GIFTS. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4860');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4861');"
                   title="Click to Show Tournament Info">SUGAR & ICE</a> <span class="subtle_nu">Adult</span> <span
                        class="tournament_date">May 26th - May 28th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: SHELTON &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: WOMEN'S HOCKEY WEEKEND&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: ALL LEVELS WELCOME</span>

            </div>
            <div id="t4861" class="tournament_listing_1 ">

                <strong>Contact: </strong> GREG MAXEY | <strong>Phone: </strong> 203-605-3739 | <strong>Email: </strong>
                GMaxey@SportsCenterCT.com <br/><strong>Website: </strong> <a class="underline"
                                                                             href="http://rinksatshelton.com/"
                                                                             target="_blank">http://rinksatshelton.com/</a>

                <br/><br/>
                WOMENS HOCKEY WEEKEND<br/>
                <br/>
                COMPETITIVE PLAY- ALL LEVELS WELCOME<br/>
                <br/>
                TEAM RECEPTION AND PLAYER GIFTS <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4861');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4890');"
                   title="Click to Show Tournament Info">DORITOS SPRING CHALLENGE</a> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, PW, Bantam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/A/A1/B/Tier 2 & 3</span>

            </div>
            <div id="t4890" class="tournament_listing_2 ">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.teamdoritos.com"
                                                                                                  target="_blank">http://www.teamdoritos.com</a>

                <br/><br/>
                 June 2-4 (games will be held on Friday evening)<br/>
                -3 Game Guarantee plus championship <br/>
                -All TIER 2/AA & Tier 3/A/Minor teams welcome <br/>
                -USA Hockey rules govern all tournament play<br/>
                -Teams play in divisions as they will next fall <br/>
                Tournament Fee: <br/>
                •Squirt/PW-$ 850.00, Bantam-$ 950.00<br/>
                Game format<br/>
                •Squirt/PW-Three-12 minute periods <br/>
                •Bantam-Three-15 minute periods <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4890');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4891');"
                   title="Click to Show Tournament Info">DORITOS MIDGET SPRING CHALLENGE</a> <span
                        class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Midget U16 Minor (01/02), Midget U18 Major (99/00)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/A/A1/B/Tier 2 & 3</span>

            </div>
            <div id="t4891" class="tournament_listing_1 ">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.teamdoritos.com"
                                                                                                  target="_blank">http://www.teamdoritos.com</a>

                <br/><br/>
                -3 Game Guarantee plus championship <br/>
                -All TIER 2/AA & Tier 3/A/Minor teams welcome <br/>
                -USA Hockey rules govern all tournament play<br/>
                -Teams play in divisions as they will next fall <br/>
                Tournament Fee: <br/>
                •Midget Major, Midget Minor-950.00<br/>
                Game format<br/>
                •15 minute periods <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4891');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4892');"
                   title="Click to Show Tournament Info">DORITOS MIDGET CUP</a> <span class="tournament_date">Aug 25th - Aug 27th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Midget U16 Minor (01/02), Midget U18 Major (99/00)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/A/A1/B/Tier 2 & 3</span>

            </div>
            <div id="t4892" class="tournament_listing_2 ">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.teamdoritos.com"
                                                                                                  target="_blank">http://www.teamdoritos.com</a>

                <br/><br/>
                -3 Game Guarantee plus championship <br/>
                -All TIER 2/AA & Tier 3/A/Minor teams welcome <br/>
                -USA Hockey rules govern all tournament play<br/>
                -Teams play in divisions as they will next fall <br/>
                Tournament Fee: <br/>
                •Midget Major/Midget Minor$ 950.00<br/>
                Game format<br/>
                •Three-15 minute periods <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4892');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4893');"
                   title="Click to Show Tournament Info">DORITOS FALL TUNE-UP</a> <span class="tournament_date">Sep 1st - Sep 3rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, PW, Bantam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/Tier 2</span>

            </div>
            <div id="t4893" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.teamdoritos.com"
                                                                                                  target="_blank">http://www.teamdoritos.com</a>

                <br/><br/>
                -3 Game Guarantee plus championship <br/>
                -All TIER 2/AA & Tier 3/A/Minor teams welcome <br/>
                -USA Hockey rules govern all tournament play<br/>
                -Teams play in divisions as they will next fall <br/>
                Tournament Fee: <br/>
                •Squirt/PW-$ 850.00, Bantam-$ 950.00<br/>
                Game format<br/>
                •Squirt/PW-Three-12 minute periods <br/>
                •Bantam-Three-15 minute periods <br/>
                <br/><br/>
                <div align="right">
                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4893');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4876');"
                   title="Click to Show Tournament Info">Silver Stick "AA" New England Regional</a> <span
                        class="tournament_date">Oct 26th - Oct 29th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, PW, Bantam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/Tier 2</span>

            </div>
            <div id="t4876" class="tournament_listing_2 ">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional"
                                                                                                  target="_blank">http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional</a>

                <br/><br/>
                3 Game Guarantee (Preliminary Round Robin) <br/>
                USA Hockey Sanctioned Tournament/USA Hockey rules govern all tournament play<br/>
                All games played to completion-no curfew<br/>
                Tournament Fee:(Credit Card or Check accepted) <br/>
                •Squirts & Pee Wee - $1,250 / Bantam - $1,350  <br/>
                Game format: Squirt & Pee Wee -12 minute periods / Bantams - 15 minute periods<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4876');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4877');"
                   title="Click to Show Tournament Info">Silver Stick "A" New England Regional</a> <span
                        class="tournament_date">Nov 21st - Nov 26th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bridgeport&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, PW, Bantam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A-A1-B/Tier 3</span>


            </div>
            <div id="t4877" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional"
                                                                                                  target="_blank">http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional</a>

                <br/><br/>
                3 Game Guarantee (Preliminary Round Robin) <br/>
                USA Hockey Sanctioned Tournament/USA Hockey rules govern all tournament play<br/>
                All games played to completion-no curfew<br/>
                Tournament Fee:(Credit Card or Check accepted) <br/>
                •Squirts & Pee Wee - $1,250 / Bantam - $1,350 <br/>
                Game format: Squirt & Pee Wee -12 minute periods / Bantams - 15 minute periods<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4877');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4878');"
                   title="Click to Show Tournament Info">Silver Stick "B" New England Regional</a> <span
                        class="tournament_date">Dec 1st - Dec 3rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Norwich&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, PW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: C/Tier 4/Undeclared</span>

            </div>
            <div id="t4878" class="tournament_listing_2 ">

                <strong>Contact: </strong> Paul Shimko | <strong>Phone: </strong> 203-258-0878 |
                <strong>Email: </strong> pshimko@optonline.net <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional"
                                                                                                  target="_blank">http://silverstick.org/Tournaments/2908/2017-18_Bridgeport_Regional</a>

                <br/><br/>
                3 Game Guarantee (Preliminary Round Robin) <br/>
                USA Hockey Sanctioned Tournament/USA Hockey rules govern all tournament play<br/>
                Teams MUST produce official/signed USA Hockey team roster to be accepted<br/>
                All games played to completion-no curfew<br/>
                Tournament Fee: (Credit Card or Check accepted) <br/>
                •Squirts & Pee Wee - $1,250 <br/>
                Game format: Squirt & Pee Wee -12 minute periods <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4878');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">Florida <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4944');"
                   title="Click to Show Tournament Info">FLORIDA PANTHERS CUP</a> <span class="tournament_date">Dec 27th - Dec 30th, 2017</span>

                <br/>
                <span class="subtle_nu"><strong>City</strong>: MIAMI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2002-03-04-05-06&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>


            </div>
            <div id="t4944" class="tournament_listing_2 ">

                <strong>Contact: </strong> floridahockeyaaacup@yahoo.com | <strong>Phone: </strong> (416) 357-2385 |
                <strong>Email: </strong> floridahockeyaaacup@yahoo.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.thehockeycup.com/florida-panthers-cup.html" target="_blank">http://www.thehockeycup.com/florida-panthers-cup.html</a>

                <br/><br/>
                THE FLORIDA PANTHERS CUP is held at the home of the NHL Florida Panthers BB&T Centre and Panthers Ice
                Den home training facility of the Florida Panthers. Minutes from the beach why not play a winter
                tournament in Paradise.<br/>
                <br/>
                AAA hockey teams from across the globe to travel to Miami to compete in a spectacular setting,<br/>
                <br/>
                Register Now! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4944');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Illinois <span class="tournament_count">3 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4911');"
                   title="Click to Show Tournament Info">Chicago May Madness</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Chicago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4911" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/143/Chicago-May-Madness-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/143/Chicago-May-Madness-Hockey-Tournament.html</a>

                <br/><br/>
                Tournament Features:<br/>
                - 5 games minimum, 6 games maximum<br/>
                - Special rates for Group Hotels<br/>
                - Non-checking options (boys divisions)<br/>
                - No gate fees<br/>
                - Impressive, newly designed awards<br/>
                - Live online scores, stats and standings<br/>
                <br/>
                Cost:<br/>
                $999 USD (taxes included) <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4911');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1 tournament_featured tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/mht.png" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4666');"
                   title="Click to Show Tournament Info">MYHockey Tournaments Spring Classic</a> <span
                        class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Chicago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt, Pee Wee, Bantam, Midget Minor, Midget Major, High School Varsity and JV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: B, A, AA</span>


            </div>
            <div id="t4666" class="tournament_listing_1 tournament_featured" class="drill_td_new">

                <strong>Contact: </strong> Chris Field | <strong>Phone: </strong> 855-898-4040 |
                <strong>Email: </strong> cfield@myhockeytournaments.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.myhockeytournaments.com" target="_blank">www.myhockeytournaments.com</a>

                | <strong>Registration Deadline: </strong> May 10th
                <br/><br/>
                MYHockey Rankings info used to place teams, Four Game Minimum, Squirt thru Midget; AA, A, B; High School
                Varsity and JV <br/><br/>
                <div align="right">

                    <div class="tournaments_left">Price: $1,000.00 (Deposit: $250.00)</div>

                    <a class="underline" href="javascript:open_close_group('t4666');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4933');"
                   title="Click to Show Tournament Info">Chicago Thanksgiving Classic</a> <span class="tournament_date">Nov 24th - Nov 26th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Chicago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>


            </div>
            <div id="t4933" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/3167/Chicago-Thanksgiving-Classic-AAU-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/3167/Chicago-Thanksgiving-Classic-AAU-Hockey-Tournament.html</a>

                <br/><br/>
                Tournament Features:<br/>
                - 5 games minimum, 6 games maximum<br/>
                - Special rates for Group Hotels<br/>
                - Non-checking options (boys divisions)<br/>
                - No gate fees<br/>
                - Impressive, newly designed awards<br/>
                - Live online scores, stats and standings<br/>
                <br/>
                Cost:<br/>
                $1099 USD (taxes included) <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4933');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">Indiana <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4932');"
                   title="Click to Show Tournament Info">Fort Wayne Pre-Season Classic</a> <span
                        class="tournament_date">Oct 13th - Oct 15th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Fort Wayne&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>


            </div>
            <div id="t4932" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/44/Fort-Wayne-Pre-Season-Classic-Boys-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/44/Fort-Wayne-Pre-Season-Classic-Boys-Hockey-Tournament.html</a>

                <br/><br/>
                Tournament Features:<br/>
                - 5 games minimum, 6 games maximum<br/>
                - Special rates for Group Hotels<br/>
                - Non-checking options (boys divisions)<br/>
                - No gate fees<br/>
                - Impressive, newly designed awards<br/>
                - Live online scores, stats and standings<br/>
                <br/>
                Cost:<br/>
                $999 USD (taxes included) <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4932');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Maryland <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4958');"
                   title="Click to Show Tournament Info">Hockey Fights MS - 6th Annual Maryland Tournament</a> <span
                        class="subtle_nu">Adult</span> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Abingdon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Men, Adult Women&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Women's (D) & Men's (A, B, C)</span>

            </div>
            <div id="t4958" class="tournament_listing_2 ">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 6th Annual Hockey Fights MS Maryland Tournament will take place June 2-4, 2017. Proceeds from this
                event will benefit The Johns Hopkins MS Center in Baltimore, MD. All games will be played at Ice World
                in Abingdon, MD.<br/>
                <br/>
                ADDITIONAL INFO:<br/>
                Divisions: Women's (D) & Men's (A, B, C)<br/>
                4 Game Guarantee for all teams<br/>
                Individual Player Registration Fee: $100/player <br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy.<br/>
                Tournament T-Shirt guaranteed to players who register before May 12, 2017.<br/>
                Your team's donation to Johns Hopkins Project RESTORE<br/>
                Opportunity to purchase tournament merchandise, team photos and action shots onsite as well as
                online.<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4958');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">Massachusetts <span class="tournament_count">2 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4956');"
                   title="Click to Show Tournament Info">Hockey Fights MS - 3rd Annual Massachusetts Tournament</a>
                <span class="subtle_nu">Adult</span> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Tewksbury&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Men, Adult Women&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Men's B/C, Women's C/D, and Co-Ed Rec teams</span>


            </div>
            <div id="t4956" class="tournament_listing_2 ">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 3rd Annual Hockey Fights MS Massachusetts Tournament will take place June 9-11, 2017. Proceeds from
                this event will support multiple sclerosis research and rehabilitation. <br/>
                <br/>
                ADDITIONAL INFO:<br/>
                <br/>
                Divisions: Men's B/C, Women's C/D, and Co-Ed Rec teams<br/>
                4 game guarantee starting at 5:00pm on Friday. <br/>
                Individual Player Registration Fee: $90/player<br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy.<br/>
                Tournament T-Shirt guaranteed to players who register before May 19, 2017.<br/>
                Opportunity to purchase tournament merchandise, team photos and action shots onsite as well as
                online.<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4956');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4725');"
                   title="Click to Show Tournament Info">4th Annual CCM Boston Elite Invite</a> <span
                        class="tournament_date">Aug 4th - Aug 6th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Foxborough&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, U16&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>


            </div>
            <div id="t4725" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> skate@ccminvite.com | <strong>Phone: </strong> | <strong>Email: </strong>
                skate@ccminvite.com <br/><strong>Website: </strong> <a class="underline" href="http://www.ccminvite.com"
                                                                       target="_blank">http://www.ccminvite.com</a>

                <br/><br/>
                Elite-Level Summer Tournament located in Foxborough &amp; Walpole MA. &nbsp;4-Game Guarantee; Levels U16
                - 2010 birth year. &nbsp;www.ccminvite.com <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>
                    <a class="underline" href="javascript:open_close_group('t4725');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">Minnesota <span class="tournament_count">3 Tournaments</span></div>
            <div class="tournament_listing_2 tournament_featured tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/1466211548.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4616');"
                   title="Click to Show Tournament Info">OneHockey 3rd Minnesota Summer Slam</a> <span
                        class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: BLAINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 03, 04, 05, 06, 07, 08, 09&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4616" class="tournament_listing_2 tournament_featured" class="drill_td_new">

                <strong>Contact: </strong> info@onehockey.com | <strong>Phone: </strong> 7038553585 |
                <strong>Email: </strong> info@onehockey.com <br/><strong>Website: </strong> <a class="underline"
                                                                                               href="http://www.onehockey.com"
                                                                                               target="_blank">http://www.onehockey.com</a>

                <br/><br/>
                OneHockey3rd Minnesota Summer Slam<br/>
                We are back in Blaine MN June 2017!<br/>
                https://onehockey.com/2017-3rd-minnesota-summer-slam/<br/>
                June 2-4, 2017<br/>
                Super Rink -&nbsp;Blaine, MN<br/>
                AAA Elite 03, 04, 05, 06, 07,&nbsp;08, 09<br/>
                AAA Open 03, 04, 05, 06, 07,&nbsp;08, 09<br/>
                5 games &nbsp;&bull; &nbsp;3 X 15 periods/stop time <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4616');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4773');"
                   title="Click to Show Tournament Info">3rd Annual State of Hockey AAA Showdown</a> <span
                        class="tournament_date">Aug 3rd - Aug 6th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Minneapolis&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Boy's AAA 2008, 2007, 2006, 2005, 2004, 2003, 2002&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA Divisions</span>


            </div>
            <div id="t4773" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Eric Knutsen | <strong>Phone: </strong> 952-920-8826 |
                <strong>Email: </strong> eric.knutsen@showcasehockey.com <br/><strong>Website: </strong> <a
                        class="underline"
                        href="http://www.showcasehockey.com/page/show/2311467-2017-state-of-hockey-showdown"
                        target="_blank">http://www.showcasehockey.com/page/show/2311467-2017-state-of-hockey-showdown</a>

                <br/><br/>
                3rd Annual State of Hockey AAA Showdown: Boy's AAA 2008-2002 Birth Years <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4773');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4772');"
                   title="Click to Show Tournament Info">14th Annual Easton AAA Cup</a> <span class="tournament_date">Aug 17th - Aug 20th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Minneapolis&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Boy's AAA 2008, 2007, 2006, 2005, 2004, 2003, 2002  ||  Girl's AAA 10U, 12U, 14U, 16U, 19U&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA Divisions</span>

            </div>
            <div id="t4772" class="tournament_listing_2 ">

                <strong>Contact: </strong> Eric Knutsen | <strong>Phone: </strong> 952-920-8826 |
                <strong>Email: </strong> eric.knutsen@showcasehockey.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.showcasehockey.com/page/show/149203-2017-easton-cup"
                        target="_blank">http://www.showcasehockey.com/page/show/149203-2017-easton-cup</a>

                <br/><br/>
                14th Annual Easton AAA Cup Tournament: &nbsp;Boy's AAA 2008-2002 Birth Years &amp; Girl's 10U-19U AAA
                Levels. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4772');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">New Hampshire <span class="tournament_count">9 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4960');"
                   title="Click to Show Tournament Info">Midget Madness</a> <span class="tournament_date">Aug 11th - Aug 13th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: U16 & U18 Midgets&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/AAA</span>

            </div>
            <div id="t4960" class="tournament_listing_2 ">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 6th Annual 2017 Midget Madness tournament. The tournament will be held in Hooksett,
                NH from August 11-13, 2017. Last year’s tournament sold out so register early with a team registration
                form and deposit so you are not left out. The tournament will be open to Midget teams at the 16U and 18U
                divisions. We are currently sending out applications all over the US and Canada. Acceptance will be on a
                first come first serve basis. Please do not hesitate, as this tournament will fill up fast. Teams that
                have attended our tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, TX,
                PA and Canadian Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great way to end
                your season or begin a new one. The tournament is USA sanctioned and all games will be USA hockey rules.
                Check out our Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more. All divisions
                will include a championship game. The length of periods will be 2—25 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests.<br/>
                <br/>
                AWARDS:<br/>
                Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at <br/>
                (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1500 per team with a $750 deposit due with registration. Balance due by July 21st,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS SUMMER !!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Work: 603-270-1021<br/>
                Cell: 207-349-0990<br/>
                E mail: nhjrmonarchs@gmail.com<br/>
                Website: www.tri-townicearena.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4960');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4961');"
                   title="Click to Show Tournament Info">Summer Sizzler</a> <span class="tournament_date">Aug 18th - Aug 20th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2005, 2004, 2003&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA/Tier 1</span>


            </div>
            <div id="t4961" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 7th Annual Summer Sizzler Showcase. The tournament will be held in Hooksett, NH from
                August 18-20, 2017. Last year’s tournament sold out so register early with a team registration form and
                deposit so you are not left out. The tournament is for Boys teams at the Peewee Major, Bantam Minor and
                Bantam Major divisions. We are currently sending out applications all over the US and Canada. Acceptance
                will be on a first come first serve basis. Please do not hesitate, as this tournament will fill up fast.
                Teams that have attended our tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA,
                AR, CA, TX, PA and Canadian Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great
                way to end your season or begin a new one. The tournament is USA sanctioned and all games will be USA
                hockey rules. Check out our Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more. All divisions
                include a championship game. The length of periods will be 3-15 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests. <br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams at all levels. MVP award at each
                level in the championship game. Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1175 per team with a $600 deposit due with registration. Balance due by July 21st,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS SUMMER !!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 207-349-0990<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4961');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4962');"
                   title="Click to Show Tournament Info">Midget Challenge Cup</a> <span class="tournament_date">Sep 15th - Sep 17th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: U16 & U18 Midgets&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/AAA</span>


            </div>
            <div id="t4962" class="tournament_listing_2 ">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> <br/><strong>Website: </strong> <a class="underline"
                                                                            href="http://www.newhampshirejrmonarchs.com"
                                                                            target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                15th Annual <br/>
                <br/>
                NH Jr. Monarchs/<br/>
                Tri-Town Ice Arena <br/>
                <br/>
                <br/>
                Midget Challenge Cup Hockey Tournament<br/>
                <br/>
                AAA Boys Level<br/>
                <br/>
                Friday through Sunday<br/>
                September 15,16,17, 2017<br/>
                <br/>
                Midget Minor Division<br/>
                (16U)<br/>
                <br/>
                Midget Major Division<br/>
                (18U)<br/>
                <br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 15th Annual Midget Challenge Cup. The tournament will be held in Hooksett, NH from
                September 15-17, 2017. Last year’s tournament sold out so register early with a team registration form
                and deposit so you are not left out. The tournament will be open to Midget teams at the 16U and 18U
                divisions. We are currently sending out applications all over the US and Canada. Acceptance will be on a
                first come first serve basis. Please do not hesitate, as this tournament will fill up fast. Teams that
                have attended our tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, TX,
                PA and Canadian Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great way to end
                your season or begin a new one. The tournament is USA sanctioned and all games will be USA hockey rules.
                Check out our Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more. All divisions
                will include a championship game. The length of periods will be 2—25 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests.<br/>
                <br/>
                AWARDS:<br/>
                Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena<br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1650 per team with a $800 deposit due with registration. Balance due by August 30th,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS SUMMER !!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 603-270-1021<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4962');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4963');"
                   title="Click to Show Tournament Info">Columbus Day Shoot-Out</a> <span class="tournament_date">Oct 6th - Oct 9th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2008, 2007, 2006, 2005, 2004, 2003&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA/Tier 1</span>

            </div>
            <div id="t4963" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                2017 Columbus Day Shoot-Out<br/>
                <br/>
                AAA/Tier 1 Boys<br/>
                <br/>
                Friday through Sunday<br/>
                October 6 - 9, 2017<br/>
                <br/>
                Squirt Minor 2008<br/>
                Squirt Major 2007<br/>
                Peewee Minor 2006 <br/>
                Peewee Major 2005<br/>
                Bantam Minor 2004<br/>
                Bantam Major 2003<br/>
                <br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our Columbus Day Shoot-Out. The tournament will be held in Hooksett, NH from October 6-9,
                2017. Last year’s tournament sold out so register early with a team registration form and deposit so you
                are not left out. The tournament is for Boys teams at the Squirt, Peewee and Bantam with Minor and
                Bantam Major divisions. We are currently sending out applications all over the US and Canada. Acceptance
                will be on a first come first serve basis. Please do not hesitate, as this tournament will fill up fast.
                Teams that have attended our tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA,
                AR, CA, TX, PA and Canadian Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great
                way to end your season or begin a new one. The tournament is USA sanctioned and all games will be USA
                hockey rules. Check out our Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more. All divisions
                include a championship game. The length of periods will be 3-12 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests. <br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams at all levels. MVP award at each
                level in the championship game. Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena<br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                (603)-206-4214<br/>
                Amanda Dicey (Quality Inn Hotel) at <br/>
                (603) 668-6110 ext. 461<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1175 per team with a $600 deposit due with registration. Balance due by Sept. 15th,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS FALL!!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 207-349-0990<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4963');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4964');"
                   title="Click to Show Tournament Info">Black Cat Classic</a> <span class="tournament_date">Oct 27th - Oct 29th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Mites, Squirt, Peewees & Bantams&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A </span>

            </div>
            <div id="t4964" class="tournament_listing_2 ">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                18th Annual BLACK CAT CLASSIC<br/>
                Invitational Tournament<br/>
                <br/>
                JOIN US FOR A HOWLING GOOD TIME!<br/>
                <br/>
                A Level Teams<br/>
                Friday through Sunday<br/>
                October 27-29, 2017<br/>
                <br/>
                Mites, Squirts, Peewees & Bantams<br/>
                <br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 18th Annual BLACK CAT CLASSIC. The tournament will be open to A level teams at the
                Squirt, Pee Wee and Bantam divisions. The tournament will be held October 27-29, 2017. Last year’s
                tournament sold out so register early with a team registration form and deposit so you are not left out.
                The tournament is for Boys teams at the Squirt, Peewee and Bantam divisions. We are currently sending
                out applications all over the US and Canada. Acceptance will be on a first come first serve basis.
                Please do not hesitate, as this tournament will fill up fast. Teams that have attended our tournaments
                in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, TX, PA and Canadian Provinces of
                Ontario, Quebec and New Brunswick. Our tournaments are a great way to end your season or begin a new
                one. The tournament is USA sanctioned and all games will be USA hockey rules. Check out our Web site at:
                <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed three games in a round robin play with the possibility of more. All divisions
                include a championship game. The length of periods will be 3-12 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests. <br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams at all levels. MVP award at each
                level in the championship game. Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena<br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                Amanda Dicey (Quality Inn Hotel) at<br/>
                603-668-6110 ext. 461<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $875 per team with a $400 deposit due with registration. Balance due by Oct. 10th,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS Fall!!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 603-270-1021<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4964');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4966');"
                   title="Click to Show Tournament Info">Elite Prospects Showdown</a> <span class="tournament_date">Nov 10th - Nov 12th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2007 Squirt Major & 2006 Peewee Minor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>


            </div>
            <div id="t4966" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                2017 Elite Prospects Showdown <br/>
                Squirt Major 2007 & Peewee Minor 2006 AAA Teams<br/>
                November 10-12, 2017<br/>
                <br/>
                We cordially invite your 2007 & 2006 teams to participate in the 2017 Elite Prospects Showdown
                Tournament. The tournament will be open to Boys teams at the Squirt Major & Peewee Minor divisions. The
                Tournament will be held on November 10-12, 2017. We are currently sending out applications all over the
                US and Canada. Acceptance will be on a first come basis. Please do not hesitate, as this tournament will
                fill up fast. Other tournaments that we have hosted brought teams from ME, NH, VT, MA, NY, CT, RI, NJ,
                GA, AR, CA, PA, TX, FL and Canada. This tournament is USA sanctioned and all games will be USA hockey
                rules. <br/>
                Check out our Web site at: www.newhampshirejrmonarchs.com<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING YOUR TEAM THIS YEAR!!<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by.Home of the NH Jr. Monarchs 311 West River
                Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more.The length of
                periods will be 3—15 minute periods. <br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit our website for schedules. Every reasonable attempt will be made to
                accommodate scheduling requests. Games may start as early as 1pm on Friday.<br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams. MVP award. Team awards for
                championship and Runner-ups. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining you may watch a movie or enjoy a show on one of our large
                screen TV’s. Our game room is always available for children or adults to enjoy. <br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of the Hotels listed below. To reserve your rooms at discounted
                prices please make sure you mention the tournament name. Kristine Moulton (Holiday Inn Express & Suites)
                at (603) 518-2002, Amanda Dicey (Quality Inn) at (603) 668-6110 ext. 484, Sherri Ferns (Hampton Inn) at
                (603) 224-5322, Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard) at (603) 668-9514, Julie
                Perkins (Holiday Inn) at (603) 573-4038 or Sheryl Metivier (Radisson Hotel & Resort) (603)
                206-4214.<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1175 per team with a $500 deposit due with registration. Balance due by November 1st
                2017. All monies are Non-Refundable. Please make checks payable to: TRI-TOWN ICE ARENA<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 603-270-1021<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4966');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4967');"
                   title="Click to Show Tournament Info">Turkey Shoot-Out</a> <span class="subtle_nu">Adult</span> <span
                        class="tournament_date">Nov 24th - Nov 26th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Mites, Squirt, Peewees & Bantams&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: B & C (Tier 3 & 4)</span>

            </div>
            <div id="t4967" class="tournament_listing_2 ">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                17th Annual Turkey Shoot-Out<br/>
                Thanksgiving Tournament<br/>
                <br/>
                B&C Level(TIER III & IV)<br/>
                Friday through Sunday<br/>
                Nov. 24, 25, 26, 2017<br/>
                <br/>
                Mites, Squirts, Peewees & Bantams<br/>
                <br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 17th Annual Turkey Shoot-Out. The tournament will be held in Hooksett, NH from
                November 24-26, 2017. Last year’s tournament sold out so register early with a team registration form
                and deposit so you are not left out. The tournament is for Boys teams at the Squirt, Peewee and Bantam
                divisions. We are currently sending out applications all over the US and Canada. Acceptance will be on a
                first come first serve basis. Please do not hesitate, as this tournament will fill up fast. Teams that
                have attended our tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, TX,
                PA and Canadian Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great way to end
                your season or begin a new one. The tournament is USA sanctioned and all games will be USA hockey rules.
                Check out our Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed 3 games in a round robin play with the possibility of more. All divisions
                include a championship game. The length of periods will be 3-12 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests. <br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams at all levels. MVP award at each
                level in the championship game. Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena<br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                Amanda Dicey (Quality Inn) at (603) 668-6110 ext. 484<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $875 per team with a $400 deposit due with registration. Balance due by Nov. 15th,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS Fall!!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 207-349-0990<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4967');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4969');"
                   title="Click to Show Tournament Info">AAA Prospects Showcase</a> <span class="tournament_date">Dec 8th - Dec 10th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Peewee Major 2005 & Bantam Minor 2004 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA Teams</span>

            </div>
            <div id="t4969" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                2017 AAA Prospects Showcase <br/>
                Peewee Major 2005 & Bantam Minor 2004 AAA Teams<br/>
                December 8-10, 2017<br/>
                <br/>
                We cordially invite your 2005 & 2004 teams to participate in the 2017 AAA Prospects Showcase Tournament.
                The tournament will be open to Boys teams at the PW Major & Bantam Minor divisions. The Tournament will
                be held on December 8-10, 2017. We are currently sending out applications all over the US and Canada.
                Acceptance will be on a first come basis. Please do not hesitate, as this tournament will fill up fast.
                Other tournaments that we have hosted brought teams from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, PA,
                TX, FL and Canada. This tournament is USA sanctioned and all games will be USA hockey rules. <br/>
                Check out our Web site at: www.newhampshirejrmonarchs.com<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING YOUR TEAM THIS YEAR!!<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs 311 West River
                Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed four games in a round robin play with the possibility of more.<br/>
                The length of periods will be 3—15 minute periods. <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit our website for schedules. Every reasonable attempt will be made to
                accommodate scheduling requests. Games may start as early as 1pm on Friday.<br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams. MVP award. Team awards for
                championship and Runner-ups. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining you may watch a movie or enjoy a show on one of our large
                screen TV’s. Our game room is always available for children or adults to enjoy. <br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of the Hotels listed below. To reserve your rooms at discounted
                prices please make sure you mention the tournament name. Kristine Moulton (Holiday Inn Express & Suites)
                at (603) 518-2002, Amanda Dicey (Quality Inn) at (603) 668-6110 ext. 484, Sherri Ferns (Hampton Inn) at
                (603) 224-5322, Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard) at (603) 668-9514, Julie
                Perkins (Holiday Inn) at (603) 573-4038 or Sheryl Metivier (Radisson Hotel & Resort) (603)
                206-4214.<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $1175 per team with a $500 deposit due with registration. Balance due by December 1st
                2017. All monies are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                Mail to:<br/>
                Tri-Town Ice Arena <br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 603-270-1021<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4969');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4968');"
                   title="Click to Show Tournament Info">Winter Classic</a> <span class="tournament_date">Dec 29th - Dec 31st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Hooksett&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Mites, Squirt, Peewees & Bantams&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: B & C (Tier 3 & 4)</span>

            </div>
            <div id="t4968" class="tournament_listing_2 ">

                <strong>Contact: </strong> Bob Ingerson | <strong>Phone: </strong> 207-349-0990 |
                <strong>Email: </strong> nhjrmonarchs@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                   href="http://www.newhampshirejrmonarchs.com"
                                                                                                   target="_blank">http://www.newhampshirejrmonarchs.com</a>

                <br/><br/>
                9th Annual Winter Classic<br/>
                Holiday Tournament<br/>
                <br/>
                B/C Tier 3 & 4 Level Teams<br/>
                Friday through Sunday<br/>
                December 29-31, 2017<br/>
                <br/>
                Mites, Squirts, Peewees & Bantams<br/>
                <br/>
                The NH Jr. Monarchs, West River Royals & Tri-Town Ice Arena invites your team or organization to
                participate in our 9th Annual WINTER CLASSIC. The tournament will be open to B/C level teams at the
                Mite, Squirt, Pee Wee and Bantam divisions. The tournament will be held December 29-31, 2017. Last
                year’s tournament sold out so register early with a team registration form and deposit so you are not
                left out. The tournament is for Boys teams at the Mite, Squirt, Peewee and Bantam divisions. We are
                currently sending out applications all over the US and Canada. Acceptance will be on a first come first
                serve basis. Please do not hesitate, as this tournament will fill up fast. Teams that have attended our
                tournaments in the past have come from ME, NH, VT, MA, NY, CT, RI, NJ, GA, AR, CA, TX, PA and Canadian
                Provinces of Ontario, Quebec and New Brunswick. Our tournaments are a great way to end your season or
                begin a new one. The tournament is USA sanctioned and all games will be USA hockey rules. Check out our
                Web site at: <br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                LOCATION:<br/>
                All games will be played at Tri-Town Ice Arena or close by. Home of the NH Jr. Monarchs. The rink
                address is - 311 West River Road (Rte. 3A) Hooksett, NH. 03106<br/>
                <br/>
                GAMES:<br/>
                All teams are guaranteed three games in a round robin play with the possibility of more. All divisions
                include a championship game. The length of periods will be 3-12 minute periods.<br/>
                <br/>
                SCHEDULING:<br/>
                All teams are instructed to visit the tournament website for schedules. Schedules will be posted about
                10 days before the start of the tournament on the website. Every reasonable attempt will be made to
                accommodate scheduling requests. <br/>
                <br/>
                AWARDS:<br/>
                Individual trophies will be awarded to championship and runner up teams at all levels. MVP award at each
                level in the championship game. Team awards for championship and Runner-up organizations. <br/>
                <br/>
                FACILITY:<br/>
                Tri-Town Ice Arena opened in March of 1998 with two NHL (200’x85’) ice surfaces. We have a Restaurant on
                site with a liquor license. While dining, enjoying a beverage or just relaxing you may watch a movie,
                enjoy a sporting event or enjoy a show on one of our large screen TV’s. Our game room is always
                available for children or adults to enjoy as well.<br/>
                <br/>
                Mail to:<br/>
                Tri-Town Ice Arena<br/>
                c/o Bob Ingerson<br/>
                311 West River Rd<br/>
                Hooksett NH 03106<br/>
                <br/>
                HOTELS:<br/>
                All teams are required to stay at one of these recommended HOTELS. To reserve your Hotel rooms at
                discounted prices please contact one of them below: Please make sure you mention the tournament to get
                the discounted rate. All the Hotels are 10 minutes from the Host Rink.<br/>
                <br/>
                Cheri Halberstadt (Holiday Inn Express & Holiday Inn Suites) at (603) 669-6800 or (603) 641-6466<br/>
                Sherri Ferns (Hampton Inn) at <br/>
                (603) 224-5322<br/>
                Emily Cregg (Springhill Suites) (Homewood Suites) (Courtyard by Marriott) at (603) 668-9514<br/>
                Julie Perkins (Holiday Inn) at <br/>
                (603) 573-4038.<br/>
                Sheryl Metivier (Radisson Hotel) at <br/>
                603-206-4214<br/>
                Amanda Dicey (Quality Inn Hotel) at<br/>
                603-668-6110 ext. 461<br/>
                <br/>
                FEE:<br/>
                Tournament fee is $875 per team with a $400 deposit due with registration. Balance due by Dec. 10th,
                2017. All Monies paid are Non-Refundable. Please make checks payable to: <br/>
                TRI-TOWN ICE ARENA<br/>
                <br/>
                No admission for spectators!!!<br/>
                <br/>
                WE LOOK FORWARD TO HOSTING<br/>
                YOUR TEAM OR ORGANIZATION<br/>
                THIS WINTER!!<br/>
                <br/>
                Tournament Director: <br/>
                Bob Ingerson <br/>
                Tel: 603-270-1021<br/>
                Fax: 603-485-4551<br/>
                Email: nhjrmonarchs@gmail.com<br/>
                www.newhampshirejrmonarchs.com<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4968');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_subheading">New Jersey <span class="tournament_count">6 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4737');"
                   title="Click to Show Tournament Info">HOCKEY BOSS SPRING CLASSIC</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Randolph&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: PEEWEE,BANTAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: PEEWEE AA, BANTAM AA, BANTAM AAA</span>

            </div>
            <div id="t4737" class="tournament_listing_2 ">

                <strong>Contact: </strong> Steven Armstrong | <strong>Phone: </strong> 9739279122 |
                <strong>Email: </strong> stevea@aspen-ice.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                 href="http://aspenicearena.com/hockey-boss-spring-classic/"
                                                                                                 target="_blank">http://aspenicearena.com/hockey-boss-spring-classic/</a>

                <br/><br/>
                COME TO OUR NEWEST TOURNAMENT AND ENJOY THE GAMES, FOOD AND FUN. REGISTER YOUR TEAM OR SEVERAL TEAMS AND
                COMPETE FOR THE CUP. WE WILL PLAY A ROUND ROBIN FORMAT. EACH TEAM WILL HAVE A MINIMUM 4 GAME GUARANTEE
                WITH 15 MINUTE PERIODS AND TIES GOING TO FINAL SHOOT-OUT BEST OF 3.&nbsp;<br/>
                - $895 PER TEAM - 4-6 TEAMS PER LEVEL - MULTI-TEAM ( +3 ) DISCOUNTS <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4737');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4948');"
                   title="Click to Show Tournament Info">Hockey Boss Spring Classic</a> <span class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Randolph&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 16UAA, 18UAA, Mite Crossice&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA</span>

            </div>
            <div id="t4948" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Linda Keane | <strong>Phone: </strong> 973,927,9122 |
                <strong>Email: </strong> Linda@aspen-ice.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                href="http://aspenicearena.com"
                                                                                                target="_blank">http://aspenicearena.com</a>

                <br/><br/>
                COME TO OUR NEWEST TOURNAMENT AND ENJOY THE GAMES, FOOD AND FUN. REGISTER YOUR TEAM OR SEVERAL TEAMS AND
                COMPETE FOR THE CUP. WE WILL PLAY A ROUND ROBIN FORMAT. EACH TEAM WILL HAVE A MINIMUM 4 GAME GUARANTEE
                WITH 15 MINUTE PERIODS AND TIES GOING TO FINAL SHOOT-OUT BEST OF 3. MITE PLAY CROSS ICE<br/>
                <br/>
                - $895 PER TEAM<br/>
                - 4-6 TEAMS PER LEVEL<br/>
                - MULTI-TEAM ( +3 ) DISCOUNTS <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4948');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4738');"
                   title="Click to Show Tournament Info">HOCKEY BOSS SPRING CLASSIC</a> <span class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Randolph&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 16U, 18U&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: 16U AAA, 16U AA, 18UAAA, 18UAA</span>

            </div>
            <div id="t4738" class="tournament_listing_2 ">

                <strong>Contact: </strong> Steven Armstrong | <strong>Phone: </strong> 9739279122 |
                <strong>Email: </strong> stevea@aspen-ice.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                 href="http://aspenicearena.com/hockey-boss-spring-classic/"
                                                                                                 target="_blank">http://aspenicearena.com/hockey-boss-spring-classic/</a>

                <br/><br/>
                COME TO OUR NEWEST TOURNAMENT AND ENJOY THE GAMES, FOOD AND FUN. REGISTER YOUR TEAM OR SEVERAL TEAMS AND
                COMPETE FOR THE CUP. WE WILL PLAY A ROUND ROBIN FORMAT. EACH TEAM WILL HAVE A MINIMUM 4 GAME GUARANTEE
                WITH 15 MINUTE PERIODS AND TIES GOING TO FINAL SHOOT-OUT BEST OF 3.&nbsp; - $895 PER TEAM - 4-6 TEAMS
                PER LEVEL - MULTI-TEAM ( +3 ) DISCOUNTS <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4738');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4722');"
                   title="Click to Show Tournament Info">Schools Out Shootout</a> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Somerset&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Bantam / Mites&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA / A / B</span>


            </div>
            <div id="t4722" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> David Ball | <strong>Phone: </strong> 908.809.9779 | <strong>Email: </strong>
                David@lbprintinginc.com

                <br/><br/>
                Bantam A / Bantam AA - 03 / 04<br/>
                4 Game Guarantee<br/>
                $1100.00<br/>
                Games at Protec Hockey Ponds and close by arenas<br/>
                Medals, Trophies and Banners to Finalists and Champions<br/>
                Vendors on hand<br/>
                Mites will play on Ponds, all others on NHL Rink<br/>
                David Ball - 908-809-9779 - David@lbprintinginc.com<br/>
                Register early , last year was a success and you will get shut out if you wait til the last minute.
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4722');">Hide Tournament Information</a>
                </div>
            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4723');"
                   title="Click to Show Tournament Info">Schools Out Shootout</a> <span class="tournament_date">Jun 16th - Jun 18th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Somerset&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Pee Wee and Mites&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA / A / B</span>

            </div>
            <div id="t4723" class="tournament_listing_2 ">

                <strong>Contact: </strong> David Ball | <strong>Phone: </strong> 908-809-9779 | <strong>Email: </strong>
                David@lbprintinginc.com

                <br/><br/>
                Pee Wee A / Pee Wee AA - 05 / 06 and Mites<br/>
                4 Game Guarantee<br/>
                $1100.00<br/>
                Games at Protec Hockey Ponds and close by arenas<br/>
                Medals, Trophies and Banners to Finalists and Champions<br/>
                Vendors on hand<br/>
                Mites will play on Ponds, all others on NHL Rink<br/>
                David Ball - 908-809-9779 - David@lbprintinginc.com<br/>
                Register early , last year was a success and you will get shut out if you wait til the last minute.
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4723');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4724');"
                   title="Click to Show Tournament Info">Schools Out Shootout</a> <span class="tournament_date">Jun 23rd - Jun 25th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Somerset&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt / Mites&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA/A/B</span>

            </div>
            <div id="t4724" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> David Ball | <strong>Phone: </strong> 908-809-9779 | <strong>Email: </strong>
                David@lbprintinginc.com

                <br/><br/>
                Squirt A / Squirt AA - 03 / 04 - Mites<br/>
                4 Game Guarantee<br/>
                $1100.00<br/>
                Games at Protec Hockey Ponds and close by arenas<br/>
                Medals, Trophies and Banners to Finalists and Champions<br/>
                Vendors on hand<br/>
                Mites will play on Ponds, all others on NHL Rink<br/>
                David Ball - 908-809-9779 - David@lbprintinginc.com<br/>
                Register early , last year was a success and you will get shut out if you wait til the last minute.
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4724');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">New York <span class="tournament_count">9 Tournaments</span></div>
            <div class="tournament_listing_2 tournament_featured tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/1484265120.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4841');"
                   title="Click to Show Tournament Info">NY SHOWDOWN </a> <span class="tournament_date">May 26th - May 29th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Brooklyn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: MITE - SQUIRT - PEEWEE - BANTAM - MIDGET - JUNIOR - GIRLS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A / AA / AAA</span>

            </div>
            <div id="t4841" class="tournament_listing_2 tournament_featured" class="drill_td_new">

                <strong>Contact: </strong> Russell Kollins | <strong>Phone: </strong> (347)486-4791 |
                <strong>Email: </strong> avangard.hockeyclub@gmail.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.NYshowdown.com "
                        target="_blank">http://www.NYshowdown.com </a>

                <br/><br/>
                The biggest Memorial day weekend tournament in the Northeast. Teams will be competing from all parts of
                the nation and internationally. <br/>
                <br/>
                <br/>
                MITE - SQUIRT -PEEWEE - BANTAM - MIDGET - JUNIOR- GIRLS<br/>
                <br/>
                Level of Play: A / AA / AAA<br/>
                <br/>
                <br/>
                <br/>
                - Four (4) guaranteed games, with teams making playoffs playing up to six (6) total.<br/>
                <br/>
                - Prizes awarded such as: Individual Game Awards, Medals, Trophies, coaches award and other tournament
                prizes.<br/>
                <br/>
                - Scouts will be invited from top regional organizations and schools.<br/>
                <br/>
                - Team photo will be taken on ice upon completion of final game.<br/>
                <br/>
                - Competition committee will ensure equal parity in each division.<br/>
                <br/>
                - Welcome packets provided to each team upon arrival.<br/>
                <br/>
                - Music played during stoppage of play.<br/>
                <br/>
                <br/>
                For more info please visit: www.NYshowdown.com <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4841');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4819');"
                   title="Click to Show Tournament Info">Buffalo Winger progressive checking tourney</a> <span
                        class="tournament_date">Aug 24th - Aug 27th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Buffalo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4819" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> buffalowinger | <strong>Phone: </strong> | <strong>Email: </strong>
                BuffaloWinger@hotmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                             href="http://www.buffalowinger.net"
                                                                             target="_blank">http://www.buffalowinger.net</a>

                <br/><br/>
                The SUMMER&nbsp;"WINGER"<br/>
                &nbsp;August 24-27, 2017Progressive Checking- Checking degree will progress over the weekendChecking
                Clinic provided by Red Line Elite Hockey<br/>
                - 15/15/15 game format- Small Format- Only 8 teams- Progressive checking format- Sponsor Hotels near the
                rinks- 5 Games guaranteed, up to 7- Hockey Canada rules apply- $1500 US with checking clinic, $1300 US
                without**Checking clinic will be held on Thursday evening 6 or 7pm @ Holiday Rinks for 50 mins of ice
                time per team on a half sheet. 2 instructors per team<br/>
                &nbsp; <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4819');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4953');"
                   title="Click to Show Tournament Info">Hockey Fights MS - 2nd Annual New York Tournament</a> <span
                        class="subtle_nu">Adult</span> <span class="tournament_date">Sep 1st - Sep 3rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Buffalo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Men, Adult Women&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Men's (A, B, C), Women's (C, D) & Co-Ed rec</span>


            </div>
            <div id="t4953" class="tournament_listing_2 ">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 2nd Annual Hockey Fights MS New York Tournament will take place Labor Day weekend - September 1-3,
                2017. All games will be played at Northtown Center at Amherst - located adjacent from the University of
                Buffalo and only minutes from downtown. Proceeds from this event will benefit multiple sclerosis
                research and rehabilitation. Registration is open for Men's (A, B, C), Women's (C, D) & Co-Ed
                recreational teams and is based on a minimum of four teams in each division. The tournament schedule
                will be posted on our website approximately two weeks before the start of the tournament.<br/>
                <br/>
                Also taking place this weekend is the 16th Annual National Buffalo Chicken Wing Festival - one of the
                biggest and best food festivals in the country! It is often referred to as the 'Super Bowl' of the
                chicken wing industry for restaurants, wing lovers and competitive eaters and has been showcased by The
                Food Network. Last year, the festival drew over 70,000 people from 50 states and 36 countries. Held at
                Coca-Cola Field in downtown Buffalo, The National Buffalo Wing Festival offers something for everyone,
                including live music, eating competitions, and so much more!<br/>
                <br/>
                ADDITIONAL INFO:<br/>
                Divisions: Men's (A, B, C), Women's (C, D) & Co-Ed rec<br/>
                4 Game Guarantee for all teams<br/>
                Individual Player Registration Fee: $95/player<br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy.<br/>
                Tournament T-Shirt guaranteed to players who register before August 11, 2017.<br/>
                One admission ticket to the 16th Annual National Buffalo Chicken Wing Festival<br/>
                Tournament merchandise and action photos available for purchase onsite<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4953');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4832');"
                   title="Click to Show Tournament Info">Buffalo Labor Day Bantam/Midget Showcase</a> <span
                        class="tournament_date">Sep 1st - Sep 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Cheektowaga&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: U13 U14 U15 U16 U18&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA and AAA</span>

            </div>
            <div id="t4832" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Peter Preteroti | <strong>Phone: </strong> 716-491-0375 |
                <strong>Email: </strong> sportsniag@aol.com <br/><strong>Website: </strong> <a class="underline"
                                                                                               href="http://www.buffalostars.com"
                                                                                               target="_blank">http://www.buffalostars.com</a>

                <br/><br/>
                Youth Hockey tournament for Bantam and Midget AA and AAA level teams.<br/>
                FOUR game guarantee tournament.<br/>
                Live internet broadcast of several games.<br/>
                NO SPECTATOR CHARGES<br/>
                USA Hockey Sanctioned event <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4832');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4938');"
                   title="Click to Show Tournament Info">Niagara Jr Purple Eagles Columbus Day Tournament</a> <span
                        class="tournament_date">Oct 6th - Oct 9th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Niagara Falls&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt Minor & Major, PeeWee Minor & Major, Bantam Minor & Major, Girls 12 U  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A, AA, AAA</span>


            </div>
            <div id="t4938" class="tournament_listing_2 ">

                <strong>Contact: </strong> 7162846871 | <strong>Phone: </strong> 7162846871 | <strong>Email: </strong>
                drobertson@adelphia.net

                <br/><br/>
                World-class Tournament featuring some of the top AAA and AA talent from the Northeast. Games played at
                spectacular Dwyer Arena, home of the Niagara Purple Eagles Division 1 Hockey team. Open to AA/AAA teams
                at the Squirt through Midget level. Our tournament features:<br/>
                <br/>
                *Four Game Guarantee<br/>
                *Great Selection of Hotels<br/>
                *Player Gifts for All Participants<br/>
                *Player Medals for Champions and Runner-Up in all divisions<br/>
                *Team Cup for Champions and Runner-Up in all divisions<br/>
                *Multi-team discount.<br/>
                *Minutes away from majestic Niagara Falls, spectacular shopping, and local attractions. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4938');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4951');"
                   title="Click to Show Tournament Info">The Cup North American Championship AA</a> <span
                        class="tournament_date">Dec 8th - Dec 10th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Buffalo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2003-04-05-06-07-08&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4951" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> tharindra.desilva@gmail.com | <strong>Phone: </strong> 416-357-2385 |
                <strong>Email: </strong> tharindra.desilva@gmail.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://thehockeycupaa.com" target="_blank">http://thehockeycupaa.com</a>

                <br/><br/>
                The Cup North American Championship AA is one of the fastest growing AA hockey Tournaments in the
                USA.<br/>
                <br/>
                Select games featured at Riverworks stunning outdoor ice facility that you have to see to believe. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4951');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4366');"
                   title="Click to Show Tournament Info">THE CUP North American Championship</a> <span
                        class="tournament_date">Jan 12th - Jan 15th, 2018</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Buffalo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2002 2003 2004 2005 2006 2007&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA Elite</span>


            </div>
            <div id="t4366" class="tournament_listing_2 ">

                <strong>Contact: </strong> 416-357-2385 | <strong>Phone: </strong> 416-357-2385 |
                <strong>Email: </strong> info@thehockeycup.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                  href="http://www.thehockeycup.com"
                                                                                                  target="_blank">http://www.thehockeycup.com</a>

                <br/><br/>
                THE CUP North America Championship<br/>
                &nbsp;<br/>
                This is North America's AAA CHAMPIONSHIP. &nbsp;Win THE CUP and you are annointed AAA North American
                Champions. There is NO tougher tournament to win. &nbsp;If your team is up for the challenge against the
                World's top AAA teams this is the tournament for you.<br/>
                &nbsp;<br/>
                Select teams will qualify to play in our Outdoor WINTER CLASSIC games at the most fantastic outdoor
                facilty in North America RIVERWORKS<br/>
                &nbsp;<br/>
                There is no tournament quite like THE CUP. &nbsp;<br/>
                &nbsp;<br/>
                Teams must qualify or be accepted to attend as this is a strict invitational not open to all AAA
                teams.<br/>
                &nbsp;<br/>
                Please contact us to submit your team for consideration <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4366');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4946');"
                   title="Click to Show Tournament Info">The Cup North American Championship </a> <span
                        class="tournament_date">Jan 12th - Jan 14th, 2018</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Buffalo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2002-03-04-05-06-07-08&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4946" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> info@thehockeycup.com | <strong>Phone: </strong> (416) 357-2385 | <strong>Email: </strong>
                info@thehockeycup.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://www.thehockeycup.com"
                                                                         target="_blank">http://www.thehockeycup.com</a>

                <br/><br/>
                The Cup North American Championship is North Americas premier tournament. This tournament is open to top
                AAA teams who want to compete for North America's crown jewel of championships<br/>
                <br/>
                Brand New venues<br/>
                <br/>
                Incredible Outdoor hockey venues are available to lucky teams<br/>
                <br/>
                Please view our website to see for yourself www.thehockeycup.com <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4946');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4939');"
                   title="Click to Show Tournament Info">Niagara Jr Purple Eagles Patriots Day Tournament</a> <span
                        class="tournament_date">Feb 16th - Feb 19th, 2018</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Niagara Falls&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Squirt Minor & Major, PeeWee Minor & Major, Bantam Minor & Major, Girls 12 U  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A, AA, AAA</span>

            </div>
            <div id="t4939" class="tournament_listing_2 ">

                <strong>Contact: </strong> David Robertson | <strong>Phone: </strong> 7164728193 |
                <strong>Email: </strong> drobertson@jrpurpleeagles.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.jrpurpleeagles.com" target="_blank">http://www.jrpurpleeagles.com</a>

                <br/><br/>
                World-class Tournament featuring some of the top AAA and AA talent from the Northeast. Games played at
                spectacular Dwyer Arena, home of the Niagara Purple Eagles Division 1 Hockey team. Open to AA/AAA teams
                at the Squirt through Midget level. Our tournament features:<br/>
                Four Game Guarantee<br/>
                Great Selection of Hotels<br/>
                Player Gifts for All Participants<br/>
                Player Medals for Champions and Runner-Up in all divisions<br/>
                Team Cup for Champions and Runner-Up in all divisions<br/>
                Multi-team discount.<br/>
                Minutes away from majestic Niagara Falls, spectacular shopping, and local attractions. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4939');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">North Carolina <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4895');"
                   title="Click to Show Tournament Info">CRAZY PUCKER BEACH BLAST 2017</a> <span
                        class="subtle_nu">Adult</span> <span class="tournament_date">Jul 28th - Jul 30th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: WILMINGTON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 33+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: ALL LEVELS</span>


            </div>
            <div id="t4895" class="tournament_listing_2 ">

                <strong>Contact: </strong> JEFF HAMILTON | <strong>Phone: </strong> 910-742-1620 |
                <strong>Email: </strong> hammy8888@gmail.com

                <br/><br/>
                * 8 TEAMS WITH TWO GROUPS<br/>
                * 3 GAME GUARANTY AND CHAMPIONSHIP GAME<br/>
                * PLAYERS 33+ IN AGE AND GOALIES 25+ IN AGE<br/>
                * BEER AFTER EVERY GAME EVEN IN THE AM<br/>
                * FAMOUS CRAZY PUCKER "T" SHIRT FOR ALL<br/>
                * PARTY ON SATURDAY NIGHT AT LOCAL BAR WITH SPECIALS<br/>
                * FEE $1100.00<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4895');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Ohio <span class="tournament_count">3 Tournaments</span></div>
            <div class="tournament_listing_2  tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/1484161871.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4836');"
                   title="Click to Show Tournament Info">Summer Blast</a> <span class="tournament_date">Aug 18th - Aug 20th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Cleveland&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Midget U15, U16, U18&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>

            </div>
            <div id="t4836" class="tournament_listing_2 ">

                <strong>Contact: </strong> Patrick Metzger | <strong>Phone: </strong> 216 394-1369 |
                <strong>Email: </strong> teamohiohockey@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                     href="http://teamohio.com"
                                                                                                     target="_blank">http://teamohio.com</a>

                <br/><br/>
                Summer Blast<br/>
                When- August 18th-20th<br/>
                Where- Winterhurst and Gilmour Academy Ice Facilities<br/>
                <br/>
                $1250<br/>
                4 Game Guarantee <br/>
                <br/>
                For more information contact Patrick Metzger at teamohiohockey@gmail.com <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4836');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/1484162150.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4837');"
                   title="Click to Show Tournament Info">Labor Day Shootout</a> <span class="tournament_date">Sep 1st - Sep 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Cleveland&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Bantam 03, Midget U15, U16, U18&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>

            </div>
            <div id="t4837" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Patrick Metzger | <strong>Phone: </strong> 216-394-1369 |
                <strong>Email: </strong> teamohiohockey@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                     href="http://teamohiohockey@gmail.com"
                                                                                                     target="_blank">http://teamohiohockey@gmail
                    .com</a>

                <br/><br/>
                4/7 Games with Round Robin and Quarterfinal Play<br/>
                <br/>
                $1500.00 per team<br/>
                <br/>
                <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4837');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_2  tournament_image">
                <span class="tournaments_style"><img src="/tournaments/images/1484162461.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4839');"
                   title="Click to Show Tournament Info">Lake Erie Cup</a> <span class="tournament_date">Oct 13th - Oct 15th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Cleveland&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Bantam 03, Midget U15, U16, U18&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>

            </div>
            <div id="t4839" class="tournament_listing_2 ">

                <strong>Contact: </strong> Patrick Metzger | <strong>Phone: </strong> 216 394-1369 |
                <strong>Email: </strong> teamohiohockey@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                     href="http://www.teamohio.com"
                                                                                                     target="_blank">http://www.teamohio.com</a>

                <br/><br/>
                4 game Guarantee. Round Robin and Medal play<br/>
                <br/>
                $1450.00 per team<br/>
                <br/>
                For more information please contact Patrick Metzger @ teamohiohockey@gmail.com. <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4839');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Pennsylvania <span class="tournament_count">2 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4954');"
                   title="Click to Show Tournament Info">Hockey Fights MS - 12th Annual Pennsylvania Women's
                    Tournament</a> <span class="subtle_nu">Adult</span> <span class="tournament_date">Aug 4th - Aug 6th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bethlehem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Women&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Women's C & D</span>


            </div>
            <div id="t4954" class="tournament_listing_2 ">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 12th Annual Hockey Fights MS Pennsylvania Women's Tournament will take place August 4-6, 2017.
                Registration is open for Women's C, D and 50+ teams. Proceeds from this event will benefit The Multiple
                Sclerosis Wellness Program at Good Shepherd Rehabilitation Network in Allentown, PA. All games will be
                played at The Steel Ice Center in Bethlehem, PA - a thriving community full of many options for
                shopping, dining, lodging and entertainment including the Sands Casion and Outlets. <br/>
                <br/>
                Also taking place during the tournament is Musikfest. Presented by ArtsQuest, this 10-day festival will
                include over 500 musical performances on 15 indoor and outdoor stages located throughout the city of
                Bethlehem. This year's performers will include Santana and Chicago. Past performers have included Stone
                Temple Pilots, Maroon 5, Boston, Fuel, Hootie & The Blowfish, Lynyrd Skynyrd, Steve Miller Band, Ke$ha,
                and many others! In addition, Musikfest will also showcase over 60 food vendors, visual arts and craft
                vendors, and a closing-night fireworks display. Hockey, live music, and an abundance of delicious food -
                what could be better?<br/>
                <br/>
                ADDITIONAL INFO:<br/>
                Divisions: Women's C & D<br/>
                4 Game Guarantee for all teams<br/>
                Individual Player Registration Fee: $95/player<br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy<br/>
                Tournament T-Shirt guaranteed to players who register before July 14, 2017<br/>
                Tournament merchandise and action photos available for purchase onsite<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4954');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_listing_1  ">

                <a class="underline" href="javascript:open_close_group('t4955');"
                   title="Click to Show Tournament Info">Hockey Fights MS - 12th Annual Pennsylvania Men's
                    Tournament</a> <span class="subtle_nu">Adult</span> <span class="tournament_date">Aug 10th - Aug 13th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Bethlehem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Men&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Men's A, B, C, & D</span>

            </div>
            <div id="t4955" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 12th Annual Hockey Fights MS Pennsylvania Men's Tournament will take place August 10-13, 2017.
                Registration is open for Men's A, B, C, and D teams. Proceeds from this event will benefit The Multiple
                Sclerosis Wellness Program at Good Shepherd Rehabilitation Network in Allentown, PA. The majority of the
                games will be played at The Steel Ice Center in Bethlehem, PA - a thriving community full of many
                options for shopping, dining, lodging and entertainment including the Sands Casino and Outlets. Games
                will also be played at The Rink at Lehigh Valley, located in Whitehall, PA.<br/>
                <br/>
                ADDITIONAL INFO:<br/>
                Divisions: Men's A, B, C, & D<br/>
                4 Game Guarantee for all teams<br/>
                Individual Player Registration Fee: $95/player<br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy.<br/>
                Tournament T-Shirt guaranteed to players who register before July 20, 2017.<br/>
                Opportunity to purchase tournament merchandise, team photos and action shots onsite as well as
                online<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4955');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Rhode Island <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4848');"
                   title="Click to Show Tournament Info">Ocean State Lobsterfest</a> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Smithfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Midget 18, Midget 16, Bantam, Peewee, Squirt, Mite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA, AA, A, A/B, B, B/C, C</span>

            </div>
            <div id="t4848" class="tournament_listing_2 ">

                <strong>Contact: </strong> Northern Vermont Wildcats | <strong>Phone: </strong> |
                <strong>Email: </strong> NVWhockey@aol.com <br/><strong>Website: </strong> <a class="underline"
                                                                                              href="http://www.northernvermontwildcats.com/lobsterfest"
                                                                                              target="_blank">http://www.northernvermontwildcats.com/lobsterfest</a>

                <br/><br/>
                The Northern Vermont Wildcats would like to personally invite your team to our 2017 Ocean State
                Lobsterfest Tournament in the gorgeous vacation areas in the state of Rhode Island! The 2017 tournament
                will be: <br/>
                <br/>
                June 2-4th<br/>
                <br/>
                The tournament will be hosted in the following areas: Cranston, Kingston, Pawtucket, Smithfield, and
                North Smithfield, RI.<br/>
                <br/>
                2017 Team Tournament Fee: $1,300<br/>
                <br/>
                We proudly hosted 60 teams from all over the United States, Canada, and Europe last year. We are on
                track to host 120 teams this year for an incredible weekend of hockey!<br/>
                <br/>
                Tournament Features:<br/>
                <br/>
                * 4 Games Guaranteed!<br/>
                <br/>
                * Contact (Checking) allowed for Bantam divisions and older!<br/>
                <br/>
                * Games will be 2-20 minute halves!<br/>
                <br/>
                * Tournament Apparel available BEFORE, DURING, and AFTER the weekend!<br/>
                <br/>
                * Team and Action Photos available!<br/>
                <br/>
                <br/>
                <br/>
                Join one of the LARGEST and BEST tournaments in the off-season, today! Don't miss out, as space will
                fill fast!<br/>
                <br/>
                <br/>
                <br/>
                We look forward to hosting your team in the Ocean State, in June! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4848');">Hide Tournament Information</a>
                </div>

            </div>

            <div class="tournament_subheading">Vermont <span class="tournament_count">1 Tournaments</span></div>
            <div class="tournament_listing_2  ">

                <a class="underline" href="javascript:open_close_group('t4957');"
                   title="Click to Show Tournament Info"> Hockey Fights MS - 10th Annual Vermont Tournament</a> <span
                        class="subtle_nu">Adult</span> <span class="tournament_date">Jul 21st - Jul 23rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Burlington&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Adult Men's, Adult Women's&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Women's (C, D), Men's (A, B, C), co-ed rec</span>

            </div>
            <div id="t4957" class="tournament_listing_2 ">

                <strong>Contact: </strong> Candice Arnold | <strong>Phone: </strong> 4845601213 |
                <strong>Email: </strong> support@hockeyfightsms.org <br/><strong>Website: </strong> <a class="underline"
                                                                                                       href="http://www.hockeyfightsms.org/"
                                                                                                       target="_blank">http://www.hockeyfightsms.org/</a>

                <br/><br/>
                The 10th Annual Hockey Fights MS Vermont Tournament will take place July 21-23, 2017. Proceeds from this
                event will benefit The MS Center at The University of Vermont. Games will be played at several rinks
                including Cairns Arena and Leddy Park Arena - located only minutes from the shores of beautiful Lake
                Champlain. Games will also be played at Stowe Arena, the University of Vermont's Gutterson Fieldhouse in
                Burlington and The Ice Center in Waterbury - minutes from the Ben & Jerry's Ice Cream Factory and Green
                Mountain Coffee Roasters. <br/>
                <br/>
                Also taking place this same weekend is the 25th Annual Vermont Brewers Festival featuring over 40
                brewers. The Brewers Festival takes place in downtown Burlington on the waterfront. Tickets can be
                purchased through the festivals website starting in May. <br/>
                <br/>
                ADDITIONAL INFO:<br/>
                <br/>
                Divisions: Women's (C, D), Men's (A, B, C), co-ed rec<br/>
                4 game guarantee starting at 12:00pm on Friday. <br/>
                Individual Player Registration Fee: $95/player<br/>
                Special gift for champions in each division and winning team's name placed on official Hockey Fights MS
                trophy.<br/>
                Special 10 year anniversary gift for all players!<br/>
                Opportunity to purchase tournament merchandise, team photos and action shots onsite as well as
                online.<br/>
                <br/>
                Please visit www.HockeyFightsMS.org for details on how to register! <br/><br/>
                <div align="right">

                    <div class="tournaments_left"></div>

                    <a class="underline" href="javascript:open_close_group('t4957');">Hide Tournament Information</a>
                </div>
            </div>
        </div>

        <div class="tournament_heading" onclick="javascript:open_close_group('can_tourns');" style="cursor:pointer;">
            Canadian Tournaments <span class="tournament_count">44 Tournaments</span></div>

        <div id="can_tourns" class="drill_td_new">
            <div class="tournament_subheading">Alberta <span class="tournament_count">3 Tournaments</span></div>
            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4913');"
                   title="Click to Show Tournament Info">Calgary Quest for the Cup</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Calgary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4913" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/3168/Calgary-Quest-for-the-Cup.html"
                                                                         target="_blank">http://ccthockey.com/3168/Calgary-Quest-for-the-Cup.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1995 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4913');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4829');"
                   title="Click to Show Tournament Info">Summit Cup</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Medicine Hat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2007&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>


            </div>
            <div id="t4829" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Kelley Kurpjuweit | <strong>Phone: </strong> 4035818978 |
                <strong>Email: </strong> k.kurp22@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                               href="http://medicinehatheat.com"
                                                                                               target="_blank">http://medicinehatheat.com</a>
                <br/><br/>
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4829');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4801');"
                   title="Click to Show Tournament Info">AAA Spring Classic</a> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Edmonton&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2010, 2009, 2008, 2007, 2006, 2005, 2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA, AA, A</span>


            </div>
            <div id="t4801" class="tournament_listing_2 ">

                <strong>Contact: </strong> Doug/Betty | <strong>Phone: </strong> 780-297-2749 | <strong>Email: </strong>
                registrar@aaaspringclassic.com <br/><strong>Website: </strong> <a class="underline"
                                                                                  href="http://aaaspringclassic.com/"
                                                                                  target="_blank">http://aaaspringclassic.com/</a>
                <br/><br/>
                <p><span><strong>Give our tournament a try!!</strong></span><br/><span><strong>Top Notch competition and a 5 game guarantee!</strong></span>
                </p>
                <p><strong>Divisions by birth year: 2010, 2009, 2008, 2007, 2006, 2005, 2004</strong></p>
                <div>&nbsp;</div>
                <div><span>&nbsp;</span><strong>Finals for all teams</strong>.&nbsp;&nbsp;Because every one pays the
                    same entry! (see last years schedule for a sampling of of our tournament games)
                </div>
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4801');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_subheading">British Columbia <span class="tournament_count">4 Tournaments</span>
            </div>
            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4912');"
                   title="Click to Show Tournament Info">BC May Madness</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Vancouver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4912" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/125/BC-May-Madness-Hockey-Tournament-presented-by-Sport-Burnaby.html"
                                                                         target="_blank">http://ccthockey.com/125/BC-May-Madness-Hockey-Tournament-presented-by-Sport-Burnaby.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1795.00 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4912');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4916');"
                   title="Click to Show Tournament Info">BC Memorial Holiday Classic</a> <span class="tournament_date">May 26th - May 28th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Vancouver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4916" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/29/BC-Memorial-Holiday-Classic-Hockey-Tournament-presented-by-Sport-Burnaby.html"
                                                                         target="_blank">http://ccthockey.com/29/BC-Memorial-Holiday-Classic-Hockey-Tournament-presented-by-Sport-Burnaby.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1795.00 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4916');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4918');"
                   title="Click to Show Tournament Info">BC Quest for the Cup</a> <span class="tournament_date">Jun 5th - Jun 4th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Vancouver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>


            </div>
            <div id="t4918" class="tournament_listing_2 ">

                <strong>Contact: </strong> Jeff Carter | <strong>Phone: </strong> 1-888-422-6526 ext. 276 | <strong>Email: </strong>
                jcarter@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/31/BC-Quest-for-the-Cup-Hockey-Tournament-presented-by-Sport-Burnaby.html"
                                                                         target="_blank">http://ccthockey.com/31/BC-Quest-for-the-Cup-Hockey-Tournament-presented-by-Sport-Burnaby.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1795.00 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4918');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4936');"
                   title="Click to Show Tournament Info">Grindstone Award Women's Charity Tournament</a> <span
                        class="tournament_date">Jul 21st - Jul 23rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: West Kelowna&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 19+ Women&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Any</span>


            </div>
            <div id="t4936" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Danielle Grundy | <strong>Phone: </strong> +1.250.863.2433 |
                <strong>Email: </strong> GrindstoneAward@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                      href="http://grindstoneaward.com/grindstone-charity-tournament/"
                                                                                                      target="_blank">http://grindstoneaward.com/grindstone-charity-tournament/</a>
                <br/><br/>
                The Grindstone Award Foundation offers participants to enjoy a 3 day event aimed to raise funds for
                female hockey players in need. Your support means you will have an impact on the lives of young female
                hockey players through fulfilling our mission to support and address the needs of female players who
                have the desire to play, but are unable to for financial reasons. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4936');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_subheading">Ontario <span class="tournament_count">35 Tournaments</span></div>
            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4870');"
                   title="Click to Show Tournament Info">13th Annual Ottawa Little Senators Spring Cup - Weekend 2</a>
                <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Ottawa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 08, 06, 04, 02&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Spring/Summer AAA</span>


            </div>
            <div id="t4870" class="tournament_listing_2 ">

                <strong>Contact: </strong> Sensplex Tournament Coordinators | <strong>Phone: </strong> 6135990227 |
                <strong>Email: </strong> tournaments@sensplex.ca <br/><strong>Website: </strong> <a class="underline"
                                                                                                    href="http://sensplex.ca/11th-annual-ottawa-little-senators-spring-cup-weekend-2-majors-girls-u10-to-u14"
                                                                                                    target="_blank">http://sensplex.ca/11th-annual-ottawa-little-senators-spring-cup-weekend-2-majors-girls-u10-to-u14</a>
                <br/><br/>
                Description
                Hosted by the Ottawa Little Senators AAA organization, this first class tournament will be headquartered
                at the state-of-the-art Bell Sensplex, 4-pad facility (3 NHL, 1 International size surfaces). Some teams
                may play games at other local facilities. Here are just some features of the Little Sens Spring Cup
                Tournament:

                NO ADMISSION CHARGE AT THE GATE!
                Prizing will be awarded to Champions & Finalists
                Awards Ceremony Following EVERY Game for Player of the Game selections from both teams
                Tournament Memento for all players
                Great competition
                Best facility anywhere for tournaments
                Fantastic hotels, shopping and amenities within 3 minutes of Tournament Headquarters (including the
                brand new Tanger Outlet Mall - just 2 minutes from the Bell Sensplex)
                Important note regarding the Major Peewee division: Please note that bodychecking will be permitted at
                this level; however, all teams will be expected to have conducted a formal bodychecking clinic for their
                players. Thank you for your cooperation in ensuring a safe tournament for all of our teams.

                IMPORTANT: Teams must be able to begin play at 8 a.m. on Friday, May 12th.

                INSURANCE: Teams should have liability coverage for their organization and proof of Insurance will be
                required upon check-in. For teams looking for affordable hockey insurance options, consider IPlayhockey
                which provides insurance for many AAA teams throughout Ottawa and beyond. For more information, contact:
                Shawn Perrier at 613-745-1352/888-361-1352 or shawn@iplayhockey.ca

                Hudson Travel Group is the exclusive travel partner for all Sensplex Tournaments. Please note that it is
                a condition of acceptance for out-of-town teams to book accomodations through this means. Accommodations
                for all events at the Sensplex facilities will be arranged on your behalf by Kelly Dooley at
                kdooley@htgsports.com or 1-800-668-5596. This will ensure your team the lowest rates on a FULL range of
                area hotels!

                Note: Registration for teams from the United States can be made in USD at the current day’s exchange
                rate less 5%.

                Format:
                Number of Teams
                Approx 50 teams

                Games Per Team
                4 Game Minimum (up to 7 possible)

                Format
                Boys divisions featured: Major Novice (08), Major Atom (06), Major Peewee (04), Major Bantam (02).

                4 round robin games (or 3 round robin followed by guaranteed playoff game) plus playoffs. Novice, Atom:
                3 X 12 minute stop time periods. Peewee, Bantam: 2 X 12, 1 X 15 minute stop time periods. Novice, & Atom
                levels will be NON-CONTACT. Major Peewee & up will be CONTACT. Forward all questions regarding this
                great event to us at (613) 599-0227 or tournaments@sensplex.ca. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4870');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">
                <a class="underline" href="javascript:open_close_group('t4914');"
                   title="Click to Show Tournament Info">Toronto Quest for the Cup</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4914" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/30/Toronto-Quest-for-the-Cup-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/30/Toronto-Quest-for-the-Cup-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4914');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4762');"
                   title="Click to Show Tournament Info">Ontario Hockey Prospects Challenge</a> <span
                        class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Kingston&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 1998-2010&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4762" class="tournament_listing_2 ">

                <strong>Contact: </strong> Joe St-Pierre | <strong>Phone: </strong> 613-3294992 |
                <strong>Email: </strong> stpierre.a@adulthockeyclassic.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://youthhockeyfestival.com" target="_blank">http://youthhockeyfestival.com</a>
                <br/><br/>
                <p align="center"><strong>BOYS U18 (98...), U16 (2000...), 2001, 2002 , 2003, 2004</strong><strong><br/>
                    </strong>12-14&nbsp; May (Mai) 2017</p>
                <p align="center">&nbsp;</p>
                <p align="center"><strong>BOYS 2005-2010 AAA</strong></p>
                <p align="center">9-11 June (Juin) 2017</p>
                <p align="center">&nbsp;</p>
                <p><strong>GIRLS U8, U10 Tier 1, U10 Tier 2, U11, U12, <br/> U13, U14, U17, U19</strong><br/> &nbsp;2-4
                    June 2017<br/></p>                    <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4762');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4842');"
                   title="Click to Show Tournament Info">GB Challenge</a> <span class="tournament_date">May 12th - May 14th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Collingwood&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2001 & 2002&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>


            </div>
            <div id="t4842" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Darin Potts | <strong>Phone: </strong> 705-606-0710 |
                <strong>Email: </strong> gbchallenge.springhockey@gmail.com <br/><strong>Website: </strong> <a
                        class="underline" href="https://beta.tournkey.ca/tournament/details/event_id/108"
                        target="_blank">https://beta.tournkey.ca/tournament/details/event_id/108</a>
                <br/><br/>
                2 Divisions - 2001 & 2002
                4 game guarantee
                Registration $1275/team

                Enjoy the renowned tourist/vacation destination of Georgian Bay (Collingwood/Blue Mountain)

                For accommodations use link below:

                http://www.visitsouthgeorgianbay.ca/ <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4842');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4915');"
                   title="Click to Show Tournament Info">Girls Victoria Day Spectacular</a> <span
                        class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4915" class="tournament_listing_2 ">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/105/Girls-Victoria-Day-Spectacular-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/105/Girls-Victoria-Day-Spectacular-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1225 CDN (HST included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4915');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4871');"
                   title="Click to Show Tournament Info">8th Annual Capital Victoria Day Classic</a> <span
                        class="tournament_date">May 20th - May 22nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Ottawa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2002- 2009 (Minor Novice - Major Bantam)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Spring AAA</span>


            </div>
            <div id="t4871" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Sensplex Tournament Coordinators | <strong>Phone: </strong> 6135990227 |
                <strong>Email: </strong> tournaments@sensplex.ca <br/><strong>Website: </strong> <a class="underline"
                                                                                                    href="http://sensplex.ca/victoriadayclassic"
                                                                                                    target="_blank">http://sensplex.ca/victoriadayclassic</a>
                <br/><br/>
                Number of Teams
                Approx 60 teams

                Games Per Team
                4 Game Guarantee (up to 6)

                Format
                Teams will compete in THREE OR FOUR preliminary round games, followed by either a guaranteed 4th game as
                playoff, or playoffs for those teams that qualify after 4 round robin games. Body Contact is permitted
                at the Major Peewee division and above - all other levels will be non-contact. All Novice & Atom level
                games will be 12-12-12 minute stop-time periods. All Peewee & Bantam level games will be 12-12-15 minute
                stop-time periods

                Description
                The Capital Victoria Day Classic is a first class tournament that will be headquartered at the
                state-of-the-art Bell Sensplex, 4-pad facility (3 NHL, 1 International size surfaces). Some teams may
                play games at other local facilities. This event will run SATURDAY thru MONDAY, meaning players will not
                have to miss any school on Friday. Here are just some features of the Capital Victoria Day Classic:

                NO ADMISSION FEE AT THE GATE
                Prizing will be awarded to Champions & Finalists
                Awards Ceremony Following EVERY Game for Player of the Game selections from both teams
                Tournament Memento for all players
                Great competition
                Best facility anywhere for tournaments
                Fantastic hotels, shopping and amenities within 3 minutes of Tournament Headquarters
                Important note regarding the Major Peewee division: Please note that bodychecking will be permitted at
                the Major Peewee level; however, all teams will be expected to have conducted a formal bodychecking
                clinic for their players. Thank you for your cooperation in ensuring a safe tournament for all of our
                teams.

                IMPORTANT: Teams must be prepared to start as early as 8 a.m. on Saturday, May 22nd (consideration will
                be given where possible to teams traveling the furthest).

                INSURANCE: Teams should have liability coverage for their organization and proof of Insurance will be
                required upon check-in. For teams looking for affordable hockey insurance options, consider IPlayhockey
                which provides insurance for many AAA teams throughout Ottawa and beyond. For more information, contact:
                Shawn Perrier at 613-745-1352/888-361-1352 or shawn@iplayhockey.ca

                Hudson Travel Group is the exclusive travel partner for all Sensplex Tournaments. Please note that it is
                a condition of acceptance for out-of-town teams to book accomodations through this means. Accommodations
                for all events at the Sensplex facilities will be arranged on your behalf by Kelly Dooley at
                kdooley@htgsports.com or 1-800-668-5596. This will ensure your team the lowest rates on a FULL range of
                area hotels!

                Note: Registration for teams from the United States can be made in USD at the current day’s exchange
                rate less 5%. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4871');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4872');"
                   title="Click to Show Tournament Info">13th Annual Ottawa Little Senators Spring Cup - Weekend 3</a>
                <span class="tournament_date">May 26th - May 28th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Ottawa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 02-09&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Girls AAA</span>


            </div>
            <div id="t4872" class="tournament_listing_2 ">

                <strong>Contact: </strong> Sensplex Tournament Coordinators | <strong>Phone: </strong> 6135990227 |
                <strong>Email: </strong> tournaments@sensplex.ca <br/><strong>Website: </strong> <a class="underline"
                                                                                                    href="http://sensplex.ca/littlesens3"
                                                                                                    target="_blank">http://sensplex.ca/littlesens3</a>
                <br/><br/>
                Description
                Hosted by the Ottawa Little Sens AAA organization, this first class tournament will be headquartered at
                the state-of-the-art Bell Sensplex 4-pad facility (3 NHL, 1 International size surfaces).Here are just
                some features of the Little Sens Spring Cup:

                Four to six games possible depending upon division size
                NO admission or gate fees charged!
                Unmatched tournament atmosphere with music
                Prizing awarded to Champions & Finalists
                Awards ceremony following every game for Player of the Game selections from both teams
                Tournament memento for all players
                Best facility anywhere for tournaments
                Extended length games featuring 3 x 12 minute periods (Novice & Atom) or 2 x 12 & 1 x 15 minute periods
                (Peewee & Bantam)
                Fantastic hotels, shopping and amenities within three minutes of tournament headquarters (including the
                brand new Tanger Outlet Mall - just 2 minutes from the Bell Sensplex)
                IMPORTANT: Teams must be able to begin play at 8 a.m. on Friday, May 26th.

                INSURANCE: Teams should have liability coverage for their organization and proof of Insurance will be
                required upon check-in. For teams looking for affordable hockey insurance options, consider IPlayhockey
                which provides insurance for many AAA teams throughout Ottawa and beyond. For more information, contact:
                Shawn Perrier at 613-745-1352/888-361-1352 or shawn@iplayhockey.ca

                Hudson Travel Group is the exclusive travel partner for all Sensplex Tournaments. Please note that it is
                a condition of acceptance for out-of-town teams to book accomodations through this means. Accommodations
                for all events at the Sensplex facilities will be arranged on your behalf by Kelly Dooley at
                kdooley@htgsports.com or 1-800-668-5596. This will ensure your team the lowest rates on a FULL range of
                area hotels!

                Note: Registration for teams from the United States can be made in USD at the current day’s exchange
                rate less 5%.

                Format:
                Number of Teams
                Approximately 40

                Games Per Team
                4 Game Minimum (up to 7 possible depending upon final division size)

                Format
                Girls divisions featured: Novice/U8 (08-09), Atom/U10 (06-07), Peewee/U12 (04-05) & Bantam/U14 (02-03).

                4 round robin games (or 3 round robin followed by guaranteed playoff game) plus playoffs. Girls U8 and
                U10: 3 X 12 minute stop time periods. Girls U12 and U14: 2 X 12, 1 X 15 minute stop time periods. All
                Girls levels will be NON-CONTACT. Forward all questions regarding this great event to us at (613)
                599-0227 or tournaments@sensplex.ca. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4872');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4917');"
                   title="Click to Show Tournament Info">Toronto Memorial Holiday Classic</a> <span
                        class="tournament_date">May 26th - May 28th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>


            </div>
            <div id="t4917" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/28/Toronto-Memorial-Holiday-Classic-Hockey-Tournaments.html"
                                                                         target="_blank">http://ccthockey.com/28/Toronto-Memorial-Holiday-Classic-Hockey-Tournaments.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4917');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4795');"
                   title="Click to Show Tournament Info">Blue Thunder Cup</a> <span class="tournament_date">May 26th - May 28th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: COLLINGWOOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2003, 2005&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>


            </div>
            <div id="t4795" class="tournament_listing_2 ">

                <strong>Contact: </strong> Roger Hannon | <strong>Phone: </strong> | <strong>Email: </strong>
                roger.hannon@sympatico.ca, bluethunderhockeydevelopment@gmail.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.bluethunderhockey.blogspot.com" target="_blank">http://www.bluethunderhockey.blogspot.com</a>
                <br/><br/>
                <p style="text-align: left;"><strong>BLUE THUNDER HOCKEY DEVELOPMENT'S ANNUAL SPRING HOCKEY TOURNAMENTS
                        IN BEAUTIFUL COLLINGWOOD/WASAGA BEACH.</strong></p>                    <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4795');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4851');"
                   title="Click to Show Tournament Info">3rd Annual Spring Fever Mens Tournament</a> <span
                        class="tournament_date">May 27th - May 27th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Cardinal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 19+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A AND B</span>

            </div>
            <div id="t4851" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> DANGLES AND DEKES | <strong>Phone: </strong> | <strong>Email: </strong>
                info@danglesanddekes.com <br/><strong>Website: </strong> <a class="underline"
                                                                            href="http://www.danglesanddekes.com"
                                                                            target="_blank">http://www.danglesanddekes.com</a>
                <br/><br/>
                3rd Annual Mens One Day Hockey Tournament
                15 Minutes East of Brockville Ontario
                $625 Guaranteed 3 Games
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4851');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4919');"
                   title="Click to Show Tournament Info">Bring Your Best</a> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4919" class="tournament_listing_2 ">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/32/Bring-Your-Best-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/32/Bring-Your-Best-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4919');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4937');"
                   title="Click to Show Tournament Info">The Weekend Classic </a> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Windsor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 19+ Competitive &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Competitive Men's League</span>

            </div>
            <div id="t4937" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dylan Denomme | <strong>Phone: </strong> 2263453101 |
                <strong>Email: </strong> denomme18@gmail.com <br/><br/>
                The 2nd Annual Weekend Classic men's league hockey tournament located at the brand new Central Park
                Athletics complex in Windsor ON. The tournament will be yet again sponsored by pro hockey life! Each
                team is guaranteed 3 games and the chance to play their way to the finals and win a cash prize of $500.
                Every team will be provided with PHL swag and there will also be MVP awards. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4937');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4796');"
                   title="Click to Show Tournament Info">Blue Thunder Cup</a> <span class="tournament_date">Jun 2nd - Jun 4th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: COLLINGWOOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2006, 2008, 2009&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>

            </div>
            <div id="t4796" class="tournament_listing_2 ">

                <strong>Contact: </strong> Roger Hannon | <strong>Phone: </strong> | <strong>Email: </strong>
                bluethunderhockeydevelopment@gmail.com, roger.hannon@gmail.com <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.bluethunderhockey.blogspot.com" target="_blank">http://www.bluethunderhockey.blogspot.com</a>
                <br/><br/>
                <p><strong>BLUE THUNDER HOCKEY DEVELOPMENT'S ANNUAL SPRING HOCKEY TOURNAMENTS IN BEAUTIFUL
                        COLLINGWOOD/WASAGA BEACH.</strong></p>                    <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4796');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4920');"
                   title="Click to Show Tournament Info">Niagara Falls Challenge I</a> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Niagara Falls&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>


            </div>
            <div id="t4920" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/34/Niagara-Falls-Youth-Challenge-1-Niagara-Falls-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/34/Niagara-Falls-Youth-Challenge-1-Niagara-Falls-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,450 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4920');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4921');"
                   title="Click to Show Tournament Info">Girls Battle of North York</a> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4921" class="tournament_listing_2 ">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/36/Girls-Battle-of-North-York-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/36/Girls-Battle-of-North-York-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1225 CDN (HST included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4921');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4797');"
                   title="Click to Show Tournament Info">Blue Thunder Cup</a> <span class="tournament_date">Jun 9th - Jun 11th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: COLLINGWOOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2004, 2007&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA, AAA</span>

            </div>
            <div id="t4797" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Roger Hannon | <strong>Phone: </strong> | <strong>Email: </strong>
                bluethunderhockeydevelopment@gmail.com, roger.hannon@sympatico.ca <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.bluethunderhockey.blogspot.com" target="_blank">http://www.bluethunderhockey.blogspot.com</a>
                <br/><br/>
                <p><strong>BLUE THUNDER HOCKEY DEVELOPMENT'S ANNUAL SPRING HOCKEY TOURNAMENTS IN BEAUTIFUL
                        COLLINGWOOD/WASAGA BEACH.</strong></p>                    <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4797');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4881');"
                   title="Click to Show Tournament Info">TEP Minor Midget Showcase</a> <span class="tournament_date">Jun 16th - Jun 18th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2002&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4881" class="tournament_listing_2 ">

                <strong>Contact: </strong> andre@tephockey.com | <strong>Phone: </strong> 416-779-0222 |
                <strong>Email: </strong> andre@tephockey.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                href="http://www.tephockey.com"
                                                                                                target="_blank">http://www.tephockey.com</a>
                <br/><br/>
                02' Birth Year <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4881');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4922');"
                   title="Click to Show Tournament Info">Summer Meltdown</a> <span class="tournament_date">Jun 16th - Jun 18th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4922" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/35/Summer-Meltdown-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/35/Summer-Meltdown-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4922');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4849');"
                   title="Click to Show Tournament Info">NIAGARA FALLS ADULT TOURNAMENT</a> <span
                        class="tournament_date">Jun 16th - Jun 18th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: NIAGARA FALLS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 19+ Mens 40+ Mens 19+ womens&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Competitive and Recreational</span>

            </div>
            <div id="t4849" class="tournament_listing_2 ">

                <strong>Contact: </strong> DANGLES AND DEKES | <strong>Phone: </strong> | <strong>Email: </strong>
                info@danglesanddekes.com <br/><strong>Website: </strong> <a class="underline"
                                                                            href="http://www.danglesanddekes.com"
                                                                            target="_blank">http://www.danglesanddekes.com</a>
                <br/><br/>
                ADULT ICE HOCKEY TOURNAMENT
                19+ COMPETITIVE 19+ RECREATIONAL
                40+ MENS 19+ WOMENS

                $950.00 PER TEAM GUARANTEED 4 GAMES
                GALE CENTER ARENA <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4849');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4923');"
                   title="Click to Show Tournament Info">OHC Girls Hockey Mania</a> <span class="tournament_date">Jun 23rd - Jun 25th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4923" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/3162/OHC-Girls-Hockey-Mania.html"
                                                                        target="_blank">http://ccthockey.com/3162/OHC-Girls-Hockey-Mania.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1225 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4923');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4873');"
                   title="Click to Show Tournament Info">7th Annual Canada Day Cup powered by Sherwood</a> <span
                        class="tournament_date">Jun 30th - Jul 2nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Ottawa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Pre-Novice, Novice, Atom, Peewee, Bantam, Girls U8, U10, U12, U14&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4873" class="tournament_listing_2 ">

                <strong>Contact: </strong> Sensplex Tournament Coordinators | <strong>Phone: </strong> 6135990227 |
                <strong>Email: </strong> tournaments@sensplex.ca <br/><strong>Website: </strong> <a class="underline"
                                                                                                    href="http://sensplex.ca/6th-annual-canada-day-cup"
                                                                                                    target="_blank">http://sensplex.ca/6th-annual-canada-day-cup</a>
                <br/><br/>
                Number of Teams
                Approx 70 teams

                Games Per Team
                4 Game Guarantee (up to 6)

                Format
                4 round robin games (or 3 round robin followed by guaranteed playoff game) plus playoffs.
                Pre-Novice/Novice/Atom (Girls U8/U10) will have 3 X 12 minutes stop time periods, Peewee/Bantam (Girls
                U12/U14) will have 2 X 12, 1 X 15 minute stop time periods. Pre-Novice, Novice, Atom, Minor Peewee & all
                Girls divisions will be non-checking. Major Peewee, Minor Bantam, and Major Bantam boys divisions will
                feature body checking. Please forward all questions regarding this great event to us at (613) 599-0227
                or tournaments@sensplex.ca.

                Description
                The 7th Annual Canada Day Cup powered by Sher-wood will be headquartered at the state-of-the-art Bell
                Sensplex (4-pad practice facility of the NHL's Ottawa Senators in west Ottawa), and the Richcraft
                Sensplex (brand new 4-pad facility in east Ottawa). The tournament will be played under (modified)
                International Ice Hockey rules. This will be sure to offer players an out-of-this-world experience like
                they've never had before! Here are just some features of the 7th Annual Canada Day Cup:

                NO ADMISSION FEE AT THE GATE
                High quality one-piece Sher-wood hockey sticks for EVERY player on EVERY championship team!
                Quality Sher-wood merchandise for EVERY player on EVERY finalist team!
                Tournament Memento for all players
                Player of the Game awards will be presented to each team following each game
                Great competition from across North America
                Canada Day Cup Tournament Party for players & spectators
                Best facility anywhere for tournaments
                Fantastic hotels, shopping and amenities within 3 minutes of tournament headquarters
                A great opportunity to visit Ottawa during Canada's 150 birthday weekend!
                Important note regarding the Major Peewee division: Please note that bodychecking will be permitted at
                the Major Peewee level; however, all teams will be expected to have conducted a formal bodychecking
                clinic for their players. Thank you for your cooperation in ensuring a safe tournament for all of our
                teams.

                IMPORTANT: Teams must be able to begin play at 8 a.m. on Friday, June 30th.

                INSURANCE: Teams should have liability coverage for their organization and proof of Insurance will be
                required upon check-in. For teams looking for affordable hockey insurance options, consider IPlayhockey
                which provides insurance for many AAA teams throughout Ottawa and beyond. For more information, contact:
                Shawn Perrier at 613-745-1352/888-361-1352 or shawn@iplayhockey.ca

                Hudson Travel Group is the exclusive travel partner for all tournaments hosted by the Sensplex
                facilities. Please note that it is a condition of acceptance for out-of-town teams to book accomodations
                through this means. Accommodations for all events at the Sensplex facilities will be arranged on your
                behalf by Kelly Dooley at kdooley@htgsports.com or 1-800-668-5596. This will ensure your team the lowest
                rates on a FULL range of area hotels!

                Note: Registration for teams from the United States can be made in USD at the current day’s exchange
                rate less 5%. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4873');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4924');"
                   title="Click to Show Tournament Info">Champions Cup of Canada</a> <span class="tournament_date">Jun 30th - Jul 2nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Oakville&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4924" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/133/Champions'-Cup-of-Canada-Oakville-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/133/Champions'-Cup-of-Canada-Oakville-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1350 CDN (tax included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4924');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4925');"
                   title="Click to Show Tournament Info">Youth Classic</a> <span class="tournament_date">Jul 7th - Jul 9th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4925" class="tournament_listing_2 ">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/97/Youth-Classic-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/97/Youth-Classic-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4925');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4926');"
                   title="Click to Show Tournament Info">Niagara Falls Challenge II</a> <span class="tournament_date">Jul 14th - Jul 16th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Niagara Falls&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4926" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/98/Niagara-Falls-Youth-Challenge-II-Niagara-Falls-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/98/Niagara-Falls-Youth-Challenge-II-Niagara-Falls-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1450 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4926');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4850');"
                   title="Click to Show Tournament Info">The Heat Is On Adult Tournament (Ottawa area)</a> <span
                        class="tournament_date">Jul 22nd - Jul 22nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Rockland&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 19+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A and B</span>

            </div>
            <div id="t4850" class="tournament_listing_2 ">

                <strong>Contact: </strong> DEKES AND DANGLES | <strong>Phone: </strong> | <strong>Email: </strong>
                info@danglesanddekes.com <br/><strong>Website: </strong> <a class="underline"
                                                                            href="http://www.danglesanddekes.com"
                                                                            target="_blank">http://www.danglesanddekes.com</a>
                <br/><br/>
                One day mens hockey tournament.

                Rockland Ontario 15-20 Minutes East of Ottawa

                $675
                3 Games Guaranteed <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4850');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4927');"
                   title="Click to Show Tournament Info">3 on 3 Nationals</a> <span class="tournament_date">Jul 28th - Jul 30th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4927" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/3170/3-on-3-North-American-Invitational.html"
                                                                         target="_blank">http://ccthockey.com/3170/3-on-3-North-American-Invitational.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1,350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4927');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4928');"
                   title="Click to Show Tournament Info">King of the Rings I</a> <span class="tournament_date">Aug 4th - Aug 6th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4928" class="tournament_listing_2 ">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/40/King-of-the-Rings-I-A-AA-Boys-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/40/King-of-the-Rings-I-A-AA-Boys-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1450 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4928');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4929');"
                   title="Click to Show Tournament Info">King of the Rings II</a> <span class="tournament_date">Aug 11th - Aug 13th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4929" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/107/King-Of-the-Rings-II-AAA-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/107/King-Of-the-Rings-II-AAA-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1450 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4929');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4930');"
                   title="Click to Show Tournament Info">Queen of the Rings</a> <span class="tournament_date">Aug 11th - Aug 13th, 2017</span>

                <br/>


                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4930" class="tournament_listing_2 ">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/132/Queen-of-the-Rings-Girls-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/132/Queen-of-the-Rings-Girls-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1450 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4930');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4931');"
                   title="Click to Show Tournament Info">Toronto Pre-Season Blast</a> <span class="tournament_date">Aug 18th - Aug 20th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4931" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Dave Fearon | <strong>Phone: </strong> 1-888-422-6526 ext. 277 | <strong>Email: </strong>
                dfearon@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://ccthockey.com/41/Toronto-Pre-Season-Blast-EARLY-BIRD-Boys-Hockey-Tournament.html"
                                                                         target="_blank">http://ccthockey.com/41/Toronto-Pre-Season-Blast-EARLY-BIRD-Boys-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1350 CDN (taxes included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4931');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4945');"
                   title="Click to Show Tournament Info">Canadiens Cup</a> <span class="tournament_date">Sep 30th - Oct 2nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2003-04-05-06-07-08&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4945" class="tournament_listing_2 ">

                <strong>Contact: </strong> canadienscup@yahoo.com | <strong>Phone: </strong> (416) 357-2385 | <strong>Email: </strong>
                canadienscup@yahoo.com <br/><strong>Website: </strong> <a class="underline"
                                                                          href="http://www.canadienscup.com"
                                                                          target="_blank">http://www.canadienscup.com</a>
                <br/><br/>
                THE CANADIENS CUP Tyler Cragg Memorial tournament is Toronto's fastest growing AAA hockey tournament.
                Come to Toronto at the best time of the year and compete against some of the best hockey teams from
                across North America.

                Brand new facilities and world class hotels. The Hockey all of fame is close by and a great treat for
                the kids. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4945');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4952');"
                   title="Click to Show Tournament Info">Everest Academy Prep School Showcase </a> <span
                        class="tournament_date">Oct 13th - Oct 15th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Prep School/High School&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: PrepSchool/High School</span>

            </div>
            <div id="t4952" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> tverberg@rogers.com | <strong>Phone: </strong> (416) 357-2385 | <strong>Email: </strong>
                tverberg@rogers.com <br/><br/>
                Everest Academy Prep School Showcase is hosted at the fabulous Pavilion Ice.

                4 game guarantee

                Open to Prep and High School hockey teams from across Canada and the US <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4952');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4959');"
                   title="Click to Show Tournament Info">Scotiabank Cup hosted by Mississauga North Stars</a> <span
                        class="tournament_date">Oct 20th - Oct 22nd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: MISSISSAUGA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2008 to Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AA</span>

            </div>
            <div id="t4959" class="tournament_listing_2 ">

                <strong>Contact: </strong> 9052772736 | <strong>Phone: </strong> 9052772736 | <strong>Email: </strong>
                mark.turkiewicz@lacapitale.com <br/><strong>Website: </strong> <a class="underline"
                                                                                  href="http://www.northstarstourney.com"
                                                                                  target="_blank">http://www.northstarstourney.com</a>
                <br/><br/>
                Hosted by the Mississauga North Stars for 20 years, GTHL and Hockey Canada sanctioned.
                4 games guaranteed, no gate fees.
                AA divisions from Minor Atom to Midget. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4959');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4894');"
                   title="Click to Show Tournament Info">2017 Shanahan</a> <span class="tournament_date">Nov 3rd - Nov 5th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Toronto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2008, 2007, 2006, 2005, 2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4894" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> 6475233437 | <strong>Phone: </strong> 4168791869 | <strong>Email: </strong>
                repsgm@rogers.com <br/><strong>Website: </strong> <a class="underline"
                                                                     href="http://www.repshockey.com/tournaments.php?who=8&whois=shanahan"
                                                                     target="_blank">http://www.repshockey.com/tournaments.php?who=8&whois=shanahan</a>
                <br/><br/>
                Elite AAA ice hockey tournament for 08, 07, 06, 05 and 04 teams
                - 4 game minimum
                - 3x15min stop time periods <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4894');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4883');"
                   title="Click to Show Tournament Info">Cogeco Horseshoe AAA Hockey Tournament</a> <span
                        class="tournament_date">Dec 27th - Dec 30th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Burlington&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Tyke, Novice, Atom, Peewee, Bantam, Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4883" class="tournament_listing_2 ">

                <strong>Contact: </strong> Kevin Charbonneau | <strong>Phone: </strong> 9053991807 |
                <strong>Email: </strong> kcharbonneau@theshoe.ca <br/><strong>Website: </strong> <a class="underline"
                                                                                                    href="http://www.TheShoe.ca"
                                                                                                    target="_blank">http://www.TheShoe.ca</a>
                <br/><br/>
                Every participant receives tournament takeaway. Division champions and runners up receive individual and
                team awards. Divisional MVP plaques and prize (hockey stick). Special effects, lighting and individual
                introductions during Finals and Opening night ceremonies. International teams from Finland, Russia,
                Japan & US have taken part in recent years. Burlington, Ontario one of the “top ten cities to live”.
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4883');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_subheading">Quebec <span class="tournament_count">3 Tournaments</span></div>
            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4934');"
                   title="Click to Show Tournament Info">Montreal Youth Fall Classic I</a> <span
                        class="tournament_date">Nov 24th - Nov 26th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Montreal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4934" class="tournament_listing_2 ">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/120/Montreal-Youth-Fall-Classic-1-Boys-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/120/Montreal-Youth-Fall-Classic-1-Boys-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1475 CDN (HST included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4934');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4935');"
                   title="Click to Show Tournament Info">Montreal Youth Fall Classic II</a> <span
                        class="tournament_date">Dec 1st - Dec 3rd, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Montreal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: Initiation - Major Midget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: Select - AAA</span>

            </div>
            <div id="t4935" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Gidon Frank | <strong>Phone: </strong> 1-888-422-6526 ext. 273 | <strong>Email: </strong>
                gfrank@icesports.com <br/><strong>Website: </strong> <a class="underline"
                                                                        href="http://ccthockey.com/2150/Montreal-Youth-Fall-Classic-2-Boys-Hockey-Tournament.html"
                                                                        target="_blank">http://ccthockey.com/2150/Montreal-Youth-Fall-Classic-2-Boys-Hockey-Tournament.html</a>
                <br/><br/>
                Tournament Features:
                - 5 games minimum, 6 games maximum
                - Special rates for Group Hotels
                - Non-checking options (boys divisions)
                - No gate fees
                - Impressive, newly designed awards
                - Live online scores, stats and standings

                Cost:
                $1475 CDN (HST included) <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4935');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 tournament_featured">
                <span class="tournaments_style"><img src="/tournaments/images/1491772047.jpg" height="50"
                                                                   width="50" border="0"
                                                                   alt="Featured Tournament Logo"/></span>
                <a class="underline" href="javascript:open_close_group('t4950');"
                   title="Click to Show Tournament Info">Granby International Bantam Tournament</a> <span
                        class="tournament_date">Feb 7th - Feb 18th, 2018</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Granby&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2003-2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA major, AAA minor, AA, BB, A, B</span>

            </div>
            <div id="t4950" class="tournament_listing_2 tournament_featured" class="drill_td_new">

                <strong>Contact: </strong> normcote1@videotron.ca | <strong>Phone: </strong> 450-775-2253 | <strong>Email: </strong>
                normcote1@videotron.ca <br/><strong>Website: </strong> <a class="underline"
                                                                          href="http://www.tournoibantamgranby.ca"
                                                                          target="_blank">http://www.tournoibantamgranby.ca</a>
                <br/><br/>
                48th Edition.

                • AAA Minor, AA and B categories will play from Thursday the 8th up to Sunday the 11th.
                • AAA Major, BB and A categories will play from Thursday the 15th up to Sunday the 18th.
                • That’s 3 games ensured, before quarter finals, semi and final. Possibility of 6 total games for teams
                making it up to the last one.
                • Open to teams with kids born in 2003 or 2004.
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4950');">Hide Tournament
                        Information</a></div>

            </div>

        </div>

        <div class="tournament_heading" onclick="javascript:open_close_group('int_tourns');" style="cursor:pointer;">
            International Tournaments <span class="tournament_count">5 Tournaments</span></div>

        <div id="int_tourns" class="drill_td_new">

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4900');"
                   title="Click to Show Tournament Info">Okanagan Hockey UK Academy Spring Showcase</a> <span
                        class="tournament_date">May 19th - May 21st, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: Swindon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 1999,2000,2001&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4900" class="tournament_listing_2 ">

                <strong>Contact: </strong> Paul Kelsall | <strong>Phone: </strong> +44 1793 581554 |
                <strong>Email: </strong> paul.kelsall@hockwyschools.co.uk <br/><strong>Website: </strong> <a
                        class="underline" href="http://www.hockeyschools.co.uk" target="_blank">http://www.hockeyschools.co.uk</a>
                <br/><br/>
                The Okanagan Hockey UK Academy is pleased to invite you to its Spring Showcase tournament. International
                teams are invited to join Okanagan UK and Okanagan Europe for what will be a special event featuring top
                U18 talent.
                Hotel, transport and food packages are available. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4900');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4844');"
                   title="Click to Show Tournament Info">Midnight Sun Tournament</a> <span
                        class="subtle_nu">Adult</span> <span class="tournament_date">Jun 23rd - Jun 24th, 2017</span>
                <br/>

                <span class="subtle_nu"><strong>City</strong>: Dordrecht&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: At least 18 years old&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: C/D and below</span>

            </div>
            <div id="t4844" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> Hans Verhoog | <strong>Phone: </strong> +31642605823 |
                <strong>Email: </strong> <br/><strong>Website: </strong> <a class="underline"
                                                                            href="http://flyingdutchmenhockey.nl/midnight-sun-tournament/"
                                                                            target="_blank">http://flyingdutchmenhockey.nl/midnight-sun-tournament/</a>
                <br/><br/>
                In 2017 the Flying Dutchmen of Hockey will organize their first Midnight Sun Tournament, taking place
                during one of the shortest nights of the year.

                8 teams from all over the world will compete through the night at this first (of hopefully many annual)
                edition of the Midnight Sun Tournament. The games start at Friday June 23 20:00 and continue through
                Saturday June 24 10:00 after which we’ll enjoy a hearty breakfast together! Picture two players fighting
                for puck possession as the sun’s first rays can be seen at 3 in the morning!

                It will be an intense tournament, with players

                Aside from the ice hockey this will be a fun experience with live music, live streaming of games in the
                Ice Bar, and good food, all while enjoying just a few hours of darkness. <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4844');">Hide Tournament
                        Information</a></div>

            </div>

            <div class="tournament_listing_2 ">

                <a class="underline" href="javascript:open_close_group('t4885');"
                   title="Click to Show Tournament Info">OSTRAVA HOCKEY GAMES</a> <span class="tournament_date">Aug 19th - Aug 20th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: OSTRAVA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4885" class="tournament_listing_2 ">

                <strong>Contact: </strong> DAVID MORAVEC HOCKEY ACADEMY | <strong>Phone: </strong> +420 724 891 043 |
                <strong>Email: </strong> moravecacademy@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                     href="http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html"
                                                                                                     target="_blank">http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html</a>
                <br/><br/>
                OSTRAVA HOCKEY GAMES 2017
                AAA Ice Hockey Tournament
                The basic information on the tournament for players born 2005 and 2004
                16 teams in each category
                Matches: 6
                Length of game: Qualifying group 3 x 12 min
                Final group 3 x 15 min

                Level Hockey:
                AAA
                participating in the best youth teams from the Czech Republic, Slovakia, Latvia, Austria, Scandinavia
                and more
                Minimum number of players per team is 15 + 2 goalkeepers
                The participation fee is 8,500 CZK - € 330
                Price of accommodation: 650 CZK € 27 per person per night with breakfast.
                Accommodation is provided by the organizer nearby venue
                Price per meal - lunch / dinner CZK 100 - € 4 per meal.
                More info: http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html

                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4885');">Hide Tournament
                        Information</a></div>
            </div>

            <div class="tournament_listing_1 ">

                <a class="underline" href="javascript:open_close_group('t4886');"
                   title="Click to Show Tournament Info">OSTRAVA HOCKEY GAMES</a> <span class="tournament_date">Aug 26th - Aug 27th, 2017</span>

                <br/>

                <span class="subtle_nu"><strong>City</strong>: OSTRAVA - CZECH REPUBLIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: AAA</span>

            </div>
            <div id="t4886" class="tournament_listing_1 " class="drill_td_new">

                <strong>Contact: </strong> DAVID MORAVEC HOCKEY ACADEMY | <strong>Phone: </strong> +420 724 891 043 |
                <strong>Email: </strong> moravecacademy@gmail.com <br/><strong>Website: </strong> <a class="underline"
                                                                                                     href="http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html"
                                                                                                     target="_blank">http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html</a>
                <br/><br/>
                OSTRAVA HOCKEY GAMES 2017 AAA Ice Hockey Tournament The basic information on the tournament for players
                born 2005 and 2004 16 teams in each category Matches: 6 Length of game: Qualifying group 3 x 12 min
                Final group 3 x 15 min Level Hockey: AAA participating in the best youth teams from the Czech Republic,
                Slovakia, Latvia, Austria, Scandinavia and more Minimum number of players per team is 15 + 2 goalkeepers
                The participation fee is 8,500 CZK - € 330 Price of accommodation: 650 CZK € 27 per person per night
                with breakfast. Accommodation is provided by the organizer nearby venue Price per meal - lunch / dinner
                CZK 100 - € 4 per meal. More info: http://www.moravecacademy.cz/aktuality/ostravske-hokejove-hry.html
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4886');">Hide Tournament
                        Information</a></div>

            </div>
            <div class="tournament_listing_2 ">
                <a class="underline" href="javascript:open_close_group('t4897');"
                   title="Click to Show Tournament Info">E2 Leksa 2018, 31.3-1.4</a> <span class="tournament_date">Mar 31st - Apr 1st, 2018</span>
                <br/>
                <span class="subtle_nu"><strong>City</strong>: Järvenpää, Finland&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Divisions</strong>: 2006&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Levels</strong>: A,AA,AAA</span>

            </div>
            <div id="t4897" class="tournament_listing_2 ">

                <strong>Contact: </strong> Vili Pesu | <strong>Phone: </strong> +358405716314 | <strong>Email: </strong>
                turnaus@leksa2018.com <br/><strong>Website: </strong> <a class="underline"
                                                                         href="http://www.leksa2018.com"
                                                                         target="_blank">http://www.leksa2018.com</a>
                <br/><br/>
                Järvenpään Haukat E2 / 06.
                31.3-1.4.2018 Leksa is the oldest E2 junior tournament organized in Finland without interrupt,
                tournament will be played in Järvenpää *about 30 km north of Helsinki. The tournament is played on three
                levels, A, AA and AAA, three ice rinks: Järvenpää Halls 1 and 2 and the ice rink in Kellokoski.
                If you are interested in participating in the tournament in spring 2018 or want more information,
                contact: turnaus@leksa2018.com
                Information about the tournament is published on www.leksa2018.com later on this spring 2017.
                Google map:
                https://www.google.fi/maps/place/J%C3%A4rvenp%C3%A4%C3%A4n+J%C3%A4%C3%A4halli/@
                60.4818416,25.0872754,15z/data=!4m2!3m1!1s0x0:0x6f56d45e622fcdef?sa=X&ved=0ahUKEwjx0Jmag9jSAhVJCywKHSgrDGQQ_BIIYTAK
                <br/>
                <div align="right"><a class="underline" href="javascript:open_close_group('t4897');">Hide Tournament
                        Information</a></div>
            </div>
        </div>

        <p align="center" class="subtle_nu">Links provided on this page are provided for reference only. In no way does
            the listing on this or subsequently related pages imply endorsement of linked resources. Please use
            discretion when booking tournaments. If you come across any listing on this page which is inaccurate or
            faudulent, please <a class="underline" href="/interact/contact.php">contact</a> us immediately.</p>

        @include('includes.commercial')
    </div>
</div>