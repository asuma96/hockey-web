@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Subscribe to the HockeyShare Newsletter</h1>

        <p class="p1">Subscribe to the HockeyShare Newsletter to receive the latest hockey drills, site updates, sale
            items, youth hockey news, and more right to your email. We never give out our subscriber list! You can
            unsubscribe at any time.</p>

        <form action="" method="post">
            <table class="newsletter" width="100%" border="0" cellspacing="3" cellpadding="3">
                <tr>
                    <td>Email:</td>
                    <td><input name="email" type="text" size="35" maxlength="255"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input checked="checked" name="newsletter" type="checkbox" value="1"/>
                        HockeyShare Newsletter <br/>
                        <input checked="checked" name="featured" type="checkbox" value="1"/>
                        Featured Drills
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input name="Submit" type="submit" value="  subscribe  "/></td>
                </tr>
            </table>
        </form>
        @include('includes.commercial')
        <br>
    </div>
</div>

