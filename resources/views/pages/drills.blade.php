@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <h1>Free Ice Hockey Drills</h1>
        <p>HockeyShare has one of the largest online hockey databases on the net. We're proud to offer all our drills
            absolutely free to view and print. Our hockey drill database continues to grow as users contribute drills to
            the community. To diagram your own drills and create your own practice plans, please visit our <a
                    href="http://hockeyshare.com/acp/learn-more/">Drill Diagrammer &amp; Practice Planner
                information</a> page.</p>

        <table class="drill_table_one">
            <tr>
                <td valign="top" width="715">

                    @if(Auth::check())
                        <div class="drills_div_new">
                            <div class="drill_div_new">Filter Drill List - <a
                                        href="javascript:open_close_group('filterForm');">Show/Hide</a></div>

                            <form name="filterForm" id="filterForm">

                                <table width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td width="125"><strong>Drill Options: </strong></td>
                                        <td><input type="checkbox" name="options[]" value="new"
                                                   onchange="saveFilters('options','new');"> Newly Added Only
                                            <input type="checkbox" name="options[]" value="video"
                                                   onchange="saveFilters('options','video');"> Drills w/ Videos Only
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>My Options: </strong></td>
                                        <td><input type="checkbox" name="options[]" value="favs"
                                                   onchange="saveFilters('options','favs');"> My Favorites Only
                                            <input type="checkbox" name="options[]" value="showall"
                                                   onchange="saveFilters('options','showall');"> Show Drills I've Hidden
                                            <input type="checkbox" name="options[]" value="newwindow"
                                                   onchange="saveFilters('options','newwindow');"> Open Drills in New
                                            Window
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Drill Source: </strong></td>
                                        <td><input type="checkbox" name="options[]" value="video"
                                                   onchange="checkDrillSource('options','hs');" id="source_1"
                                                   checked="checked"> Drills from HockeyShare
                                            <input type="checkbox" name="options[]" value="video"
                                                   onchange="checkDrillSource('options','user');" checked="checked"
                                                   id="userdrills"> User-Submitted Drills <span
                                                    class="" drill_input
                                                    id="source_error">Check at least one drill source</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Drill Type/Lang: </strong></td>
                                        <td><input type="checkbox" name="half[]" value="0"
                                                   onchange="checkDrillIce('half','0');" checked="checked"
                                                   id="ice_full"> Full-Ice
                                            <input type="checkbox" name="half[]" value="1"
                                                   onchange="checkDrillIce('half','1');" checked="checked"
                                                   id="ice_half"> ½ Ice <span
                                                    class="" drill_input
                                                    id="ice_error">&lt;-- Check one</span>
                                            <input type="checkbox" name="drilllang" value="en"
                                                   onchange="checkDrillLang('drilllang','en');" checked="checked"
                                                   id="lang_en"> English
                                            <input type="checkbox" name="drilllang" value="fr"
                                                   onchange="checkDrillLang('drilllang','fr');" id="lang_fr"> French
                                            <span class="drill_input"
                                                  id="lang_error">&lt;-- Check one</span></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><strong>Age Level: </strong></td>
                                        <td><input type="checkbox" name="ageid[]" value="1"
                                                   onchange="checkDrillAge('ageid','1');" checked="checked" id="age_1">
                                            Novice/Mite
                                            <input type="checkbox" name="ageid[]" value="2"
                                                   onchange="checkDrillAge('ageid','2');" checked="checked" id="age_2">
                                            Atom/Squirt
                                            <input type="checkbox" name="ageid[]" value="3"
                                                   onchange="checkDrillAge('ageid','3');" checked="checked" id="age_3">
                                            Peewee
                                            <input type="checkbox" name="ageid[]" value="4"
                                                   onchange="checkDrillAge('ageid','4');" checked="checked" id="age_4">
                                            Bantam
                                            <input type="checkbox" name="ageid[]" value="5"
                                                   onchange="checkDrillAge('ageid','5');" checked="checked" id="age_5">
                                            Midget/HS
                                            <input type="checkbox" name="ageid[]" value="6"
                                                   onchange="checkDrillAge('ageid','6');" checked="checked" id="age_6">
                                            Junior &amp; Above<br><span
                                                    class="" drill_input
                                                    id="age_error">Select at least one age level</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>

                            <div id="filterSave"
                                 class="drill_filter"></div>
                        </div>
                    @else
                        <div class="string_table">
                            <div class="Filter_drills">Filter Drill List - <a
                                        class="underline"
                                        href="javascript:open_close_group('filterForm');">Show/Hide</a></div>
                            <p class="login_drills"><a class="underline" href="http://hockeyshare.com/login/">Log
                                    In</a> or <a class="underline"
                                                 href="http://hockeyshare.com/register/">register</a> for a free
                                account to
                                filter the drill list below.</p>
                            @endif

                            <div id="filterSave"
                                 class="filterSave"></div>
                        </div>

                        <table class="drill_table_two">
                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(25, 'drillname', 0)">Agility<a name="category_25">
                                            &nbsp;</a>
                                        <span id="found_25"></span> <span
                                                id="new_25">0 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_25" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(11, 'drillname', 0)">Backchecking<a name="category_11">
                                            &nbsp;</a> <span id="found_11"></span> <span
                                                id="new_11">27 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_11" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(6, 'drillname', 0)">Competitive<a name="category_6">
                                            &nbsp;</a>
                                        <span id="found_6"></span> <span
                                                id="new_6">199 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_6" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(26, 'drillname', 0)">Defense<a name="category_26">
                                            &nbsp;</a>
                                        <span id="found_26"></span> <span
                                                id="new_26">0 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_26" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(14, 'drillname', 0)">Dryland Training<a
                                                name="category_14">
                                            &nbsp;</a> <span id="found_14"></span> <span
                                                id="new_14">16 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_14" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(27, 'drillname', 0)">Forwards<a name="category_27">
                                            &nbsp;</a>
                                        <span id="found_27"></span> <span
                                                id="new_27">0 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_27" class="drill_td_new">
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(8, 'drillname', 0)">Goalie<a name="category_8">
                                            &nbsp;</a>
                                        <span
                                                id="found_8"></span> <span
                                                id="new_8">74 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_8" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(24, 'drillname', 0)">Import<a name="category_24">
                                            &nbsp;</a>
                                        <span id="found_24"></span> <span
                                                id="new_24">0 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_24" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(3, 'drillname', 0)">Passing<a name="category_3">
                                            &nbsp;</a>
                                        <span id="found_3"></span> <span
                                                id="new_3">189 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_3" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(1, 'drillname', 0)">Puck Control<a name="category_1">
                                            &nbsp;</a>
                                        <span id="found_1"></span> <span
                                                id="new_1">47 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_1" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(4, 'drillname', 0)">Shooting<a name="category_4">
                                            &nbsp;</a>
                                        <span id="found_4"></span> <span
                                                id="new_4">104 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_4" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(5, 'drillname', 0)">Skating<a name="category_5">
                                            &nbsp;</a>
                                        <span id="found_5"></span> <span
                                                id="new_5">78 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_5" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(7, 'drillname', 0)">Small Game<a name="category_7">
                                            &nbsp;</a>
                                        <span id="found_7"></span> <span
                                                id="new_7">77 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_7" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(13, 'drillname', 0)">Stations<a name="category_13">
                                            &nbsp;</a>
                                        <span id="found_13"></span> <span
                                                id="new_13">41 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_13" class="drill_td_new">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(2, 'drillname', 0)">Stickhandling<a name="category_2">
                                            &nbsp;</a> <span id="found_2"></span> <span
                                                id="new_2">38 Total </span></div>


                                </td>
                            </tr>
                            <tr>
                                <td id="cat_2" class="drill_td_new">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(9, 'drillname', 0)">Systems<a name="category_9">
                                            &nbsp;</a>
                                        <span id="found_9"></span> <span
                                                id="new_9">53 Total </span></div>
                                </td>
                            </tr>
                            <tr>

                                <td id="cat_9" class="drill_td_new">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(10, 'drillname', 0)">Timing<a name="category_10">
                                            &nbsp;</a>
                                        <span id="found_10"></span> <span
                                                id="new_10">188 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_10" class="drill_td_new">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div
                                            onclick="loadDrills(12, 'drillname', 0)">Warmup<a name="category_12">
                                            &nbsp;</a>
                                        <span id="found_12"></span> <span
                                                id="new_12">77 Total </span></div>
                                </td>
                            </tr>
                            <tr>
                                <td id="cat_12" class="drill_td_new">
                                </td>
                            </tr>

                        </table>
                        <p>&nbsp;</p>
                        <div align="center"><a href="http://www.drawlaxdrills.com/demo/?hs_a_id=1036"
                                               target="_blank"><img
                                        src="//d3e9hat72fmrwm.cloudfront.net/images/drills/dld-promo.jpg" height="121"
                                        width="655"
                                        alt="DrawLaxDrills.com Online Lacrosse Drill Diagrammer &amp; Practice Planner"
                                        border="0"/></a></div>

                </td>
                <td width="235" valign="top" class="table_drills">
                    <div class="drills_subheader drill_div">Drill Count</div>
                    <p><strong>1,213</strong> Drills Displayed</p>
                    <p><strong>1,213</strong> Total Public Drills</p>
                    <p><strong>416,347</strong> <a class="underline" href="http://hockeyshare.com/acp/learn-more/">Drills
                            Drawn on HockeyShare</a></p>
                    <br>
                    @if (!Auth::check())
                        <div class="drills_subheader drill_div">Upgrade Your Account</div>

                        <ul>
                            <li>Draw &amp; Animate Drills</li>
                            <li>Create Practice Plans</li>
                            <li>No Drill or Practice Limit</li>
                            <li><a href="/acp/learn-more/" class="underline margin_drills">Learn More</a></li>
                        </ul>
                    @else
                    @endif
                    <br>
                    <div class="drills_subheader drill_div">Categories</div>
                    <br>
                    <div class="drills_ul">
                        <div><a href="#category_25">Agility</a></div>
                        <div><a href="#category_11">Backchecking</a></div>
                        <div><a href="#category_6">Competitive</a></div>
                        <div><a href="#category_26">Defense</a></div>
                        <div><a href="#category_14">Dryland Training</a></div>
                        <div><a href="#category_27">Forwards</a></div>
                        <div><a href="#category_8">Goalie</a></div>
                        <div><a href="#category_24">Import</a></div>
                        <div><a href="#category_3">Passing</a></div>
                        <div><a href="#category_1">Puck Control</a></div>
                        <div><a href="#category_4">Shooting</a></div>
                        <div><a href="#category_5">Skating</a></div>
                        <div><a href="#category_7">Small Game</a></div>
                        <div><a href="#category_13">Stations</a></div>
                        <div><a href="#category_2">Stickhandling</a></div>
                        <div><a href="#category_9">Systems</a></div>
                        <div><a href="#category_10">Timing</a></div>
                        <div><a href="#category_12">Warmup</a></div>
                    </div>
                    <br>
                </td>
            </tr>
        </table>
        @include('includes.commercial')
        <br>
    </div>
</div>