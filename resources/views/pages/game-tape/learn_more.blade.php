@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>

        <div class="breadcrumb"><span class="breadcrumb_title">Game Tape Review</span>&nbsp;<a
                    href="http://hockeyshare.com/game-tape/" class="breadcrumb_link">Game Tape Home</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/game-tape/manage.php?id=" class="breadcrumb_link">Association</a>
        </div>
        <h1>Game Tape Review</h1>
        <p>HockeyShare's Game Tape Review feature allows you to make notes for your game film and share them with your
            players. Below is an overview video covering the basic functionality of the section. Currently this feature
            is in beta testing. If you are interested in becoming a beta tester for this feature, please contact us.</p>

        <div class="center">
            <iframe width="853" height="480" src="//www.youtube.com/embed/ezo9CCvbFNo?rel=0" frameborder="0"
                    allowfullscreen></iframe>
        </div>

        @include('includes.commercial')
        <br>
    </div>
</div>

