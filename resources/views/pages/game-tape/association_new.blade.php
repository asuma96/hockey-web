@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">

        <br/>
        <h2>Please Register or Log In to Continue</h2>
        <p>In order to access this resource, you must <a href="http://hockeyshare.com/login/">log in</a> or <a
                    href="http://.hockeyshare.com/register/">register</a>.</p>
        @include('includes.commercial')
        <br>
    </div>
</div>

