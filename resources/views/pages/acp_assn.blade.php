@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        <br/>
        <div class="breadcrumb"><span class="breadcrumb_title">Advanced Coaching Platform</span>&nbsp;<a
                    href="http://hockeyshare.com/drills/my-drills/" class="breadcrumb_link">My Drills</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/plans/"
                                                               class="breadcrumb_link">Practice Plans</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email-lists/"
                                                               class="breadcrumb_link">Email Lists</a><span
                    class="bradcrumb_divider">&raquo;</span><a href="http://hockeyshare.com/acp/email/history/"
                                                               class="breadcrumb_link">Email Tracking</a><span
                    class="bradcrumb_divider">&raquo;</span><a
                    href="http://hockeyshare.com/drills/practiceplans/edit_customlogo.php" class="breadcrumb_link">Custom
                Logo</a></div>

        <h2>Advanced Coaching Platform - Team / Association Upgrade</h2>

        <form method="post" action="acp_assn.php" id="checkoutForm" name="checkoutForm">

            <table border="0" width="100%">
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2"
                                    class="assn_td">
                                    Billing Information
                                </td>
                            </tr>
                            <tr>
                                <td width="165">Username:</td>
                                <td>{{--{{$email}}--}}</td>
                            </tr>
                            <tr>
                                <td>First Name:</td>
                                <td><input type="text" name="first" value="{{--{{$name}}--}}"/></td>
                            </tr>
                            <tr>
                                <td>Last Name:</td>
                                <td><input type="text" name="last" value="{{--{{$last}}--}}"/></td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td><input type="text" name="address" value=""/></td>
                            </tr>
                            <tr>
                                <td>Address (cont):</td>
                                <td><input type="text" name="address2" value=""/>
                                    <span class="subtle_nu">Optional</span></td>
                            </tr>
                            <tr>
                                <td>City:</td>
                                <td><input type="text" name="city" value=""/></td>
                            </tr>
                            <tr>
                                <td>State/Prov:</td>
                                <td><select name="state" id="state" onchange="showTotal();">
                                        <option value="" selected="selected" disabled="disabled">-- State/Prov --
                                        </option>
                                        <option value="NA"> -- N/A --</option>
                                        <option value="AB">Alberta</option>
                                        <option value="BC">British Columbia</option>
                                        <option value="MB">Manitoba</option>
                                        <option value="NB">New Brunswick</option>
                                        <option value="NL">Newfoundland and Labrador</option>
                                        <option value="NT">Northwest Territories</option>
                                        <option value="NU">Nunavut</option>
                                        <option value="NS">Nova Scotia</option>
                                        <option value="ON">Ontario</option>
                                        <option value="PE">Prince Edward Island</option>
                                        <option value="QC">Quebec</option>
                                        <option value="SK">Saskatchewan</option>
                                        <option value="YT">Yukon</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>Zip/Postal:</td>
                                <td><input type="text" name="zip" value=""/></td>
                            </tr>
                            <tr>
                                <td>Country:</td>
                                <td><select name="country">
                                        <option value="" selected="selected" disabled="disabled">-- Country --</option>
                                        <option value="CA">Canada</option>
                                        <option value="US">United States</option>
                                        <option value="" disabled="disabled">--------------</option>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AX">Aland Islands</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AQ">Antarctica</option>
                                        <option value="AG">Antigua And Bar...</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BA">Bosnia And Herz...</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BV">Bouvet Island</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian ...</option>
                                        <option value="BN">Brunei Darussal...</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM">Cameroon</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African...</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CX">Christmas Islan...</option>
                                        <option value="CC">Cocos (keeling)...</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, The Demo...</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">C�te D'ivoire</option>
                                        <option value="HR">Croatia</option>
                                        <option value="CU">Cuba</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Repub...</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guin...</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Island...</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesi...</option>
                                        <option value="TF">French Southern...</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GG">Guernsey</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard Island An...</option>
                                        <option value="VA">Vatican City St...</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IR">Iran, Islamic R...</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IM">Isle Of Man</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JE">Jersey</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KP">Korea, Democrat...</option>
                                        <option value="KR">Korea, Republic...</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Lao People's De...</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libyan Arab Jam...</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macao</option>
                                        <option value="MK">Macedonia, The ...</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Island...</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia, Fed...</option>
                                        <option value="MD">Moldova, Republ...</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="ME">Montenegro</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="AN">Netherlands Ant...</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Marian...</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PS">Palestinian Ter...</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guine...</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">R�union</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russian Federat...</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="SH">Saint Helena</option>
                                        <option value="KN">Saint Kitts And...</option>
                                        <option value="LC">Saint Lucia</option>
                                        <option value="PM">Saint Pierre An...</option>
                                        <option value="VC">Saint Vincent A...</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome And Pr...</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="RS">Serbia</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SK">Slovakia</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="GS">South Georgia</option>
                                        <option value="ES">Spain</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SD">Sudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard And Ja...</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="SY">Syrian Arab Rep...</option>
                                        <option value="TW">Taiwan, Provinc...</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania, Unite...</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TL">Timor-leste</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad And To...</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks And Caico...</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emi...</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="UM">United States M...</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Viet Nam</option>
                                        <option value="VG">Virgin Islands,...</option>
                                        <option value="VI">Virgin Islands,...</option>
                                        <option value="WF">Wallis And Futu...</option>
                                        <option value="EH">Western Sahara</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td colspan="2"
                                    class="assn_td">
                                    Payment Details
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Credit Card Number:</td>
                                <td valign="top"><input type="text" id="ccnum" name="ccnum" value=""/>
                                    <span class="subtle_nu">No dashes or spaces</span></td>
                            </tr>
                            <tr>
                                <td valign="top">Expiration Date:</td>
                                <td valign="top"><select name="ccexp_mon" id="ccexp_mon">
                                        <option value=""> -- Month --</option>
                                        <option value="01">01 - January</option>
                                        <option value="02">02 - February</option>
                                        <option value="03">03 - March</option>
                                        <option value="04">04 - April</option>
                                        <option value="05" selected="selected">05 - May</option>
                                        <option value="06">06 - June</option>
                                        <option value="07">07 - July</option>
                                        <option value="08">08 - August</option>
                                        <option value="09">09 - September</option>
                                        <option value="10">10 - October</option>
                                        <option value="11">11 - November</option>
                                        <option value="12">12 - December</option>
                                    </select>
                                    <select name="ccexp_yr" id="ccexp_yr">
                                        <option value=""> -- Year --</option>
                                        <option value="2017" selected="selected">
                                            2017
                                        </option>
                                        <option value="2018">
                                            2018
                                        </option>
                                        <option value="2019">
                                            2019
                                        </option>
                                        <option value="2020">
                                            2020
                                        </option>
                                        <option value="2021">
                                            2021
                                        </option>
                                        <option value="2022">
                                            2022
                                        </option>
                                        <option value="2023">
                                            2023
                                        </option>
                                        <option value="2024">
                                            2024
                                        </option>
                                        <option value="2025">
                                            2025
                                        </option>
                                        <option value="2026">
                                            2026
                                        </option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>CVV Code:</td>
                                <td><input type="text" id="cvv" name="cvv" maxlength="4" size="4" value=""/>
                                    <span class="subtle_nu"> 3-4 digit security code</span></td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <!-- Billing Info -->
                            <tr>
                                <td colspan="2"
                                    class="assn_td">
                                    Options
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="165"># of Users:</td>
                                <td valign="top">
                                    <select name="team_count" id="team_count" onchange="showTotal()">
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>
                                        <option value="36">36</option>
                                        <option value="37">37</option>
                                        <option value="38">38</option>
                                        <option value="39">39</option>
                                        <option value="40">40</option>
                                        <option value="41">41</option>
                                        <option value="42">42</option>
                                        <option value="43">43</option>
                                        <option value="44">44</option>
                                        <option value="45">45</option>
                                        <option value="46">46</option>
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                        <option value="49">49</option>
                                        <option value="50">50</option>
                                        <option value="51">51</option>
                                        <option value="52">52</option>
                                        <option value="53">53</option>
                                        <option value="54">54</option>
                                        <option value="55">55</option>
                                        <option value="56">56</option>
                                        <option value="57">57</option>
                                        <option value="58">58</option>
                                        <option value="59">59</option>
                                        <option value="60">60</option>
                                        <option value="61">61</option>
                                        <option value="62">62</option>
                                        <option value="63">63</option>
                                        <option value="64">64</option>
                                        <option value="65">65</option>
                                        <option value="66">66</option>
                                        <option value="67">67</option>
                                        <option value="68">68</option>
                                        <option value="69">69</option>
                                        <option value="70">70</option>
                                        <option value="71">71</option>
                                        <option value="72">72</option>
                                        <option value="73">73</option>
                                        <option value="74">74</option>
                                        <option value="75">75</option>
                                        <option value="76">76</option>
                                        <option value="77">77</option>
                                        <option value="78">78</option>
                                        <option value="79">79</option>
                                        <option value="80">80</option>
                                        <option value="81">81</option>
                                        <option value="82">82</option>
                                        <option value="83">83</option>
                                        <option value="84">84</option>
                                        <option value="85">85</option>
                                        <option value="86">86</option>
                                        <option value="87">87</option>
                                        <option value="88">88</option>
                                        <option value="89">89</option>
                                        <option value="90">90</option>
                                        <option value="91">91</option>
                                        <option value="92">92</option>
                                        <option value="93">93</option>
                                        <option value="94">94</option>
                                        <option value="95">95</option>
                                        <option value="96">96</option>
                                        <option value="97">97</option>
                                        <option value="98">98</option>
                                        <option value="99">99</option>
                                        <option value="100">100</option>
                                        <option value="101">101</option>
                                        <option value="102">102</option>
                                        <option value="103">103</option>
                                        <option value="104">104</option>
                                        <option value="105">105</option>
                                        <option value="106">106</option>
                                        <option value="107">107</option>
                                        <option value="108">108</option>
                                        <option value="109">109</option>
                                        <option value="110">110</option>
                                        <option value="111">111</option>
                                        <option value="112">112</option>
                                        <option value="113">113</option>
                                        <option value="114">114</option>
                                        <option value="115">115</option>
                                        <option value="116">116</option>
                                        <option value="117">117</option>
                                        <option value="118">118</option>
                                        <option value="119">119</option>
                                        <option value="120">120</option>
                                        <option value="121">121</option>
                                        <option value="122">122</option>
                                        <option value="123">123</option>
                                        <option value="124">124</option>
                                        <option value="125">125</option>
                                        <option value="126">126</option>
                                        <option value="127">127</option>
                                        <option value="128">128</option>
                                        <option value="129">129</option>
                                        <option value="130">130</option>
                                        <option value="131">131</option>
                                        <option value="132">132</option>
                                        <option value="133">133</option>
                                        <option value="134">134</option>
                                        <option value="135">135</option>
                                        <option value="136">136</option>
                                        <option value="137">137</option>
                                        <option value="138">138</option>
                                        <option value="139">139</option>
                                        <option value="140">140</option>
                                        <option value="141">141</option>
                                        <option value="142">142</option>
                                        <option value="143">143</option>
                                        <option value="144">144</option>
                                        <option value="145">145</option>
                                        <option value="146">146</option>
                                        <option value="147">147</option>
                                        <option value="148">148</option>
                                        <option value="149">149</option>
                                        <option value="150">150</option>
                                        <option value="151">151</option>
                                        <option value="152">152</option>
                                        <option value="153">153</option>
                                        <option value="154">154</option>
                                        <option value="155">155</option>
                                        <option value="156">156</option>
                                        <option value="157">157</option>
                                        <option value="158">158</option>
                                        <option value="159">159</option>
                                        <option value="160">160</option>
                                        <option value="161">161</option>
                                        <option value="162">162</option>
                                        <option value="163">163</option>
                                        <option value="164">164</option>
                                        <option value="165">165</option>
                                        <option value="166">166</option>
                                        <option value="167">167</option>
                                        <option value="168">168</option>
                                        <option value="169">169</option>
                                        <option value="170">170</option>
                                        <option value="171">171</option>
                                        <option value="172">172</option>
                                        <option value="173">173</option>
                                        <option value="174">174</option>
                                        <option value="175">175</option>
                                        <option value="176">176</option>
                                        <option value="177">177</option>
                                        <option value="178">178</option>
                                        <option value="179">179</option>
                                        <option value="180">180</option>
                                        <option value="181">181</option>
                                        <option value="182">182</option>
                                        <option value="183">183</option>
                                        <option value="184">184</option>
                                        <option value="185">185</option>
                                        <option value="186">186</option>
                                        <option value="187">187</option>
                                        <option value="188">188</option>
                                        <option value="189">189</option>
                                        <option value="190">190</option>
                                        <option value="191">191</option>
                                        <option value="192">192</option>
                                        <option value="193">193</option>
                                        <option value="194">194</option>
                                        <option value="195">195</option>
                                        <option value="196">196</option>
                                        <option value="197">197</option>
                                        <option value="198">198</option>
                                        <option value="199">199</option>
                                        <option value="200">200</option>
                                        <option value="201">201</option>
                                        <option value="202">202</option>
                                        <option value="203">203</option>
                                        <option value="204">204</option>
                                        <option value="205">205</option>
                                        <option value="206">206</option>
                                        <option value="207">207</option>
                                        <option value="208">208</option>
                                        <option value="209">209</option>
                                        <option value="210">210</option>
                                        <option value="211">211</option>
                                        <option value="212">212</option>
                                        <option value="213">213</option>
                                        <option value="214">214</option>
                                        <option value="215">215</option>
                                        <option value="216">216</option>
                                        <option value="217">217</option>
                                        <option value="218">218</option>
                                        <option value="219">219</option>
                                        <option value="220">220</option>
                                        <option value="221">221</option>
                                        <option value="222">222</option>
                                        <option value="223">223</option>
                                        <option value="224">224</option>
                                        <option value="225">225</option>
                                        <option value="226">226</option>
                                        <option value="227">227</option>
                                        <option value="228">228</option>
                                        <option value="229">229</option>
                                        <option value="230">230</option>
                                        <option value="231">231</option>
                                        <option value="232">232</option>
                                        <option value="233">233</option>
                                        <option value="234">234</option>
                                        <option value="235">235</option>
                                        <option value="236">236</option>
                                        <option value="237">237</option>
                                        <option value="238">238</option>
                                        <option value="239">239</option>
                                        <option value="240">240</option>
                                        <option value="241">241</option>
                                        <option value="242">242</option>
                                        <option value="243">243</option>
                                        <option value="244">244</option>
                                        <option value="245">245</option>
                                        <option value="246">246</option>
                                        <option value="247">247</option>
                                        <option value="248">248</option>
                                        <option value="249">249</option>
                                        <option value="250">250</option>
                                        <option value="251">251</option>
                                        <option value="252">252</option>
                                        <option value="253">253</option>
                                        <option value="254">254</option>
                                        <option value="255">255</option>
                                        <option value="256">256</option>
                                        <option value="257">257</option>
                                        <option value="258">258</option>
                                        <option value="259">259</option>
                                        <option value="260">260</option>
                                        <option value="261">261</option>
                                        <option value="262">262</option>
                                        <option value="263">263</option>
                                        <option value="264">264</option>
                                        <option value="265">265</option>
                                        <option value="266">266</option>
                                        <option value="267">267</option>
                                        <option value="268">268</option>
                                        <option value="269">269</option>
                                        <option value="270">270</option>
                                        <option value="271">271</option>
                                        <option value="272">272</option>
                                        <option value="273">273</option>
                                        <option value="274">274</option>
                                        <option value="275">275</option>
                                        <option value="276">276</option>
                                        <option value="277">277</option>
                                        <option value="278">278</option>
                                        <option value="279">279</option>
                                        <option value="280">280</option>
                                        <option value="281">281</option>
                                        <option value="282">282</option>
                                        <option value="283">283</option>
                                        <option value="284">284</option>
                                        <option value="285">285</option>
                                        <option value="286">286</option>
                                        <option value="287">287</option>
                                        <option value="288">288</option>
                                        <option value="289">289</option>
                                        <option value="290">290</option>
                                        <option value="291">291</option>
                                        <option value="292">292</option>
                                        <option value="293">293</option>
                                        <option value="294">294</option>
                                        <option value="295">295</option>
                                        <option value="296">296</option>
                                        <option value="297">297</option>
                                        <option value="298">298</option>
                                        <option value="299">299</option>
                                        <option value="300">300</option>
                                        <option value="301">301</option>
                                        <option value="302">302</option>
                                        <option value="303">303</option>
                                        <option value="304">304</option>
                                        <option value="305">305</option>
                                        <option value="306">306</option>
                                        <option value="307">307</option>
                                        <option value="308">308</option>
                                        <option value="309">309</option>
                                        <option value="310">310</option>
                                        <option value="311">311</option>
                                        <option value="312">312</option>
                                        <option value="313">313</option>
                                        <option value="314">314</option>
                                        <option value="315">315</option>
                                        <option value="316">316</option>
                                        <option value="317">317</option>
                                        <option value="318">318</option>
                                        <option value="319">319</option>
                                        <option value="320">320</option>
                                        <option value="321">321</option>
                                        <option value="322">322</option>
                                        <option value="323">323</option>
                                        <option value="324">324</option>
                                        <option value="325">325</option>
                                        <option value="326">326</option>
                                        <option value="327">327</option>
                                        <option value="328">328</option>
                                        <option value="329">329</option>
                                        <option value="330">330</option>
                                        <option value="331">331</option>
                                        <option value="332">332</option>
                                        <option value="333">333</option>
                                        <option value="334">334</option>
                                        <option value="335">335</option>
                                        <option value="336">336</option>
                                        <option value="337">337</option>
                                        <option value="338">338</option>
                                        <option value="339">339</option>
                                        <option value="340">340</option>
                                        <option value="341">341</option>
                                        <option value="342">342</option>
                                        <option value="343">343</option>
                                        <option value="344">344</option>
                                        <option value="345">345</option>
                                        <option value="346">346</option>
                                        <option value="347">347</option>
                                        <option value="348">348</option>
                                        <option value="349">349</option>
                                        <option value="350">350</option>
                                        <option value="351">351</option>
                                        <option value="352">352</option>
                                        <option value="353">353</option>
                                        <option value="354">354</option>
                                        <option value="355">355</option>
                                        <option value="356">356</option>
                                        <option value="357">357</option>
                                        <option value="358">358</option>
                                        <option value="359">359</option>
                                        <option value="360">360</option>
                                        <option value="361">361</option>
                                        <option value="362">362</option>
                                        <option value="363">363</option>
                                        <option value="364">364</option>
                                        <option value="365">365</option>
                                        <option value="366">366</option>
                                        <option value="367">367</option>
                                        <option value="368">368</option>
                                        <option value="369">369</option>
                                        <option value="370">370</option>
                                        <option value="371">371</option>
                                        <option value="372">372</option>
                                        <option value="373">373</option>
                                        <option value="374">374</option>
                                        <option value="375">375</option>
                                        <option value="376">376</option>
                                        <option value="377">377</option>
                                        <option value="378">378</option>
                                        <option value="379">379</option>
                                        <option value="380">380</option>
                                        <option value="381">381</option>
                                        <option value="382">382</option>
                                        <option value="383">383</option>
                                        <option value="384">384</option>
                                        <option value="385">385</option>
                                        <option value="386">386</option>
                                        <option value="387">387</option>
                                        <option value="388">388</option>
                                        <option value="389">389</option>
                                        <option value="390">390</option>
                                        <option value="391">391</option>
                                        <option value="392">392</option>
                                        <option value="393">393</option>
                                        <option value="394">394</option>
                                        <option value="395">395</option>
                                        <option value="396">396</option>
                                        <option value="397">397</option>
                                        <option value="398">398</option>
                                        <option value="399">399</option>
                                        <option value="400">400</option>
                                        <option value="401">401</option>
                                        <option value="402">402</option>
                                        <option value="403">403</option>
                                        <option value="404">404</option>
                                        <option value="405">405</option>
                                        <option value="406">406</option>
                                        <option value="407">407</option>
                                        <option value="408">408</option>
                                        <option value="409">409</option>
                                        <option value="410">410</option>
                                        <option value="411">411</option>
                                        <option value="412">412</option>
                                        <option value="413">413</option>
                                        <option value="414">414</option>
                                        <option value="415">415</option>
                                        <option value="416">416</option>
                                        <option value="417">417</option>
                                        <option value="418">418</option>
                                        <option value="419">419</option>
                                        <option value="420">420</option>
                                        <option value="421">421</option>
                                        <option value="422">422</option>
                                        <option value="423">423</option>
                                        <option value="424">424</option>
                                        <option value="425">425</option>
                                        <option value="426">426</option>
                                        <option value="427">427</option>
                                        <option value="428">428</option>
                                        <option value="429">429</option>
                                        <option value="430">430</option>
                                        <option value="431">431</option>
                                        <option value="432">432</option>
                                        <option value="433">433</option>
                                        <option value="434">434</option>
                                        <option value="435">435</option>
                                        <option value="436">436</option>
                                        <option value="437">437</option>
                                        <option value="438">438</option>
                                        <option value="439">439</option>
                                        <option value="440">440</option>
                                        <option value="441">441</option>
                                        <option value="442">442</option>
                                        <option value="443">443</option>
                                        <option value="444">444</option>
                                        <option value="445">445</option>
                                        <option value="446">446</option>
                                        <option value="447">447</option>
                                        <option value="448">448</option>
                                        <option value="449">449</option>
                                        <option value="450">450</option>
                                        <option value="451">451</option>
                                        <option value="452">452</option>
                                        <option value="453">453</option>
                                        <option value="454">454</option>
                                        <option value="455">455</option>
                                        <option value="456">456</option>
                                        <option value="457">457</option>
                                        <option value="458">458</option>
                                        <option value="459">459</option>
                                        <option value="460">460</option>
                                        <option value="461">461</option>
                                        <option value="462">462</option>
                                        <option value="463">463</option>
                                        <option value="464">464</option>
                                        <option value="465">465</option>
                                        <option value="466">466</option>
                                        <option value="467">467</option>
                                        <option value="468">468</option>
                                        <option value="469">469</option>
                                        <option value="470">470</option>
                                        <option value="471">471</option>
                                        <option value="472">472</option>
                                        <option value="473">473</option>
                                        <option value="474">474</option>
                                        <option value="475">475</option>
                                        <option value="476">476</option>
                                        <option value="477">477</option>
                                        <option value="478">478</option>
                                        <option value="479">479</option>
                                        <option value="480">480</option>
                                        <option value="481">481</option>
                                        <option value="482">482</option>
                                        <option value="483">483</option>
                                        <option value="484">484</option>
                                        <option value="485">485</option>
                                        <option value="486">486</option>
                                        <option value="487">487</option>
                                        <option value="488">488</option>
                                        <option value="489">489</option>
                                        <option value="490">490</option>
                                        <option value="491">491</option>
                                        <option value="492">492</option>
                                        <option value="493">493</option>
                                        <option value="494">494</option>
                                        <option value="495">495</option>
                                        <option value="496">496</option>
                                        <option value="497">497</option>
                                        <option value="498">498</option>
                                        <option value="499">499</option>
                                        <option value="500">500</option>
                                    </select></i>
                                </td>

                            </tr>
                            <tr>
                                <td valign="top">Length:</td>
                                <td>1 year</td>
                            </tr>
                            <tr>
                                <td colspan="2"
                                    class="assn_td">
                                    Order Details
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="order_details">
                                        <table width="100%" border="0">
                                            <tbody>
                                            <tr>
                                                <td>Per-User Rate:</td>
                                                <td>$50.00 per user</td>
                                            </tr>
                                            <tr>
                                                <td width="165">Subtotal:</td>
                                                <td>$150.00</td>
                                            </tr>
                                            <tr>
                                                <td>Tax:</td>
                                                <td>$0.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="subtle_nu">WI residents subject to 5.5% sales tax</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="assn_td_new"><span
                                                            class="assn_span">Order Total:</span><span id="order_total">$150.00</span>
                                                    <span class="subtle_nu">USD</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td valign="top" colspan="2"><span ><input type="checkbox" name="agree"
                                                                                   value="1"/>
            I have read, understand, and accept the <a href="http://hockeyshare.com/tos.htm" target="_blank"
                                                       class="subtle">Terms and Conditions</a> and <a
                                                href="http://hockeyshare.com/privacy.htm" target="_blank"
                                                class="subtle">Privacy Policy</a>.</td>
                            </tr>

                            <tr>
                                <td><input type="submit" name="Submit" value="Process Order" onclick="enableCC()"
                                           class="pbutton"/>
                                </td>
                                <td align="right" valign="top">
                                    <!-- (c) 2005, 2013. Authorize.Net is a registered trademark of CyberSource Corporation -->
                                    <div class="AuthorizeNetSeal">

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table border="0" width="100%">
                <tr>
                    <td width="50"
                        class="assn_td_one">
                        <div class="subtle_nu">You are connected to HockeyShare via a secure encrypted connection to
                            enhance security.
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="assnid" value="0"/>
            <input type="hidden" name="extend" value="0"/>
        </form>
    </div>
</div>