<table cellpadding='0' cellspacing='4' border='0' width='95%' align='center'>
    <tr>
        <td valign='middle' align='left'>
            <span class='pagetitle'>HockeyShare.com (HockeyShare LLC): COPPA Permission Form</span>
            <br/>
            <br/>
            <b><span style='font-size:12px'>Instructions for a parent or guardian<br/><br/>Please print this form out, complete it and fax it to the number specified (if present) or mail to the mailing address below.</span></b><br/><br/>
            <b>Mailing Address:</b>
            <br/>
            Att: Kevin Muller<br/>
            COPPA Registration<br/>
            5744 80th St - Suite #8<br/>
            Kenosha, WI 53142
        </td>
    </tr>
</table>
<br/>
<table cellpadding='4' cellspacing='2' border='1' width='95%' align='center'>
    <tr>
        <td width='40%'><b>Choose a Member Name</b><br/>Member names must be between 3 and 32 characters long.</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'><b>Choose a Password</b><br/>Passwords must be between 3 and 32 characters long.</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'><b>Your email address</b><br/>You will need to enter a <i>real</i> email address.</td>
        <td>&nbsp;</td>
    </tr>
</table>
<br/>
<table cellpadding='0' cellspacing='4' border='0' width='95%' align='center'>
    <tr>
        <td valign='middle' align='left'>
            <b><span style='font-size:12px'>Please sign the form below and send to us.<br/><br/>I understand that the information that the child has supplied is correct. I understand that the profile information may be changed by entering a password and I understand that I may ask for this registration profile to be removed.</span></b>
        </td>
    </tr>
</table>
<br/>
<table cellpadding='10' cellspacing='2' border='1' width='95%' align='center'>
    <tr>
        <td width='40%'>Parent / Legal Guardian FULL name</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'>Relation to child</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'>Signature</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'>Email Address</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'>Phone Number</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width='40%'>Date</td>
        <td>&nbsp;</td>
    </tr>
</table>