@extends('layouts.app')
@include('includes.header')
@include('includes.menu')
<div>
    <div class="page_content">
        @if(!Auth::check())
            <div class="signup_page">
                <br/>
                <h1>Gift Subscription</h1>

                <p>Gift the gift of coaching organization! Use the form below to purchase a gift subscription for
                    someone
                    you know.</p>

                <form method="post" action="" id="checkoutForm" name="checkoutForm">
                    <table border="0" width="100%">
                        <tr>
                            <td valign="top">

                                <table width="100%" border="0">
                                    <tr>
                                        <td colspan="2"
                                            class="assn_td margin_gifts">
                                            Billing Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="165">Username:</td>
                                        <td>{{--{{$email}}--}}</td>
                                    </tr>
                                    <tr>
                                        <td>First Name:</td>
                                        <td><input type="text" name="first" value="{{--{{$name}}--}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Last Name:</td>
                                        <td><input type="text" name="last" value="{{--{{$first}}--}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td><input type="text" name="address" value="{{--{{$address}}--}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Address (cont):</td>
                                        <td><input type="text" name="address2" value="{{--{{$address2}}--}}"/> <span
                                                    class="subtle_nu">Optional</span></td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td><input type="text" name="city" value="{{--{{$city}}--}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>State/Prov:</td>
                                        <td>
                                            <select name="state">
                                                <option value="" selected="selected" disabled="disabled">-- State/Prov
                                                    --
                                                </option>
                                                <option value="NA"> -- N/A --</option>
                                                <option value="AB">Alberta</option>
                                                <option value="BC">British Columbia</option>
                                                <option value="MB">Manitoba</option>
                                                <option value="NB">New Brunswick</option>
                                                <option value="NL">Newfoundland and Labrador</option>
                                                <option value="NT">Northwest Territories</option>
                                                <option value="NU">Nunavut</option>
                                                <option value="NS">Nova Scotia</option>
                                                <option value="ON">Ontario</option>
                                                <option value="PE">Prince Edward Island</option>
                                                <option value="QC">Quebec</option>
                                                <option value="SK">Saskatchewan</option>
                                                <option value="YT">Yukon</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Zip/Postal:</td>
                                        <td><input type="text" name="zip" value="{{--{{$zip}}--}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Country:</td>
                                        <td>
                                            <select name="country">
                                                <option value="" selected="selected" disabled="disabled">-- Country --
                                                </option>
                                                <option value="CA">Canada</option>
                                                <option value="US">United States</option>
                                                <option value="" disabled="disabled">--------------</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AX">Aland Islands</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua And Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia And Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, The Democratic Republic Of The</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">C�te D'ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GG">Guernsey</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island And Mcdonald Islands</option>
                                                <option value="VA">Vatican City State</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic Of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IM">Isle Of Man</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JE">Jersey</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic Of</option>
                                                <option value="KR">Korea, Republic Of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libyan Arab Jamahiriya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States Of</option>
                                                <option value="MD">Moldova, Republic Of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="ME">Montenegro</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">R�union</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="SH">Saint Helena</option>
                                                <option value="KN">Saint Kitts And Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="PM">Saint Pierre And Miquelon</option>
                                                <option value="VC">Saint Vincent And The Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome And Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="RS">Serbia</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard And Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province Of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic Of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad And Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks And Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="UM">United States Min. Outlying Isl.</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.s.</option>
                                                <option value="WF">Wallis And Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"
                                            class="assn_td margin_gifts">
                                            Send Options (Optional)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><p>Use the options below if you would like us to <strong>send
                                                    the
                                                    gift code directly to the recipient via email</strong> upon
                                                successfully
                                                purchase. If you would rather send the gift code yourself, please leave
                                                the
                                                fields below blank. Gift codes can be found on the My Subscriptions /
                                                Billings page located under the My Account menu.</p>
                                    </tr>
                                    <tr>
                                        <td valign="top">Send via Email:</td>
                                        <td><input type="text" name="email" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">Sent From:</td>
                                        <td><input type="text" name="sent_from" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"
                                            class="assn_td margin_gifts">
                                            Total
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td>
                                            <div id="checkoutTotal">
                                                <p><span class="signup_style">$96</span>&nbsp;&nbsp;&nbsp;$75.00
                                                    (1 Year Subscription)&nbsp;&nbsp;<span
                                                            class="premiumonly">Save 20%</span></p>
                                            </div>
                                            <div class="subtle_nu">NOTE: Prices shown in USD. Wisconsin residents are
                                                subject to a 5.5% sales tax on all purchases.
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2"
                                            class="assn_td margin_gifts">
                                            Payment Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Credit Card Number:</td>
                                        <td><input type="text" name="ccnum" value=""/> <span class="subtle_nu">Do NOT enter dashes in the number!</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Expiration Date:</td>
                                        <td>

                                            <input type="text" name="ccexp_mon" maxlength="2" size="2" value="05"/>
                                            <span class="subtle_nu">Month  (MM)</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" name="ccexp_yr" maxlength="4" size="4"
                                                   value="2017"/> <span
                                                    class="subtle_nu">Year (YYYY)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CVV Code:</td>
                                        <td><input type="text" name="cvv" maxlength="4" size="4" value=""/> <span
                                                    class="subtle_nu">
3 or 4 digit code on the back of the card</span></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>


                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="checkbox" name="agree" value="1"/> I have read, understand, and
                                            accept the <a href="http://hockeyshare.com/tos.htm" target="_blank">Terms
                                                and
                                                Conditions</a> and <a href="http://hockeyshare.com/privacy.htm"
                                                                      target="_blank">Privacy Policy</a>.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" name="Submit" value="Complete Purchase"
                                                   class="pbutton"/>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td width="235" valign="top" style="padding:10px;"><h2>More Details</h2>
                                <p class="sign_up">
                                    <strong>Security</strong></p>
                                <p>For your safety, all transactions are handled using encrypted transmissions. Please
                                    be
                                    sure the URL in your address bar begins with: </p>
                                <p>http<span style="color:#F00; font-weight:bold; text-decoration:underline;">s</span>://www.hockeyshare.com
                                </p>
                                <p class="sign_up">
                                    <strong>Questions</strong></p>
                                <p>For questions, or to register via phone, call us today at <strong>(262)
                                        672-4126</strong>
                                </p>


                                <div class="drills_subheader">Association Signup</div>
                                <p>If you would like to sign up for an association account to allow multiple users to
                                    share
                                    drills and practice plans together, please visit our association signup page: </p>
                                <p><a href="http://hockeyshare.com/drills/signup_assn.php">Association Signup Page</a>
                                </p>

                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="extend" value=""/>
                </form>
            </div>
        @else
            <br/>
            <h1>Gift Subscription</h1>

            <p>In order to purchase a gift subscription, you must be logged in. If you have not already registered for a
                free HockeyShare.com account, please use the link below to create an account, then return to this
                page.</p>
            <br/><br/>
            <p align="center" class="border_signup_new"><a href="http://hockeyshare.com/login" class="pbutton">Log
                    In</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://hockeyshare.com/register" class="pbutton">Register New
                    Account</a></p>
        @endif
        @include('includes.commercial')
        <br>
    </div>
</div>

