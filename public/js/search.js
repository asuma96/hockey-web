(function () {
    $(document).ready(function () {
        $('.header_search').click(function () {
            var value = $('.header_input').val()
            $.ajax({
                type: 'GET',
                url: '/search',
                data: {q: value},
                success: function (data) {
                    window.location.href = '/search?q=' + value + '&Submit=Go'
                }/*,
                error:function (data) {
                    window.location.href = '/search?q=' + value + '&Submit=Go'
                }*/
            })
        })
    })
})()