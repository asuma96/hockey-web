<?php

Route::get('/', function () {
    return view('pages.home');
});
Route::get('/login', function () {
    return view('pages.auth.login');
});
Route::get('/register', function () {
    return view('pages.auth.register');
});
Route::get('/drills', function () {
    return view('pages.drills');
});
Route::get('/drill-store', function () {
    return view('pages.drill-store');
});
Route::get('/acp/learn-more', function () {
    return view('pages.learn-more');
});
Route::get('/acp/compare/', function () {
    return view('pages.compare');
});
Route::get('/redeem', function () {
    return view('pages.user.redeem');
});
Route::get('/stat_tracking_guide.php', function () {
    return view('pages.help.stat_track_guide');
});
Route::get('/teams/video_guides.php', function () {
    return view('pages.help.video_guides');
});
Route::get('/search', function () {
    return view('pages.search');
});
Route::get('/tournaments', function () {
    return view('pages.tournaments');
});
Route::get('/tournaments/list.php', function () {
    return view('pages.tournaments-list');
});
Route::get('/10000pucks/', function () {
    return view('pages.10000pucks');
});
Route::get('/10000pucks/player', function () {
    return view('pages.10000pucks.player');
});
Route::get('/10000pucks/player/start.php', function () {
    return view('pages.10000pucks.player-start');
});
Route::get('/10000pucks/team/new_team.php', function () {
    return view('pages.10000pucks.team-new-team');
});
Route::get('/10000pucks/association/new_association.php', function () {
    return view('pages.10000pucks.association-new-association');
});
Route::get('/10000pucks/instructions.php', function () {
    return view('pages.10000pucks.instructions');
});
Route::get('/video', function () {
    return view('pages.video');
});
Route::get('/training', function () {
    return view('pages.training');
});
Route::get('/blog', function () {
    return view('pages.blog');
});
Route::get('/contact', function () {
    return view('pages.contact_us');
});
Route::get('/interact/contact.php', function () {
    return view('pages.contact_us');
});
Route::get('/newsletter', function () {
    return view('pages.newsletter');
});
Route::get('/interact/rss.php', function () {
    return view('pages.interact');
});
Route::get('/ttp/attackpad', function () {
    return view('pages.attackpad');
});
Route::get('/game-tape', function () {
    return view('pages.game-tape');
});
Route::get('/gifts', function () {
    return view('pages.gifts');
});
Route::get('/game-tape/association_new.php', function () {
    return view('pages.game-tape.association_new');
});
Route::get('/game-tape/learn_more.php', function () {
    return view('pages.game-tape.learn_more');
});
Route::get('/login/forgot.php', function () {
    return view('pages.auth.forgot');
});
Route::get('/acp/walkthrough-1.php', function () {
    return view('pages.help.walkthrough-1');
});
Route::get('/drills/compare.php', function () {
    return view('pages.compare');
});
Route::get('/acp/walkthrough-2.php', function () {
    return view('pages.help.walkthrough-2');
});
Route::get('/acp/walkthrough-3.php', function () {
    return view('pages.help.walkthrough-3');
});
Route::get('/acp/walkthrough-4.php', function () {
    return view('pages.help.walkthrough-4');
});
Route::get('/acp/association_overview.php', function () {
    return view('pages.help.association_overview');
});
Route::get('/drills/diagrammer_help.php', function () {
    return view('pages.help.diagrammer_help');
});
Route::get('/drills/practiceplanner_help.php', function () {
    return view('pages.help.practiceplanner_help');
});
Route::get('/support/attackpad.php', function () {
    return view('pages.help.attackpad');
});
Route::get('/privacy.htm', function () {
    return view('pages.privacy');
});
Route::get('/coppa.html', function () {
    return view('pages.coppa');
});
Route::get('/acp/compare/full/', function () {
    return view('pages.full');
});
Route::get('/acp/compare/full/', function () {
    return view('pages.full');
});
Route::get('/10000pucks/rules.php', function () {
    return view('pages.10000pucks.rules');
});
Route::get('/training/d-vol1/', function () {
    return view('pages.d-vol1');
});
Route::get('/training/different-approach/', function () {
    return view('pages.different-approach');
});
Route::get('/drills/signup_gift.php', function () {
    return view('pages.signup_gift');
});
Route::get('/blog/category/10000-pucks/', function () {
    return view('pages.blog.category.10000-pucks');
});
Route::get('/blog/category/a-different-approach', function () {
    return view('pages.blog.category.a-different-approach');
});
Route::get('/blog/category/comments-thoughts', function () {
    return view('pages.blog.category.comments-thoughts');
});
Route::get('/blog/category/cool-links', function () {
    return view('pages.blog.category.cool-links');
});
Route::get('/blog/category/hockey-drills', function () {
    return view('pages.blog.category.hockey-drills');
});
Route::get('/blog/category/hockey-systems', function () {
    return view('pages.blog.category.hockey-systems');
});
Route::get('/blog/category/hockey-tips', function () {
    return view('pages.blog.category.hockey-tips');
});
Route::get('/blog/category/hockeyshare-contests', function () {
    return view('pages.blog.category.hockeyshare-contests');
});
Route::get('/blog/category/hockeyshare-surveys', function () {
    return view('pages.blog.category.hockeyshare-surveys');
});
Route::get('/blog/category/hockeyshare-com-features', function () {
    return view('pages.blog.category.hockeyshare-com-features');
});
Route::get('/blog/category/hockey-instructional-video', function () {
    return view('pages.blog.category.hockey-instructional-video');
});
Route::get('/blog/category/learn-from-the-pros', function () {
    return view('pages.blog.category.learn-from-the-pros');
});
Route::get('/blog/category/non-hockey', function () {
    return view('pages.blog.category.non-hockey');
});
Route::get('/blog/category/practice-plans', function () {
    return view('pages.blog.category.practice-plans');
});
Route::get('/blog/category/resources', function () {
    return view('pages.blog.category.resources');
});
Route::get('/blog/category/scooters-corner', function () {
    return view('pages.blog.category.scooters-corner');
});
Route::get('/blog/category/uncategorized', function () {
    return view('pages.blog.category.uncategorized');
});
Route::get('/blog/comments-thoughts/the-coaches-ultimate-guide-to-stress-free-try-outs', function () {
    return view('pages.blog.recent-posts.the-coaches-ultimate-guide-to-stress-free-try-outs');
});
Route::get('/blog/comments-thoughts/tips-for-getting-the-most-out-of-your-spring-training', function () {
    return view('pages.blog.recent-posts.tips-for-getting-the-most-out-of-your-spring-training');
});
Route::get('/blog/a-different-approach/coaching-leadership-self-awareness-questions', function () {
    return view('pages.blog.recent-posts.coaching-leadership-self-awareness-questions');
});
Route::get('/blog/cool-links/3-on-2-rushes-plays', function () {
    return view('pages.blog.recent-posts.rushes-plays');
});
Route::get('/blog/hockey-instructional-video/penalty-killing-tips-tricks', function () {
    return view('pages.blog.recent-posts.penalty-killing-tips-tricks');
});
Route::get('/blog/a-different-approach/high-quality-year-end-meetings', function () {
    return view('pages.blog.recent-posts.high-quality-year-end-meetings');
});
Route::get('/blog/learn-from-the-pros/importance-of-driving-the-net', function () {
    return view('pages.blog.recent-posts.importance-of-driving-the-net');
});
Route::get('/blog/hockey-instructional-video/attacking-the-high-seam-video-examples', function () {
    return view('pages.blog.recent-posts.attacking-the-high-seam-video-examples');
});
Route::get('/blog/10000-pucks/10000-pucks-contest-2013', function () {
    return view('pages.blog.recent-posts.10000-pucks-contest-2013');
});
Route::get('/blog/comments-thoughts/kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast', function () {
    return view('pages.blog.recent-posts.kevin-from-hockeyshare-on-weiss-tech-hockeys-podcast');
});
Route::get('/blog/tag/podcast', function () {
    return view('pages.blog.podcast');
});
Route::get('/blog/tag/audio', function () {
    return view('pages.blog.audio');
});
Route::get('/blog/tag/summer-training', function () {
    return view('pages.blog.summer-training');
});
Route::get('/blog/tag/10k-pucks', function () {
    return view('pages.blog.category.10000-pucks');
});
Route::get('/blog/tag/contest', function () {
    return view('pages.blog.blog-tag-contest');
});
Route::get('/blog/tag/results', function () {
    return view('pages.blog.blog-tag-results');
});
Route::get('/blog/hockey-instructional-video/off-ice-shooting-games', function () {
    return view('pages.blog.off-ice-shooting-games');
});
Route::get('/blog/tag/off-ice-shooting', function () {
    return view('pages.blog.off-ice-shooting');
});
Route::get('/drills/learn_more.php', function () {
    return view('pages.learn-more');
});
Route::get('/tos.htm', function () {
    return view('pages.tos_htm');
});
Route::get('/blog/tag/off-season', function () {
    return view('pages.blog.off-season');
});
Route::get('/blog/tag/spring-hockey', function () {
    return view('pages.blog.spring-hocke');
});
Route::get('/account', function () {
    return view('pages.user.account');
});
Route::get('/account/billing', function () {
    return view('pages.user.billing');
});
Route::get('/upgrade/acp.php', function () {
    return view('pages.user.acp-php');
});
Route::get('/drills/my-drills', function () {
    return view('pages.user.my-drills');
});
Route::get('/drills/add', function () {
    return view('pages.user.add');
});
Route::get('/freetrial', function () {
    return view('pages.user.freetrial');
});
Route::get('/plans', function () {
    return view('pages.user.plans');
});
Route::get('/drills/practiceplans/edit.php', function () {
    return view('pages.user.edit');
});
Route::get('/drills/practice_plan_defaults.php', function () {
    return view('pages.user.practice_plan_defaults');
});
Route::get('/teams', function () {
    return view('pages.user.teams');
});
Route::get('/teams/addteam.php', function () {
    return view('pages.user.addteam');
});
Route::get('/drills/my-drills/inbox/sent', function () {
    return view('pages.user.sent');
});
Route::get('/drills/my-drills/inbox', function () {
    return view('pages.user.inbox');
});
Route::get('/10000pucks/team', function () {
    return view('pages.10000pucks.team-new-team');
});
Route::get('/10000pucks/association', function () {
    return view('pages.10000pucks.association-new-association');
});
Route::get('/drill-store/purchase.php?id=turk7', function () {
    return view('pages.purchase');
});
Route::get('/training/different-approach/members.php', function () {
    return view('pages.members');
});
Route::get('/blog/tag/10000-pucks', function () {
    return view('pages.blog.10000-pucks');
});
Route::get('/blog/tag/shooting', function () {
    return view('pages.blog.shooting');
});
Route::get('/blog/tag/video', function () {
    return view('pages.blog.video');
});
Route::get('/blog/tag/10kpucks', function () {
    return view('pages.blog.10kpucks');
});
Route::get('/blog/10000-pucks/10000-pucks-winners-2011', function () {
    return view('pages.blog.10000-pucks-winners-2011');
});
Route::get('/blog/hockey-instructional-video/backhand-off-ice-shooting-tips-video', function () {
    return view('pages.blog.backhand-off-ice-shooting-tips-video');
});
Route::get('/blog/tag/hockey-instructional-video', function () {
    return view('pages.blog.hockey-instructional-video');
});
Route::get('/blog/10000-pucks/10000-pucks-2010-final-results', function () {
    return view('pages.blog.10000-pucks-2010-final-results');
});
Route::get('/blog/tag/contests', function () {
    return view('pages.blog.contests');
});
Route::get('/blog/tag/hockeyshare', function () {
    return view('pages.blog.hockeyshare');
});
Route::get('/blog/10000-pucks/10000-pucks-update', function () {
    return view('pages.blog.10000-pucks-update');
});
Route::get('/blog/10000-pucks/10000-pucks-winners', function () {
    return view('pages.blog.10000-pucks-winners');
});
Route::get('/blog/tag/summer-hockey', function () {
    return view('pages.blog.summer-hockey');
});
Route::get('/blog/comments-thoughts/backyard-ice-rinks-friendly-psa', function () {
    return view('pages.blog.backyard-ice-rinks-friendly-psa');
});
Route::get('/blog/tag/random-thoughts', function () {
    return view('pages.blog.random-thoughts');
});
Route::get('/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules', function () {
    return view('pages.blog.your-opinion-usa-hockey-age-specific-coaching-modules');
});
Route::get('/blog/tag/usa-hockey', function () {
    return view('pages.blog.usa-hockey');
});
Route::get('/blog/tag/opinion', function () {
    return view('pages.blog.opinion');
});
Route::get('/blog/comments-thoughts/your-opinion-usa-hockey-age-specific-coaching-modules/feed', function () {
    return view('pages.blog.feed');
});
Route::get('/blog/comments-thoughts/10-early-season-team-building-ideas', function () {
    return view('pages.blog.10-early-season-team-building-ideas');
});
Route::get('/blog/tag/early-season', function () {
    return view('pages.blog.early-season');
});
Route::get('/blog/tag/team-activities', function () {
    return view('pages.blog.team-activities');
});
Route::get('/blog/tag/team-building', function () {
    return view('pages.blog.team-building');
});
Route::get('/blog/tag/team-chemistry', function () {
    return view('pages.blog.team-chemistry');
});
Route::get('/blog/uncategorized/team-building-resources', function () {
    return view('pages.blog.team-building-resources');
});
Route::get('/blog/comments-thoughts/tryouts-7-factors-other-than-talent', function () {
    return view('pages.blog.tryouts-7-factors-other-than-talent');
});
Route::get('/blog/tag/talent', function () {
    return view('pages.blog.talent');
});
Route::get('/blog/tag/tryouts', function () {
    return view('pages.blog.tryouts');
});
Route::get('/blog/comments-thoughts/summer-training-thoughts', function () {
    return view('pages.blog.summer-training-thoughts');
});
Route::get('/blog/tag/dryland', function () {
    return view('pages.blog.dryland');
});
Route::get('/blog/tag/off-ice-training', function () {
    return view('pages.blog.off-ice-training');
});
Route::get('/blog/comments-thoughts/usa-hockey’s-checking-rule-change-proposal-–-hitting-the-mark', function () {
    return view('pages.blog.usa-hockey’s-checking-rule-change-proposal-–-hitting-the-mark');
});
Route::get('/blog/comments-thoughts/hockey-new-year-resolutions', function () {
    return view('pages.blog.hockey-new-year-resolutions');
});
Route::get('/blog/tag/goals', function () {
    return view('pages.blog.goals');
});
Route::get('/blog/tag/hockey-coaching', function () {
    return view('pages.blog.hockey-coaching');
});
Route::get('/acp/video/overview', function () {
    return view('pages.overview');
});
Route::get('/acp/video/animation', function () {
    return view('pages.animation');
});
Route::get('/upgrade/acp_assn', function () {
    return view('pages.acp_assn');
});
Route::get('/10000pucks/player/start2.php', function () {
    return view('pages.10000pucks.start2');
});
Route::get('/10000pucks/player/gear.php', function () {
    return view('pages.10000pucks.gear');
});
Route::get('/training/d-vol1/purchase.php', function () {
    return view('pages.user.purchase');
});
Route::get('/account/update_password.php', function () {
    return view('pages.user.update_password');
});
Route::get('/upgrade/acp_assn.php', function () {
    return view('pages.user.acp_assn');
});
Route::get('/drills/practiceplans/edit_customlogo.php', function () {
    return view('pages.user.customlogo_upgrade');
});
Route::get('/acp/email/history', function () {
    return view('pages.history');
});
Route::get('/acp/email-lists', function () {
    return view('pages.email-lists');
});
Route::get('/drills/my_practice_plans_stats.php', function () {
    return view('pages.my_practice_plans_stats');
});
Route::get('/account/change_email.php', function () {
    return view('pages.user.change_email');
});
